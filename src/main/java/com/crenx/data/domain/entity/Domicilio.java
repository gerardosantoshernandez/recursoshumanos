package com.crenx.data.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Domicilio {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_domicilio", unique = true)
	private String idDomicilio;
	
	@Column(name = "coordenadas_geoloc",length = 100)
	private String coordenadasGeoloc;
	
	@Column(name = "codigo_postal",length = 100)
	private String codigoPostal;
	
	@Column(name = "calle",length = 100)
	private String calle;

	@Column(name = "num_ext",length = 50)
	private String numExt;
	
	@Column(name = "num_int",length = 50)
	private String numInt;
	
	@Column(name = "colonia",length = 100)
	private String colonia;
	
	@Column(name = "municipio",length = 100)
	private String municipio;
	
	@Column(name = "estado",length = 100)
	private String estado;
	
	@Column(name = "pais",length = 100)
	private String pais;
	
	@Column(name = "cve_pais",length = 50)
	private String cvePais;

	public Domicilio(){}
	public Domicilio(String codigoPostal, String calle, String numExt, String numInt, String colonia, String municipio, String estado, String pais)
	{
		this.calle = calle;
		this.codigoPostal = codigoPostal;
		this.colonia =  colonia;
		this.estado =  estado;
		this.municipio =  municipio;
		this.numExt =  numExt;
		this.numInt = numInt;
		this.pais = pais;
	}
	
	
	
	public String getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(String idDomicilio) {
		this.idDomicilio = idDomicilio;
	}
	


	public String getCoordenadasGeoloc() {
		return coordenadasGeoloc;
	}



	public void setCoordenadasGeoloc(String coordenadasGeoloc) {
		this.coordenadasGeoloc = coordenadasGeoloc;
	}



	public String getCodigoPostal() {
		return codigoPostal;
	}



	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumExt() {
		return numExt;
	}

	public void setNumExt(String numExt) {
		this.numExt = numExt;
	}

	public String getNumInt() {
		return numInt;
	}

	public void setNumInt(String numInt) {
		this.numInt = numInt;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCvePais() {
		return cvePais;
	}

	public void setCvePais(String cvePais) {
		this.cvePais = cvePais;
	}
	
	
	
}

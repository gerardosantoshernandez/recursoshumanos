'use strict';

angular.module('crenx.catalogocontrollers', []).
controller('CatController', function ($scope, $rootScope, apiService, $stateParams, $state) {
	$scope.catalogItems = [];
    $scope.catalogName = $state.current.data.catName;
    
    function loadData(tipoCat) {
        apiService.post('/api/catalogo/items', tipoCat,
                    lcoationLoadCompleted,
                    locationLoadFailed);

    }

    function lcoationLoadCompleted(result) {
        $scope.catalogo = result.data;
    }

    function locationLoadFailed(response) {
        alert(response);
    }
        
    loadData($state.current.data.tipoCat);
}).
controller('FormCatalogoController', function ($scope, $rootScope, apiService, $location, $http, $cookieStore, $stateParams,$modal) {
    
    $scope.openModal = function (modal_id, modal_size) {
        $rootScope.permisson = { RoleID: $scope.rol.ID };
        $rootScope.currentModal = $modal.open({
            templateUrl: modal_id,
            size: modal_size,
            backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
        });
    };
    $scope.parentId = $stateParams.parentId;
    $scope.parentName = $stateParams.parentName;
    if ($stateParams.catalogoItem) {
        $scope.catalogoItem = $stateParams.catalogoItem;
        $scope.catalogoItem.clave = parseFloat($scope.catalogoItem.clave);
        $scope.isUpdate = true;
    }else
	{
    	
    	$scope.catalogoItem = {parentId:$scope.parentId};
	}


    $scope.submitTheForm = function (item, event) {
        apiService.post('/api/catalogo/savecatalogo', $scope.catalogoItem,
                        ServiceSuccessComplete,
                        ServiceError);
    }


    function ServiceSuccessComplete(result) {

        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        toastr.success("Se guardó satisfactoriamente el catálogo.", "Guardar catálogo", opts);

        $scope.isUpdate = true;
        $scope.user = result.data;
        $scope.errorsubmit = '';

    }

    function ServiceError(response) {
        $scope.errorsubmit = response.data;
    }

}).
	controller('ReglasGastosController', function ($scope, $rootScope, apiService, $stateParams, $state, $modal) {
		$scope.reglasItems = [];
	    
	    function loadData() {
	        apiService.get('/api/catalogo/reglasGastos', null,
	                    lcoationLoadCompleted,
	                    locationLoadFailed);

	    }

	    function lcoationLoadCompleted(result) {
	        $scope.reglasItems = result.data;
	    }

	    function locationLoadFailed(response) {
	        alert(response);
	    }
	        
	    loadData();
	    
	    $scope.openModal = function (modal_id, modal_size, reglaid) {
            $rootScope.reglaid = reglaid;
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
            });
        };
        
	      $rootScope.deleteRegla = function (item, event) {
	            apiService.get('/api/catalogo/deleteReglaGasto/' + $rootScope.reglaid, null,
	                function (result) {
	                    $scope.questionary = result.data;
	                    toastr.success("Se eliminó satisfactoriamente la regla de gasto.", "Eliminar regla de gasto", opts);
	                    $rootScope.currentModal.close();
	                   $scope.reglasItems = result.data;
	                }
	                , serviceError);
	        };

	        function serviceError(response) {
	            toastr.error("Sucedió un error", "Error", opts);
	        }
	        
	        var opts = {
	                "closeButton": true,
	                "debug": false,
	                "positionClass": "toast-top-right",
	                "onclick": null,
	                "showDuration": "300",
	                "hideDuration": "1000",
	                "timeOut": "5000",
	                "extendedTimeOut": "1000",
	                "showEasing": "swing",
	                "hideEasing": "linear",
	                "showMethod": "fadeIn",
	                "hideMethod": "fadeOut"
	            };
	  
	}).	
controller('ReglasGastosFormController', function ($scope, $rootScope, apiService, $location, $http, $cookieStore, $stateParams,$modal,$window) {
    
    if ($stateParams.reglaItem) {
        $scope.reglaItem = $stateParams.reglaItem;
        $scope.isUpdate = true;
    }else
	{
    	
    	$scope.reglaItem = {};
	}

    function loadCatalogo(catType) {
        apiService.post('/api/catalogo/items', catType,
        		catalogComplete,
                ServiceError);

    }
    function catalogComplete(result)
    {
    	if (result.data.parent.claveInterna=='CAT_PERIODO_GASTO')
		{
    		$scope.allPeriodoGasto = result.data;
		}
    	if (result.data.parent.claveInterna=='CAT_TIPO_GASTO')
		{
    		$scope.allTipoGasto = result.data;
		}
    }
    loadCatalogo('CAT_PERIODO_GASTO');
    loadCatalogo('CAT_TIPO_GASTO');
    function loadRoles() {
		apiService.post('/api/cuenta/objetosseguridad', "SEC_ROLE",
                lcoationLoadCompleted,
                locationLoadFailed);

	}

	function lcoationLoadCompleted(result) {
	    $scope.allRoles = result.data;
	}
	
	function locationLoadFailed(response) {
	    alert(response);
	}
	loadRoles();
    

    $scope.submitTheForm = function (item, event) {
        apiService.post('/api/catalogo/savereglaGasto', $scope.reglaItem,
                        ServiceSuccessComplete,
                        ServiceError);
    }


    function ServiceSuccessComplete(result) {

        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        toastr.success("Se guardó satisfactoriamente la regla.", "Guardar regla", opts);

        $scope.isUpdate = true;
        $scope.user = result.data;
        $scope.errorsubmit = '';
        $window.location.href = '#/app/reglas-reglasGastos';

    }

    function ServiceError(response) {
        $scope.errorsubmit = response.data;
    }

}).
	controller('ReglasGastosRolController', function ($scope, $rootScope, apiService, $stateParams, $state) {
		$scope.reglasItems = [];
	    
	    function loadData() {
	        apiService.get('/api/catalogo/reglasGastosRol', null,
	                    lcoationLoadCompleted,
	                    locationLoadFailed);

	    }

	    function lcoationLoadCompleted(result) {
	        $scope.reglasItems = result.data;
	    }

	    function locationLoadFailed(response) {
	        alert(response);
	    }
	        
	    loadData();
	}).
controller('ReglasGastosRolFormController', function ($scope, $rootScope, apiService, $location, $http, $cookieStore, $stateParams,$modal) {
    
    if ($stateParams.reglaItem) {
        $scope.reglaItem = $stateParams.reglaItem;
        $scope.isUpdate = true;
    }else
	{
    	
    	$scope.reglaItem = {};
	}

    function loadCatalogo(catType) {
        apiService.post('/api/catalogo/items', catType,
        		catalogComplete,
                ServiceError);

    }
    function catalogComplete(result)
    {
    	if (result.data.parent.claveInterna=='CAT_TIPO_GASTO')
		{
    		$scope.allTipoGasto = result.data;
		}
    	
    	
    	
    }
    loadCatalogo('CAT_TIPO_GASTO');
    
    function loadData() {
		apiService.post('/api/cuenta/objetosseguridad', "SEC_ROLE",
                lcoationLoadCompleted,
                locationLoadFailed);

	}

	function lcoationLoadCompleted(result) {
	    $scope.allRoles = result.data;
	}
	
	function locationLoadFailed(response) {
	    alert(response);
	}
	loadData();
    
    $scope.submitTheForm = function (item, event) {
        apiService.post('/api/catalogo/savereglaGastoRol', $scope.reglaItem,
                        ServiceSuccessComplete,
                        ServiceError);
    }


    function ServiceSuccessComplete(result) {

        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        toastr.success("Se guardó satisfactoriamente la regla.", "Guardar regla", opts);

        $scope.isUpdate = true;
        $scope.user = result.data;
        $scope.errorsubmit = '';

    }

    function ServiceError(response) {
        $scope.errorsubmit = response.data;
    }

}).
	controller('ReglasCreditoController', function ($scope, $rootScope, apiService, $stateParams, $state,$modal) {
		$scope.reglasItems = [];
	    
	    function loadData() {
	        apiService.get('/api/catalogo/reglasCreditos', null,
	                    lcoationLoadCompleted,
	                    locationLoadFailed);

	    }

	    function lcoationLoadCompleted(result) {
	        $scope.reglasItems = result.data;
	    }

	    function locationLoadFailed(response) {
	        alert(response);
	    }
	        
	    loadData();
	    $scope.openModal = function (modal_id, modal_size, reglaid) {
            $rootScope.reglaid = reglaid;
            $rootScope.currentModal = $modal.open({
                templateUrl: modal_id,
                size: modal_size,
                backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
            });
        };
        
        $rootScope.deleteRegla = function (item, event) {
            apiService.deleteobj('/api/catalogo/' + $rootScope.reglaid, null,
                function (result) {
                    $scope.questionary = result.data;
                    toastr.success("Se eliminó satisfactoriamente la regla de crédito.", "Eliminar regla de crédito", opts);
                    $rootScope.currentModal.close();
                    $scope.reglasItems = result.data;
                }
                , serviceError);
        };
        
        function serviceError(response) {
            toastr.error("Sucedió un error", "Error", opts);
        }
        
        var opts = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
	}).
	
controller('ReglasCreditoFormController', function ($scope, $rootScope, apiService, $location, $http, $cookieStore, $stateParams,$modal,$window) {
    
    if ($stateParams.reglaItem) {
        $scope.reglaItem = $stateParams.reglaItem;
        $scope.isUpdate = true;
    }else
	{
    	
    	$scope.reglaItem = {};
	}

    function loadRoles() {
		apiService.post('/api/cuenta/objetosseguridad', "SEC_ROLE",
                lcoationLoadCompleted,
                locationLoadFailed);

	}

	function lcoationLoadCompleted(result) {
	    $scope.allRoles = result.data;
	}
	
	function locationLoadFailed(response) {
	    alert(response);
	}
	loadRoles();
    

    $scope.submitTheForm = function (item, event) {
        apiService.post('/api/catalogo/savereglaCredito', $scope.reglaItem,
                        ServiceSuccessComplete,
                        ServiceError);
    }


    function ServiceSuccessComplete(result) {

        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        toastr.success("Se guardó satisfactoriamente la regla.", "Guardar regla", opts);

        $scope.isUpdate = true;
        $scope.user = result.data;
        $scope.errorsubmit = '';
        $window.location.href = '#/app/reglas-reglasCredito';
    }

    function ServiceError(response) {
        $scope.errorsubmit = response.data;
    }

});

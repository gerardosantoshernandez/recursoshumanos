package com.crenx.services;

import java.util.Collection;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crenx.data.domain.entity.Vacaciones;
import com.crenx.data.domain.repository.VacacionesRepository;
import com.crenx.data.domain.vo.FiltrosVacacionesVO;
import com.crenx.data.domain.vo.VacacionesTOVOModelMap;
import com.crenx.data.domain.vo.VacacionesVO;
import com.google.common.collect.Lists;

import org.springframework.util.StringUtils;
import java.lang.reflect.Type;
import com.google.common.reflect.TypeToken;

@Service
public class VacacionesServices {
	@Autowired
	private VacacionesRepository vacacionesRepository;
	
	public Collection<VacacionesVO> filtrarVacaciones(FiltrosVacacionesVO filtros){
		Collection<Vacaciones> vacaciones = null;
		
		String nombre = StringUtils.isEmpty(filtros.getNombre())?"%":filtros.getNombre()+"%";
		List<String> asunto = Lists.newArrayList(filtros.getAsunto());
		
		if (filtros.getAsunto().size()>0) {
			vacaciones = Lists.newArrayList(this.vacacionesRepository.findByAsunto_IdCatalogoInAndDescripcion(filtros.getAsunto(), filtros.getNombre()));
		}else{
			vacaciones = Lists.newArrayList(this.vacacionesRepository.findByDescripcionIsLike(nombre));
		}
		
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new VacacionesTOVOModelMap());
		Type listType = new TypeToken<List<VacacionesVO>>() {}.getType();
		List<VacacionesVO> listaVacaciones = mapper.map(vacaciones, listType);
		return listaVacaciones;
	}
}

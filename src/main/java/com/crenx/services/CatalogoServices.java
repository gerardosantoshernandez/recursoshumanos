package com.crenx.services;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.crenx.br.CatalogoBRImpl;
import com.crenx.data.domain.entity.Caja;
import com.crenx.data.domain.entity.Calendario;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Domicilio;
import com.crenx.data.domain.entity.Empleado;
import com.crenx.data.domain.entity.Horario;
import com.crenx.data.domain.entity.ObjetoSeguridad;
import com.crenx.data.domain.entity.ObjetoSeguridadRelaciones;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Parametros;
import com.crenx.data.domain.entity.Producto;
import com.crenx.data.domain.entity.ReglaCredito;
import com.crenx.data.domain.entity.ReglaGasto;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.repository.CajaRepository;
import com.crenx.data.domain.repository.CalendarioRepository;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.DomicilioRepository;
import com.crenx.data.domain.repository.EmpleadoRepository;
import com.crenx.data.domain.repository.HorarioRepository;
import com.crenx.data.domain.repository.ObjetoSeguridadRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.repository.ParametrosRepository;
import com.crenx.data.domain.repository.ProductoRepository;
import com.crenx.data.domain.repository.ReglaCreditoRepository;
import com.crenx.data.domain.repository.ReglaGastoRepository;
import com.crenx.data.domain.repository.UsuarioRepository;
import com.crenx.data.domain.vo.CatalogoVO;
import com.crenx.data.domain.vo.CrearObjSecVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.data.domain.vo.OrgToTreeModelMap;
import com.crenx.data.domain.vo.OrgVO;
import com.crenx.data.domain.vo.OrgVOToOrgModelMap;
import com.crenx.data.domain.vo.ReglaCreditoToVOModelMapper;
import com.crenx.data.domain.vo.ReglaCreditoVO;
import com.crenx.data.domain.vo.ReglaGastoToVOModelMapper;
import com.crenx.data.domain.vo.ReglaGastoVO;
import com.crenx.security.UserAuthentication;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@Service
public class CatalogoServices {

	@Autowired
	private CalendarioRepository calendarioRepository;
	@Autowired
	private HorarioRepository horarioRepository;
	@Autowired
	private CatalogoRepository catalogoRepository;
	@Autowired
	private DomicilioRepository domicilioRepository;
	@Autowired
	private ParametrosRepository paramRepository;
	@Autowired
	private CalendarioRepository calRepository;
	@Autowired
	private OrgRepository orgRepository;
	@Autowired
	private UsuarioRepository usrRepository;
	@Autowired
	private ObjetoSeguridadRepository objetoSeguridadRepository;
	@Autowired
	private SeguridadServices secServices;
	@Autowired
	private DomicilioRepository domRepository;
	@Autowired
	private EmpleadoRepository emplRepository;
	@Autowired
	private ProductoRepository productoRepository;
	@Autowired
	private CajaRepository cajaRepository;
	@Autowired
	private CalendarioServices calServices;
	@Autowired
	private CatalogoBRImpl catBR;
	@Autowired
	private ReglaGastoRepository regGasRepository;
	@Autowired
	private ReglaCreditoRepository regCredRepository;
	@Autowired
	private EmpleadoRepository empleadoRepository;

	public void addDefaultData() {
		Catalogo tipoCat = getCatalogoCveInterna("CAT_TIP_DOC_ID");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Tipo de Documento Identidad", "Catalogo de docs de identidad", "CAT_TIP_DOC_ID",
					null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("INE/IFE");
			oneItem.setClave("13");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Cartilla Militar");
			oneItem.setClave("14");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Licencia de manejar");
			oneItem.setClave("8");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Cédula profesional");
			oneItem.setClave("8");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}
		tipoCat = getCatalogoCveInterna("CAT_FIG");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Figura", "Figuras participantes en créditos", "CAT_FIG", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Cliente");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("FIG_CLIENTE");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Co-Deudor");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("FIG_CODEUDOR");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Usuario");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("FIG_USUARIO");
			catalogoRepository.save(oneItem);
		}
		
		//CATEGORIA DE PRODUCTO
		tipoCat = getCatalogoCveInterna("CAT_PRO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Producto","Categorias de productos", "CAT_PRO",null,null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Automotriz");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Computación");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Electrónica");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Hogar y Jardin");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Electrodomésticos");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Oficina");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("videojuegos");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Limpieza");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}
		
		tipoCat = getCatalogoCveInterna("CAT_CARGO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Cargo", "Tipos de cargo", "CAT_CARGO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Accionista", "1", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Dir. Admin Proyec", "7", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Director General", "2", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Dir. Operaciones", "8", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Ejecutivo CYC", "12", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Ejecutivo volante", "15", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Gerente GCC", "11", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Gerente volante", "14", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Jefe Divisional", "9", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Jefe Regional", "10", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Lider Supervisor", "13", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Soporte de op", "18", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}
		tipoCat = getCatalogoCveInterna("CAT_TIP_CUADRE");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Tipo de cuadre", "Tipos de cuadre", "CAT_TIP_CUADRE", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Mostrar cuadre recaudador con totales");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Mostrar cuadre recaudador sin totales de caja");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("No mostrar información del cuadre");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}
		
		tipoCat = getCatalogoCveInterna("CAT_TIP_AVISO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Tipo de aviso", "Tipos de aviso", "CAT_TIP_AVISO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Inasistencia");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Retardo");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_GEN");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Género sexual", "Género sexual", "CAT_GEN", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Femenino");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Masculino");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}
		tipoCat = getCatalogoCveInterna("CAT_PAIS");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Paises", "Paises", "CAT_PAIS", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("México", "México", "MX", "mx", tipoCat.getIdCatalogo());
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_OCUP");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Ocupaciones", "Ocupaciones", "CAT_OCUP", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Contador");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Tendero");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_UNI_PLAZO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Plazos", "Plazos para créditos", "CAT_UNI_PLAZO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Días");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Semana");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Quincena");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Mes");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Bimestre");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Semestre");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Año");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_PERIODO_GASTO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Plazos", "Periodos para gastos", "CAT_PERIODO_GASTO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Evento", "PERIODO_EVENTO", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Diario", "PERIODO_DIARIO", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Semanal", "PERIODO_SEMANA", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Quincenal", "PERIODO_QUINCENA", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Mensual", "PERIODO_MENSUAL", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_FREC_COBRO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Frecuencia de cobro", "Frecuencia de cobro", "CAT_FREC_COBRO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Diaria");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Semanal");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Quincenal");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Mensual");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Bimestral");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Semestral");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Anual");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_FAM_PROD");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Tipos de producto", "Tipos de producto", "CAT_FAM_PROD", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Créditos Emprendedores");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("PyME");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Hipotecarios");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Empleados");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Pago activos");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_MEDIOS_PAGO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Medios de pago", "Medios de pago", "CAT_MEDIOS_PAGO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Efectivo");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Transferencia bancaria");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Cargo a cuenta");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_TIPO_GASTO");
		if (tipoCat == null) {
			tipoCat = addCatalogo(new Catalogo("Tipo de gasto", "Tipos de gasto", "CAT_TIPO_GASTO", null, null));
		}
		addCatalogo(new Catalogo("Aguinaldo", "G13", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("ANUNCIOS", "G21", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Capacitacion", "G7", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Casetas", "G2", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("CELULAR", "G12", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Comidas", "G9", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Gasolina", "G1", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("General", "G0", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Hotel", "G8", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("LIMPIEZA", "G20", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Médicos", "G17", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("MOBILIARIO Y EQUIPO", "G19", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("MOTOS", "G14", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Mtto Motos", "G3", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Mtto Vehiculos", "G4", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Papeleria", "G6", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Recargas Moviles", "G5", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Rentas Móviles", "G15", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Seguro", "G16", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Sistema", "G18", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Transporte/camiones", "G11", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Varios", "G10", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Lavanderia", "G22", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Calendarios", "G222", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Camisas", "G223", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Cascos", "G224", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Rollos", "G225", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Compra de Impermeables", "G226", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Compra de Planes", "G227", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Emplacado", "G228", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Mkt", "G229", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Nómina Outsourcing", "G230", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Pago de Impresoras", "G231", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Periódico", "G232", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Playeras", "G233", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Servicios", "G234", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Tarjetas", "G235", tipoCat.getIdCatalogo()));

		tipoCat = getCatalogoCveInterna("CAT_TIPO_PAGO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Tipos de pago", "Tipos de pago", "CAT_TIPO_PAGO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Amortización");
			oneItem.setClaveInterna("TIPO_PAGO_ABONO");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Comisiones");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("TIPO_PAGO_COMISION");
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_TIPO_ACTIVO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Tipos de activo", "Tipos de activo", "CAT_TIPO_ACTIVO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Celular");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Motocicleta");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Computadora");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Tableta");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}
		tipoCat = getCatalogoCveInterna("CAT_TIP_DOC");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Tipos de documento", "Tipos de documento", "CAT_TIP_DOC", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Identificación");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Identificación reverso");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Comprobante de domicilio");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Contrato");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Pagaré");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}
		tipoCat = getCatalogoCveInterna("CAT_ESTATUS_PAGO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Estatus pago", "Estatus pago", "CAT_ESTATUS_PAGO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Pendiente");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("PAGO_PENDIENTE");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Pagado");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("PAGO_PAGADO");
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_PAIS");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Pais", "Paises", "CAT_PAIS", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("México");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("Mx");
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_MOV_CAJA");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Movimientos Caja", "Mov. Caja", "CAT_MOV_CAJA", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Cargo");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("MOV_CAJA_CARGO");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Abono");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("MOV_CAJA_ABONO");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Corte");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("MOV_CAJA_CORTE");
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_TIPO_OPERACION");
		if (tipoCat == null) {
			tipoCat = addCatalogo(
					new Catalogo("Tipo operación de caja", "Tipo operación", "CAT_TIPO_OPERACION", null, null));
		}
		addCatalogo(new Catalogo("Desembolso por crédito", "TIPO_OPE_CREDITO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Desembolso por gasto", "TIPO_OPE_GASTO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Ingreso por fondeo", "TIPO_OPE_FONDEO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Ingreso por transferencia", "TIPO_OPE_ING_TRANS", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Egreso por transferencia", "TIPO_OPE_EGR_TRANS", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Ingreso por cobranza", "TIPO_OPE_ING_COBRO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Ingreso por comisión", "TIPO_OPE_ING_COMISION", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Ingreso por corrección de crédito", "TIPO_OPE_ING_CORR_CREDITO",
				tipoCat.getIdCatalogo()));
		addCatalogo(
				new Catalogo("Egreso por corrección de crédito", "TIPO_OPE_EGR_CORR_CREDITO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Egreso comisión corrección de crédito", "TIPO_OPE_EGR_CORR_CREDITO_COM",
				tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Egreso por cancelación de pago", "TIPO_OPE_EGR_CANC_PAGO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Egreso por cancelación de pago comisión", "TIPO_OPE_EGR_CANC_PAGO_COM",
				tipoCat.getIdCatalogo()));
		addCatalogo(
				new Catalogo("Ingreso por cancelación de gasto", "TIPO_OPE_ING_CANC_GASTO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Egreso por cancelación de transferencia", "TIPO_OPE_EGR_CANC_TRANS",
				tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Ingreso por cancelación de transferencia", "TIPO_OPE_ING_CANC_TRANS",
				tipoCat.getIdCatalogo()));

		tipoCat = getCatalogoCveInterna("CAT_ESTATUS_CREDITO");
		if (tipoCat == null) {
			tipoCat = addCatalogo(
					new Catalogo("Estatus Crédito", "Estatus crédito", "CAT_ESTATUS_CREDITO", null, null));
		}
		addCatalogo(new Catalogo("Activo", "CRED_ACTIVO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Inactivo", "CRED_INACTIVO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Pendiente Aprobación", "CRED_PENDIENTE", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Pendiente Entrega", "CRED_PENDIENTE_ENT", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Crédito rechazado", "CRED_RECHAZADO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Crédito pagado", "CRED_PAGADO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Crédito cancelado", "CRED_CANCELADO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Crédito incobrable", "CRED_INCOBRABLE", tipoCat.getIdCatalogo()));

		tipoCat = getCatalogoCveInterna("CAT_TIPO_CREDITO");
		if (tipoCat == null) {
			tipoCat = addCatalogo(new Catalogo("Tipo Crédito", "TIPO crédito", "CAT_TIPO_CREDITO", null, null));
		}
		addCatalogo(new Catalogo("Nuevo", "CRED_NUEVO", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Recompra", "CRED_RECOMPRA", tipoCat.getIdCatalogo()));

		tipoCat = getCatalogoCveInterna("CAT_ENTIDAD_FED");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Pais", "Entidades Federativas", "CAT_ENTIDAD_FED", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Aguascalientes", "AS", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Baja California", "BC", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Baja California Sur", "BS", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Campeche", "CC", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Chiapas", "CS", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Chihuahua", "CH", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Distrito Federal", "DF", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Coahuila", "CL", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Colima", "CM", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Durango", "DG", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Estado de México", "MC", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Guanajuato", "GT", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Guerrero", "GR", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Hidalgo", "HG", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Jalisco", "JC", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Michoacán", "MN", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Morelos", "MS", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Nayarit", "NT", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Nuevo León", "NL", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Oaxaca", "OC", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Puebla", "PL", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Querétaro", "QT", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Quintana Roo", "QR", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("San Luis Potosí", "SP", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Sinaloa", "SL", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Sonora", "SR", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Tabasco", "TC", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Tamaulipas", "TS", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Tlaxcala", "TL", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Veracruz", "VZ", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Yucatán", "YN", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Zacatecas", "ZS", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Nacido en el extranjero", "NE", tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_ESTATUS_GEN");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Estatus generales", "Estatus pago", "CAT_ESTATUS_GEN", null, null);
			catalogoRepository.save(tipoCat);
		}
		addCatalogo(new Catalogo("Activo", "...", "ESTATUS_ACTIVO", "1", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Inactivo", "...", "ESTATUS_INACTIVO", "2", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Pendiente", "...", "ESTATUS_GPENDIENTE", "3", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Rechazado", "...", "ESTATUS_GRECHAZADO", "4", tipoCat.getIdCatalogo()));
		addCatalogo(new Catalogo("Cancelado", "...", "ESTATUS_CANCELADO", "5", tipoCat.getIdCatalogo()));

		tipoCat = getCatalogoCveInterna("CAT_CUENTAS_BANCO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Cuentas bancarias", "Estatus pago", "CAT_CUENTAS_BANCO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Cuenta maestra");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CUENTA_MAESTRA");
			oneItem.setClave("1");
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_TIPO_CRED_EMPL");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Tipo crédito empleado", "Estatus pago", "CAT_TIPO_CRED_EMPL", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Motocicleta");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CRED_EMP_MOTO");
			oneItem.setClave("1");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Vale");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CRED_EMP_VALE");
			oneItem.setClave("2");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Otro");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CRED_EMP_OTRO");
			oneItem.setClave("3");
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_MOTIVO_NOPAGO");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Motivo de no pago", "Motivo de no pago", "CAT_MOTIVO_NOPAGO", null, null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("No se encuentra");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CAT_MOTIVO_NOPAGO_NE");
			oneItem.setClave("1");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Sin dinero");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CAT_MOTIVO_NOPAGO_SD");
			oneItem.setClave("2");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Otro");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CAT_MOTIVO_NOPAGO_OT");
			oneItem.setClave("3");
			catalogoRepository.save(oneItem);
			oneItem = new Catalogo("Cobro Efectuado");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CAT_MOTIVO_NOPAGO_CE");
			oneItem.setClave("4");
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_MOTIVO_CANCEL_CRED");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Motivo cancelación crédito", "Motivo de cancelación", "CAT_MOTIVO_CANCEL_CRED",
					null, null);
			catalogoRepository.save(tipoCat);

			Catalogo oneItem = new Catalogo("Crédito incobrable");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CAT_MOTIVO_CANCEL_CRED_INC");
			oneItem.setClave("1");
			catalogoRepository.save(oneItem);

			oneItem = new Catalogo("Credito ficto");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CAT_MOTIVO_CANCEL_CRED_FICTO");
			oneItem.setClave("2");
			catalogoRepository.save(oneItem);
			
			oneItem = new Catalogo("Unificación crédito");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CAT_MOTIVO_CANCEL_CRED_UNIF");
			oneItem.setClave("3");
			catalogoRepository.save(oneItem);
		}

		tipoCat = getCatalogoCveInterna("CAT_MOTIVO_TRANSF_CLIE");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Motivo transferencia cliente", "Motivo de transferencia", "CAT_MOTIVO_TRANSF_CLIE",
					null, null);
			catalogoRepository.save(tipoCat);

			Catalogo oneItem = new Catalogo("Unificación cartera");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			oneItem.setClaveInterna("CAT_MOTIVO_TRANSF_CLIE_UNIF");
			oneItem.setClave("1");
			catalogoRepository.save(oneItem);

		}

		//Catalogo de idiomas
		tipoCat = getCatalogoCveInterna("CAT_IDIOMA");
		if (tipoCat == null) {
			tipoCat = new Catalogo("Idiomas","Idiomas hablado, escrito o comprendido","CAT_IDIOMA",null,null);
			catalogoRepository.save(tipoCat);
			Catalogo oneItem = new Catalogo("Español");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			
			oneItem = new Catalogo("Inglés");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			
			oneItem = new Catalogo("Italiano");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
			
			oneItem = new Catalogo("Frances");
			oneItem.setParentId(tipoCat.getIdCatalogo());
			catalogoRepository.save(oneItem);
		}
		//Catalogo de Estado Civil
				tipoCat = getCatalogoCveInterna("CAT_ESTADO_CIVIL");
				if (tipoCat == null) {
					tipoCat = new Catalogo("Estado Civil","Estado civil de cada empleado de la empresa","CAT_ESTADO_CIVIL",null,null);
					catalogoRepository.save(tipoCat);
					Catalogo oneItem = new Catalogo("Soltero");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					
					oneItem = new Catalogo("Casado");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					
					oneItem = new Catalogo("Union libre");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					
					oneItem = new Catalogo("Viudo");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
				}
				//Catalogo de Nacionalidad
				tipoCat = getCatalogoCveInterna("CAT_NACIONALIDAD");
				if (tipoCat == null) {
					tipoCat = new Catalogo("Nacionalidad del empleado","Nacionalidad del empleado que trabaja en la empresa","CAT_NACIONALIDAD",null,null);
					catalogoRepository.save(tipoCat);
					Catalogo oneItem = new Catalogo("Mexicana");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					
					oneItem = new Catalogo("Extranjero");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
				}
				//Catalogo de Tipo de Solicitudes (Vacaciones, permiso)
				tipoCat = getCatalogoCveInterna("CAT_SOLICITUD");
				if (tipoCat == null) {
					tipoCat = new Catalogo("Solicitud del empleado","Solicitud del empleado","CAT_SOLICITUD",null,null);
					catalogoRepository.save(tipoCat);
					Catalogo oneItem = new Catalogo("Vacaciones");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					oneItem = new Catalogo("Permiso (Inasistencia)");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
				}
				//Catalogo de titulo
				tipoCat = getCatalogoCveInterna("CAT_TITULO");
				if (tipoCat == null) {
					tipoCat = new Catalogo("Documento","Documento obtenido en la institucion donde realizo sus estudios","CAT_TITULO",null,null);
					catalogoRepository.save(tipoCat);
					Catalogo oneItem = new Catalogo("Titulo");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					
					oneItem = new Catalogo("Certificado carrera terminada");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					
					oneItem = new Catalogo("Certificado de carrera trunca");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					
					oneItem = new Catalogo("Cédula Profesional");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					
					oneItem = new Catalogo("Constancia");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
					
					oneItem = new Catalogo("Diploma");
					oneItem.setParentId(tipoCat.getIdCatalogo());
					catalogoRepository.save(oneItem);
				}
		

		// Parámetros
		Parametros tipoParam = getParametroByName("TOPE_PERF_AGENTE");
		if (tipoParam == null) {
			tipoParam = new Parametros("TOPE_PERF_AGENTE", 99000);
			paramRepository.save(tipoParam);
		}
		tipoParam = getParametroByName("TOPE_AVAL");
		if (tipoParam == null) {
			tipoParam = new Parametros("TOPE_AVAL", 6000);
			paramRepository.save(tipoParam);
		}
		tipoParam = getParametroByName("CREDITOS_ACTIVOS");
		if (tipoParam == null) {
			tipoParam = new Parametros("CREDITOS_ACTIVOS", 1);
			paramRepository.save(tipoParam);
		}
		tipoParam = getParametroByName("TOPE_ROLE_USER");
		if (tipoParam == null) {
			tipoParam = new Parametros("TOPE_ROLE_USER", 99999);
			paramRepository.save(tipoParam);
		}
		tipoParam = getParametroByName("COMISION_REESTRUCTURA");
		if (tipoParam == null) {
			tipoParam = new Parametros("COMISION_REESTRUCTURA", 10);
			paramRepository.save(tipoParam);
		}
		tipoParam = getParametroByName("TASA_IVA");
		if (tipoParam == null) {
			tipoParam = new Parametros("TASA_IVA", 16);
			paramRepository.save(tipoParam);
		}
		tipoParam = getParametroByName("SEMANA_ARRANQUE");
		if (tipoParam == null) {
			tipoParam = new Parametros("SEMANA_ARRANQUE", 10);
			paramRepository.save(tipoParam);
		}

		tipoParam = getParametroByName("BASE_NOM_ARRANQUE");
		if (tipoParam == null) {
			tipoParam = new Parametros("BASE_NOM_ARRANQUE", 2000);
			paramRepository.save(tipoParam);
		}

	}

	private Catalogo addCatalogo(Catalogo cat) {
		Catalogo item = this.getCatalogoCveInterna(cat.getClaveInterna());
		if (item == null)
			catalogoRepository.save(cat);
		return cat;
	}

	public Calendario addDefaultCalendar() {
		Calendario cal = new Calendario();
		cal.setNombre("CRENX Default");
		cal.setTimeZone("CST");
		calendarioRepository.save(cal);
		Horario hor = new Horario();
		hor.setDiasEfectivos(63);
		hor.setHoraInicial("09:00");
		hor.setHoraFinal("17:00");
		hor.setNombre("CRENX Default");
		hor.setCalendario(cal);
		horarioRepository.save(hor);
		return cal;
	}

	public Calendario defaultCalendario() {
		Collection<Calendario> cats = Lists.newArrayList(calRepository.findByNombre("CRENX Default"));
		Calendario cat = null;
		if (cats.size() == 0)
			cat = addDefaultCalendar();
		else
			cat = cats.iterator().next();
		return cat;
	}

	public Collection<Calendario> allCalendario() {
		Collection<Calendario> cats = Lists.newArrayList(calRepository.findAll());
		return cats;
	}

	public Calendario calendarioById(String idCalendario) {
		Calendario cal = calRepository.findOne(idCalendario);
		return cal;
	}

	public Catalogo getCatalogoByNombre(String name) {
		Collection<Catalogo> cats = Lists.newArrayList(catalogoRepository.findByNombre(name));
		Catalogo cat = cats.iterator().next();
		return cat;

	}

	public Catalogo getCatalogoCveInterna(String name) {
		Collection<Catalogo> cats = Lists.newArrayList(catalogoRepository.findByClaveInterna(name));
		if (cats.iterator().hasNext())
			return cats.iterator().next();
		else
			return null;
	}

	public Parametros getParametroByName(String name) {
		Collection<Parametros> params = Lists.newArrayList(paramRepository.findByNombre(name));
		if (params.iterator().hasNext())
			return params.iterator().next();
		else
			return null;
	}

	public Catalogo getCatalogoCveInternaClave(String claveInterna, String clave) {
		Collection<Catalogo> cats = Lists
				.newArrayList(catalogoRepository.findByClaveInternaAndClave(claveInterna, clave));
		if (cats.iterator().hasNext())
			return cats.iterator().next();
		else
			return null;
	}

	public Catalogo getCatalogoId(String idCatalogo) {
		return catalogoRepository.findOne(idCatalogo);
	}
	
	public Empleado getEmpleadoId(String idEmpleado){
		return empleadoRepository.findOne(idEmpleado);
	}
	
	public Domicilio getDomicilioId(String idDomicilio){
		return domicilioRepository.findOne(idDomicilio);
	}

	public List<OrgVO> getAssignedNodes() {
		List<Organizacion> nodos = Lists.newArrayList(orgRepository.findAll());

		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new OrgToTreeModelMap());
		Type listType = new TypeToken<List<OrgVO>>() {
		}.getType();
		List<OrgVO> listaNodos = mapper.map(nodos, listType);
		return listaNodos;
	}

	public Organizacion getNodoById(String idNodo) {
		if (!StringUtils.isEmpty(idNodo) && idNodo.equals("null")) {
			return orgRepository.findByClave("ROOT");
		}
		return orgRepository.findOne(idNodo);
	}

	private String usuarioAasignado(OrgVO orgNode, boolean titular) {
		String retVal = null;
		if (titular) {
			if (orgNode.getIdTitular() != null) {
				List<Organizacion> nodos = orgRepository.findByTitularIdUsuario(orgNode.getIdTitular());
				if (orgNode.getId() == null && nodos.size() > 0)
					return nodos.iterator().next().getNombre();
				for (Organizacion oneNode : nodos) {
					if (!oneNode.getIdOrg().equals(orgNode.getId())) {
						retVal = oneNode.getNombre();
						break;
					}
				}
			}
		} else {
			if (orgNode.getIdSuplente() != null) {
				List<Organizacion> nodos = orgRepository.findBySuplenteIdUsuario(orgNode.getIdSuplente());
				if (orgNode.getId() == null && nodos.size() > 0)
					return nodos.iterator().next().getNombre();
				for (Organizacion oneNode : nodos) {
					if (!oneNode.getIdOrg().equals(orgNode.getId())) {
						retVal = oneNode.getNombre();
						break;
					}
				}
			}

		}
		return retVal;
	}

	@Transactional
	public OrgVO saveNodo(OrgVO orgNode) throws Exception {
		String idTitularCurrent = null;
		String idSuplenteCurrent = null;

		Organizacion currentNode = null;
		if (orgNode.getId() != null)
			currentNode = orgRepository.findOne(orgNode.getId());
		if (currentNode != null) {
			idTitularCurrent = currentNode.getTitular() != null ? currentNode.getTitular().getIdUsuario() : null;
			idSuplenteCurrent = currentNode.getSuplente() != null ? currentNode.getSuplente().getIdUsuario() : null;
		}

		// Verificar que el Titular/Suplente no esté asignado a otro Nodo
		String orgName = usuarioAasignado(orgNode, true);
		if (orgName != null)
			throw new Exception("El titular ya está asignado a: " + orgName + ", primero tiene que desasignarlo");
		orgName = usuarioAasignado(orgNode, false);
		if (orgName != null)
			throw new Exception("El suplente ya está asignado a: " + orgName + ", primero tiene que desasignarlo");
		
		
		Catalogo estatus = null;
		if (orgNode.getIdStatus() == null)
			estatus = getCatalogoCveInternaClave("ESTATUS_ACTIVO", "1");
		else if (orgNode.getIdStatus().equals("1"))
			estatus = getCatalogoCveInternaClave("ESTATUS_ACTIVO", "1");
		else
			estatus = getCatalogoCveInternaClave("ESTATUS_INACTIVO", "2");

		Calendario cal = null;
		if (orgNode.getIdCalendario() == null) {
			cal = defaultCalendario();
		} else
			cal = calendarioById(orgNode.getIdCalendario());

		
		Organizacion parent = null;
		if (orgNode.getIdParent() == null) {
			if (!orgNode.getKey().equals("ROOT")) {
				Organizacion thisItem = orgRepository.findOne(orgNode.getId());
				parent = thisItem.getParent();
			}
		} else {
			parent = orgRepository.findOne(orgNode.getIdParent());
		}
		
		
		Organizacion orgItem = new Organizacion();
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new OrgVOToOrgModelMap());
		orgItem = mapper.map(orgNode, Organizacion.class);
		orgItem.setEstatus(estatus);
		orgItem.setCalendario(cal);
		
		if (parent != null)
			orgItem.setParent(parent);
		
		Usuario titular = null;
		if (orgNode.getIdTitular() != null)
			titular = usrRepository.findOne(orgNode.getIdTitular());
		
		Usuario suplente = null;
		if (orgNode.getIdSuplente() != null && !orgNode.getIdSuplente().equals(""))
			suplente = usrRepository.findOne(orgNode.getIdSuplente());
		else {
			// Suplente viene nulo, hay que checar si tenía ya un suplente
			if (currentNode != null) {
				if (currentNode.getSuplente() != null) { // ya tenía un suplente
					Usuario suplente2 = currentNode.getSuplente();
					CrearObjSecVO userRel = new CrearObjSecVO();
					userRel.setIdUsuario(suplente2.getIdUsuario());

					ObjetoSeguridad newRol = null;
					switch (orgItem.getTipoNodo()) {
					case "RR":
						newRol = encuentraObjeto("ROL_EJECUTIVO_VB");
						break;
					case "G":
						newRol = encuentraObjeto("ROL_GERENTE_V");
						break;
					case "R":
						newRol = encuentraObjeto("ROL_REGIONAL_V");
						break;
					case "D":
						newRol = encuentraObjeto("ROL_DIVISIONAL_V");
						break;
					}

					userRel.setIdObjetoSeguridadParent(newRol.getIdObjetoSeguridad());
					secServices.removeUserRoleRelation(userRel);

				}

			}
		}
		orgItem.setTitular(titular);
		orgItem.setSuplente(suplente);
		orgRepository.save(orgItem);
		// Crear Caja si no existe
		Caja cajaDB = cajaRepository.findByUnidadIdOrg(orgItem.getIdOrg());
		if (cajaDB == null) {
			cajaDB = new Caja();
			cajaDB.setUnidad(orgItem);
			cajaDB.setFechaCorte(new Date());
			cajaDB.setSaldoFinal(0);
			cajaDB.setSaldoInicial(0);
			cajaDB.setTotalAbonos(0);
			cajaDB.setTotalCargos(0);
			cajaRepository.save(cajaDB);
		}
		mapper = new ModelMapper();
		mapper.addMappings(new OrgToTreeModelMap());

		// Asignación-Remoción de Usuarios
		ObjetoSeguridad newRol = null;
		switch (orgItem.getTipoNodo()) {
		case "RR":
			newRol = encuentraObjeto("ROL_EJECUTIVO");
			break;
		case "G":
			newRol = encuentraObjeto("ROL_GERENTE");
			break;
		case "R":
			newRol = encuentraObjeto("ROL_REGIONAL");
			break;
		case "D":
			newRol = encuentraObjeto("ROL_DIVISIONAL");
			break;
		}
		ObjetoSeguridad newRolSuplente = null;
		switch (orgItem.getTipoNodo()) {
		case "RR":
			newRolSuplente = encuentraObjeto("ROL_EJECUTIVO_VB");
			break;
		case "G":
			newRolSuplente = encuentraObjeto("ROL_GERENTE_V");
			break;
		case "R":
			newRolSuplente = encuentraObjeto("ROL_REGIONAL_V");
			break;
		case "D":
			newRolSuplente = encuentraObjeto("ROL_DIVISIONAL_V");
			break;
		}


		ObjetoSeguridad currentRol = null;
		if (currentNode != null) {
			switch (currentNode.getTipoNodo()) {
			case "RR":
				currentRol = encuentraObjeto("ROL_EJECUTIVO");
				break;
			case "G":
				currentRol = encuentraObjeto("ROL_GERENTE");
				break;
			case "R":
				currentRol = encuentraObjeto("ROL_REGIONAL");
				break;
			case "D":
				currentRol = encuentraObjeto("ROL_DIVISIONAL");
				break;
			}
		}

		// Agregar al Rol
		CrearObjSecVO userRel = new CrearObjSecVO();
		if (titular != null) {
			userRel.setIdUsuario(titular.getIdUsuario());
			userRel.setIdObjetoSeguridadParent(newRol.getIdObjetoSeguridad());
			secServices.agregaUsuarioRol(userRel);
		}
		if (suplente != null) {
			userRel.setIdUsuario(suplente.getIdUsuario());
			userRel.setIdObjetoSeguridadParent(newRolSuplente.getIdObjetoSeguridad());
			secServices.agregaUsuarioRol(userRel);
		}

		// Remover del al Rol
		userRel = new CrearObjSecVO();
		if (idTitularCurrent != null && currentRol != null) {
			if (idTitularCurrent != (titular == null ? "" : titular.getIdUsuario())) {
				userRel.setIdUsuario(idTitularCurrent);
				userRel.setIdObjetoSeguridadParent(currentRol.getIdObjetoSeguridad());
				secServices.removeUserRoleRelation(userRel);
			}
		}
		if (idSuplenteCurrent != null && currentRol != null) {
			if (idSuplenteCurrent != (suplente == null ? "" : suplente.getIdUsuario())) {
				userRel.setIdUsuario(idSuplenteCurrent);
				userRel.setIdObjetoSeguridadParent(newRolSuplente.getIdObjetoSeguridad());
				secServices.removeUserRoleRelation(userRel);
			}
		}
		return mapper.map(orgItem, OrgVO.class);
	}

	// Cuenta Post-Construct
	// Post Construct Cuentas
	public void validateSecurityObjects() {
		List<ObjetoSeguridadRelaciones> listObjetos = new ArrayList<>();
		Catalogo estatus = getCatalogoCveInterna("ESTATUS_ACTIVO");
		ObjetoSeguridad organizerCrenx = encuentraObjeto("ORG_CRENX");
		if (organizerCrenx == null) {
			organizerCrenx = new ObjetoSeguridad("SEC_ORGANIZER", "CRENX", "Organizer crenx", "ORG_CRENX", "",
					new Date());
		}
		organizerCrenx.setEstatusActual(estatus);

		ObjetoSeguridad administrador = encuentraObjeto("ROL_ADMIN");
		if (administrador == null) {
			administrador = new ObjetoSeguridad("SEC_ROLE", "Administrador", "Usuario administrador", "ROL_ADMIN",
					"app.usuario", new Date());
			administrador.setEstatusActual(estatus);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(administrador);
			listObjetos.add(adminRel);
		}
		ObjetoSeguridad adminParams = encuentraObjeto("ROL_ADMIN_PARAM");
		if (adminParams == null) {
			adminParams = new ObjetoSeguridad("SEC_ROLE", "Administrador de parámetros",
					"Usuario administrador de parámetros", "ROL_ADMIN_PARAM", "app.usuario", new Date());
			adminParams.setEstatusActual(estatus);
			objetoSeguridadRepository.save(adminParams);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(adminParams);
			listObjetos.add(adminRel);
		}

		ObjetoSeguridad adminSeg = encuentraObjeto("ROL_ADMIN_SEG");
		if (adminSeg == null) {
			adminSeg = new ObjetoSeguridad("SEC_ROLE", "Administrador de seguridad",
					"Usuario administrador de seguridad", "ROL_ADMIN_SEG", "app.usuario", new Date());
			adminSeg.setEstatusActual(estatus);
			objetoSeguridadRepository.save(adminSeg);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(adminSeg);
			listObjetos.add(adminRel);
		}

		ObjetoSeguridad rolCorporativo = encuentraObjeto("ROL_CORPORATIVO");
		if (rolCorporativo == null) {
			rolCorporativo = new ObjetoSeguridad("SEC_ROLE", "Ejecutivo corporativo", "Usuario corporativo",
					"ROL_CORPORATIVO", "app.usuario", new Date());
			rolCorporativo.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolCorporativo);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolCorporativo);
			listObjetos.add(adminRel);
		}

		ObjetoSeguridad rolSoporteOp = encuentraObjeto("ROL_SOPORTE_OP");
		if (rolSoporteOp == null) {
			rolSoporteOp = new ObjetoSeguridad("SEC_ROLE", "Soporte y operación", "Usuario de soporte y operación",
					"ROL_SOPORTE_OP", "app.usuario", new Date());
			rolSoporteOp.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolSoporteOp);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolSoporteOp);
			listObjetos.add(adminRel);
		} else {
			rolSoporteOp.setNombre("Soporte y operación");
			rolSoporteOp.setDescripcion("Usuario de soporte y operación");
			objetoSeguridadRepository.save(rolSoporteOp);
		}

		ObjetoSeguridad rolDivisional = encuentraObjeto("ROL_DIVISIONAL");
		if (rolDivisional == null) {
			rolDivisional = new ObjetoSeguridad("SEC_ROLE", "Ejecutivo divisional", "Usuario ejecutivo divisional",
					"ROL_DIVISIONAL", "app.usuario", new Date());
			rolDivisional.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolDivisional);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolDivisional);
			listObjetos.add(adminRel);
		}
		
		ObjetoSeguridad rolDivisionalVolante = encuentraObjeto("ROL_DIVISIONAL_V");
		if (rolDivisionalVolante == null) {
			rolDivisionalVolante = new ObjetoSeguridad("SEC_ROLE", "Ejecutivo divisional volante", "Usuario ejecutivo divisional volante",
					"ROL_DIVISIONAL_V", "app.usuario", new Date());
			rolDivisionalVolante.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolDivisionalVolante);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolDivisionalVolante);
			listObjetos.add(adminRel);
		}
		else {
			rolDivisionalVolante.setNombre("Ejecutivo divisional volante");
			rolDivisionalVolante.setDescripcion("Usuario ejecutivo divisional volante");
			objetoSeguridadRepository.save(rolDivisionalVolante);
		}

		ObjetoSeguridad rolRegional = encuentraObjeto("ROL_REGIONAL");
		if (rolRegional == null) {
			rolRegional = new ObjetoSeguridad("SEC_ROLE", "Ejecutivo regional CyC", "Usuario ejecutivo regional CyC",
					"ROL_REGIONAL", "app.usuario", new Date());
			rolRegional.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolRegional);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolRegional);
			listObjetos.add(adminRel);
		} else {
			rolRegional.setNombre("Ejecutivo regional CyC");
			rolRegional.setDescripcion("Usuario ejecutivo regional CyC");
			objetoSeguridadRepository.save(rolRegional);
		}

		ObjetoSeguridad rolRegionalAdm = encuentraObjeto("ROL_REGIONAL_ADM");
		if (rolRegionalAdm == null) {
			rolRegionalAdm = new ObjetoSeguridad("SEC_ROLE", "Ejecutivo regional Adm", "Usuario ejecutivo regional Adm",
					"ROL_REGIONAL_ADM", "app.usuario", new Date());
			rolRegionalAdm.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolRegionalAdm);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolRegionalAdm);
			listObjetos.add(adminRel);
		} else {
			rolRegionalAdm.setNombre("Ejecutivo regional Adm");
			rolRegionalAdm.setDescripcion("Usuario ejecutivo regional Adm");
			objetoSeguridadRepository.save(rolRegionalAdm);
		}


		ObjetoSeguridad rolRegionalVolante = encuentraObjeto("ROL_REGIONAL_V");
		if (rolRegionalVolante == null) {
			rolRegionalVolante = new ObjetoSeguridad("SEC_ROLE", "Ejecutivo regional V", "Usuario ejecutivo regional V",
					"ROL_REGIONAL_V", "app.usuario", new Date());
			rolRegionalVolante.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolRegionalVolante);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolRegionalAdm);
			listObjetos.add(adminRel);
		} else {
			rolRegionalVolante.setNombre("Ejecutivo regional V");
			rolRegionalVolante.setDescripcion("Usuario ejecutivo regional V");
			objetoSeguridadRepository.save(rolRegionalVolante);
		}
		
		
		ObjetoSeguridad rolGerente = encuentraObjeto("ROL_GERENTE");
		if (rolGerente == null) {
			rolGerente = new ObjetoSeguridad("SEC_ROLE", "Gerente de rutas", "Usuario gerente de rutas", "ROL_GERENTE",
					"app.usuario", new Date());
			rolGerente.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolGerente);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolGerente);
			listObjetos.add(adminRel);
		} else {
			rolGerente.setNombre("Gerente CyC");
			rolGerente.setDescripcion("Usuario gerente de credito y cobranza");
			objetoSeguridadRepository.save(rolGerente);
		}

		ObjetoSeguridad rolGerenteV = encuentraObjeto("ROL_GERENTE_V");
		if (rolGerenteV == null) {
			rolGerenteV = new ObjetoSeguridad("SEC_ROLE", "Supervisor", "Gerente suplente",
					"ROL_GERENTE_V", "app.usuario", new Date());
			rolGerenteV.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolGerenteV);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolGerenteV);
			listObjetos.add(adminRel);
		} else {
			rolGerenteV.setNombre("Supervisor");
			rolGerenteV.setDescripcion("Gerente suplente");
			objetoSeguridadRepository.save(rolGerenteV);
		}

		ObjetoSeguridad rolEjecutivo = encuentraObjeto("ROL_EJECUTIVO");
		if (rolEjecutivo == null) {
			rolEjecutivo = new ObjetoSeguridad("SEC_ROLE", "Ejecutivo CyC", "Usuario ejecutivo credito y cobranza",
					"ROL_EJECUTIVO", "app.usuario", new Date());
			rolEjecutivo.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolEjecutivo);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolEjecutivo);
			listObjetos.add(adminRel);
		} else {
			rolEjecutivo.setNombre("Ejecutivo CyC");
			rolEjecutivo.setDescripcion("Usuario ejecutivo credito y cobranza");
			objetoSeguridadRepository.save(rolEjecutivo);
		}

		ObjetoSeguridad rolEjecutivoVB = encuentraObjeto("ROL_EJECUTIVO_VB");
		if (rolEjecutivoVB == null) {
			rolEjecutivoVB = new ObjetoSeguridad("SEC_ROLE", "Ejecutivo Volante B", "Usuario ejecutivo volante B",
					"ROL_EJECUTIVO_VB", "app.usuario", new Date());
			rolEjecutivoVB.setEstatusActual(estatus);
			objetoSeguridadRepository.save(rolEjecutivoVB);
			ObjetoSeguridadRelaciones adminRel = new ObjetoSeguridadRelaciones();
			adminRel.setParent(organizerCrenx);
			adminRel.setChild(rolEjecutivoVB);
			listObjetos.add(adminRel);
		}

		agregaObjeto("SEC_OBJECT", "Menu.Usuario", "Permiso para visualizar menu usuario", "OBJ_MENUUSUARIOS", "",
				listObjetos, estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Menu.Usuario", "Permiso para visualizar menu usuario", "OBJ_MENUUSUARIOS", "",
				listObjetos, estatus, rolSoporteOp);
		
		agregaObjeto("SEC_OBJECT", "Menu.Rol", "Permiso para visualizar menu rol", "OBJ_MENUROL", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Menu.Rol", "Permiso para visualizar menu rol", "OBJ_MENUROL", "", listObjetos,
				estatus, rolSoporteOp);
		
		agregaObjeto("SEC_OBJECT", "Org.Fondear", "Permiso para fondear", "OBJ_FONDEAR", "", listObjetos, estatus,
				administrador);
		agregaObjeto("SEC_OBJECT", "Org.Fondear", "Permiso para fondear", "OBJ_FONDEAR", "", listObjetos, estatus,
				rolSoporteOp);
		
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, rolDivisional);
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, rolDivisionalVolante);
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, rolRegional);
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, rolRegionalVolante);
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, rolGerente);
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, rolGerenteV);
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "Org.RegGastos", "Permiso para registrar gastos", "OBJ_REG_GASTOS", "", listObjetos,
				estatus, rolEjecutivoVB);
		
		agregaObjeto("SEC_OBJECT", "Org.CanGastos", "Permiso para cancelar gastos", "OBJ_CAN_GASTOS", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Org.CanGastos", "Permiso para cancelar gastos", "OBJ_CAN_GASTOS", "", listObjetos,
				estatus, rolSoporteOp);
		
		agregaObjeto("SEC_OBJECT", "Org.Transfer", "Permiso para registrar transferencias", "OBJ_REG_TRANSFER", "",
				listObjetos, estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Org.Transfer", "Permiso para registrar transferencias", "OBJ_REG_TRANSFER", "",
				listObjetos, estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Org.Transfer", "Permiso para registrar transferencias", "OBJ_REG_TRANSFER", "",
				listObjetos, estatus, rolDivisional);
		agregaObjeto("SEC_OBJECT", "Org.Transfer", "Permiso para registrar transferencias", "OBJ_REG_TRANSFER", "",
				listObjetos, estatus, rolRegional);
		agregaObjeto("SEC_OBJECT", "Org.Transfer", "Permiso para registrar transferencias", "OBJ_REG_TRANSFER", "",
				listObjetos, estatus, rolGerente);
		agregaObjeto("SEC_OBJECT", "Org.Transfer", "Permiso para registrar transferencias", "OBJ_REG_TRANSFER", "",
				listObjetos, estatus, rolEjecutivo);
		
		
		agregaObjeto("SEC_OBJECT", "Org.CanTransfer", "Permiso para cancelar transferencias", "OBJ_CAN_TRANSFER", "",
				listObjetos, estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Org.CanTransfer", "Permiso para cancelar transferencias", "OBJ_CAN_TRANSFER", "",
				listObjetos, estatus, rolSoporteOp);

		agregaObjeto("SEC_OBJECT", "Org.Editar", "Permiso para editar organización", "OBJ_ORG_EDITAR", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Org.Editar", "Permiso para editar organización", "OBJ_ORG_EDITAR", "", listObjetos,
				estatus, rolSoporteOp);
		
		agregaObjeto("SEC_OBJECT", "Org.Crear", "Permiso para crear organización", "OBJ_ORG_CREAR", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Org.Crear", "Permiso para crear organización", "OBJ_ORG_CREAR", "", listObjetos,
				estatus, rolSoporteOp);

		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				administrador);
		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				rolDivisional);
		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				rolDivisionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				rolRegional);
		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				rolRegionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				rolGerente);
		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				rolGerenteV);
		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "Cli.Ver", "Permiso para ver clientes", "OBJ_CLI_VER", "", listObjetos, estatus,
				rolEjecutivoVB);
		
		
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus, administrador);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus,rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus,rolDivisional);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus,rolDivisionalVolante);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus, rolRegional);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus, rolGerente);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus, rolGerenteV);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus, rolRegional);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus, rolRegionalVolante);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus, rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "ReportVisitas.Ver", "Permiso para ver Reporte Visitas", "OBJ_REPORT_VIS", "",
				listObjetos, estatus, rolEjecutivoVB);
		

		agregaObjeto("SEC_OBJECT", "Cli.Trasp", "Permiso para traspaso de clientes", "OBJ_CLI_TRASP", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Cli.Trasp", "Permiso para traspaso de clientes", "OBJ_CLI_TRASP", "", listObjetos,
				estatus, rolSoporteOp);

		agregaObjeto("SEC_OBJECT", "Cli.Crear", "Permiso para crear clientes", "OBJ_CLI_CREAR", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Cli.Crear", "Permiso para crear clientes", "OBJ_CLI_CREAR", "", listObjetos,
				estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Cli.Crear", "Permiso para crear clientes", "OBJ_CLI_CREAR", "", listObjetos,
				estatus, rolEjecutivo);
		
		
		agregaObjeto("SEC_OBJECT", "CliCo.Crear", "Permiso para crear codeudores", "OBJ_CLI_CO_CREAR", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "CliCo.Crear", "Permiso para crear codeudores", "OBJ_CLI_CO_CREAR", "", listObjetos,
				estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "CliCo.Crear", "Permiso para crear codeudores", "OBJ_CLI_CO_CREAR", "", listObjetos,
				estatus, rolEjecutivo);

		
		agregaObjeto("SEC_OBJECT", "Cli.Docs", "Permiso para documentos de clientes", "OBJ_CLI_DOCS", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Cli.Docs", "Permiso para documentos de clientes", "OBJ_CLI_DOCS", "", listObjetos,
				estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Cli.Docs", "Permiso para documentos de clientes", "OBJ_CLI_DOCS", "", listObjetos,
				estatus, rolEjecutivo);
		
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, rolDivisional);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, rolDivisionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, rolRegional);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, rolRegionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, rolGerente);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, rolGerenteV);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCliente", "Permiso para ver documentos de clientes", "OBJ_CLI_DOCS_VER",
				"", listObjetos, estatus, rolEjecutivoVB);
		
		
		agregaObjeto("SEC_OBJECT", "Cli.DocsCrearCliente", "Permiso para crear documentos de clientes",
				"OBJ_CLI_DOCS_CREAR", "", listObjetos, estatus, rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "Cli.DocsCrearCliente", "Permiso para crear documentos de clientes",
				"OBJ_CLI_DOCS_CREAR", "", listObjetos, estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Cli.DocsCrearCliente", "Permiso para crear documentos de clientes",
				"OBJ_CLI_DOCS_CREAR", "", listObjetos, estatus, administrador);
		
		agregaObjeto("SEC_OBJECT", "Cli.DocsCrearCod", "Permiso para crear documentos de codeudor",
				"OBJ_CLI_CO_DOCS_CREAR", "", listObjetos, estatus, rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "Cli.DocsCrearCod", "Permiso para crear documentos de codeudor",
				"OBJ_CLI_CO_DOCS_CREAR", "", listObjetos, estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Cli.DocsCrearCod", "Permiso para crear documentos de codeudor",
				"OBJ_CLI_CO_DOCS_CREAR", "", listObjetos, estatus, administrador);

		
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, rolDivisional);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, rolDivisionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, rolRegional);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, rolRegionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, rolGerente);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, rolGerenteV);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "Cli.DocsVerCod", "Permiso para ver documentos de codeudor", "OBJ_CLI_CO_DOCS_VER",
				"", listObjetos, estatus, rolEjecutivoVB);
		
		
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, rolDivisional);
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, rolDivisionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, rolRegional);
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, rolRegionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, rolGerente);
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, rolGerenteV);
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "Cli.VerCod", "Permiso para ver codeudor", "OBJ_CO_VER",
				"", listObjetos, estatus, rolEjecutivoVB);
		
		

		agregaObjeto("SEC_OBJECT", "Cred.Crear", "Permiso para crear créditos", "OBJ_CRED_CREAR", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Cred.Crear", "Permiso para crear créditos", "OBJ_CRED_CREAR", "", listObjetos,
				estatus, rolGerente);
		agregaObjeto("SEC_OBJECT", "Cred.Crear", "Permiso para crear créditos", "OBJ_CRED_CREAR", "", listObjetos,
				estatus, rolEjecutivo);

		
		agregaObjeto("SEC_OBJECT", "Pago.Crear", "Permiso para crear pagos", "OBJ_PAGO_CREAR", "", listObjetos, estatus,
				administrador);
		agregaObjeto("SEC_OBJECT", "Pago.Crear", "Permiso para crear pagos", "OBJ_PAGO_CREAR", "", listObjetos, estatus,
				rolEjecutivo);
		
		
		agregaObjeto("SEC_OBJECT", "Visita.Reg", "Permiso para registrar visita", "OBJ_VIS_REG", "", listObjetos, estatus,
				rolDivisional);
		agregaObjeto("SEC_OBJECT", "Visita.Reg", "Permiso para registrar visita", "OBJ_VIS_REG", "", listObjetos, estatus,
				rolDivisionalVolante);
		agregaObjeto("SEC_OBJECT", "Visita.Reg", "Permiso para registrar visita", "OBJ_VIS_REG", "", listObjetos, estatus,
				rolRegional);
		agregaObjeto("SEC_OBJECT", "Visita.Reg", "Permiso para registrar visita", "OBJ_VIS_REG", "", listObjetos, estatus,
				rolRegionalVolante);
		agregaObjeto("SEC_OBJECT", "Visita.Reg", "Permiso para registrar visita", "OBJ_VIS_REG", "", listObjetos, estatus,
				rolGerente);
		agregaObjeto("SEC_OBJECT", "Visita.Reg", "Permiso para registrar visita", "OBJ_VIS_REG", "", listObjetos, estatus,
				rolGerenteV);
		agregaObjeto("SEC_OBJECT", "Visita.Reg", "Permiso para registrar visita", "OBJ_VIS_REG", "", listObjetos, estatus,
				rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "Visita.Reg", "Permiso para registrar visita", "OBJ_VIS_REG", "", listObjetos, estatus,
				rolEjecutivoVB);
		
		
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, rolDivisional);
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, rolDivisionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, rolRegional);
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, rolRegionalVolante);
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, rolGerente);
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, rolGerenteV);
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, rolEjecutivo);
		agregaObjeto("SEC_OBJECT", "Cli.Hist", "Consultar historial crediticio", "OBJ_CLI_HIST", "", listObjetos,
				estatus, rolEjecutivoVB);
		

		agregaObjeto("SEC_OBJECT", "Cred.Reest", "Permiso re-estructurar créditos", "OBJ_CRED_REEST", "", listObjetos,
				estatus, rolSoporteOp);
		
		agregaObjeto("SEC_OBJECT", "Cred.Edit", "Permiso editar créditos", "OBJ_CRED_EDIT", "", listObjetos, estatus,
				rolSoporteOp);
		
		agregaObjeto("SEC_OBJECT", "ADM.Sueldos", "Reporte de sueldos", "OBJ_ADM_SUELDO", "", listObjetos, estatus,
				rolSoporteOp);
		agregaObjeto("SEC_OBJECT", "ADM.Sueldos", "Reporte de sueldos", "OBJ_ADM_SUELDO", "", listObjetos, estatus,
				administrador);
		
		agregaObjeto("SEC_OBJECT", "Cred.Entrega", "Entregar crédito", "OBJ_CRED_ENTREGA", "", listObjetos, estatus,
				rolGerente);
		
		agregaObjeto("SEC_OBJECT", "Cli.Unif", "Permiso para unificación de clientes", "OBJ_CLI_UNIF", "", listObjetos,
				estatus, administrador);
		agregaObjeto("SEC_OBJECT", "Cli.Unif", "Permiso para unificación de clientes", "OBJ_CLI_UNIF", "", listObjetos,
				estatus, rolSoporteOp);
		
		
		ObjetoSeguridad perfil = encuentraObjeto("PERF_AGENTE");
		if (objetoSeguridadRepository.count() == 0) {
			perfil.setNombre("PERF_AGENTE");
			perfil.setCreacion(new Date());
			perfil.setLlaveNegocio("PERF_AGENTE");
			perfil.setTipoObje("SEC_PROFILE");
			perfil.setDescripcion("Perfil para agentes");
			perfil.setEstatusActual(estatus);
			objetoSeguridadRepository.save(perfil);
		}

		objetoSeguridadRepository.save(administrador);
		objetoSeguridadRepository.save(organizerCrenx);

		for (ObjetoSeguridadRelaciones oneRel : listObjetos) {
			CrearObjSecVO objSec = new CrearObjSecVO();
			objSec.setIdObjetoSeguridadParent(oneRel.getParent().getIdObjetoSeguridad());
			objSec.setIdObjetoSeguridad(oneRel.getChild().getIdObjetoSeguridad());
			try {
				secServices.agregaPermisoRol(objSec);
			} catch (Exception ex) {
				System.out.println("Agregando relaciones..." + oneRel.getParent().getNombre() + "-->"
						+ oneRel.getChild().getNombre() + " " + ex.getMessage());
			}
		}
	}

	public void agregaReglas() {

		Catalogo periodoEvento = this.getCatalogoCveInterna("PERIODO_EVENTO");
		Catalogo periodoDia = this.getCatalogoCveInterna("PERIODO_DIARIO");
		Catalogo periodoSemana = this.getCatalogoCveInterna("PERIODO_SEMANA");
		Catalogo periodoQuincena = this.getCatalogoCveInterna("PERIODO_QUINCENA");
		Catalogo periodoMes = this.getCatalogoCveInterna("PERIODO_MENSUAL");

		Catalogo gastoGasolina = this.getCatalogoCveInterna("G1");
		Catalogo gastoCal = this.getCatalogoCveInterna("G222");
		Catalogo gastoCamisas = this.getCatalogoCveInterna("G223");
		Catalogo gastoCapacitacion = this.getCatalogoCveInterna("G7");
		Catalogo gastoCascos = this.getCatalogoCveInterna("G224");
		Catalogo gastoCasetas = this.getCatalogoCveInterna("G2");
		Catalogo gastoComidas = this.getCatalogoCveInterna("G9");
		Catalogo gastoCelular = this.getCatalogoCveInterna("G12");
		Catalogo gastoMotos = this.getCatalogoCveInterna("G14");
		Catalogo gastoRollos = this.getCatalogoCveInterna("G225");
		Catalogo gastoImpermeables = this.getCatalogoCveInterna("G226");
		Catalogo gastoPlanes = this.getCatalogoCveInterna("G227");
		Catalogo gastoEmpleacado = this.getCatalogoCveInterna("G228");
		Catalogo gastoMedicos = this.getCatalogoCveInterna("G17");
		Catalogo gastoHotel = this.getCatalogoCveInterna("G8");
		Catalogo gastoLavanderia = this.getCatalogoCveInterna("G22");
		Catalogo gastoLimpieza = this.getCatalogoCveInterna("G20");
		Catalogo gastoMkt = this.getCatalogoCveInterna("G229");
		Catalogo gastoMttoMoto = this.getCatalogoCveInterna("G3");
		Catalogo gastoMttoVehi = this.getCatalogoCveInterna("G4");
		Catalogo gastoMobEqui = this.getCatalogoCveInterna("G19");
		Catalogo gastoNomina = this.getCatalogoCveInterna("G230");
		Catalogo gastoImpresoras = this.getCatalogoCveInterna("G231");
		Catalogo gastoRentas = this.getCatalogoCveInterna("G15");
		Catalogo gastoSistema = this.getCatalogoCveInterna("G18");
		Catalogo gastoPapeleria = this.getCatalogoCveInterna("G6");
		Catalogo gastoPeriodico = this.getCatalogoCveInterna("G232");
		Catalogo gastoPlayeras = this.getCatalogoCveInterna("G233");
		Catalogo gastoRecargas = this.getCatalogoCveInterna("G5");
		Catalogo gastoSeguros = this.getCatalogoCveInterna("G16");
		Catalogo gastoServicios = this.getCatalogoCveInterna("G234");
		Catalogo gastoTarjetas = this.getCatalogoCveInterna("G235");
		Catalogo gastoTransporte = this.getCatalogoCveInterna("G11");

		agregaReglaGasto("ROL_ADMIN", gastoCal, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoCamisas, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoMotos, periodoEvento, 9999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoRollos, periodoMes, 5000.0, true);
		agregaReglaGasto("ROL_ADMIN", gastoImpermeables, periodoMes, 15000.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoEmpleacado, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoMedicos, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoLimpieza, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoMkt, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoMobEqui, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoNomina, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoImpresoras, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoSistema, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoPapeleria, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoPeriodico, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoPlayeras, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoRentas, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoSeguros, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoServicios, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_ADMIN", gastoTarjetas, periodoEvento, 999999.0, false);

		agregaReglaGasto("ROL_SOPORTE_OP", gastoCal, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoCamisas, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoMotos, periodoEvento, 9999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoRollos, periodoMes, 5000.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoImpermeables, periodoMes, 15000.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoEmpleacado, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoMedicos, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoLimpieza, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoMkt, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoMobEqui, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoNomina, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoImpresoras, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoSistema, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoPapeleria, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoPeriodico, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoPlanes, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoRentas, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoSeguros, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoServicios, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_SOPORTE_OP", gastoTarjetas, periodoEvento, 999999.0, false);

		agregaReglaGasto("ROL_EJECUTIVO", gastoGasolina, periodoEvento, 400.0, false);
		agregaReglaGasto("ROL_EJECUTIVO", gastoMedicos, periodoEvento, 500.0, false);
		agregaReglaGasto("ROL_EJECUTIVO", gastoMttoMoto, periodoEvento, 1000.0, true);
		agregaReglaGasto("ROL_EJECUTIVO", gastoRentas, periodoEvento, 1000.0, true);
		agregaReglaGasto("ROL_EJECUTIVO", gastoPapeleria, periodoEvento, 150.0, false);
		agregaReglaGasto("ROL_EJECUTIVO", gastoRecargas, periodoEvento, 100.0, true);
		agregaReglaGasto("ROL_EJECUTIVO", gastoTransporte, periodoEvento, 250.0, false);

		agregaReglaGasto("ROL_GERENTE", gastoCasetas, periodoEvento, 100.0, false);
		agregaReglaGasto("ROL_GERENTE", gastoGasolina, periodoEvento, 300.0, true);
		agregaReglaGasto("ROL_GERENTE", gastoMedicos, periodoEvento, 500.0, false);
		agregaReglaGasto("ROL_GERENTE", gastoMttoMoto, periodoEvento, 1500.0, true);
		agregaReglaGasto("ROL_GERENTE", gastoRentas, periodoEvento, 1500.0, true);
		agregaReglaGasto("ROL_GERENTE", gastoPapeleria, periodoSemana, 150.0, false);
		agregaReglaGasto("ROL_GERENTE", gastoRecargas, periodoEvento, 100.0, true);
		agregaReglaGasto("ROL_GERENTE", gastoTransporte, periodoEvento, 500.0, false);

		agregaReglaGasto("ROL_GERENTE_V", gastoComidas, periodoDia, 500.0, false);
		agregaReglaGasto("ROL_GERENTE_V", gastoGasolina, periodoEvento, 300.0, true);
		agregaReglaGasto("ROL_GERENTE_V", gastoMedicos, periodoEvento, 500.0, false);
		agregaReglaGasto("ROL_GERENTE_V", gastoHotel, periodoDia, 350.0, true);
		agregaReglaGasto("ROL_GERENTE_V", gastoLavanderia, periodoEvento, 100.0, false);
		agregaReglaGasto("ROL_GERENTE_V", gastoMttoMoto, periodoEvento, 1500.0, true);
		agregaReglaGasto("ROL_GERENTE_V", gastoPapeleria, periodoSemana, 150.0, false);
		agregaReglaGasto("ROL_GERENTE_V", gastoRecargas, periodoEvento, 100.0, false);
		agregaReglaGasto("ROL_GERENTE_V", gastoTransporte, periodoEvento, 1000.0, false);

		agregaReglaGasto("ROL_REGIONAL", gastoCapacitacion, periodoQuincena, 500.0, false);
		agregaReglaGasto("ROL_REGIONAL", gastoCascos, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_REGIONAL", gastoCasetas, periodoEvento, 1000.0, false);
		agregaReglaGasto("ROL_REGIONAL", gastoComidas, periodoSemana, 500.0, false);
		agregaReglaGasto("ROL_REGIONAL", gastoCelular, periodoEvento, 2500.0, true);
		agregaReglaGasto("ROL_REGIONAL", gastoMotos, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_REGIONAL", gastoPlanes, periodoEvento, 2500.0, true);
		agregaReglaGasto("ROL_REGIONAL", gastoGasolina, periodoEvento, 2000.0, true);
		agregaReglaGasto("ROL_REGIONAL", gastoMedicos, periodoEvento, 500.0, true);
		agregaReglaGasto("ROL_REGIONAL", gastoHotel, periodoDia, 450.0, true);
		agregaReglaGasto("ROL_REGIONAL", gastoMttoVehi, periodoEvento, 99999.0, false);
		agregaReglaGasto("ROL_REGIONAL", gastoImpresoras, periodoEvento, 999999.0, false);
		agregaReglaGasto("ROL_REGIONAL", gastoRentas, periodoMes, 2500.0, true);
		agregaReglaGasto("ROL_REGIONAL", gastoPapeleria, periodoSemana, 150.0, false);
		agregaReglaGasto("ROL_REGIONAL", gastoTransporte, periodoEvento, 250.0, false);

		agregaReglaCredito("ROL_EJECUTIVO", 0);
		agregaReglaCredito("ROL_GERENTE", 8000);
		agregaReglaCredito("ROL_REGIONAL", 10000);
		agregaReglaCredito("ROL_ADMIN", 990000000);

	}

	private void agregaReglaGasto(String rol, Catalogo gasto, Catalogo periodo, double maximo, boolean requiereCFDI) {
		List<ReglaGasto> reglas = regGasRepository.findByRolLlaveNegocioAndGastoIdCatalogo(rol, gasto.getIdCatalogo());
		if (reglas.size() > 0)
			return;
		ReglaGasto regla = new ReglaGasto();
		regla.setRol(secServices.getByLLaveNegocio(rol));
		regla.setGasto(gasto);
		regla.setPeriodo(periodo);
		regla.setMaximo(maximo);
		regla.setRequiereCFDI(requiereCFDI);
		regGasRepository.save(regla);
	}

	private void agregaReglaCredito(String rol, double maximo) {
		List<String> roles = new ArrayList<String>();
		roles.add(rol);
		List<ReglaCredito> reglas = regCredRepository.findByRolLlaveNegocioIn(roles);
		if (reglas.size() > 0)
			return;
		ReglaCredito regla = new ReglaCredito();
		regla.setRol(secServices.getByLLaveNegocio(rol));
		regla.setMaximo(maximo);
		regCredRepository.save(regla);
	}

	void agregaObjeto(String tipo, String nombre, String descripcion, String clave, String interno,
			List<ObjetoSeguridadRelaciones> listObjetos, Catalogo estatus, ObjetoSeguridad administrador) {
		ObjetoSeguridad userObj = encuentraObjeto(clave);
		if (userObj == null) {
			userObj = new ObjetoSeguridad(tipo, nombre, descripcion, clave, interno, new Date());
			userObj.setEstatusActual(estatus);
			objetoSeguridadRepository.save(userObj);
		}
		ObjetoSeguridadRelaciones seg1Rel = new ObjetoSeguridadRelaciones();
		seg1Rel.setParent(administrador);
		seg1Rel.setChild(userObj);
		listObjetos.add(seg1Rel);
	}

	ObjetoSeguridad encuentraObjeto(String llaveNegocio) {
		List<ObjetoSeguridad> objSeguridad = objetoSeguridadRepository.findByLlaveNegocio(llaveNegocio);
		if (objSeguridad.isEmpty())
			return null;
		return objSeguridad.get(0);
	}

	public void generateDefaultUser() {

		Usuario usuario = secServices.getPrincipal("admin");
		if (usuario == null) {
			usuario = new Usuario();

			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode("admin");
			usuario.setHashContrasena(hashedPassword);
			usuario.setCorreoElectronico("admin@admin.com");
			usuario.setNombreUsuario("admin");
			usuario.setNombre("admin");
			usuario.setApellidoPaterno("admin");
			usuario.setApellidoMaterno("admin");
			usuario.setCreacion(new Date());
			usuario.setEstatusActual(getCatalogoCveInterna("ESTATUS_ACTIVO"));
			usrRepository.save(usuario);
			Empleado empl = new Empleado();
			empl.setApellidoMaterno(usuario.getApellidoMaterno());
			empl.setApellidoPaterno(usuario.getApellidoPaterno());
			empl.setCorreoElectronico(usuario.getCorreoElectronico());
			empl.setNombre(usuario.getNombre());

			empl.setCreacion(new Date());
			empl.setFechaNacimiento(new Date());
			empl.setCurp("CURP");
			empl.setCveFiscal("RFC");
			empl.setDocIdentificacion("FUJASDSADA");
			Domicilio dom = new Domicilio("03020", "Rochester", "49", "piso 2", "Napoles", "BJ", "CDMX", "Mexico");
			domRepository.save(dom);
			empl.setDomicilioEmpleado(dom);
			empl.setTipoDocId(catalogoRepository.findByNombre("Licencia de manejar"));
			empl.setGenero(catalogoRepository.findByNombre("Masculino"));
			empl.setEstatusActual(getCatalogoCveInterna("ESTATUS_ACTIVO"));
			empl.setCargoEmpleado(catalogoRepository.findByNombre("Soporte de op"));
			empl.setSalarioAsimilado(0.0);
			empl.setSalarioBase(0.0);
			empl.setSemanasGracia(0);
			empl.setFechaIngreso(new Date());
			empl.setNombreUsuario(usuario.getNombreUsuario());
			emplRepository.save(empl);
		}
		CrearObjSecVO userRel = new CrearObjSecVO();
		userRel.setIdUsuario(usuario.getIdUsuario());
		ObjetoSeguridad administrador = encuentraObjeto("ROL_ADMIN");
		userRel.setIdObjetoSeguridadParent(administrador.getIdObjetoSeguridad());
		secServices.agregaUsuarioRol(userRel);

		userRel = new CrearObjSecVO();
		userRel.setIdUsuario(usuario.getIdUsuario());
		ObjetoSeguridad adminParam = encuentraObjeto("ROL_ADMIN_PARAM");
		userRel.setIdObjetoSeguridadParent(adminParam.getIdObjetoSeguridad());
		secServices.agregaUsuarioRol(userRel);

		userRel = new CrearObjSecVO();
		userRel.setIdUsuario(usuario.getIdUsuario());
		ObjetoSeguridad adminSeg = encuentraObjeto("ROL_ADMIN_SEG");
		userRel.setIdObjetoSeguridadParent(adminSeg.getIdObjetoSeguridad());
		secServices.agregaUsuarioRol(userRel);

		userRel = new CrearObjSecVO();
		userRel.setIdUsuario(usuario.getIdUsuario());
		ObjetoSeguridad corpUser = encuentraObjeto("ROL_CORPORATIVO");
		userRel.setIdObjetoSeguridadParent(corpUser.getIdObjetoSeguridad());
		secServices.agregaUsuarioRol(userRel);

	}

	public void validateOrg() throws Exception {
		boolean found = encuentraTree();
		Organizacion oneNode = null;
		OrgVO parent = null;
		if (!found) {
			ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new OrgToTreeModelMap());

			ModelMapper reversMapping = new ModelMapper();
			reversMapping.addMappings(new OrgVOToOrgModelMap());

			oneNode = new Organizacion();
			oneNode.setNombre("CRENX");
			oneNode.setTipoNodo("ROOT");
			oneNode.setClave("ROOT");
			parent = saveNodo(mapper.map(oneNode, OrgVO.class));
			oneNode = reversMapping.map(parent, Organizacion.class);

			Organizacion mex = new Organizacion();
			mex.setParent(oneNode);
			mex.setNombre("México");
			mex.setClave("MX");
			mex.setTipoNodo("P");
			parent = saveNodo(mapper.map(mex, OrgVO.class));
			mex = reversMapping.map(parent, Organizacion.class);

			Organizacion d1 = new Organizacion();
			d1.setParent(mex);
			d1.setNombre("División 1");
			d1.setClave("D1");
			d1.setTipoNodo("D");
			parent = saveNodo(mapper.map(d1, OrgVO.class));
			d1 = reversMapping.map(parent, Organizacion.class);

			Organizacion d2 = new Organizacion();
			d2.setParent(mex);
			d2.setNombre("División 2");
			d2.setTipoNodo("D");
			parent = saveNodo(mapper.map(d2, OrgVO.class));
			d2 = reversMapping.map(parent, Organizacion.class);

			Organizacion r1 = new Organizacion();
			r1.setParent(d1);
			r1.setNombre("Región Nicolás Romero");
			r1.setTipoNodo("R");
			r1.setClave("NR");
			parent = saveNodo(mapper.map(r1, OrgVO.class));
			r1 = reversMapping.map(parent, Organizacion.class);

			Organizacion r2 = new Organizacion();
			r2.setParent(d1);
			r2.setNombre("Región 2");
			r2.setTipoNodo("R");
			parent = saveNodo(mapper.map(r2, OrgVO.class));
			r2 = reversMapping.map(parent, Organizacion.class);

			Organizacion g1 = new Organizacion();
			g1.setParent(r1);
			g1.setNombre("Gerencia NR.VCR");
			g1.setTipoNodo("G");
			g1.setClave("NR.VCR");
			parent = saveNodo(mapper.map(g1, OrgVO.class));
			g1 = reversMapping.map(parent, Organizacion.class);

			Organizacion g2 = new Organizacion();
			g2.setParent(r1);
			g2.setNombre("Gerencia 2");
			g2.setTipoNodo("G");
			parent = saveNodo(mapper.map(g2, OrgVO.class));
			g2 = reversMapping.map(parent, Organizacion.class);

			Organizacion r11 = new Organizacion();
			r11.setParent(g1);
			r11.setNombre("Ruta NR-GUU");
			r11.setTipoNodo("RR");
			r11.setClave("NR-GUU");
			parent = saveNodo(mapper.map(r11, OrgVO.class));
			r11 = reversMapping.map(parent, Organizacion.class);

			Organizacion r12 = new Organizacion();
			r12.setParent(g1);
			r12.setNombre("Ruta NR-TAX");
			r12.setTipoNodo("RR");
			r12.setClave("NR-TAX");
			parent = saveNodo(mapper.map(r12, OrgVO.class));
			r12 = reversMapping.map(parent, Organizacion.class);

			Organizacion r13 = new Organizacion();
			r13.setParent(g1);
			r13.setNombre("Ruta NR-CAHU");
			r13.setTipoNodo("RR");
			r13.setClave("NR-CAHU");
			parent = saveNodo(mapper.map(r13, OrgVO.class));
			r13 = reversMapping.map(parent, Organizacion.class);

			Organizacion r14 = new Organizacion();
			r14.setParent(g2);
			r14.setNombre("Ruta Pruebas");
			r14.setTipoNodo("RR");
			r14.setClave("PRUEBAS");
			parent = saveNodo(mapper.map(r14, OrgVO.class));
			r14 = reversMapping.map(parent, Organizacion.class);

		}

	}

	private ArrayList<String> getChildren(String orgId) {
		ArrayList<String> retVal = new ArrayList<String>();
		List<Organizacion> children = Lists.newArrayList(orgRepository.findByParentIdOrg(orgId));
		for (Organizacion oneItem : children) {
			retVal.add(oneItem.getIdOrg());
		}
		return retVal;
	}

	public ArrayList<String> filtraRutas(FiltrosOrgVO filtros) {
		ArrayList<String> retVal = new ArrayList<String>();
		if (filtros.getRutas().size() > 0) {

			for (NameValueVO ruta : filtros.getRutas()) {
				retVal.add(ruta.getId());
			}
		}
		if (filtros.getGerencias().size() > 0) {
			for (NameValueVO ruta : filtros.getGerencias()) {
				retVal.addAll(getChildren(ruta.getId()));
			}
		}
		if (filtros.getRegiones().size() > 0) {
			ArrayList<String> gerencias = new ArrayList<String>();
			for (NameValueVO ruta : filtros.getRegiones()) {
				gerencias.addAll(getChildren(ruta.getId()));
			}
			for (String gerencia : gerencias) {
				retVal.addAll(getChildren(gerencia));
			}
		}
		return retVal;
	}

	public boolean encuentraTree() {
		List<Organizacion> prodItem = Lists.newArrayList(orgRepository.findAll());
		return prodItem.size() > 0;
	}

	public void validateProductCatalogs() {
		boolean found = encuentraProd();
		Producto oneProduct = null;
		if (!found) {
			Catalogo familia = catalogoRepository.findByNombre("Créditos Emprendedores");
			Catalogo categoria = catalogoRepository.findByNombre("Automotriz");
			Catalogo unidadPlazo = catalogoRepository.findByNombre("Días");
			Catalogo frecuenciaCobro = catalogoRepository.findByNombre("Diario");

			oneProduct = new Producto();
			oneProduct.setNombre("Clásico 27d");
			oneProduct.setFamiliaProducto(familia);
			oneProduct.setCategoriaProducto(categoria);
			oneProduct.setDescripcion("....");
			short plazo = 27;
			oneProduct.setFechaCreacion(new Date());
			byte estatus = 1;
			oneProduct.setEstatus(estatus);
			productoRepository.save(oneProduct);

			oneProduct = new Producto();
			oneProduct.setNombre("Oportuno 40d");
			oneProduct.setFamiliaProducto(familia);
			oneProduct.setCategoriaProducto(categoria);
			oneProduct.setDescripcion("....");
			plazo = 40;
			oneProduct.setFechaCreacion(new Date());
			estatus = 1;
			oneProduct.setEstatus(estatus);
			productoRepository.save(oneProduct);

			oneProduct = new Producto();
			oneProduct.setNombre("Comercial 50d");
			oneProduct.setFamiliaProducto(familia);
			oneProduct.setCategoriaProducto(categoria);
			oneProduct.setDescripcion("....");
			plazo = 50;
			oneProduct.setFechaCreacion(new Date());
			estatus = 1;
			oneProduct.setEstatus(estatus);
			productoRepository.save(oneProduct);

			oneProduct = new Producto();
			oneProduct.setNombre("Flexible 68d");
			oneProduct.setFamiliaProducto(familia);
			oneProduct.setCategoriaProducto(categoria);
			oneProduct.setDescripcion("....");
			plazo = 68;
			oneProduct.setFechaCreacion(new Date());
			estatus = 1;
			oneProduct.setEstatus(estatus);
			productoRepository.save(oneProduct);

			oneProduct = new Producto();
			oneProduct.setNombre("MycroPYME27");
			oneProduct.setFamiliaProducto(familia);
			oneProduct.setCategoriaProducto(categoria);
			oneProduct.setDescripcion("....");
			plazo = 27;
			oneProduct.setFechaCreacion(new Date());
			estatus = 1;
			oneProduct.setEstatus(estatus);
			productoRepository.save(oneProduct);

			oneProduct = new Producto();
			oneProduct.setNombre("Restructura");
			oneProduct.setFamiliaProducto(familia);
			oneProduct.setCategoriaProducto(categoria);
			oneProduct.setDescripcion("....");
			plazo = 27;
			oneProduct.setFechaCreacion(new Date());
			estatus = 1;
			oneProduct.setEstatus(estatus);
			productoRepository.save(oneProduct);

			oneProduct = new Producto();
			oneProduct.setNombre("Personal");
			oneProduct.setFamiliaProducto(familia);
			oneProduct.setCategoriaProducto(categoria);
			oneProduct.setDescripcion("....");
			plazo = 27;
			oneProduct.setFechaCreacion(new Date());
			estatus = 1;
			oneProduct.setEstatus(estatus);
			productoRepository.save(oneProduct);

		}
	}

	private boolean encuentraProd() {
		List<Producto> prodItem = Lists.newArrayList(productoRepository.findAll());
		return prodItem.size() > 0;
	}

	public List<String> getCatalogoIds(String claveCatalogo) {
		Catalogo catParent = catalogoRepository.findByClaveInterna(claveCatalogo);
		List<Catalogo> items = catalogoRepository.findByParentId(catParent.getIdCatalogo());
		ArrayList<String> reValue = new ArrayList<String>();
		for (Catalogo item : items) {
			reValue.add(item.getIdCatalogo());
		}
		return reValue;
	}

	public List<String> getCatalogoIdsByClavesInternas(List<String> clavesInternas) {
		List<Catalogo> items = catalogoRepository.findByClaveInternaIn(clavesInternas);
		ArrayList<String> reValue = new ArrayList<String>();
		for (Catalogo item : items) {
			reValue.add(item.getIdCatalogo());
		}
		return reValue;
	}

	public CatalogoVO catalogItems(UserAuthentication profile, String tipoCat) {
		CatalogoVO catVo = new CatalogoVO();
		List<String> roles = profile.getDetails().getPerfil().getRoles();
		List<String> items = catBR.filtraItems(roles, tipoCat);
		Catalogo parentCat = getCatalogoCveInterna(tipoCat);
		if (parentCat != null) {
			catVo.setParent(parentCat);
			Collection<Catalogo> catItems = null;
			// null significa que no tiene restricciones
			if (items == null)
				catItems = Lists.newArrayList(this.catalogoRepository.findByParentId(parentCat.getIdCatalogo()));
			else
				catItems = catalogoRepository.findByIdCatalogoIn(items);
			catVo.setItems(catItems);
		}
		return catVo;
	}

	public List<ReglaGastoVO> catalogReglasGastos(UserAuthentication profile) {
		if(profile == null){
			throw new SecurityException("Unknow Profile");
		}
		List<ReglaGasto> reglas = Lists
				.newArrayList(regGasRepository.findByRolLlaveNegocioIn(profile.getDetails().getPerfil().getRoles()));
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ReglaGastoToVOModelMapper());
		Type listType = new TypeToken<List<ReglaGastoVO>>() {
		}.getType();
		List<ReglaGastoVO> reglasVO = mapper.map(reglas, listType);
		return reglasVO;
	}

	public List<ReglaGastoVO> getReglaGasto(String idRegla, List<String> roles) {
		List<ReglaGasto> reglas = regGasRepository.findByRolLlaveNegocioInAndGastoIdCatalogo(roles, idRegla);
		System.out.println("Encontre: " + reglas.size());
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ReglaGastoToVOModelMapper());
		Type listType = new TypeToken<List<ReglaGastoVO>>() {
		}.getType();
		List<ReglaGastoVO> reglasVO = mapper.map(reglas, listType);
		return reglasVO;
	}

	public void saveReglaGasto(ReglaGastoVO reglaItem) {
		ReglaGasto regla = new ReglaGasto();
		Catalogo gasto = this.getCatalogoId(reglaItem.getIdGasto());
		ObjetoSeguridad rol = secServices.getById(reglaItem.getIdRol());
		Catalogo periodo = this.getCatalogoId(reglaItem.getIdPeriodo());
		regla.setIdRegla(reglaItem.getIdRegla());
		regla.setGasto(gasto);
		regla.setRol(rol);
		regla.setPeriodo(periodo);
		regla.setMaximo(reglaItem.getMaximo());
		regla.setRequiereCFDI(reglaItem.isRequiereCFDI());
		regGasRepository.save(regla);
	}

	public List<ReglaCreditoVO> catalogReglasCredito(UserAuthentication profile) {
		List<ReglaCredito> reglas = Lists.newArrayList(regCredRepository.findAll());
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ReglaCreditoToVOModelMapper());
		Type listType = new TypeToken<List<ReglaCreditoVO>>() {
		}.getType();
		List<ReglaCreditoVO> reglasVO = mapper.map(reglas, listType);
		return reglasVO;
	}

	public List<Catalogo> getTiposMovimiento() {
		return Lists.newArrayList(catalogoRepository.findByClaveInternaIsLike("MOV_CAJA%"));
	}

	public List<Catalogo> getTiposOperacion() {
		Catalogo cat = catalogoRepository.findByClaveInterna("CAT_TIPO_OPERACION");
		return Lists.newArrayList(catalogoRepository.findByParentId(cat.getIdCatalogo()));
	}

	public void saveReglaCredito(ReglaCreditoVO reglaItem) {
		ReglaCredito regla = new ReglaCredito();
		ObjetoSeguridad rol = secServices.getById(reglaItem.getIdRol());
		regla.setIdRegla(reglaItem.getIdRegla());
		regla.setRol(rol);
		regla.setMaximo(reglaItem.getMaximo());
		regCredRepository.save(regla);
	}

	public void deleteReglaCredito(String id) {
		ReglaCredito regla = new ReglaCredito();
		regla.setIdRegla(id);
		regCredRepository.delete(regla);
	}
	
	public void deleteReglaGasto(String id) {
		ReglaGasto regla = new ReglaGasto();
		regla.setIdRegla(id);
		regGasRepository.delete(regla);
		
	}
	/*
	 * public List<ReglaGastoRolVO> catalogReglasGastosRol(UserAuthentication
	 * profile) { List<ReglaGastoRol> reglas =
	 * Lists.newArrayList(regGasRolRepository.findAll()); ModelMapper mapper =
	 * new ModelMapper(); mapper.addMappings(new
	 * ReglaGastoRolToVOModelMapper()); Type listType = new
	 * TypeToken<List<ReglaGastoRolVO>>() {}.getType(); List<ReglaGastoRolVO>
	 * reglasVO = mapper.map(reglas, listType); return reglasVO; }
	 * 
	 * public void saveReglaGastoRol(ReglaGastoRolVO reglaItem) { ReglaGastoRol
	 * regla = new ReglaGastoRol(); Catalogo gasto =
	 * this.getCatalogoId(reglaItem.getIdGasto()); ObjetoSeguridad rol =
	 * secServices.getById(reglaItem.getIdRol()); regla.setGasto(gasto);
	 * regla.setRol(rol); regGasRolRepository.save(regla); }
	 */

	private void addFilter(FiltrosOrgVO filters, Organizacion theChild, OrgVO orgVo, ModelMapper mapper,
			OrgVO childOrgVo) {
		if (orgVo.getNodes() != null)
			orgVo.getNodes().add(childOrgVo);
		if (filters != null) {
			if (theChild.getTipoNodo().equals("D"))
				filters.getDivisiones().add(new NameValueVO(theChild.getIdOrg(), theChild.getNombre()));
			if (theChild.getTipoNodo().equals("R"))
				filters.getRegiones().add(new NameValueVO(theChild.getIdOrg(), theChild.getNombre()));
			if (theChild.getTipoNodo().equals("G"))
				filters.getGerencias().add(new NameValueVO(theChild.getIdOrg(), theChild.getNombre()));
			if (theChild.getTipoNodo().equals("RR"))
				filters.getRutas().add(new NameValueVO(theChild.getIdOrg(), theChild.getNombre()));

		}
	}

	public Organizacion getChildren(Organizacion parent, OrgVO orgVo, FiltrosOrgVO filters, ModelMapper mapper,
			boolean forFilters) {
		/*
		 * if (parent.getTipoNodo().equals("RR")) {
		 * addFilter(filters,parent,orgVo,mapper, orgVo); return parent; }
		 */
		// List<Organizacion> children = parent.getChildren();
		List<Organizacion> children = Lists.newArrayList(orgRepository.findByParentIdOrg(parent.getIdOrg()));
		parent.setChildren(children);
		if (children.size() == 0) {
			if (orgVo.getIdParent() == null)
				orgVo.setIdParent(parent.getParent().getIdOrg());
			// FIXME: Creo que aquí debe preguntar por el tipo de Nodo pero del
			// parent
			// if (forFilters && filters.getTipoNodo().equals("RR"))

			if (forFilters && parent.getTipoNodo().equals("RR")) {
				OrgVO childOrgVo = mapper.map(parent, OrgVO.class);
				childOrgVo.setIdParent(parent.getParent().getIdOrg());
				// addFilter(filters, parent, orgVo, mapper, childOrgVo);
			}
			return parent;
		} else {
			orgVo.setNodes(new ArrayList<OrgVO>());
			for (Organizacion theChild : children) {
				OrgVO childOrgVo = mapper.map(theChild, OrgVO.class);
				childOrgVo.setIdParent(parent.getIdOrg());
				addFilter(filters, theChild, orgVo, mapper, childOrgVo);
				theChild = getChildren(theChild, childOrgVo, filters, mapper, forFilters);
			}
		}
		return parent;
	}

	public Organizacion getDirectChildren(Organizacion parent, OrgVO orgVo, FiltrosOrgVO filters, ModelMapper mapper,
			boolean forFilters) {
		List<Organizacion> children = Lists.newArrayList(orgRepository.findByParentIdOrg(parent.getIdOrg()));
		parent.setChildren(children);
		orgVo.setNodes(new ArrayList<OrgVO>());
		for (Organizacion theChild : children) {
			OrgVO childOrgVo = mapper.map(theChild, OrgVO.class);
			childOrgVo.setIdParent(parent.getIdOrg());
			addFilter(filters, theChild, orgVo, mapper, childOrgVo);
		}
		return parent;
	}
}

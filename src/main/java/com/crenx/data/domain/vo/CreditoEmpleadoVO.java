package com.crenx.data.domain.vo;

import java.util.Date;

public class CreditoEmpleadoVO {

	private String idCreditoEmpl;
	private long id;
	private String idEmpleado;
	private String nombreEmpleado;
	private String idTipoCredito;
	private String nombreTipoCredito;
	private String fechaEmision;
	private String fechaUltimoPago;
	private double monto;
	private double comision;
	private short plazo=0;
	private double pago=0;
	private double  saldo=0;
	private String idEstatusCredito;
	private String nombreEstatusCredito;
	public String getIdCreditoEmpl() {
		return idCreditoEmpl;
	}
	public void setIdCreditoEmpl(String idCreditoEmpl) {
		this.idCreditoEmpl = idCreditoEmpl;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getNombreEmpleado() {
		return nombreEmpleado;
	}
	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}
	public String getIdTipoCredito() {
		return idTipoCredito;
	}
	public void setIdTipoCredito(String idTipoCreditoo) {
		this.idTipoCredito = idTipoCreditoo;
	}
	public String getNombreTipoCredito() {
		return nombreTipoCredito;
	}
	public void setNombreTipoCredito(String nombreTipoCredito) {
		this.nombreTipoCredito = nombreTipoCredito;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getFechaUltimoPago() {
		return fechaUltimoPago;
	}
	public void setFechaUltimoPago(String fechaUltimoPago) {
		this.fechaUltimoPago = fechaUltimoPago;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public short getPlazo() {
		return plazo;
	}
	public void setPlazo(short plazo) {
		this.plazo = plazo;
	}
	public double getPago() {
		return pago;
	}
	public void setPago(double pago) {
		this.pago = pago;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public String getIdEstatusCredito() {
		return idEstatusCredito;
	}
	public void setIdEstatusCredito(String idEstatusCreditoo) {
		this.idEstatusCredito = idEstatusCreditoo;
	}
	public String getNombreEstatusCredito() {
		return nombreEstatusCredito;
	}
	public void setNombreEstatusCredito(String nombreEstatusCredito) {
		this.nombreEstatusCredito = nombreEstatusCredito;
	}
	
	
}

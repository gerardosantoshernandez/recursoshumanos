package com.crenx.security.activity;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.persistence.entity.GroupEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import com.crenx.ApplicationContextHolder;
import com.crenx.data.domain.vo.GroupVO;
import com.crenx.services.SeguridadServices;

public class GlobalGroupManager extends GroupEntityManager{
	
	
	@Override
	public void insertGroup(Group group) {
        throw new UnsupportedOperationException();
    }

	@Override
	public void updateGroup(Group updatedGroup) {
        throw new UnsupportedOperationException();
	}

	@Override
	public void deleteGroup(String groupId) {
        throw new UnsupportedOperationException();
    }


	@Override
	public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
		// TODO Auto-generated method stub
		return super.findGroupByQueryCriteria(query, page);
	}

	@Override
	public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
		// TODO Auto-generated method stub
		return super.findGroupCountByQueryCriteria(query);
	}

	@Override
	public List<Group> findGroupsByUser(String userId) {	
		SeguridadServices secServices = ApplicationContextHolder.getContext().getBean(SeguridadServices.class);
		List<GroupVO> grupos = secServices.getUserGroups(userId);
		List<Group> groups = (List)grupos;
		return groups;
	}

	@Override
	public List<Group> findGroupsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return super.findGroupsByNativeQuery(parameterMap, firstResult, maxResults);
	}

	@Override
	public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
		// TODO Auto-generated method stub
		return super.findGroupCountByNativeQuery(parameterMap);
	}

	@Override
	public boolean isNewGroup(Group group) {
		// TODO Auto-generated method stub
		return super.isNewGroup(group);
	}

}

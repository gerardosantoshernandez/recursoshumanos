package com.crenx.data.domain.vo;

import java.util.Date;

public class AvisoVO {
	private String idAviso;
	private String asunto;
	private String descripcion;
	private String fechaAviso;
	private String lugar;
	private String idEmpleado;
	private String Empleado;
	private String idTipoAviso;
	private String tipoAviso;
	private String horaInicial;
	private String horaFinal;
	
	public String getHoraInicial() {
		return horaInicial;
	}
	public void setHoraInicial(String horaInicial) {
		this.horaInicial = horaInicial;
	}
	public String getHoraFinal() {
		return horaFinal;
	}
	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}
	public String getFechaAviso() {
		return fechaAviso;
	}
	public void setFechaAviso(String fechaAviso) {
		this.fechaAviso = fechaAviso;
	}
	public String getIdAviso() {
		return idAviso;
	}
	public void setIdAviso(String idAviso) {
		this.idAviso = idAviso;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getLugar() {
		return lugar;
	}
	public void setLugar(String lugar) {
		this.lugar = lugar;
	}
	public String getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getIdTipoAviso() {
		return idTipoAviso;
	}
	public void setIdTipoAviso(String idTipoAviso) {
		this.idTipoAviso = idTipoAviso;
	}
	public String getTipoAviso() {
		return tipoAviso;
	}
	public void setTipoAviso(String tipoAviso) {
		this.tipoAviso = tipoAviso;
	}
	public String getEmpleado() {
		return Empleado;
	}
	public void setEmpleado(String empleado) {
		Empleado = empleado;
	}
	
}

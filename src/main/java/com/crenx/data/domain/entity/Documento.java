package com.crenx.data.domain.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;



@Entity
public class Documento {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_documento", unique = true)
	private String idDocumento;
	
	@Column(name = "nombre",length = 100)
	private String nombre;
	
	@Column(name = "descripcion",length = 200)
	private String descripcion;
	
	@Column(name = "tipo",length = 50)
	private String tipo;
	
	@Column(name = "referencia",length = 200)
	private String referencia;
	
	@Column(name = "file_name",length = 250)
	private String fileName;
	
	@Column(name = "content_type",length = 100)
	private String contentType;
	
	@Column(name = "id_autor",length = 40)
	private String idAutor;
	
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;
	
	@Column(name = "version")
	private double version;
	
	@Column(name = "id_parent",length = 10)
	private String idParent;

	@OneToMany(mappedBy="documento",cascade=CascadeType.ALL)
	private List<PropiedadesDoc> propiedadesDocs;
	
	/**
	 * @return the propiedadesDocs
	 */
	public List<PropiedadesDoc> getPropiedadesDocs() {
		return propiedadesDocs;
	}

	/**
	 * @param propiedadesDocs the propiedadesDocs to set
	 */
	public void setPropiedadesDocs(List<PropiedadesDoc> propiedadesDocs) {
		this.propiedadesDocs = propiedadesDocs;
	}

	public String getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getIdAutor() {
		return idAutor;
	}

	public void setIdAutor(String idAutor) {
		this.idAutor = idAutor;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public double getVersion() {
		return version;
	}

	public void setVersion(double version) {
		this.version = version;
	}

	public String getIdParent() {
		return idParent;
	}

	public void setIdParent(String idParent) {
		this.idParent = idParent;
	}
	

}

package com.crenx.controller;
import java.security.Principal;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.vo.CatalogoVO;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.ReglaCreditoVO;
import com.crenx.data.domain.vo.ReglaGastoRolVO;
import com.crenx.data.domain.vo.ReglaGastoVO;
import com.crenx.services.CalendarioServices;
import com.crenx.services.CatalogoServices;
import com.crenx.security.UserAuthentication;
import com.google.common.collect.Lists;

@RestController
@RequestMapping("/api/catalogo")
public class CatalogoRestController {

	@Autowired
	private CatalogoRepository catalogoRepository;
	@Autowired
	private CalendarioServices calServices;
	@Autowired
	private CatalogoServices catServices;
	
	
	@RequestMapping(value="items", method = RequestMethod.POST)
	ResponseEntity<?> getItems(Principal principal, @RequestBody String tipoCat) {
		try
		{
			UserAuthentication profile = (UserAuthentication)principal;
			
			CatalogoVO catVo =  catServices.catalogItems(profile, tipoCat);
			return new ResponseEntity<>(catVo, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al consultar los catálogos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@RequestMapping(value="savecatalogo", method = RequestMethod.POST)
	Catalogo saveCatalogo(@RequestBody Catalogo catalogoItem) {
		catalogoRepository.save(catalogoItem);
		return catalogoItem;
	}

	@PostConstruct
	void validateCatalogs() throws Exception {
		catServices.addDefaultData();
		catServices.validateSecurityObjects();
		catServices.generateDefaultUser();
		catServices.validateOrg();
		catServices.validateProductCatalogs();
		catServices.agregaReglas();
	}

	
	
	@RequestMapping(value="reglasCreditos", method = RequestMethod.GET)
	ResponseEntity<?> getReglasGredito(Principal principal) {
		try
		{
			UserAuthentication profile = (UserAuthentication)principal;			
			List<ReglaCreditoVO> catVo =  catServices.catalogReglasCredito(profile);
			return new ResponseEntity<>(catVo, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("SucediÃ³ un error al consultar los catÃ¡logos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@RequestMapping(value="savereglaCredito", method = RequestMethod.POST)
	ResponseEntity<?> saveReglaCredito(@RequestBody ReglaCreditoVO reglaItem) {
		try
		{
			catServices.saveReglaCredito(reglaItem);
			return new ResponseEntity<>(reglaItem, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("SucediÃ³ un error al guardar los datos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}
	}
	

	@RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
	ResponseEntity<?>  deleteReglaCredito(@PathVariable String id, Principal principal){
		catServices.deleteReglaCredito(id);
		UserAuthentication profile = (UserAuthentication)principal;			
		List<ReglaCreditoVO> catVo =  catServices.catalogReglasCredito(profile);
		return new ResponseEntity<>(catVo, HttpStatus.CREATED);
		 
	}
	
	
	@RequestMapping(value="tiposMovimiento", method = RequestMethod.POST)
	ResponseEntity<?> getTiposMovimiento() {
		try
		{
			List<Catalogo> tiposMovimiento =  catServices.getTiposMovimiento();
			return new ResponseEntity<>(tiposMovimiento, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al consultar los catálogos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@RequestMapping(value="tiposOperacion", method = RequestMethod.POST)
	ResponseEntity<?> getTiposOperacion() {
		try
		{
			List<Catalogo> tiposOperacion =  catServices.getTiposOperacion();
			return new ResponseEntity<>(tiposOperacion, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al consultar los catálogos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	/*

	@RequestMapping(value="reglasGastosRol", method = RequestMethod.GET)
	ResponseEntity<?> getReglasGastosRol(Principal principal) {
		try
		{
			UserAuthentication profile = (UserAuthentication)principal;			
			List<ReglaGastoRolVO> catVo =  catServices.catalogReglasGastosRol(profile);
			return new ResponseEntity<>(catVo, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("SucediÃ³ un error al consultar los catÃ¡logos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@RequestMapping(value="savereglaGastoRol", method = RequestMethod.POST)
	ResponseEntity<?> saveReglaGastoRol(@RequestBody ReglaGastoRolVO reglaItem) {
		try
		{
			catServices.saveReglaGastoRol(reglaItem);
			return new ResponseEntity<>(reglaItem, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("SucediÃ³ un error al guardar los datos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}
	}
	*/
}

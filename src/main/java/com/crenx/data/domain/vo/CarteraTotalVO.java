package com.crenx.data.domain.vo;

import java.util.List;

public class CarteraTotalVO {

	private List<IndicadorCarteraDiarioVO> carteraDia;
	private IndicadorCreditoVO cartera;
	private IndicadorCreditoVO carteraVencida;
	private IndicadorCreditoVO creditosNuevos;
	private IndicadorCreditoVO clientesNuevos;	
	private IndicadorCreditoVO clientesTotales;	
	private IndicadorCreditoVO clientesActivos;	
	private List<InidicadorRutaVO> indicadoresCobranza;
	
	public IndicadorCreditoVO getCartera() {
		return cartera;
	}
	public void setCartera(IndicadorCreditoVO cartera) {
		this.cartera = cartera;
	}
	public IndicadorCreditoVO getCarteraVencida() {
		return carteraVencida;
	}
	public void setCarteraVencida(IndicadorCreditoVO carteraVencida) {
		this.carteraVencida = carteraVencida;
	}
	public IndicadorCreditoVO getCreditosNuevos() {
		return creditosNuevos;
	}
	public void setCreditosNuevos(IndicadorCreditoVO creditosNuevos) {
		this.creditosNuevos = creditosNuevos;
	}
	public IndicadorCreditoVO getClientesNuevos() {
		return clientesNuevos;
	}
	public void setClientesNuevos(IndicadorCreditoVO clientesNuevos) {
		this.clientesNuevos = clientesNuevos;
	}
	public List<InidicadorRutaVO> getIndicadoresCobranza() {
		return indicadoresCobranza;
	}
	public void setIndicadoresCobranza(List<InidicadorRutaVO> indicadoresCobranza) {
		this.indicadoresCobranza = indicadoresCobranza;
	}
	public IndicadorCreditoVO getClientesTotales() {
		return clientesTotales;
	}
	public void setClientesTotales(IndicadorCreditoVO clientesTotales) {
		this.clientesTotales = clientesTotales;
	}
	public IndicadorCreditoVO getClientesActivos() {
		return clientesActivos;
	}
	public void setClientesActivos(IndicadorCreditoVO clientesActivos) {
		this.clientesActivos = clientesActivos;
	}
	public List<IndicadorCarteraDiarioVO> getCarteraDia() {
		return carteraDia;
	}
	public void setCarteraDia(List<IndicadorCarteraDiarioVO> carteraDia) {
		this.carteraDia = carteraDia;
	}

	
	
}

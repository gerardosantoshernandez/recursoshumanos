package com.crenx.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Anuncio;
import com.crenx.data.domain.entity.Aviso;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Empleado;
import com.crenx.data.domain.repository.AvisoRepository;
import com.crenx.data.domain.vo.AnuncioVO;
import com.crenx.data.domain.vo.AvisoVO;
import com.crenx.data.domain.vo.AvisoVOToAvisoModelMap;
import com.crenx.data.domain.vo.FiltrosAvisosVO;
import com.crenx.data.domain.vo.FiltrosProductoVO;
import com.crenx.services.AvisoServices;
import com.crenx.services.CatalogoServices;

@RestController
@RequestMapping("/api/aviso")
public class AvisoRestController {
	@Autowired
	private AvisoRepository avisoRepository;
	@Autowired
	private AvisoServices avisoServices;
	
	@Autowired
	CatalogoServices catServices;
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> getAvisos(@RequestBody FiltrosAvisosVO filtros) {
		return new ResponseEntity<>(avisoServices.filtrarAvisos(filtros),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="saveaviso",method = RequestMethod.POST)
	Aviso createAnuncio(@RequestBody AvisoVO avisoVO){
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new AvisoVOToAvisoModelMap());	
		Catalogo tipoAviso = catServices.getCatalogoId(avisoVO.getIdTipoAviso());
		Aviso aviso = mapper.map(avisoVO, Aviso.class);
		aviso.setIdTipoAviso(tipoAviso);
		avisoRepository.save(aviso);
		return aviso;
	}
}

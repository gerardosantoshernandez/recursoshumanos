package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Aviso;

public interface AvisoRepository extends CrudRepository<Aviso, String>{
	List<Aviso> findByIdTipoAviso_IdCatalogoInAndAsunto(List<String> categorias, String asunto);
	List<Aviso> findByAsuntoIsLike(String asunto);
	List<Aviso> findByIdAviso(String IdAviso);
}

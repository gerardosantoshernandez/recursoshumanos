package com.crenx.data.domain.vo;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Caja;

public class CajaToVOModelMapper extends PropertyMap<Caja, CajaVO>{
	
	   @Override
	    protected void configure() {
			map().setIdRuta(source.getUnidad().getIdOrg());
			map().setNombre(source.getUnidad().getNombre());
			using(dateToString).map(source.getFechaCorte()).setFechaC(null);
	    }
	   
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				return df.format(source);
		   }
	};
}


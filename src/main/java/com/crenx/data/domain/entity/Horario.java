package com.crenx.data.domain.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Horario {
	
	

	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_horario", unique = true)
	private String idHorario;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_calendario", nullable=false, referencedColumnName="id_calendario")
	private Calendario calendario;
	
	@Column(name = "nombre",length = 50)
	private String nombre;
	
	@Column(name = "hora_inicial",length = 20)
	private String horaInicial;

	@Column(name = "hora_final",length = 20)
	private String horaFinal;
	
	@Column(name = "dias_efectivos")
	private int diasEfectivos;
		

	public String getIdHorario() {
		return idHorario;
	}

	public void setIdHorario(String idHorario) {
		this.idHorario = idHorario;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(String horaInicial) {
		this.horaInicial = horaInicial;
	}

	public String getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}

	public int getDiasEfectivos() {
		return diasEfectivos;
	}

	public void setDiasEfectivos(int diasEfectivos) {
		this.diasEfectivos = diasEfectivos;
	}


	public Calendario getCalendario() {
		return calendario;
	}

	public void setCalendario(Calendario calendario) {
		this.calendario = calendario;
	}
	
	
	
}

package com.crenx.data.domain.vo;

public class EstadoCarteraVO {

	String ruta;
	String gerencia;
	String region;
	String division;
	long contador=0;
	long creditosNuevos;
	double creditosNuevosImp;
	long creditosRecompra;
	double creditosNuevosRecompra;
	double monto=0;
	double carteraSana=0;
	long numCarteraSana=0;
	double carteraPreventiva=0;
	long numCarteraPreventiva=0;
	double carteraRiesgo=0;
	long numCarteraRiesgo=0;
	double carteraVencida=0;
	long numCarteraVencida=0;
	double carteraIncobrable=0;
	long numCarteraIncobrable=0;
	

	public EstadoCarteraVO(){}
	public EstadoCarteraVO(
			String gerencia,
			String ruta,
			long contador,
			double monto
			)
	{
		this.gerencia = gerencia;
		this.ruta = ruta;
		this.contador = contador;
		this.monto = monto;
	}
	
	public EstadoCarteraVO(
			String division,
			String region,
			String gerencia,
			String ruta,
			long contador,
			double monto
			)
	{
		this.gerencia = gerencia;
		this.ruta = ruta;
		this.region = region;
		this.division = division;
		this.contador = contador;
		this.monto = monto;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public long getContador() {
		return contador;
	}
	public void setContador(long contador) {
		this.contador = contador;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public double getCarteraSana() {
		return carteraSana;
	}
	public void setCarteraSana(double carteraSana) {
		this.carteraSana = carteraSana;
	}
	public long getNumCarteraSana() {
		return numCarteraSana;
	}
	public void setNumCarteraSana(long numCarteraSana) {
		this.numCarteraSana = numCarteraSana;
	}
	public double getCarteraPreventiva() {
		return carteraPreventiva;
	}
	public void setCarteraPreventiva(double carteraPreventiva) {
		this.carteraPreventiva = carteraPreventiva;
	}
	public long getNumCarteraPreventiva() {
		return numCarteraPreventiva;
	}
	public void setNumCarteraPreventiva(long numCarteraPreventiva) {
		this.numCarteraPreventiva = numCarteraPreventiva;
	}
	public double getCarteraRiesgo() {
		return carteraRiesgo;
	}
	public void setCarteraRiesgo(double carteraRiesgo) {
		this.carteraRiesgo = carteraRiesgo;
	}
	public long getNumCarteraRiesgo() {
		return numCarteraRiesgo;
	}
	public void setNumCarteraRiesgo(long numCarteraRiesgo) {
		this.numCarteraRiesgo = numCarteraRiesgo;
	}
	public double getCarteraVencida() {
		return carteraVencida;
	}
	public void setCarteraVencida(double carteraVencida) {
		this.carteraVencida = carteraVencida;
	}
	public long getNumCarteraVencida() {
		return numCarteraVencida;
	}
	public void setNumCarteraVencida(long numCarteraVencida) {
		this.numCarteraVencida = numCarteraVencida;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public double getCarteraIncobrable() {
		return carteraIncobrable;
	}
	public void setCarteraIncobrable(double carteraIncobrable) {
		this.carteraIncobrable = carteraIncobrable;
	}
	public long getNumCarteraIncobrable() {
		return numCarteraIncobrable;
	}
	public void setNumCarteraIncobrable(long numCarteraIncobrable) {
		this.numCarteraIncobrable = numCarteraIncobrable;
	}
	public long getCreditosNuevos() {
		return creditosNuevos;
	}
	public void setCreditosNuevos(long creditosNuevos) {
		this.creditosNuevos = creditosNuevos;
	}
	public double getCreditosNuevosImp() {
		return creditosNuevosImp;
	}
	public void setCreditosNuevosImp(double creditosNuevosImp) {
		this.creditosNuevosImp = creditosNuevosImp;
	}
	public long getCreditosRecompra() {
		return creditosRecompra;
	}
	public void setCreditosRecompra(long creditosRecompra) {
		this.creditosRecompra = creditosRecompra;
	}
	public double getCreditosNuevosRecompra() {
		return creditosNuevosRecompra;
	}
	public void setCreditosNuevosRecompra(double creditosNuevosRecompra) {
		this.creditosNuevosRecompra = creditosNuevosRecompra;
	}

	
	
}

package com.crenx.data.domain.vo;

import java.util.List;

public class CrearObjSecVO {

	private String idObjSegRel;
	private String idObjetoSeguridad;
	private String nombre;
	private String descripcion;
	private String tipoObje;
	private String idObjetoSeguridadParent;
	private String idUsuario;
	private List<CrearObjSecVO> permissions; 
	
	public String getIdObjetoSeguridad() {
		return idObjetoSeguridad;
	}
	public void setIdObjetoSeguridad(String idObjetoSeguridad) {
		this.idObjetoSeguridad = idObjetoSeguridad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTipoObje() {
		return tipoObje;
	}
	public void setTipoObje(String tipoObje) {
		this.tipoObje = tipoObje;
	}
	public String getIdObjetoSeguridadParent() {
		return idObjetoSeguridadParent;
	}
	public void setIdObjetoSeguridadParent(String idObjetoSeguridadParent) {
		this.idObjetoSeguridadParent = idObjetoSeguridadParent;
	}
	public List<CrearObjSecVO> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<CrearObjSecVO> permissions) {
		this.permissions = permissions;
	}
	public String getIdObjSegRel() {
		return idObjSegRel;
	}
	public void setIdObjSegRel(String idObjSegRel) {
		this.idObjSegRel = idObjSegRel;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
}

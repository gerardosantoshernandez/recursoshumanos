package com.crenx.data.domain.vo;

public class ReglaGastoVO {

	private String idRegla;
	private String idGasto;
	private String nombreGasto;
	private String idRol;
	private String nombreRol;
	private String idPeriodo;
	private String nombrePeriodo;
	private double maximo;
	private boolean requiereCFDI=false;
	public String getIdRegla() {
		return idRegla;
	}
	public void setIdRegla(String idRegla) {
		this.idRegla = idRegla;
	}

	public String getIdRol() {
		return idRol;
	}
	public void setIdRol(String idRol) {
		this.idRol = idRol;
	}
	public String getNombreRol() {
		return nombreRol;
	}
	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}
	public String getIdPeriodo() {
		return idPeriodo;
	}
	public void setIdPeriodo(String idPeriodo) {
		this.idPeriodo = idPeriodo;
	}
	public String getNombrePeriodo() {
		return nombrePeriodo;
	}
	public void setNombrePeriodo(String nombrePeriodo) {
		this.nombrePeriodo = nombrePeriodo;
	}
	public double getMaximo() {
		return maximo;
	}
	public void setMaximo(double maximo) {
		this.maximo = maximo;
	}
	public boolean isRequiereCFDI() {
		return requiereCFDI;
	}
	public void setRequiereCFDI(boolean requiereCFDI) {
		this.requiereCFDI = requiereCFDI;
	}
	public String getIdGasto() {
		return idGasto;
	}
	public void setIdGasto(String idGasto) {
		this.idGasto = idGasto;
	}
	public String getNombreGasto() {
		return nombreGasto;
	}
	public void setNombreGasto(String nombreGasto) {
		this.nombreGasto = nombreGasto;
	}

	
}

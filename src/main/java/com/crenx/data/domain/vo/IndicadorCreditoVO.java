package com.crenx.data.domain.vo;

public class IndicadorCreditoVO {

	public IndicadorCreditoVO(){}
	public IndicadorCreditoVO(long countCreditos,double totalCartera, double carteraCapital)
	{
		this.countCreditos =countCreditos;
		this.totalCartera = totalCartera;
		this.carteraCapital = carteraCapital;
	}
	
	/**
	 * Constructor que agrega el dato de Id Credito
	 * @param idCredito
	 * @param countCreditos
	 * @param totalCartera
	 * @param carteraCapital
	 */
	public IndicadorCreditoVO(final String idCredito, final long countCreditos,final double totalCartera, final double carteraCapital)
	{
		this.countCreditos =countCreditos;
		this.totalCartera = totalCartera;
		this.carteraCapital = carteraCapital;
		this.idCredito = idCredito;
	}
	
	private long countCreditos;
	private double totalCartera;
	private double carteraCapital;
	private String idCredito;
	
	public long getCountCreditos() {
		return countCreditos;
	}
	public void setCountCreditos(long countCreditos) {
		this.countCreditos = countCreditos;
	}
	public double getTotalCartera() {
		return totalCartera;
	}
	public void setTotalCartera(double totalCartera) {
		this.totalCartera = totalCartera;
	}
	public double getCarteraCapital() {
		return carteraCapital;
	}
	public void setCarteraCapital(double carteraCapital) {
		this.carteraCapital = carteraCapital;
	}
	/**
	 * @return the idCredito
	 */
	public String getIdCredito() {
		return idCredito;
	}
	/**
	 * @param idCredito the idCredito to set
	 */
	public void setIdCredito(String idCredito) {
		this.idCredito = idCredito;
	}
		
}

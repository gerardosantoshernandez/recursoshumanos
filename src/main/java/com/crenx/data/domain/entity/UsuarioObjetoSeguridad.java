package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="usuario_objetoseguridad")
public class UsuarioObjetoSeguridad   {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_usuarioobjetoseguridad", unique = true)
	private String idUsuarioObjetoSeguridad;
	
	@Column(name = "creacion")
	private Date creacion;

	@OneToOne
	@JoinColumn(name="id_usuario", nullable=false, referencedColumnName="id_usuario")
	private Usuario usuario;
	
	@OneToOne
	@JoinColumn(name="id_objetoSeguridad", nullable=false, referencedColumnName="id_objetoSeguridad")
	private ObjetoSeguridad role;
	

	public String getIdUsuarioObjetoSeguridad() {
		return idUsuarioObjetoSeguridad;
	}

	public void setIdUsuarioObjetoSeguridad(String idUsuarioObjetoSeguridad) {
		this.idUsuarioObjetoSeguridad = idUsuarioObjetoSeguridad;
	}

	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public ObjetoSeguridad getRole() {
		return role;
	}

	public void setRole(ObjetoSeguridad role) {
		this.role = role;
	}
		
}

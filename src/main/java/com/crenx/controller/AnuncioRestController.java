package com.crenx.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Anuncio;
import com.crenx.data.domain.entity.Aviso;
import com.crenx.data.domain.repository.AnuncioRepository;
import com.crenx.data.domain.vo.AnuncioVO;
import com.crenx.data.domain.vo.AvisoVO;

@RestController
@RequestMapping("/api/anuncio")
public class AnuncioRestController {
	
	@Autowired
	private AnuncioRepository anuncioRepository;

	@RequestMapping(value="saveanuncio",method = RequestMethod.POST)
	Anuncio createAnuncio(@RequestBody AnuncioVO anuncioVO){
		ModelMapper mapper = new ModelMapper();
		Anuncio aviso = mapper.map(anuncioVO, Anuncio.class);
		anuncioRepository.save(aviso);
		return aviso;
	}
}

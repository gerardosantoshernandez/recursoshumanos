package com.crenx.data.domain.vo;

public class VisitaVO {
	private String idVisita;
	private String fechaVisita;
	private String geoPosicion;
	private String idCliente;
	private String idCredito;
	private String idDispositivo;
	private String idUsuario;
	private String nombreCliente;
	private String idMotivo;
	private String idRuta;
	private String idOrg;
	private String credito;
	private String rutaCliente;
	
	
	
	
	
	public String getRutaCliente() {
		return rutaCliente;
	}
	public void setRutaCliente(String rutaCliente) {
		this.rutaCliente = rutaCliente;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public String getIdCredito() {
		return idCredito;
	}
	public void setIdCredito(String idCredito) {
		this.idCredito = idCredito;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getIdMotivo() {
		return idMotivo;
	}
	public void setIdMotivo(String idMotivo) {
		this.idMotivo = idMotivo;
	}
	
	public String getCredito() {
		return credito;
	}
	public void setCredito(String credito) {
		this.credito = credito;
	}
	
	
	public String getIdOrg() {
		return idOrg;
	}
	public void setIdOrg(String idOrg) {
		this.idOrg = idOrg;
	}
	public String getIdVisita() {
		return idVisita;
	}
	public void setIdVisita(String idVisita) {
		this.idVisita = idVisita;
	}
	public String getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(String idRuta) {
		this.idRuta = idRuta;
	}
	public String getFechaVisita() {
		return fechaVisita;
	}
	public void setFechaVisita(String fechaVisita) {
		this.fechaVisita = fechaVisita;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getGeoPosicion() {
		return geoPosicion;
	}
	public void setGeoPosicion(String geoPosicion) {
		this.geoPosicion = geoPosicion;
	}
	public String getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	@Override
	public String toString() {
		return "VisitaVO [idVisita=" + idVisita + ", fechaVisita=" + fechaVisita + ", geoPosicion=" + geoPosicion
				+ ", idCliente=" + idCliente + ", idCredito=" + idCredito + ", idDispositivo=" + idDispositivo
				+ ", idUsuario=" + idUsuario + ", nombreCliente=" + nombreCliente + ", idMotivo=" + idMotivo
				+ ", idRuta=" + idRuta + ", idOrg=" + idOrg + ", credito=" + credito + "]";
	}

	
}

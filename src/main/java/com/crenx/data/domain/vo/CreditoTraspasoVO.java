package com.crenx.data.domain.vo;

public class CreditoTraspasoVO {
	private long id;	
	private String idCredito;
	private double monto;
	//private short plazo;
	//private double pago;
	private String fechaEmision;
	//private String fechaVencimiento;
	//private String fechaUltimoPago;	
	//private String fechaCancelacion;	
	//private String producto;
	//private String idCliente;
	//private String nombreCliente;
	//private String idEstatusCredito;
	//private String idRuta;
	private String ruta;
	private String estatusCredito;
	private double saldo;
	//private double saldoInsoluto;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIdCredito() {
		return idCredito;
	}
	public void setIdCredito(String idCredito) {
		this.idCredito = idCredito;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getEstatusCredito() {
		return estatusCredito;
	}
	public void setEstatusCredito(String estatusCredito) {
		this.estatusCredito = estatusCredito;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
}

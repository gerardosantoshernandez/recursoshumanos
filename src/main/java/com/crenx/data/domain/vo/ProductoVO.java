package com.crenx.data.domain.vo;

import java.util.Date;


public class ProductoVO {
	private String idProducto;
	private String idFamiliaProducto;
	private String familiaProducto;
	private String nombre;
	private String descripcion;
	private byte estatus;
	private String categoriaProducto;
	private String idCateProducto;
	private String fechaCreacion;
	private double precio;
	
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getIdCateProducto() {
		return idCateProducto;
	}
	public void setIdCateProducto(String idCateProducto) {
		this.idCateProducto = idCateProducto;
	}
	public String getCategoriaProducto() {
		return categoriaProducto;
	}
	public void setCategoriaProducto(String categoriaProducto) {
		this.categoriaProducto = categoriaProducto;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public String getIdFamiliaProducto() {
		return idFamiliaProducto;
	}
	public void setIdFamiliaProducto(String idFamiliaProducto) {
		this.idFamiliaProducto = idFamiliaProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFamiliaProducto() {
		return familiaProducto;
	}
	public void setFamiliaProducto(String familiaProducto) {
		this.familiaProducto = familiaProducto;
	}
	public byte getEstatus() {
		return estatus;
	}
	public void setEstatus(byte estatus) {
		this.estatus = estatus;
	}
}

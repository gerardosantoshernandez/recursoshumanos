package com.crenx.data.domain.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Ruta {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	
	@Column(name = "id_ruta", unique = true)
	private String idRuta;
	
	@Column(name = "facturar")
	private byte facturar;
	
	@Column(name = "mensaje_factura",length = 1000)
	private String mensajeFactura;
	
	@Column(name = "cve_fiscal",length = 40)
	private String cveFiscal;
	
	@Column(name = "tel_fijo",length = 20)
	private String telFijo;
	
	@Column(name = "tel_cel",length = 20)
	private String telCel;
	
	@Column(name = "permite_abonos")
	private byte permiteAbonos;
	
	@Column(name = "crea_clientes")
	private byte creaClientes;
	
	@Column(name = "id_domicilio", length = 10)
	private String idDomicilio;
	
	@Column(name = "id_tipo_cuadre_cel", length = 40)
	private String idTipoCuadreCel;

	public String getIdRuta() {
		return idRuta;
	}

	public void setIdRuta(String idRuta) {
		this.idRuta = idRuta;
	}

	public byte getFacturar() {
		return facturar;
	}

	public void setFacturar(byte facturar) {
		this.facturar = facturar;
	}

	public String getMensajeFactura() {
		return mensajeFactura;
	}

	public void setMensajeFactura(String mensajeFactura) {
		this.mensajeFactura = mensajeFactura;
	}

	public String getCveFiscal() {
		return cveFiscal;
	}

	public void setCveFiscal(String cveFiscal) {
		this.cveFiscal = cveFiscal;
	}

	public String getTelFijo() {
		return telFijo;
	}

	public void setTelFijo(String telFijo) {
		this.telFijo = telFijo;
	}

	public String getTelCel() {
		return telCel;
	}

	public void setTelCel(String telCel) {
		this.telCel = telCel;
	}

	public byte getPermiteAbonos() {
		return permiteAbonos;
	}

	public void setPermiteAbonos(byte permiteAbonos) {
		this.permiteAbonos = permiteAbonos;
	}

	public byte getCreaClientes() {
		return creaClientes;
	}

	public void setCreaClientes(byte creaClientes) {
		this.creaClientes = creaClientes;
	}

	public String getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(String idDomicilio) {
		this.idDomicilio = idDomicilio;
	}

	public String getIdTipoCuadreCel() {
		return idTipoCuadreCel;
	}

	public void setIdTipoCuadreCel(String idTipoCuadreCel) {
		this.idTipoCuadreCel = idTipoCuadreCel;
	}

	
}

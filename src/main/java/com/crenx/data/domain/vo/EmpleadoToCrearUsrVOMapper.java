package com.crenx.data.domain.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Empleado;
public class EmpleadoToCrearUsrVOMapper	 extends PropertyMap<Empleado,CrearUserVO>{
	   @Override
	    protected void configure() {
		   map().setIdCargo(source.getIdCargoEmpleado());
		   map().setIdGenero(source.getIdGenero());
		   map().setTipoDocId(source.getIdTipoDocId());
		   using(dateToString).map(source.getFechaNacimiento()).setFechaNacimiento(null);
		   using(dateToString).map(source.getFechaIngreso()).setFechaIngreso(null);
		   using(dateToString).map(source.getFechaBaja()).setFechaBaja(null);

	    }
	   Converter<String, Date> stringToDate = new AbstractConverter<String, Date>() {
		   protected Date convert(String source) {
			   if (source==null)
				   return null;
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				try {
					return df.parse(source);
				} catch (ParseException e) {
					System.out.println(e.getMessage());
					return null;
				}
		   }
		 };

	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };
		 
}

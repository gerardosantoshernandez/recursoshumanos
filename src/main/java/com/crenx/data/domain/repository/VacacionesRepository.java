package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Vacaciones;

public interface VacacionesRepository extends CrudRepository<Vacaciones, String>{

	List<Vacaciones> findByAsunto_IdCatalogoInAndDescripcion(List<String> asunto, String nombre);
	List<Vacaciones> findByDescripcionIsLike(String nombre);
	List<Vacaciones> findByIdVacaciones(String IdVacaciones);
}

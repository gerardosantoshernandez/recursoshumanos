'use strict';
angular.module('crenx.productocontrollers', [])
.controller(
		'ProductoController',
function($scope, $rootScope, apiService, $stateParams, $state,
		$timeout) {
	$scope.filters = {
		familias : [],
		nombre : null
	};
	$scope.listaFamilias = {
		parent : null,
		items : null
	};
	$scope.productos = [];

	var filterTextTimeout;
	$scope.$watch('filters.familias', function(val) {
		if (filterTextTimeout)
			$timeout.cancel(filterTextTimeout);
		filterTextTimeout = $timeout(function() {
			loadData();
		}, 250); // delay 250 ms
	})

	$scope.$watch('filters.nombre', function(val) {
		if (filterTextTimeout)
			$timeout.cancel(filterTextTimeout);
		filterTextTimeout = $timeout(function() {
			loadData();
		}, 250); // delay 250 ms
	})

	function loadFamilias() {
		apiService.post('/api/catalogo/items', 'CAT_PRO',
				function loadCompleted(result) {
					$scope.listaFamilias = result.data;
				}, serviceError);

	}

	function prodLoadCompleted(result) {
		$scope.productos = result.data;
	}

	function loadData() {
		apiService.post('/api/producto', $scope.filters,
				prodLoadCompleted, serviceError);

	}

	function serviceError(response) {
		alert(response);
	}

	loadFamilias();
}).controller(
'FormProductoController',
function($scope, $rootScope, apiService, $location, $http,
		$cookieStore, $stateParams, $modal) {
	$scope.listaFamilias = {
		parent : null,
		items : null
	};
	$scope.listaUnidadPlazo = {
		parent : null,
		items : null
	};
	$scope.listaFrecuenciaCobro = {
		parent : null,
		items : null
	};

	function loadItemsCatFamilia() {
		apiService.post('/api/catalogo/items', 'CAT_PRO',
				function loadCompleted(result) {
					$scope.listaFamilias = result.data;
				}, ServiceError);
	}

	function loadItemsCatPlazo() {
		apiService.post('/api/catalogo/items', 'CAT_UNI_PLAZO',
				function loadCompleted(result) {
					$scope.listaUnidadPlazo = result.data;
				}, ServiceError);
	}

	function loadItemsCatFrecCobro() {
		apiService.post('/api/catalogo/items',
				'CAT_FREC_COBRO',
				function loadCompleted(result) {
					$scope.listaFrecuenciaCobro = result.data;
				}, ServiceError);
	}

	$scope.submitTheForm = function() {
		apiService.post('/api/producto/saveproducto',
				$scope.productoItem, ServiceSuccessComplete,
				ServiceError);
	}
	function ServiceSuccessComplete(result) {

		var opts = {
			"closeButton" : true,
			"debug" : false,
			"positionClass" : "toast-top-right",
			"onclick" : null,
			"showDuration" : "300",
			"hideDuration" : "1000",
			"timeOut" : "5000",
			"extendedTimeOut" : "1000",
			"showEasing" : "swing",
			"hideEasing" : "linear",
			"showMethod" : "fadeIn",
			"hideMethod" : "fadeOut"
		};

		toastr.success(
				"Se guardó satisfactoriamente el producto.",
				"Guardar producto", opts);

		$scope.isUpdate = true;
		$scope.user = result.data;
		$scope.errorsubmit = '';

	}

	function ServiceError(response) {
		$scope.errorsubmit = response.data;
	}
	if ($stateParams.productoItem) {
		$scope.productoItem = $stateParams.productoItem;
		$scope.isUpdate = true;
	} else {

		$scope.productoItem = {};
	}
	if (!$stateParams.listaFamilias) {
		loadItemsCatFamilia();
	} else {
		$scope.listaFamilias = $stateParams.listaFamilias;

	}

	loadItemsCatPlazo();
	loadItemsCatFrecCobro();

	if ($stateParams.productoItem) {
		$scope.productoItem = $stateParams.productoItem;
	} else {
		$scope.productoItem = {};
	}

});

package com.crenx.data.domain.vo;

import java.util.ArrayList;
import java.util.Collection;

public class FiltrosCredEmplVO {
	Collection<String> tiposCredito;
	Collection<String> estatusCredito;
	
	
	String nombre;
	String apellidoPaterno;
	String idEmpleado;
	
	public Collection<String> getTiposCredito() {
		return tiposCredito;
	}
	public void setTiposCredito(Collection<String> tiposCredito) {
		this.tiposCredito = tiposCredito;
	}
	public Collection<String> getEstatusCredito() {
		return estatusCredito;
	}
	public void setEstatusCredito(Collection<String> estatusCredito) {
		if (estatusCredito==null)
			estatusCredito =  new ArrayList<String>();
		this.estatusCredito = estatusCredito;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}	
	
	
}

package com.crenx.data.domain.vo;

public class CreditosNominaVO {

	private double descuentoMoto;
	private double descuentoOtros;
	public double getDescuentoMoto() {
		return descuentoMoto;
	}
	public void setDescuentoMoto(double descuentoMoto) {
		this.descuentoMoto = descuentoMoto;
	}
	public double getDescuentoOtros() {
		return descuentoOtros;
	}
	public void setDescuentoOtros(double descuentoOtros) {
		this.descuentoOtros = descuentoOtros;
	}

	
}

package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.Organizacion;

public interface OrgRepository extends CrudRepository<Organizacion, String>{

	List<Organizacion> findByTipoNodo(String tipoNodo);

	Organizacion findByClave(String clave);

	List<Organizacion> findByParentIdOrg(String idParent);

	List<Organizacion> findByNombre(String nombre);

	List<Organizacion> findByTitularIdUsuario(String idUsuario);

	List<Organizacion> findBySuplenteIdUsuario(String idUsuario);

	List<Organizacion> findByTitularIsNullOrSuplenteIsNull();

	List<Organizacion> findByIdOrgIn(List<String> ids);
	Organizacion findByIdOrg(String ids);

	/**
	 * Regresa un con junto de rutas a un nivel de organización superior
	 * (gerencias, divisones o regiones).
	 * 
	 * Los parámetros de all<X> son auxiliares que permiten simular una búsqueda
	 * con la combinación de cualquier organización superior.
	 * 
	 * Para que este método funcione adecuadamente:
	 * 
	 * 1) Si la búsqueda no incluye una determinada organización superior
	 * entonces la lista de dicha organización debe ser NULL y el valor auxiliar
	 * correspondiente debe ser el Wildcard % (@HelperRepository.LIKE_WILDCARD_ZERO_MORE)
	 * 
	 * 2) Si se desea buscar por organizaciones superiores específicas, entonces
	 * la lista de la organización debe contener al menos un valor y el valor
	 * auxiliar respectivo debe ser NULL
	 * 
	 * 
	 * @param divisiones
	 *            Colección de ids de las divisiones donde pertenece la ruta
	 * @param allDivs
	 *            Su valor debe de ser % si no se busca por divisiones, de lo
	 *            contrario debe ser NULL.
	 * @param regiones
	 *            Colección de ids de las regiones donde pertenece la ruta
	 * @param allRegs
	 *            Su valor debe de ser % si no se busca por regiones, de lo
	 *            contrario debe ser NULL
	 * @param gerencias
	 *            Colección de ids de las gerencias donde pertenece la ruta
	 * @param allGcias
	 *            Su valor debe de ser % si no se busca por gerencias, de lo
	 *            contrariodebe ser NULL
	 * @return
	 */
	@Query("select ruta from Organizacion as ruta " 
			+ "inner join ruta.parent as gcia with gcia.tipoNodo = 'G' "
			+ "inner join gcia.parent as reg with reg.tipoNodo = 'R' "
			+ "inner join reg.parent as div with div.tipoNodo = 'D' " 
			+ "where ruta.tipoNodo = 'RR' "
			+ "and (div.idOrg in (:divisiones) or div.idOrg like :allDivs) "
			+ "and (reg.idOrg in (:regiones) or reg.idOrg like :allRegs) "
			+ "and (gcia.idOrg in (:gerencias) or gcia.idOrg like :allGcias) ")
	List<Organizacion> findRutas(@Param("divisiones") List<String> divisiones, @Param("allDivs") String allDivs,
			@Param("regiones") List<String> regiones, @Param("allRegs") String allRegs,
			@Param("gerencias") List<String> gerencias, @Param("allGcias") String allGcias);
}

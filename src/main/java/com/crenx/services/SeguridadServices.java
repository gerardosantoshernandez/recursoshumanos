package com.crenx.services;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Domicilio;
import com.crenx.data.domain.entity.Empleado;
import com.crenx.data.domain.entity.ObjetoSeguridad;
import com.crenx.data.domain.entity.ObjetoSeguridadRelaciones;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.entity.UsuarioObjetoSeguridad;
import com.crenx.data.domain.repository.DomicilioRepository;
import com.crenx.data.domain.repository.EmpleadoRepository;
import com.crenx.data.domain.repository.ObjetoSeguridadRelacionesRepository;
import com.crenx.data.domain.repository.ObjetoSeguridadRepository;
import com.crenx.data.domain.repository.ProductoRepository;
import com.crenx.data.domain.repository.UsuarioObjetoSeguridadRepository;
import com.crenx.data.domain.repository.UsuarioRepository;
import com.crenx.data.domain.vo.CrearObjSecVO;
import com.crenx.data.domain.vo.CrearUserVO;
import com.crenx.data.domain.vo.CrearUsuarioVOToEmpleadoMapper;
import com.crenx.data.domain.vo.EmpleadoToVOModelMapper;
import com.crenx.data.domain.vo.EmpleadoVO;
import com.crenx.data.domain.vo.FiltroUsuarioVO;
import com.crenx.data.domain.vo.GroupVO;
import com.crenx.data.domain.vo.ListaUsuarioVO;
import com.crenx.data.domain.vo.ObjSecRelToCrearObjSecVOModelMapper;
import com.crenx.data.domain.vo.UserRoleGroupModelMapper;
import com.crenx.data.domain.vo.UsuarioToUsuarioVO;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@Service
public class SeguridadServices {

	@Autowired
	ObjetoSeguridadRepository objSecRepo;

	@Autowired
	ObjetoSeguridadRelacionesRepository objSecRelRepo;

	@Autowired
	UsuarioRepository usuarioRepository;
	@Autowired
	UsuarioObjetoSeguridadRepository usrRolRepository;
	@Autowired
	private CatalogoServices catServices;
	@Autowired
	private ProductoRepository productoRepository;
	@Autowired
	EmpleadoRepository emplRepository;
	@Autowired
	DomicilioRepository domRepository;

	public ObjetoSeguridad getById(String id) {
		return objSecRepo.findOne(id);
	}

	public ObjetoSeguridad getByLLaveNegocio(String llaveNegocio) {
		ObjetoSeguridad retVal = null;
		List<ObjetoSeguridad> lista = objSecRepo.findByLlaveNegocio(llaveNegocio);
		if (lista.size() > 0)
			retVal = lista.get(0);
		return retVal;
	}

	public void removeRelation(String relationId) {
		objSecRelRepo.delete(relationId);
	}

	public void removeUserRoleRelation(CrearObjSecVO objSec) {

		UsuarioObjetoSeguridad theRel = filtraRelacion(objSec.getIdUsuario(), objSec.getIdObjetoSeguridadParent());
		if (theRel != null)
			usrRolRepository.delete(theRel.getIdUsuarioObjetoSeguridad());

	}

	public void inactivaUsuario(String idUsuario) {
		Usuario usu = usuarioRepository.findOne(idUsuario);
		Catalogo inactivo = catServices.getCatalogoCveInterna("ESTATUS_INACTIVO");
		Catalogo activo = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		if (usu.getEstatusActual().getClaveInterna().equals(activo.getClaveInterna())) {// si
																						// esta
																						// activo
																						// ->
																						// inactivo
			usu.setEstatusActual(inactivo);
		} else // esta inactivo -> activo
			usu.setEstatusActual(activo);

		usuarioRepository.save(usu);
	}

	public List<UsuarioObjetoSeguridad> getUserRoles(String userName) {
		return usrRolRepository.findByUsuarioNombreUsuario(userName);

	}

	public List<GroupVO> getUserGroups(String userName) {
		List<UsuarioObjetoSeguridad> usrRole = getUserRoles(userName);
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new UserRoleGroupModelMapper());
		Type listType = new TypeToken<List<GroupVO>>() {
		}.getType();
		List<GroupVO> groups = mapper.map(usrRole, listType);
		return groups;
	}

	public List<String> getStrUserGroups(String userName) {
		List<String> returnValue = new ArrayList<String>();
		List<UsuarioObjetoSeguridad> usrRole = getUserRoles(userName);
		for (UsuarioObjetoSeguridad unObj : usrRole) {
			returnValue.add(unObj.getRole().getLlaveNegocio());
		}
		return returnValue;
	}

	public List<EmpleadoVO> getEmpleados(FiltroUsuarioVO filtro) {
		filtro.setApellido(filtro.getApellido() == null ? "%" : filtro.getApellido() + "%");
		filtro.setNombre(filtro.getNombre() == null ? "%" : filtro.getNombre() + "%");
		List<Empleado> empleados = emplRepository.findByNombreLikeAndApellidoPaternoLike(filtro.getNombre(),
				filtro.getApellido());

		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new EmpleadoToVOModelMapper());
		Type listType = new TypeToken<List<EmpleadoVO>>() {
		}.getType();
		List<EmpleadoVO> listaEmpleados = mapper.map(empleados, listType);
		return listaEmpleados;
	}

	public Usuario getPrincipal(String nombreUsuario) {
		List<Usuario> usuarios = usuarioRepository.findByNombreUsuario(nombreUsuario);
		if (usuarios.iterator().hasNext())
			return usuarios.iterator().next();
		return null;
	}

	public Collection<CrearObjSecVO> getChildren(String idParent) {
		List<CrearObjSecVO> listaChildren = null;
		if (idParent != null) {
			Collection<ObjetoSeguridadRelaciones> children = Lists
					.newArrayList(objSecRelRepo.findByParentIdObjetoSeguridad(idParent));
			ModelMapper mapper = new ModelMapper();

			mapper.addMappings(new ObjSecRelToCrearObjSecVOModelMapper());
			Type listType = new TypeToken<List<CrearObjSecVO>>() {
			}.getType();
			listaChildren = mapper.map(children, listType);
		} else {
			Collection<ObjetoSeguridad> items = Lists.newArrayList(objSecRepo.findByTipoObje("SEC_OBJECT"));
			ModelMapper mapper = new ModelMapper();
			Type listType = new TypeToken<List<CrearObjSecVO>>() {
			}.getType();
			listaChildren = mapper.map(items, listType);
		}
		return listaChildren;
	}

	public Collection<ListaUsuarioVO> filtraUsuarios(FiltroUsuarioVO filtros) {
		Collection<Usuario> usuarios = null;

		String nombre = StringUtils.isEmpty(filtros.getNombre()) ? "%" : filtros.getNombre() + "%";
		String apellidoPaterno = StringUtils.isEmpty(filtros.getApellido()) ? "%" : filtros.getApellido() + "%";

		List<String> roles = Lists.newArrayList(filtros.getRoles());
		List<String> estatus = new ArrayList<String>();
		if (filtros.getEstatus() != null)
			estatus = Lists.newArrayList(filtros.getEstatus());

		if (filtros.getRoles().size() > 0) {
			if (estatus.size() <= 0)
				usuarios = Lists.newArrayList(
						this.usuarioRepository.findByRolesIdObjetoSeguridadInAndNombreIsLikeAndApellidoPaternoIsLike(
								roles, nombre, apellidoPaterno));
			else
				usuarios = Lists.newArrayList(this.usuarioRepository
						.findByRolesIdObjetoSeguridadInAndNombreIsLikeAndApellidoPaternoIsLikeAndEstatusActualIdCatalogoIn(
								roles, nombre, apellidoPaterno, estatus));
		} else {
			if (estatus.size() <= 0)
				usuarios = Lists.newArrayList(
						this.usuarioRepository.findByNombreIsLikeAndApellidoPaternoIsLike(nombre, apellidoPaterno));
			else
				usuarios = Lists.newArrayList(
						this.usuarioRepository.findByNombreIsLikeAndApellidoPaternoIsLikeAndEstatusActualIdCatalogoIn(
								nombre, apellidoPaterno, estatus));
		}

		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new UsuarioToUsuarioVO());
		Type listType = new TypeToken<List<ListaUsuarioVO>>() {
		}.getType();
		List<ListaUsuarioVO> listaUsuarios = mapper.map(usuarios, listType);
		return listaUsuarios;
	}

	public UsuarioObjetoSeguridad filtraRelacion(String idUsuario, String idObjetoSeguridad) {
		Collection<UsuarioObjetoSeguridad> relaciones = Lists.newArrayList(
				this.usrRolRepository.findByUsuarioIdUsuarioAndRoleIdObjetoSeguridad(idUsuario, idObjetoSeguridad));
		return relaciones.iterator().hasNext() ? relaciones.iterator().next() : null;
	}

	public CrearObjSecVO agregaUsuarioRol(CrearObjSecVO relacion) {
		Usuario usuario = usuarioRepository.findOne(relacion.getIdUsuario());
		ObjetoSeguridad role = getById(relacion.getIdObjetoSeguridadParent());
		List<UsuarioObjetoSeguridad> asignados = usrRolRepository.findByUsuarioNombreUsuarioAndRoleIdObjetoSeguridad(
				usuario.getNombreUsuario(), role.getIdObjetoSeguridad());
		if (asignados.size() > 0)
			return relacion;
		UsuarioObjetoSeguridad rel = new UsuarioObjetoSeguridad();
		rel.setUsuario(usuario);
		rel.setRole(role);
		rel.setCreacion(new Date());
		usrRolRepository.save(rel);
		return relacion;
	}

	public CrearObjSecVO agregaPermisoRol(CrearObjSecVO objSec) {
		ObjetoSeguridadRelaciones rel = new ObjetoSeguridadRelaciones();
		rel.setCreacion(new Date());
		ObjetoSeguridad parent = getById(objSec.getIdObjetoSeguridadParent());
		ObjetoSeguridad child = getById(objSec.getIdObjetoSeguridad());
		rel.setParent(parent);
		rel.setChild(child);
		objSecRelRepo.save(rel);
		return objSec;
	}

	@Transactional
	public void resetPassword(String userId, String contrasena) {
		Usuario user = usuarioRepository.findOne(userId);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = null;
		hashedPassword = passwordEncoder.encode(contrasena);
		user.setHashContrasena(hashedPassword);
		usuarioRepository.save(user);
	}

	@Transactional
	public CrearUserVO saveUser(CrearUserVO usuario) {
		ModelMapper mapper = new ModelMapper();
		Usuario usuarioDB = mapper.map(usuario, Usuario.class);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		Usuario currentUser = null;
		String hashedPassword = null;
		if (usuario.getIdUsuario() != null) {
			currentUser = usuarioRepository.findOne(usuario.getIdUsuario());
			hashedPassword = currentUser.getHashContrasena();
			usuarioDB.setCreacion(currentUser.getCreacion());
		} else {
			hashedPassword = passwordEncoder.encode(usuario.getContrasena());
			usuarioDB.setCreacion(new Date());
		}

		usuarioDB.setHashContrasena(hashedPassword);

		ModelMapper mapperFig = new ModelMapper();
		mapperFig.addMappings(new CrearUsuarioVOToEmpleadoMapper());
		Empleado empl = emplRepository.findByNombreUsuario(usuario.getNombreUsuario());
		String empId = empl != null ? empl.getIdEmpleado() : null;
		empl = mapperFig.map(usuario, Empleado.class);
		empl.setIdEmpleado(empId);
		if (usuario.getTipoDocId() != null) {
			Catalogo tipDoc = catServices.getCatalogoId(usuario.getTipoDocId());
			empl.setTipoDocId(tipDoc);
		}
		if (usuario.getIdGenero() != null) {
			Catalogo genero = catServices.getCatalogoId(usuario.getIdGenero());
			empl.setGenero(genero);
		}
		
		/*Domicilio domicilio = catServices.getDomicilioId("402880835bc1f4d7015bc1f6eccc0195");
		empl.setDomicilioEmpleado(domicilio);*/

		if (empl.getDomicilioEmpleado()!=null)
			domRepository.save(empl.getDomicilioEmpleado());
		if (empl.getDomicilioEmpleadoLaboral()!=null)
			domRepository.save(empl.getDomicilioEmpleadoLaboral());
		
		Catalogo cargo = catServices.getCatalogoId(usuario.getIdCargo());
		empl.setCargoEmpleado(cargo);

		Catalogo estatus = catServices
				.getCatalogoCveInterna(usuario.isActivo() ? "ESTATUS_ACTIVO" : "ESTATUS_INACTIVO");
		usuarioDB.setEstatusActual(estatus);
		empl.setEstatusActual(estatus);
		empl.setCreacion(new Date());
		empl.setTelefonoCasa(usuario.getTelefonoCasa());
		empl.setTelefonoCelular(usuario.getTelefonoCelular());

		usuarioRepository.save(usuarioDB);
		emplRepository.save(empl);

		return usuario;

	}

}

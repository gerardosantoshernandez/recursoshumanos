package com.crenx.data.domain.vo;

import java.util.Date;

public class IndicadorNegocioVO {

	String idIndicadorNegocio;
	String division;
	String region;
	String gerencia;
	String ruta;
	Date fechaProceso;
	int semana;
	int mes;
	int trimestre;
	int semestre;
	int anio;
	double cobranza;
	double colocacion;
	long creditosNuevos;
	double carteraTotal;
	double carteraCapital;
	double carteraVencida;
	double carteraRetraso;
	double carteraNueva;
	
	long totalClientes;
	long clientesNuevos;
	public String getIdIndicadorNegocio() {
		return idIndicadorNegocio;
	}
	public void setIdIndicadorNegocio(String idIndicadorNegocio) {
		this.idIndicadorNegocio = idIndicadorNegocio;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public Date getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public int getSemana() {
		return semana;
	}
	public void setSemana(int semana) {
		this.semana = semana;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getTrimestre() {
		return trimestre;
	}
	public void setTrimestre(int trimestre) {
		this.trimestre = trimestre;
	}
	public int getSemestre() {
		return semestre;
	}
	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public double getCobranza() {
		return cobranza;
	}
	public void setCobranza(double cobranza) {
		this.cobranza = cobranza;
	}
	public double getColocacion() {
		return colocacion;
	}
	public void setColocacion(double colocacion) {
		this.colocacion = colocacion;
	}
	public long getCreditosNuevos() {
		return creditosNuevos;
	}
	public void setCreditosNuevos(long creditosNuevos) {
		this.creditosNuevos = creditosNuevos;
	}
	public double getCarteraTotal() {
		return carteraTotal;
	}
	public void setCarteraTotal(double carteraTotal) {
		this.carteraTotal = carteraTotal;
	}
	public double getCarteraCapital() {
		return carteraCapital;
	}
	public void setCarteraCapital(double carteraCapital) {
		this.carteraCapital = carteraCapital;
	}
	public double getCarteraVencida() {
		return carteraVencida;
	}
	public void setCarteraVencida(double carteraVencida) {
		this.carteraVencida = carteraVencida;
	}
	public double getCarteraRetraso() {
		return carteraRetraso;
	}
	public void setCarteraRetraso(double carteraRetraso) {
		this.carteraRetraso = carteraRetraso;
	}
	public long getTotalClientes() {
		return totalClientes;
	}
	public void setTotalClientes(long totalClientes) {
		this.totalClientes = totalClientes;
	}
	public long getClientesNuevos() {
		return clientesNuevos;
	}
	public void setClientesNuevos(long clientesNuevos) {
		this.clientesNuevos = clientesNuevos;
	}
	public double getCarteraNueva() {
		return carteraNueva;
	}
	public void setCarteraNueva(double carteraNueva) {
		this.carteraNueva = carteraNueva;
	}
	
}

package com.crenx.data.domain.vo;

import java.util.Date;

public class DocumentoVO {

	private String idDocumento;
	private String nombre;
	private String descripcion;
	private String tipo;
	private String referencia;
	private String fileName;
	private String contentType;
	private String idAutor;
	private Date fechaCreacion;
	private double version;
	private String idParent;
	private String tipoDocumento;
	// Este atributo es el id o referencia al objeto (organización, cliente,
	// etcétera) con que se relaciona el documento mediante una propiedad de
	// documento
	private String objetoRelacionado;

	/**
	 * Contructor default
	 */
	public DocumentoVO() {
		// constructor default
	}

	/**
	 * Contructor con todos los elemento. es usado para el paso de datos desde
	 * el repository
	 * 
	 * @param idDocumento
	 * @param nombre
	 * @param descripcion
	 * @param tipo
	 * @param referencia
	 * @param fileName
	 * @param contentType
	 * @param idAutor
	 * @param fechaCreacion
	 * @param version
	 * @param idParent
	 * @param tipoDocumento
	 * @param objetoRelacionado
	 */
	public DocumentoVO(final String idDocumento, final String nombre, final String descripcion, final String tipo,
			final String referencia, final String fileName, final String contentType, final String idAutor,
			final Date fechaCreacion, final double version, final String idParent, final String tipoDocumento,
			final String objetoRelacionado) {
		super();
		this.idDocumento = idDocumento;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tipo = tipo;
		this.referencia = referencia;
		this.fileName = fileName;
		this.contentType = contentType;
		this.idAutor = idAutor;
		this.fechaCreacion = fechaCreacion;
		this.version = version;
		this.idParent = idParent;
		this.tipoDocumento = tipoDocumento;
		this.objetoRelacionado = objetoRelacionado;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getIdAutor() {
		return idAutor;
	}

	public void setIdAutor(String idAutor) {
		this.idAutor = idAutor;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public double getVersion() {
		return version;
	}

	public void setVersion(double version) {
		this.version = version;
	}

	public String getIdParent() {
		return idParent;
	}

	public void setIdParent(String idParent) {
		this.idParent = idParent;
	}

	/**
	 * @return the objetoRelacionado
	 */
	public String getObjetoRelacionado() {
		return objetoRelacionado;
	}

	/**
	 * @param objetoRelacionado
	 *            the objetoRelacionado to set
	 */
	public void setObjetoRelacionado(String objetoRelacionado) {
		this.objetoRelacionado = objetoRelacionado;
	}

}

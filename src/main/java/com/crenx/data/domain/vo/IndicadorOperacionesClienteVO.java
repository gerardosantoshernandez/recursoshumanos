package com.crenx.data.domain.vo;

import java.util.Date;

public class IndicadorOperacionesClienteVO {

	private String tipoOperacion;
	private String ruta;	
	private String gerencia;	
	private String region;	
	private String division;	
	private String idCliente;
	private double monto;
	private Date periodo;

	public IndicadorOperacionesClienteVO(){}
	public IndicadorOperacionesClienteVO(
			String division,
			String region,
			String gerencia,
			String ruta,
			String cliente,
			double monto)
	{
		this.division = division;
		this.region = region;
		this.gerencia = gerencia;
		this.ruta = ruta;
		this.idCliente = cliente;
		this.monto = monto;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}

	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public Date getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Date periodo) {
		this.periodo = periodo;
	}
	

}

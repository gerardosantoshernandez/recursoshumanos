package com.crenx.data.domain.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Catalogo {
	
	public Catalogo(){}
	public Catalogo(String nombre, String parentId)
	{
		this.nombre = nombre;
		this.parentId = parentId;
		
	}

	public Catalogo(String nombre, String descripcion, String claveInterna, String clave, String parentId)
	{
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.claveInterna = claveInterna;
		this.clave = clave;
	}
	public Catalogo(String nombre, String claveInterna, String parentId)
	{
		this.nombre = nombre;
		this.claveInterna = claveInterna;
		this.parentId = parentId;
	}
	
	public Catalogo(String nombre)
	{
		this.nombre = nombre;
	}
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_catalogo", unique = true)
	private String idCatalogo;
	
	@Column(name = "nombre",length = 50)
	private String nombre;
	
	@Column(name = "descripcion",length = 100)
	private String descripcion;
	
	@Column(name = "clave_interna",length = 50)
	private String claveInterna;
	
	@Column(name = "clave",length = 10)
	private String clave;
		
	@Column(name = "id_parent",length = 40)
	private String parentId;

	@Column(name = "ptsRiesgo")
	private Integer puntosRiesgo=0;
	
	/*
	@ManyToOne
    @JoinColumn(name="id_parent", referencedColumnName="id_catalogo" ,insertable=false, updatable=false)
    private Catalogo parent;
	
	@OneToMany(mappedBy="parent")
	private Collection<Catalogo> items;
	 */

	
	public String getIdCatalogo() {
		return idCatalogo;
	}
	public int getPuntosRiesgo() {
		return puntosRiesgo;
	}
	public void setPuntosRiesgo(int puntosRiesgo) {
		this.puntosRiesgo = puntosRiesgo;
	}
	public void setIdCatalogo(String idCatalogo) {
		this.idCatalogo = idCatalogo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getClaveInterna() {
		return claveInterna;
	}
	public void setClaveInterna(String claveInterna) {
		this.claveInterna = claveInterna;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	/*
	public Catalogo getParent() {
		return parent;
	}
	public void setParent(Catalogo parent) {
		this.parent = parent;
	}
	public Collection<Catalogo> getItems() {
		return items;
	}
	public void setItems(Collection<Catalogo> items) {
		this.items = items;
	}
	*/
	/*
	@OneToOne(cascade=CascadeType.ALL, mappedBy="Producto")
	private Producto producto;
	*/
}

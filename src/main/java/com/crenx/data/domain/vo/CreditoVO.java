package com.crenx.data.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditoVO {

	private long id;	
	private String idCredito;
	private double monto;
	private double comision;
	private short plazo;
	private double tasa;
	private double pago;
	private String fechaEmision;
	private String fechaVencimiento;
	private String fechaUltimoPago;	
	private String fechaCancelacion;	
	private String idUnidadPlazo;
	private String idFrecuenciaCobro;
	private String idProducto;
	private String producto;
	private String idCliente;
	private String nombreCliente;
	private String idEstatusCredito;
	private String idRuta;
	private String ruta;
	private String titularRuta;
	private String unidadPlazo;
	private String frecuenciaCobro;
	private String estatusCredito;
	private String idCoDedudor;
	private String nombreCoDeudor;
	private double saldo;
	private double saldoInsoluto;
	private int totPagos;
	private int totPagoATiempo;
	private int calificacion;
	private double saldoComision;
	private String idDispositivo;
	private String geoPosicion;
	private boolean aprobado=false;
	private String taskId;
	private String fechaUltimaVisita;	
	private double saldoAnterior;
	private double pagoAdicional;
	private double pagoComision;
	private double pagoAbonado;
	private double pagoPendienteAplicar;
	private String rolAprobador;
	private String motivoCancelacion;
	private String observaciones;
	private String tipoCartera;
	private String gerencia;
	private String region;
	private String division;
	private String rutaCliente;
	private String fechaPagoNoPagado;
	private int diasAtraso = 0;
	private String cveFiscal;
	
	
	
	
	
	
	public String getCveFiscal() {
		return cveFiscal;
	}
	public void setCveFiscal(String cveFiscal) {
		this.cveFiscal = cveFiscal;
	}
	public int getDiasAtraso() {
		return diasAtraso;
	}
	public void setDiasAtraso(int diasAtraso) {
		this.diasAtraso = diasAtraso;
	}
	public String getFechaPagoNoPagado() {
		return fechaPagoNoPagado;
	}
	public void setFechaPagoNoPagado(String fechaPagoNoPagado) {
		this.fechaPagoNoPagado = fechaPagoNoPagado;
	}
	public String getRutaCliente() {
		return rutaCliente;
	}
	public void setRutaCliente(String rutaCliente) {
		this.rutaCliente = rutaCliente;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}
	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}
	public String getIdCoDedudor() {
		return idCoDedudor;
	}
	public void setIdCoDedudor(String idCoDedudor) {
		this.idCoDedudor = idCoDedudor;
	}
	public String getIdCredito() {
		return idCredito;
	}
	public void setIdCredito(String idCredito) {
		this.idCredito = idCredito;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public short getPlazo() {
		return plazo;
	}
	public void setPlazo(short plazo) {
		this.plazo = plazo;
	}
	public double getTasa() {
		return tasa;
	}
	public void setTasa(double tasa) {
		this.tasa = tasa;
	}
	public double getPago() {
		return pago;
	}
	public void setPago(double pago) {
		this.pago = pago;
	}
	public String getIdUnidadPlazo() {
		return idUnidadPlazo;
	}
	public void setIdUnidadPlazo(String idUnidadPlazo) {
		this.idUnidadPlazo = idUnidadPlazo;
	}
	public String getIdFrecuenciaCobro() {
		return idFrecuenciaCobro;
	}
	public void setIdFrecuenciaCobro(String idFrecuenciaCobro) {
		this.idFrecuenciaCobro = idFrecuenciaCobro;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public String getIdEstatusCredito() {
		return idEstatusCredito;
	}
	public void setIdEstatusCredito(String idEstatusCredito) {
		this.idEstatusCredito = idEstatusCredito;
	}
	public String getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(String idRuta) {
		this.idRuta = idRuta;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getUnidadPlazo() {
		return unidadPlazo;
	}
	public void setUnidadPlazo(String unidadPlazo) {
		this.unidadPlazo = unidadPlazo;
	}
	public String getFrecuenciaCobro() {
		return frecuenciaCobro;
	}
	public void setFrecuenciaCobro(String frecuenciaCobro) {
		this.frecuenciaCobro = frecuenciaCobro;
	}
	public String getEstatusCredito() {
		return estatusCredito;
	}
	public void setEstatusCredito(String estatusCredito) {
		this.estatusCredito = estatusCredito;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getFechaUltimoPago() {
		return fechaUltimoPago;
	}
	public void setFechaUltimoPago(String fechaUltimoPago) {
		this.fechaUltimoPago = fechaUltimoPago;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public int getTotPagos() {
		return totPagos;
	}
	public void setTotPagos(int totPagos) {
		this.totPagos = totPagos;
	}
	public int getTotPagoATiempo() {
		return totPagoATiempo;
	}
	public void setTotPagoATiempo(int totPagoATiempo) {
		this.totPagoATiempo = totPagoATiempo;
	}
	public int getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}
	public double getSaldoComision() {
		return saldoComision;
	}
	public void setSaldoComision(double saldoComision) {
		this.saldoComision = saldoComision;
	}
	public String getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public double getSaldoInsoluto() {
		return saldoInsoluto;
	}
	public void setSaldoInsoluto(double saldoInsoluto) {
		this.saldoInsoluto = saldoInsoluto;
	}
	public String getFechaCancelacion() {
		return fechaCancelacion;
	}
	public void setFechaCancelacion(String fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}
	public String getGeoPosicion() {
		return geoPosicion;
	}
	public void setGeoPosicion(String geoPosicion) {
		this.geoPosicion = geoPosicion;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getTitularRuta() {
		return titularRuta;
	}
	public void setTitularRuta(String titularRuta) {
		this.titularRuta = titularRuta;
	}
	public String getNombreCoDeudor() {
		return nombreCoDeudor;
	}
	public void setNombreCoDeudor(String nombreCoDeudor) {
		this.nombreCoDeudor = nombreCoDeudor;
	}
	public boolean isAprobado() {
		return aprobado;
	}
	public void setAprobado(boolean aprobado) {
		this.aprobado = aprobado;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFechaUltimaVisita() {
		return fechaUltimaVisita;
	}
	public void setFechaUltimaVisita(String fechaUltimaVisita) {
		this.fechaUltimaVisita = fechaUltimaVisita;
	}
	public double getSaldoAnterior() {
		return saldoAnterior;
	}
	public void setSaldoAnterior(double saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}
	public double getPagoAdicional() {
		return pagoAdicional;
	}
	public void setPagoAdicional(double pagoAdicional) {
		this.pagoAdicional = pagoAdicional;
	}
	public double getPagoComision() {
		return pagoComision;
	}
	public void setPagoComision(double pagoComision) {
		this.pagoComision = pagoComision;
	}
	public double getPagoAbonado() {
		return pagoAbonado;
	}
	public void setPagoAbonado(double pagoAbonado) {
		this.pagoAbonado = pagoAbonado;
	}
	/**
	 * @return the pagoPendienteAplicar
	 */
	public double getPagoPendienteAplicar() {
		return pagoPendienteAplicar;
	}
	/**
	 * @param pagoPendienteAplicar the pagoPendienteAplicar to set
	 */
	public void setPagoPendienteAplicar(double pagoPendienteAplicar) {
		this.pagoPendienteAplicar = pagoPendienteAplicar;
	}
	public String getRolAprobador() {
		return rolAprobador;
	}
	public void setRolAprobador(String rolAprobador) {
		this.rolAprobador = rolAprobador;
	}
	public String getTipoCartera() {
		return tipoCartera;
	}
	public void setTipoCartera(String tipoCartera) {
		this.tipoCartera = tipoCartera;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	
	
}

'use strict';
angular.module('crenx.perfilcontrollers', [])
.controller(
				'PerfilUserController',
				function($scope, $rootScope, apiService, $modal, $timeout,
						$state) {
					$scope.allUsers = [];
					$scope.filters = {
						roles : [],
						estatus : []
					};
					$scope.tipoObjeto = $state.current.params.tipoObjeto;

					var filterTextTimeout;
					$scope.$watch('filters.correoElectronico', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 600); // delay 250 ms
					})

					$scope.$watch('filters.nombre', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 300); // delay 250 ms
					})

					$scope.$watch('filters.apellido', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 300); // delay 250 ms
					})

					// Open Simple Modal
					$scope.openModal = function(modal_id, modal_size, userid,
							accion) {
						$rootScope.userid = userid;
						$rootScope.accion = accion;
						$rootScope.currentModal = $modal
								.open({
									templateUrl : modal_id,
									size : modal_size,
									backdrop : typeof modal_backdrop == 'undefined' ? true
											: modal_backdrop
								});
					};

					$rootScope.resetPwd = {
						idUsuario : null,
						contrasena : null
					};
					$scope.resetPassword = function(modal_id, modal_size,
							userid, userName) {
						$rootScope.userid = userid;
						$rootScope.userName = userName;
						$rootScope.resetPwd.idUsuario = userid;

						$rootScope.currentModal = $modal
								.open({
									templateUrl : modal_id,
									size : modal_size,
									backdrop : typeof modal_backdrop == 'undefined' ? true
											: modal_backdrop
								});
					};

					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					jQuery(document)
							.ready(
									function($) {
										$("#rolefilter")
												.select2(
														{
															placeholder : 'Tipo de usuario',
															allowClear : true
														})
												.on(
														'select2-open',
														function() {
															$(this).data(
																	'select2').results
																	.addClass(
																			'overflow-hidden')
																	.perfectScrollbar();
														}).on('change',
														function(e) {
															changeRol(e);
														})

										$("#estatusfilter")
												.select2(
														{
															placeholder : 'Estatus de usuario',
															allowClear : true
														})
												.on(
														'select2-open',
														function() {
															$(this).data(
																	'select2').results
																	.addClass(
																			'overflow-hidden')
																	.perfectScrollbar();
														}).on('change',
														function(e) {
															changeEstatus(e);
														})

									});

					function loadData() {
						apiService.post('/api/cuenta', $scope.filters,
								lcoationLoadCompleted, serviceError);

					}

					function addSelectItems(selectControl, listaItems) {
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(
											value.idObjetoSeguridad).text(
											value.nombre));
						});

					}
					function addSelectItems2(selectControl, listaItems) {
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>")
											.val(value.idCatalogo).text(
													value.nombre));
						});

					}

					function loadRole() {
						apiService.post('/api/cuenta/objetosseguridad',
								$scope.tipoObjeto,
								function lcoationLoadCompleted(result) {
									$scope.allRoles = result.data;
									addSelectItems("#rolefilter",
											$scope.allRoles);
								}, serviceError);

					}
					function loadEstatus() {
						apiService.post('/api/catalogo/items',
								'CAT_ESTATUS_GEN', function loadOk(result) {
									$scope.estatus = result.data.items;
									addSelectItems2("#estatusfilter",
											$scope.estatus);
								}, serviceError);
					}

					function lcoationLoadCompleted(result) {
						$scope.allUsers = result.data;

					}

					function serviceError(response) {
						toastr.error("Sucedión un error", "Error", opts);
					}

					function changeRol(e) {
						$scope.filters.roles = e.val;
						loadData();
					}
					;

					function changeEstatus(e) {
						$scope.filters.estatus = e.val;
						loadData();
					}
					;

					loadRole();
					loadEstatus();
					loadData();

					$rootScope.resetPassword = function() {
						apiService
								.post(
										'/api/cuenta/resetPassword/',
										$rootScope.resetPwd,
										function(result) {
											toastr
													.success(
															"Se eliminó satisfactoriamente el ususrio.",
															"Eliminar usuario",
															opts);
											$rootScope.currentModal.close();
										}, serviceError);
					}
					$rootScope.deleteUser = function(item, event) {
						apiService
								.post(
										'/api/cuenta/inactivaUsuario/',
										$rootScope.userid,
										function(result) {
											$scope.questionary = result.data;
											var mensaje = "";
											if ($rootScope.accion == 'Activar')
												mensaje = "activo";
											else
												mensaje = "desactivo";
											toastr
													.success(
															"Se "
																	+ mensaje
																	+ " satisfactoriamente el usuario.",
															"Cambiar estatus",
															opts);
											$rootScope.currentModal.close();
											loadData();
										}, serviceError);
					};

					$scope.showRoles = function(nombreUsuario, modal_id,
							modal_size, modal_backdrop) {
						// modal
						loadRolesUsuario(nombreUsuario);
						// $rootScope.motivo ="";
						$rootScope.currentModal = $modal
								.open({
									templateUrl : 'showRoles',
									size : modal_size,
									backdrop : typeof modal_backdrop == 'undefined' ? true
											: modal_backdrop
								});

					};

					function loadRolesUsuario(nombreUsuario) {
						apiService.post('/api/cuenta/getRolesUsuario',
								nombreUsuario, function(result) {
									$rootScope.roles = result.data;
								}, serviceError);
					}

				})

.
controller('PerfilControllers', function ($scope, $rootScope, apiService,$http, $stateParams, $state, $timeout, $modal) {
	$scope.allUsers = [];
	$scope.user = $stateParams.user;
	$scope.user = {};
	$scope.isUpdate = false;
	$scope.tipoObjeto = $stateParams.tipoObjeto;
	
	// Open Simple Modal
	$scope.openModal = function(modal_id, modal_size, projectid) {
		$rootScope.userrole = {
			UserId : $scope.user.ID
		};
		$rootScope.currentModal = $modal
				.open({
					templateUrl : modal_id,
					size : modal_size,
					backdrop : typeof modal_backdrop == 'undefined' ? true
							: modal_backdrop
				});
	};
	var opts = {
		"closeButton" : true,
		"debug" : false,
		"positionClass" : "toast-top-right",
		"onclick" : null,
		"showDuration" : "300",
		"hideDuration" : "1000",
		"timeOut" : "5000",
		"extendedTimeOut" : "1000",
		"showEasing" : "swing",
		"hideEasing" : "linear",
		"showMethod" : "fadeIn",
		"hideMethod" : "fadeOut"
	};
	
	/*Operacion completada*/
	function locationLoadCompleted(result) {
		$scope.allUsers = result.data;

	}
	
	/*Operacion erronea*/
	function serviceError(response) {
		toastr.error("Sucedión un error", "Error", opts);
	}
	
	function loadData() {
		apiService.post('/api/cuenta', $scope.filters,
				locationLoadCompleted, serviceError);

	}
	
	loadData();
	
	function prepareListItems(control, placeHolderVal) {
		$(control).select2({
			placeholder : placeHolderVal,
			allowClear : true
		}).on(
				'select2-open',
				function() {
					// Adding Custom Scrollbar
					$(this).data('select2').results.addClass(
							'overflow-hidden')
							.perfectScrollbar();
				});

	}

	jQuery(document).ready(
			function($) {
				prepareListItems("#genero", 'Género Sexual');
				prepareListItems("#tipoDoc", 'Tipo de Documento de Identidad');
				prepareListItems("#cargo", 'Cargo');
				prepareListItems("#estadoNac", 'Estado de nacimiento');
				prepareListItems("#idioma", 'Idioma hablado');
			});

	function addSelectedItems(selectControl, listaItems,
			selectedValue) {
		$
				.each(
						listaItems,
						function(key, value) {
							var selected = selectedValue == value.idCatalogo ? true
									: false;
							var opt = new Option(value.nombre,
									value.idCatalogo);
							$(selectControl).append(opt);
							if (selected)
								opt.setAttribute("selected",
										selected);
						});
	}

	function loadCatalogo(catType) {
		apiService.post('/api/catalogo/items', catType,
				catalogComplete, ServiceError);

	}

	function catalogComplete(result) {
		if (result.data.parent.claveInterna == 'CAT_TIP_DOC_ID') {
			// addSelectedItems("#tipoDoc", result.data.items,
			// $scope.tipoDocId);
			$scope.allTipoDoc = result.data;
		}
		if (result.data.parent.claveInterna == 'CAT_GEN') {
			$scope.allGenero = result.data;
			// addSelectedItems("#genero", result.data.items,
			// $scope.genero);
		}
		if (result.data.parent.claveInterna == 'CAT_ENTIDAD_FED') {
			// addSelectedItems("#estadoNac", result.data.items,
			// $scope.entidadNacimiento);
			$scope.allEntidadNac = result.data;
		}
		if (result.data.parent.claveInterna == 'CAT_CARGO') {
			// addSelectedItems("#cargo", result.data.items,
			// $scope.cargo);
			$scope.allCargo = result.data;
		}
		if (result.data.parent.claveInterna == 'CAT_IDIOMA') {
			// addSelectedItems("#cargo", result.data.items,
			// $scope.cargo);
			$scope.allIdioma = result.data;
		}
		if (result.data.parent.claveInterna == 'CAT_NACIONALIDAD') {
			$scope.allNacionalidad = result.data;
		}
		if (result.data.parent.claveInterna == 'CAT_ESTADO_CIVIL') {
			$scope.allEstadoCivil = result.data;
		}
		if (result.data.parent.claveInterna == 'CAT_TITULO') {
			$scope.allTitulo = result.data;
		}
	}

	if ($stateParams.userId) {
		$scope.isUpdate = true;
		$scope.userId = $stateParams.userId;
		apiService.get('/api/cuenta/detailuser/'
				+ $stateParams.userId, $scope.user,
				function ServiceSuccessComplete(result) {
					$scope.user = result.data;
					$scope.tipoDocId = result.data.tipoDocId;
					$scope.genero = result.data.idGenero;
					$scope.cargo = result.data.idCargo;

				}, ServiceError);
	}

	loadCatalogo('CAT_TIP_DOC_ID');
	loadCatalogo('CAT_GEN');
	loadCatalogo("CAT_CARGO");
	loadCatalogo('CAT_ENTIDAD_FED');
	loadCatalogo('CAT_IDIOMA');
	loadCatalogo('CAT_NACIONALIDAD');
	loadCatalogo('CAT_ESTADO_CIVIL');
	loadCatalogo('CAT_TITULO');

	$scope.submitTheForm = function(item, event) {
		apiService.post('/api/cuenta/create', $scope.user,
				ServiceSuccessComplete, ServiceError);
	}

	var filterTextTimeout;
	//Domicilio Residencial
	$scope
			.$watch(
					'user.domicilioEmpleado.codigoPostal',
					function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(
								function(val) {
									if ($scope.user.domicilioEmpleado == undefined)
										return;
									if ($scope.user.domicilioEmpleado.codigoPostal == undefined)
										return;
									if ($scope.user.domicilioEmpleado.codigoPostal.length == 5)
										loadColonias(
												$scope.user.domicilioEmpleado.codigoPostal,
												"E");
								}, 250);
					})
					
					//Domicilio Laboral
					$scope
			.$watch(
					'user.domicilioEmpleadoLaboral.codigoPostal',
					function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(
								function(val) {
									if ($scope.user.domicilioEmpleadoLaboral == undefined)
										return;
									if ($scope.user.domicilioEmpleadoLaboral.codigoPostal == undefined)
										return;
									if ($scope.user.domicilioEmpleadoLaboral.codigoPostal.length == 5)
										loadColonias(
												$scope.user.domicilioEmpleadoLaboral.codigoPostal,
												"L");
								}, 250);
					})
					
	// Function para cargar colonias
	function loadColonias(codigo, tipo) {
		apiService
				.get(
						'/api/sepomex/colonias/' + codigo,
						null,
						function ServiceSuccessComplete(result) {

							if (tipo == "E") {
								$scope.user.domicilioEmpleado.municipio = result.data.municipio;
								$scope.user.domicilioEmpleado.estado = result.data.estado;
								$scope.user.domicilioEmpleado.pais = result.data.pais;

								$("#municipio_emp").value = result.data.municipio;
								$("#estado_emp").value = result.data.estado;
								$("#pais_emp").value = result.data.pais;
								$("#colonia_emp")
										.empty();
								$("#colonia_emp")
										.append(
												new Option(
														"----Seleccione una----",
														""));
								$
										.each(
												result.data.colonias,
												function(key,
														value) {
													var selected = $scope.user.domicilioEmpleado.colonia == value ? true
															: false;
													var opt = new Option(
															value,
															value);
													$(
															"#colonia_emp")
															.append(
																	opt);
													if (selected)
														opt
																.setAttribute(
																		"selected",
																		selected);
												});

							}else {
								$scope.user.domicilioEmpleadoLaboral.municipio = result.data.municipio;
								$scope.user.domicilioEmpleadoLaboral.estado = result.data.estado;
								$scope.user.domicilioEmpleadoLaboral.pais = result.data.pais;

								$("#municipio_emp_lab").value = result.data.municipio;
								$("#estado_emp_lab").value = result.data.estado;
								$("#pais_emp_lab").value = result.data.pais;
								$("#colonia_emp_lab").empty();
								$("#colonia_emp_lab")
										.append(
												new Option(
														"----Seleccione una----",
														""));
								$
										.each(
												result.data.colonias,
												function(key,
														value) {
													var selected = $scope.user.domicilioEmpleadoLaboral.colonia == value ? true
															: false;
													var opt = new Option(
															value,
															value);
													$(
															"#colonia_emp_lab")
															.append(
																	opt);
													if (selected)
														opt
																.setAttribute(
																		"selected",
																		selected);
												});

							}
						}, ServiceError);

	}
	
	//RFC Y CURP
	$scope.ValidaRfc = function(rfc) {
		var rfcStr;
		rfcStr = rfc;
		if (rfc
				.match('^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?')) {

		} else {
			alert('RFC Incorrecto');
		}

	}
	$scope.ValidaCURP = function(curp) {
		var strCurp;
		strCurp = curp;
		if (curp
				.match(/^([a-z]{4})([0-9]{6})([a-z]{6})([0-9]{2})$/i)) {

		} else {
			alert('CURP Incorrecto');
		}

	}

	$scope.generaRFC = function() {

		if (($scope.user.nombre != "" && $scope.user.nombre != null)
				&& ($scope.user.apellidoPaterno != "" && $scope.user.apellidoPaterno != null)
				&& ($scope.user.apellidoMaterno != "" && $scope.user.apellidoMaterno != null)
				&& ($scope.user.fechaNacimiento != "" && $scope.user.fechaNacimiento != null)) {
			apiService
					.post(
							'/api/cuenta/getRFC',
							$scope.user,
							function(result) {
								$scope.user.cveFiscal = result.data.valor;
							}, ServiceError);

		} else
			return;
	}

	$scope.generaCURP = function() {

		if (($scope.user.nombre != "" && $scope.user.nombre != null)
				&& ($scope.user.apellidoPaterno != "" && $scope.user.apellidoPaterno != null)
				&& ($scope.user.apellidoMaterno != "" && $scope.user.apellidoMaterno != null)
				&& ($scope.user.fechaNacimiento != "" && $scope.user.fechaNacimiento != null)
				&& ($scope.user.idGenero != "" && $scope.user.idGenero != null)
				&& ($scope.user.idEntidadNacimiento != "" && $scope.user.idEntidadNacimiento != null)) {
			apiService
					.post(
							'/api/cuenta/getCURP',
							$scope.user,
							function(result) {
								$scope.user.curp = result.data.valor;
							}, ServiceError);

		} else
			return;
	}

	function ServiceSuccessComplete(result) {
		$scope.isUpdate = true;
		$scope.user = result.data;
		$scope.errorsubmit = '';
		toastr.success(
				"Se guardo satisfactoriamente el usuario",
				"Crea usuario", opts);
		$state.transitionTo('app.usuario', {
			tipoObjeto : "SEC_ROLE"
		});
		$scope.isUpdate = true;
		$rootScope.currentModal.close();

	}

	function ServiceError(response) {
		$scope.errorsubmit = response.data;
		$scope.errorsubmit = '';
		toastr.error("Sucedió un error al crear el usuario",
				"Error", opts);

	}

	$rootScope.submitRoleForm = function(item, event) {

		apiService
				.post(
						'/api/cuenta/addroluser',
						$rootScope.userrole,
						function(result) {
							$scope.user = result.data;
							var opts = {
								"closeButton" : true,
								"debug" : false,
								"positionClass" : "toast-top-right",
								"onclick" : null,
								"showDuration" : "300",
								"hideDuration" : "1000",
								"timeOut" : "5000",
								"extendedTimeOut" : "1000",
								"showEasing" : "swing",
								"hideEasing" : "linear",
								"showMethod" : "fadeIn",
								"hideMethod" : "fadeOut"
							};

							toastr
									.success(
											"Se guardo satisfactoriamente el rol.",
											"Asignacion de roles",
											opts);
							$scope.isUpdate = true;
							$rootScope.currentModal.close();
						}, ServiceError);
	};
});

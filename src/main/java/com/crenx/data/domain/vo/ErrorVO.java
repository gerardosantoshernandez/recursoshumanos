package com.crenx.data.domain.vo;

public class ErrorVO {

	String sourceMessage;
	String errMessage;
	String errorCode;
	String actions;
	String motivoRechazo;
	boolean runTimeError;
	ErrorType errorType;
	
	
	public String getSourceMessage() {
		return sourceMessage;
	}


	public void setSourceMessage(String sourceMessage) {
		this.sourceMessage = sourceMessage;
	}


	public String getErrMessage() {
		return errMessage;
	}


	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}


	public String getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}


	public String getActions() {
		return actions;
	}


	public void setActions(String actions) {
		this.actions = actions;
	}


	public boolean isRunTimeError() {
		return runTimeError;
	}


	public void setRunTimeError(boolean runTimeError) {
		this.runTimeError = runTimeError;
	}


	public ErrorType getErrorType() {
		return errorType;
	}


	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}


	public enum ErrorType {
	    System, Runtime, BusinessRule 
	}


	public String getMotivoRechazo() {
		return motivoRechazo;
	}


	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}
	
}

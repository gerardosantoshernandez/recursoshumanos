package com.crenx.data.domain.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Gerencia {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_gerencia", unique = true)
	private String idGerencia;
	

	@Column(name = "id_domicilio", length = 10)
	private Date idDomicilio;
	
	@Column(name = "fecha_inicio_penalizacion")
	private Date fecha_Inicio_Penalizacion;
	
	@Column(name = "dias_pen_cred_diario")
	private byte dias_pen_cred_Diario;
	
	@Column(name = "dias_pen_cred_semanal")
	private byte dias_pen_cred_Semanal;
	
	@Column(name = "dias_pen_cred_quincenal")
	private byte dias_pen_cred_Quincenal;
	
	@Column(name = "dias_pen_cred_mensual")
	private byte dias_pen_cred_Mensual;
	
	@Column(name = "max_dias_para_penalizar")
	private byte max_dias_para_Penalizar;
	
	@Column(name = "porcentaje_min_pago_cuota")
	private double porcentaje_Min_Pago_Cuota;
	
	@Column(name = "porcentaje_pen_credito")
	private double porcentaje_Pen_Credito;
	
	@Column(name = "manejar_devolucion_pen")
	private byte manejar_Devolucion_Pen;

	@Column(name = "crear_credito_sin_sol")
	private byte crear_Credito_Sin_Sol;
	
	@Column(name = "valor_por_visita")
	private double valor_Por_Visita;
	
	@Column(name = "cliente_varias_rutas")
	private byte cliente_Varias_Rutas;
	
	@Column(name = "id_tipo_penalizacion", length = 40)
	private String idTipoPenalizacion;

	public String getIdGerencia() {
		return idGerencia;
	}

	public void setIdGerencia(String idGerencia) {
		this.idGerencia = idGerencia;
	}

	public Date getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(Date idDomicilio) {
		this.idDomicilio = idDomicilio;
	}

	public Date getFecha_Inicio_Penalizacion() {
		return fecha_Inicio_Penalizacion;
	}

	public void setFecha_Inicio_Penalizacion(Date fecha_Inicio_Penalizacion) {
		this.fecha_Inicio_Penalizacion = fecha_Inicio_Penalizacion;
	}

	public byte getDias_pen_cred_Diario() {
		return dias_pen_cred_Diario;
	}

	public void setDias_pen_cred_Diario(byte dias_pen_cred_Diario) {
		this.dias_pen_cred_Diario = dias_pen_cred_Diario;
	}

	public byte getDias_pen_cred_Semanal() {
		return dias_pen_cred_Semanal;
	}

	public void setDias_pen_cred_Semanal(byte dias_pen_cred_Semanal) {
		this.dias_pen_cred_Semanal = dias_pen_cred_Semanal;
	}

	public byte getDias_pen_cred_Quincenal() {
		return dias_pen_cred_Quincenal;
	}

	public void setDias_pen_cred_Quincenal(byte dias_pen_cred_Quincenal) {
		this.dias_pen_cred_Quincenal = dias_pen_cred_Quincenal;
	}

	public byte getDias_pen_cred_Mensual() {
		return dias_pen_cred_Mensual;
	}

	public void setDias_pen_cred_Mensual(byte dias_pen_cred_Mensual) {
		this.dias_pen_cred_Mensual = dias_pen_cred_Mensual;
	}

	public byte getMax_dias_para_Penalizar() {
		return max_dias_para_Penalizar;
	}

	public void setMax_dias_para_Penalizar(byte max_dias_para_Penalizar) {
		this.max_dias_para_Penalizar = max_dias_para_Penalizar;
	}

	public double getPorcentaje_Min_Pago_Cuota() {
		return porcentaje_Min_Pago_Cuota;
	}

	public void setPorcentaje_Min_Pago_Cuota(double porcentaje_Min_Pago_Cuota) {
		this.porcentaje_Min_Pago_Cuota = porcentaje_Min_Pago_Cuota;
	}

	public double getPorcentaje_Pen_Credito() {
		return porcentaje_Pen_Credito;
	}

	public void setPorcentaje_Pen_Credito(double porcentaje_Pen_Credito) {
		this.porcentaje_Pen_Credito = porcentaje_Pen_Credito;
	}

	public byte getManejar_Devolucion_Pen() {
		return manejar_Devolucion_Pen;
	}

	public void setManejar_Devolucion_Pen(byte manejar_Devolucion_Pen) {
		this.manejar_Devolucion_Pen = manejar_Devolucion_Pen;
	}

	public byte getCrear_Credito_Sin_Sol() {
		return crear_Credito_Sin_Sol;
	}

	public void setCrear_Credito_Sin_Sol(byte crear_Credito_Sin_Sol) {
		this.crear_Credito_Sin_Sol = crear_Credito_Sin_Sol;
	}

	public double getValor_Por_Visita() {
		return valor_Por_Visita;
	}

	public void setValor_Por_Visita(double valor_Por_Visita) {
		this.valor_Por_Visita = valor_Por_Visita;
	}

	public byte getCliente_Varias_Rutas() {
		return cliente_Varias_Rutas;
	}

	public void setCliente_Varias_Rutas(byte cliente_Varias_Rutas) {
		this.cliente_Varias_Rutas = cliente_Varias_Rutas;
	}

	public String getIdTipoPenalizacion() {
		return idTipoPenalizacion;
	}

	public void setIdTipoPenalizacion(String idTipoPenalizacion) {
		this.idTipoPenalizacion = idTipoPenalizacion;
	}

	
}

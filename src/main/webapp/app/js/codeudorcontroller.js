'use strict';
angular
		.module('crenx.clientecontrollers')
		.controller(
				'CoDeudorFormController',
				function($scope, $rootScope, apiService, $location, $timeout,
						$http, $cookieStore, $stateParams, $modal, $state) {
					$scope.figura = $state.current.params.figura;
					$scope.cliente = $stateParams.cliente;
					$scope.clienteId = $stateParams.clienteId;
					$scope.clienteRaiz = $stateParams.clienteRaiz;
					// var idcliente = $stateParams.cliente.idCliente;
					console.log('idcliente:' + $scope.clienteId);
					console.log($scope.cliente);
					console.log($scope.clienteRaiz);
					loadCatalogo('CAT_FIG');
					loadCatalogo('CAT_TIP_DOC_ID');
					loadCatalogo('CAT_GEN');
					loadCatalogo('CAT_TIP_DOC');
					$scope.auxDocumento = {};

					// Permisos
					var permisos = $rootScope.repository.loggedUser.permissions;
					$scope.crearCodeudor = ($.inArray('OBJ_CLI_CO_CREAR',
							permisos) > -1) ? true : false;
					$scope.verCodeudor = ($.inArray('OBJ_CO_VER', permisos) > -1) ? true
							: false;

					$scope.canEdit = !$scope.crearCodeudor;

					function loadCatalogo(catType) {
						apiService.post('/api/catalogo/items', catType,
								catalogComplete, ServiceError);

					}

					function catalogComplete(result) {

						if (result.data.parent.claveInterna == 'CAT_FIG') {
							$scope.allTipoFigura = result.data;
							angular
									.forEach(
											$scope.allTipoFigura.items,
											function(value, key) {
												if ($scope.figura == value.claveInterna) {
													$scope.cliente.idTipoFigura = value.idCatalogo;
													$scope.ItemTipoFigura = [ value ];
												}
											});
						}
						if (result.data.parent.claveInterna == 'CAT_TIP_DOC_ID') {
							$scope.allTipoDocId = result.data;
						}
						if (result.data.parent.claveInterna == 'CAT_GEN') {
							$scope.allGenero = result.data;
						}
						if (result.data.parent.claveInterna == 'CAT_TIP_DOC') {
							$scope.allTipoDoc = result.data;
						}

					}
					function ServiceError(response) {
						$scope.errorsubmit = response.data;
					}
					var filterTextTimeout;

					$scope
							.$watch(
									'cliente.domicilioResidencial.codigoPostal',
									function(val) {
										if (filterTextTimeout)
											$timeout.cancel(filterTextTimeout);
										filterTextTimeout = $timeout(
												function(val) {
													if ($scope.cliente.domicilioResidencial != null
															&& $scope.cliente.domicilioResidencial.codigoPostal.length == 5)
														loadColonias($scope.cliente.domicilioResidencial.codigoPostal,"R");
												}, 250); // delay 250 ms
									});

					$scope
							.$watch(
									'cliente.domicilioLaboral.codigoPostal',
									function(val) {
										if (filterTextTimeout)
											$timeout.cancel(filterTextTimeout);
										filterTextTimeout = $timeout(
												function(val) {
													if ($scope.cliente.domicilioLaboral == undefined)
														return;
													if ($scope.cliente.domicilioLaboral.codigoPostal == undefined)
														return;
													if ($scope.cliente.domicilioLaboral.codigoPostal.length == 5)
														loadColonias(
																$scope.cliente.domicilioLaboral.codigoPostal,
																"L");
												}, 250); // delay 250 ms
									})

					function loadColonias(codigo,tipo) {
						apiService
						.get(
								'/api/sepomex/colonias/' + codigo,
								null,
								function ServiceSuccessComplete(result) {

									if (tipo == "R") {
										$scope.cliente.domicilioResidencial.municipio = result.data.municipio;
										$scope.cliente.domicilioResidencial.estado = result.data.estado;
										$scope.cliente.domicilioResidencial.pais = result.data.pais;

										$("#municipioRes").value = result.data.municipio;
										$("#estado_res").value = result.data.estado;
										$("#paisRes").value = result.data.pais;
										$("#colonia_residencial")
												.empty();
										$("#colonia_residencial")
												.append(
														new Option(
																"----Seleccione una----",
																""));
										$
												.each(
														result.data.colonias,
														function(key,
																value) {
															var selected = $scope.cliente.domicilioResidencial.colonia == value ? true
																	: false;
															var opt = new Option(
																	value,
																	value);
															$(
																	"#colonia_residencial")
																	.append(
																			opt);
															if (selected)
																opt
																		.setAttribute(
																				"selected",
																				selected);
														});

									} else {
										$scope.cliente.domicilioLaboral.municipio = result.data.municipio;
										$scope.cliente.domicilioLaboral.estado = result.data.estado;
										$scope.cliente.domicilioLaboral.pais = result.data.pais;

										$("#municipio").value = result.data.municipio;
										$("#estado_emp").value = result.data.estado;
										$("#paisLaboral").value = result.data.pais;
										$("#colonia").empty();
										$("#colonia")
												.append(
														new Option(
																"----Seleccione una----",
																""));
										$
												.each(
														result.data.colonias,
														function(key,
																value) {
															var selected = $scope.cliente.domicilioLaboral.colonia == value ? true
																	: false;
															var opt = new Option(
																	value,
																	value);
															$(
																	"#colonia")
																	.append(
																			opt);
															if (selected)
																opt
																		.setAttribute(
																				"selected",
																				selected);
														});

									}

								}, ServiceError);

			}
					$scope.copyDom = function() {
						loadColonias($scope.cliente.domicilioLaboral.codigoPostal,"R");
						$scope.cliente.domicilioResidencial = clone($scope.cliente.domicilioLaboral);

					}
					function clone(obj) {
						if (obj === null || typeof obj !== 'object') {
							return obj;
						}

						var temp = obj.constructor();
						for ( var key in obj) {
							temp[key] = clone(obj[key]);
						}

						return temp;
					}

					$scope.guardarCodeudor = function(item, event) {
						console.log($scope.cliente);
						$scope.cliente.parentId = $scope.clienteId;
						console.log($scope.cliente.fechaNacimiento);
						apiService.post('/api/cliente/savecliente',
								$scope.cliente, ServiceSuccessComplete,
								ServiceError);
					}
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "500",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					function ServiceSuccessComplete(result) {

						toastr.success(
								"Se guardó satisfactoriamente el CoDeudor.",
								"Guardar CoDeudor", opts);
						$scope.errorsubmit = '';
						$state.transitionTo('app.codeudor', {
							figura : "FIG_CODEUDOR",
							cliente : $scope.clienteRaiz
						});

					}

					function ServiceError(response) {
						$scope.errorsubmit = response.data;

						toastr.error("Ocurrio un error al guardar:"
								+ response.data.sourceMessage, "Error", opts);

					}

					$scope.getRequeridos = function(algo) {
						$scope.carRequeridos = algo.clave;
						$scope.cliente.idTipoDocumentoIdentidad = algo.idCatalogo;
					}

				});
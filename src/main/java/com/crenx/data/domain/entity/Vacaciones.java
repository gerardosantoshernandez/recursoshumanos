package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Vacaciones {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_vacaciones", unique = true)
	private String idVacaciones;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_empleado", nullable=true, referencedColumnName="id_empleado")
	private Empleado idEmpleado;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_asunto", nullable = false, referencedColumnName = "id_catalogo")
	private Catalogo asunto;
	
	@Column(name = "dias",length = 100)
	private String dias;
	
	@Column(name = "descripcion",length = 100)
	private String descripcion;
	
	@Column(name = "fecha_solicitada")
	private Date fechaSolicitada;

	public String getIdVacaciones() {
		return idVacaciones;
	}

	public void setIdVacaciones(String idVacaciones) {
		this.idVacaciones = idVacaciones;
	}

	public Empleado getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Empleado idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getDias() {
		return dias;
	}

	public void setDias(String dias) {
		this.dias = dias;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaSolicitada() {
		return fechaSolicitada;
	}

	public void setFechaSolicitada(Date fechaSolicitada) {
		this.fechaSolicitada = fechaSolicitada;
	}

	public Catalogo getAsunto() {
		return asunto;
	}

	public void setAsunto(Catalogo asunto) {
		this.asunto = asunto;
	}
}

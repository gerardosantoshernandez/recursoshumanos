package com.crenx.data.domain.vo;

import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Organizacion;

public class OrgVOToOrgModelMap extends PropertyMap<OrgVO,Organizacion>{
	   @Override
	    protected void configure() {
			map().setIdOrg(source.getId());
			map().setClave(source.getKey());
			map().setNombre(source.getText());
	    }		 		 
}
package com.crenx.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.util.StringUtils;

public class FileUtils {

	public static final String SEPARATOR_UNIX = "";

	/**
	 * 
	 * Antepone o pospone una clave única (UUID) al parametro fileName
	 * 
	 * @param fileName
	 *            nombre del archivo
	 * @param pox
	 *            si es true indica que la clave unica se concatena al final, de
	 *            lo contrario se concatena al inicio
	 * @return
	 */
	public static String stampWithUUID(final String fileName) {
		final String uuid = UUID.randomUUID().toString();
		final StringBuffer result = new StringBuffer();
		if (StringUtils.hasText(fileName)) {
			result.append(fileName);
		}
		return result.insert(0, uuid).toString();
	}
	
	/**
	 * Coloca al final  de fileName la marca de tiempo (yyyyMMddHHmmssSSS)
	 * @param fileName
	 * @param time
	 * @return
	 */
	public static String stampWithTime(final String fileName, final Calendar time){
		final StringBuffer result = new StringBuffer();
		final DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		if(StringUtils.hasText(fileName)){			
			result.append(fileName);
		}
		result.append( df.format((time == null)?Calendar.getInstance().getTime():time));
		return result.toString();
	}

	/**
	 * Escribe el contenido de un stream a un archivo 
	 * @param input
	 * @param file
	 * @throws IOException
	 */
	public static void writeInputStreamToFile(final InputStream input, File file) throws IOException {
		final FileOutputStream output = new FileOutputStream(file);
		final byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) !=-1) {
			output.write(buffer, 0, bytesRead);
		}
		input.close();
		output.flush();
		output.close();		
	}

	/**
	 * Convierte un archivo en cadena
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static String fileToString(final File file)throws IOException{
		StringBuffer buffer = new StringBuffer();
		final FileInputStream fis = new FileInputStream(file);
		final BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		String line = null;
		while ((line = br.readLine()) != null){
			buffer.append(line);
		}
		br.close();
		return buffer.toString();
	}
	/**
	 * Concatena un Path.
	 * 
	 * @param basePath
	 * @param complementPath
	 * @param separator
	 *            . Caracter separador del path. Esta funcion trabajo con dos
	 *            separadores uno estilo Windows y otro stilo Unix. Si el
	 *            separador es distinto a estos el separador usado es el del
	 *            sistema
	 * @return Retorna el path concatenado. Si el path resultante es invalido el
	 *         método regresa el basePath
	 */
	public static String concatPath(final String basePath, final String complementPath, final char separator) {
		String path = FilenameUtils.concat(basePath, complementPath);
		if (path == null) {
			path = basePath;
		} else {
			switch (separator) {
			case IOUtils.DIR_SEPARATOR_UNIX:
				path = FilenameUtils.separatorsToUnix(path);
				break;
			case IOUtils.DIR_SEPARATOR_WINDOWS:
				path = FilenameUtils.separatorsToWindows(path);
				break;
			default:
				path = FilenameUtils.separatorsToSystem(path);
			}
		}
		return path;
	}

	public static String listFiles(final File file, final int depth) {
		StringBuffer result = new StringBuffer();
		if (file != null && file.exists()) {
			if (depth > 0) {
				result.append("\n");
				result.append(getDepth(depth, ' '));
			}
			if (file.isDirectory()) {
				result.append("+").append(file.getName()).append(File.separator);
				File[] children = file.listFiles();
				if (children != null && children.length > 0) {
					for (File child : children) {
						result.append(listFiles(child, depth + 1));
					}
				}
			} else {
				result.append("-").append(file.getName());
			}
		}
		return result.toString();
	}

	private static String getDepth(final int depth, final char depthChar) {
		StringBuffer result = new StringBuffer();
		if (depth > 0) {
			int index = depth;
			while (index > 0) {
				result.append(depthChar);
				index--;
			}
		}

		return result.toString();
	}
}

package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;


/*
@Table(name="person",  uniqueConstraints={
   @UniqueConstraint(columnNames={"code", "uid"}),
   @UniqueConstraint(columnNames={"anotherField", "uid"})
})
FIXME: Agregar esta relaciòn única de una combinación de campos
 */
@Entity
@Table(name="objseg_objseg", 
uniqueConstraints= @UniqueConstraint(columnNames={"id_parent", "id_child"}))
public class ObjetoSeguridadRelaciones   {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_objsegrel", unique = true)
	private String idObjSegRel;
	
	@Column(name = "creacion")
	private Date creacion;
	
	@ManyToOne
	@JoinColumn(name="id_parent", nullable=false, referencedColumnName="id_objetoSeguridad")
	private ObjetoSeguridad parent;
	
	@ManyToOne
	@JoinColumn(name="id_child", nullable=false, referencedColumnName="id_objetoSeguridad")
	private ObjetoSeguridad child;

	public String getIdObjSegRel() {
		return idObjSegRel;
	}

	public void setIdObjSegRel(String idObjSegRel) {
		this.idObjSegRel = idObjSegRel;
	}

	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}

	public ObjetoSeguridad getParent() {
		return parent;
	}

	public void setParent(ObjetoSeguridad parent) {
		this.parent = parent;
	}

	public ObjetoSeguridad getChild() {
		return child;
	}

	public void setChild(ObjetoSeguridad child) {
		this.child = child;
	}

}

'use strict';

var app = angular.module('xenon-app', [ 'ngCookies',

'ui.router', 'ui.bootstrap',

'oc.lazyLoad',

'xenon.controllers', 'xenon.directives', 'xenon.factory', 'xenon.services',

// Added in v1.3
'FBAngular','crenx.securitycontrollers'
,'crenx.catalogocontrollers'
,'crenx.productocontrollers'
,'crenx.orgcontrollers'
,'crenx.clientecontrollers'
,'crenx.documentoscontrollers'
,'crenx.repcontrollers'
,'crenx.cajatestcontrollers'
,'crenx.indicadorescontrollers'
,'crenx.processcontrollers'
,'crenx.credemplcontrollers'
// Extra
,'crenx.perfilcontrollers'
,'crenx.avisocontrollers'
,'crenx.reembolsoscontrollers'
,'crenx.vacacionescontrollers'

,'blockUI'
]);

app.run(function($http,$rootScope,$cookieStore,documentService) {
	// Page Loading Overlay
	public_vars.$pageLoadingOverlay = jQuery('.page-loading-overlay');

	 $rootScope.repository = $cookieStore.get('repository') || {};
	 console.log($rootScope.repository);
    if ($rootScope.repository.loggedUser) {
    	console.log('logged');
        $http.defaults.headers.common['X-AUTH-TOKEN'] = $rootScope.repository.loggedUser.authdata;
    }
    


			// Se crean las funciones globales
    		/**
			 * Esta función descarga un documento que se obtiene, usando
			 * HTTP/GET, desde url_location.
			 * 
			 * @param doctoName.
			 *            Nombre del documento con extensión
			 * @param url_location.
			 *            URL que se invoca para descargar el documento
			 */
			$rootScope.downloadDocument = function(url_location, doctoName) {				
				documentService.download(url_location,
						doctoName);
			};

			/**
			 * Despliega en una ventana modal el documento que se obtiene,
			 * usando HTTP/GET, desde url_location.
			 * 
			 * @param url_location:
			 *            URL con la que se obtiene el documento a desplagar
			 * @modal_id. Id de la seccion de ng-template donde se desea
			 *            desplegar el documento. Por default se trata de usar
			 *            el id : modal-display
			 */
			$rootScope.displayDocument = function(url_location, modal_id) {
				documentService.preview(url_location, (modal_id) ? modal_id
						: 'modal-display');
			};
			
			/**
			 * Inicializa todas las variables globales relacionadas a los
			 * Documentos
			 */
			$rootScope.cleanEnvDocuments= function() {
				$rootScope.fileDownloaded = null;
                $rootScope.typeFileDownloaded = null;
                $rootScope.currentDocument = null;
			};
    
	jQuery(window).load(function() {
		public_vars.$pageLoadingOverlay.addClass('loaded');
	})
});

app.config(function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider,blockUIConfig,
		ASSETS) {

	// Se configura BlockUI
	  blockUIConfig.message = 'Cargando...';
	  blockUIConfig.autoBlock = true;
	  blockUIConfig.delay = 100;
	
	$urlRouterProvider.otherwise('/app/cuenta-usuario');

	$stateProvider.
	// Main Layout Structure
	state('app', {
		abstract : true,
		url : '/app',
		templateUrl : appHelper.templatePath('layout/app-body'),
		controller : function($rootScope) {
			$rootScope.isLoginPage = false;
			$rootScope.isLightLoginPage = false;
			$rootScope.isLockscreenPage = false;
			$rootScope.isMainPage = true;
		},
		resolve : {
			resources : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize,
						ASSETS.extra.toastr, ]);
			},
		}
	}).

	state(
			'app.indicadores',
			{
				url : '/dashboards-indicadores?mobile_interface',
				templateUrl : appHelper.templatePath('dashboards/indicadores'),
				controller: 'CarteraController',
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize,
								ASSETS.extra.toastr, ]);
					},
					dxCharts : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxCharts, ]);
					},
					select2 : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
					},
					datepicker : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
					},            			
					
				}
			}).
			
			state(
					'app.process',
					{
						url : '/process-procesoform',
						templateUrl : appHelper.templatePath('process/procesoform'),
						controller: 'TaskController',
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize,
										ASSETS.extra.toastr, ]);
							},
							dxCharts : function($ocLazyLoad) {
								return $ocLazyLoad.load([ ASSETS.charts.dxCharts, ]);
							},
							select2 : function($ocLazyLoad) {
								return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
							},
							
						}
					}).
			
			
			
	// Clientes
	state('app.clientes', {
		url : '/clientes-clientes',
		templateUrl : appHelper.templatePath('clientes/clientes'),
        controller: 'ClienteControllers',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
		}

	}).
	
	state('app.perfiledit', {
		url : '/perfil-perfiledit',
		params: {
            userId: null,
        },
		templateUrl : appHelper.templatePath('perfil/perfiledit'),
        controller: 'PerfilControllers',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
		}

	}).
	state('app.perfilusuario', {
		url : '/perfil-perfilusuario',
		templateUrl : appHelper.templatePath('perfil/perfilusuario'),
		params: {
        	tipoObjeto: "SEC_ROLE",
        },
        controller: 'PerfilUserController',
        resolve: {
	        deps: function ($ocLazyLoad) {
	            return $ocLazyLoad.load([
					ASSETS.tables.rwd,
	            ]);
	        },
	        jqui: function ($ocLazyLoad) {
	            return $ocLazyLoad.load({
	                files: ASSETS.core.jQueryUI
	            });
	        },
	        select2: function ($ocLazyLoad) {
	            return $ocLazyLoad.load([
					ASSETS.forms.select2,
	            ]);
	        },
	        jQueryValidate: function ($ocLazyLoad) {
	            return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
	            ]);
	        }
	    }

	})
	// Aviso
	.state('app.avisos', {
					url : '/avisos-aviso',
					templateUrl : appHelper.templatePath('avisos/aviso'),
			        params: {
			        	task: null
			        },
			        controller: 'AvisoController',
					resolve : {
						deps : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
						},
			            jqui: function ($ocLazyLoad) {
			                return $ocLazyLoad.load({
			                    files: ASSETS.core.jQueryUI
			                });
			            },
			            select2: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.select2,
			                ]);
			            },
			            resources: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
								ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
			                ]);
			            },
			            dropzone : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.forms.dropzone]);
						}
						
					}

				}).state('app.avisos-registro', {
					url : '/avisos-registro',
					templateUrl : appHelper.templatePath('avisos/registro'),
					controller : 'FormAvisoController',
			        params: {avisoItem: null, listaAvisos:null},
			        resolve: {
			        	bootstrap : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.core.bootstrap, ]);
						},
						bootstrapWysihtml5 : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.forms.bootstrapWysihtml5, ]);
						},
			            deps: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.tables.rwd,
			                ]);
			            },
			            jqui: function ($ocLazyLoad) {
			                return $ocLazyLoad.load({
			                    files: ASSETS.core.jQueryUI
			                });
			            },
			            select2: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.select2,
			                ]);
			            },
			            resources: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
								ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
			                ]);
			            },
			            selectboxit: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.selectboxit,
			                ]);
			            },
						datepicker : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
						},            
						inputmask : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.forms.inputmask, ]);
						},
			            multiSelect: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.multiSelect,
			                ]);
			            },  
					}
				}).
				// Reembolso
				state('app.reembolsos', {
					url : '/solicitudes-reembolsos',
					templateUrl : appHelper.templatePath('solicitudes/reembolsos'),
			        params: {
			        	task: null
			        },
			        controller: 'ReembolsosController',
					resolve : {
						deps : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
						},
			            jqui: function ($ocLazyLoad) {
			                return $ocLazyLoad.load({
			                    files: ASSETS.core.jQueryUI
			                });
			            },
			            select2: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.select2,
			                ]);
			            },
			            resources: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
								ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
			                ]);
			            },
						
					}

			        //Vacaciones
				}).state('app.vacaciones', {
					url : '/solicitudes-vacaciones',
					controller : 'VacacionesController',
					templateUrl : appHelper.templatePath('solicitudes/vacaciones'),
					resolve: {
				       	 deps: function ($ocLazyLoad) {
				                return $ocLazyLoad.load([
									ASSETS.tables.rwd,
				                ]);
				            },
				           resources: function ($ocLazyLoad) {
				               return $ocLazyLoad.load([
									ASSETS.forms.jQueryValidate,
				               ]);
				           },
				           select2: function ($ocLazyLoad) {
				               return $ocLazyLoad.load([
				                   ASSETS.forms.select2,
				               ]);
				           },
				           multiSelect: function ($ocLazyLoad) {
				               return $ocLazyLoad.load([
				                   ASSETS.forms.multiSelect,
				               ]);
				           },
				       }
				}).state('app.vacaciones-registro', {
					url : '/solicitudes-registrovacaciones',
					controller : 'FormVacacionesController',
					templateUrl : appHelper.templatePath('solicitudes/registrovacaciones'),
					params: {vacacionesItem: null, listaVacaciones:null},
					resolve: {
			        	bootstrap : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.core.bootstrap, ]);
						},
						bootstrapWysihtml5 : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.forms.bootstrapWysihtml5, ]);
						},
			            deps: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.tables.rwd,
			                ]);
			            },
			            jqui: function ($ocLazyLoad) {
			                return $ocLazyLoad.load({
			                    files: ASSETS.core.jQueryUI
			                });
			            },
			            select2: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.select2,
			                ]);
			            },
			            resources: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
								ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
			                ]);
			            },
			            selectboxit: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.selectboxit,
			                ]);
			            },
						datepicker : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
						},            
						inputmask : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.forms.inputmask, ]);
						},
			            multiSelect: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.multiSelect,
			                ]);
			            },  
					}
				})
	
	
	
	

	// Clientes
	.state('app.clientes-creditos', {
		url : '/clientes-creditos',
		templateUrl : appHelper.templatePath('clientes/creditos'),
        params: {
        	figura: "FIG_CLIENTE",
        },
        data: {
        	figura: "FIG_CLIENTE",
        },
		controller: 'CreditosController',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			}
		}

	})
	
	
	.state('app.clientes-traspaso', {
		url : '/clientes-traspaso',
		templateUrl : appHelper.templatePath('clientes/traspaso'),
        controller: 'ClienteTraspasoController',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			}, 
		}

	}).state('app.clientes-unificacion', {
		url : '/clientes-unificacion',
		templateUrl : appHelper.templatePath('clientes/unificacion'),
        controller: 'ClienteUnificacionController',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			}, 
		}

	})
	.state('app.documentos', {
		url : '/clientes-adjuntardocumentos',
		templateUrl : appHelper.templatePath('clientes/adjuntardocumentos'),
        controller: 'DocumentosController',
        params: {
        	figura: "FIG_CLIENTE",
        	entidad: null
        },
        data: {
        	figura: "FIG_CLIENTE",
        },
       resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			dropzone : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.dropzone, ]);
			},
		}

	}).

	state('app.clienteform', {
		url : '/clientes-registro',
		templateUrl : appHelper.templatePath('clientes/registro'),
        controller: 'ClienteFormController',
        params: {
        	clienteId: null,
        	cliente: null,
        	rutas : [],
        	figura: "FIG_CLIENTE",
        },
        data: {
        	clienteId: null,
        	cliente: null,
        	rutas : [],
        	figura: "FIG_CLIENTE",
        },
		resolve : {
			bootstrap : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.core.bootstrap, ]);
			},
			bootstrapWysihtml5 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.bootstrapWysihtml5, ]);
			},
            deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.tables.rwd,
                ]);
            },
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },
            select2: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.select2,
                ]);
            },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
                ]);
            },
            selectboxit: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.selectboxit,
                ]);
            },
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
			inputmask : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.inputmask, ]);
			},
            multiSelect: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.multiSelect,
                ]);
            },
			dropzone : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.dropzone, ]);
			},    
		}
	}).

	state('app.codeudor', {
		url : '/clientes-codeudor',
		templateUrl : appHelper.templatePath('clientes/codeudor'),
        params: {
        	cliente: null,
        	figura: "FIG_CODEUDOR",
        },
        controller: 'CoDeudorController',		
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
		}

	}).
	state('app.codeudorform', {
		url : '/clientes-codeudorform',
		templateUrl : appHelper.templatePath('clientes/codeudorform'),
        controller: 'CoDeudorFormController',
        params: {
        	clienteId: null,
        	cliente: null,
        	rutas : [],
        	figura: "FIG_CODEUDOR",
        	clienteRaiz: null,
        },
		resolve : {
			bootstrap : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.core.bootstrap, ]);
			},
			bootstrapWysihtml5 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.bootstrapWysihtml5, ]);
			},
            deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.tables.rwd,
                ]);
            },
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },
            select2: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.select2,
                ]);
            },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
                ]);
            },
            selectboxit: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.selectboxit,
                ]);
            },
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
			inputmask : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.inputmask, ]);
			},
            multiSelect: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.multiSelect,
                ]);
            },
			dropzone : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.dropzone, ]);
			},            			
		}
	}).

	// Creditos
	 state('app.clientescreditos', {
		url : '/clientes-creditoscliente',
		templateUrl : appHelper.templatePath('clientes/creditoscliente'),
        controller: 'CreditosClienteController',
        params: {
        	cliente: null,
        },
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			resorces: function($ocLazyLoad){
				return $ocLazyLoad.load([ASSETS.extra.archivos,]);
			},
		}

	}).

	 state('app.clientescreditoform', {
			url : '/clientes-creditosform',
			templateUrl : appHelper.templatePath('clientes/creditosform'),
	        params: {
	        	cliente: null,
	        	credito: null,
	        	tipo: null
	        },
	        controller: 'CreditoFormController',
			resolve : {
				deps : function($ocLazyLoad) {
					return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
				},
	            jqui: function ($ocLazyLoad) {
	                return $ocLazyLoad.load({
	                    files: ASSETS.core.jQueryUI
	                });
	            },
	            select2: function ($ocLazyLoad) {
	                return $ocLazyLoad.load([
	                    ASSETS.forms.select2,
	                ]);
	            },
	            resources: function ($ocLazyLoad) {
	                return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
	                ]);
	            },
				datepicker : function($ocLazyLoad) {
					return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
				},            
	            
				
			}

		}).
		 state('app.clientescreditorest', {
				url : '/clientes-creditosformrest',
				templateUrl : appHelper.templatePath('clientes/creditosformrest'),
		        params: {
		        	cliente: null,
		        	credito: null,
		        	tipo: 'REST'
		        },
		        controller: 'CreditoFormController',
				resolve : {
					deps : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
					},
		            jqui: function ($ocLazyLoad) {
		                return $ocLazyLoad.load({
		                    files: ASSETS.core.jQueryUI
		                });
		            },
		            select2: function ($ocLazyLoad) {
		                return $ocLazyLoad.load([
		                    ASSETS.forms.select2,
		                ]);
		            },
		            resources: function ($ocLazyLoad) {
		                return $ocLazyLoad.load([
							ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
		                ]);
		            },
					datepicker : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
					},            
					
				}

			}).
			 state('app.clientescreditonego', {
					url : '/clientes-creditosformnego',
					templateUrl : appHelper.templatePath('clientes/creditosformnego'),
			        params: {
			        	cliente: null,
			        	credito: null,
			        	tipo: 'NEGO'
			        },
			        controller: 'CreditoFormController',
					resolve : {
						deps : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
						},
			            jqui: function ($ocLazyLoad) {
			                return $ocLazyLoad.load({
			                    files: ASSETS.core.jQueryUI
			                });
			            },
			            select2: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.select2,
			                ]);
			            },
			            resources: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
								ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
			                ]);
			            },
						datepicker : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
						},            
						
					}

				}).
				 state('app.creditopagoform', {
						url : '/clientes-pagosform',
						templateUrl : appHelper.templatePath('clientes/pagosform'),
				        params: {
				        	credito: null,
				        	cliente:null,
				        },
				        controller: 'PagoFormController',
						resolve : {
							deps : function($ocLazyLoad) {
								return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
							},
				            jqui: function ($ocLazyLoad) {
				                return $ocLazyLoad.load({
				                    files: ASSETS.core.jQueryUI
				                });
				            },
				            select2: function ($ocLazyLoad) {
				                return $ocLazyLoad.load([
				                    ASSETS.forms.select2,
				                ]);
				            },
				            resources: function ($ocLazyLoad) {
				                return $ocLazyLoad.load([
									ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
				                ]);
				            },
							
						}

					}).

		 state('app.editpagoform', {
				url : '/clientes-editpagosform',
				templateUrl : appHelper.templatePath('clientes/editpagosform'),
		        params: {
		        	idPago: null,
		        },
		        controller: 'EditPagoFormController',
				resolve : {
					deps : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
					},
		            jqui: function ($ocLazyLoad) {
		                return $ocLazyLoad.load({
		                    files: ASSETS.core.jQueryUI
		                });
		            },
		            select2: function ($ocLazyLoad) {
		                return $ocLazyLoad.load([
		                    ASSETS.forms.select2,
		                ]);
		            },
		            resources: function ($ocLazyLoad) {
		                return $ocLazyLoad.load([
							ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
		                ]);
		            },
					
				}

			})
			.state('app.tareas', {
				url : '/activiti-tareas',
				controller : 'TaskLoadController',
				templateUrl : appHelper.templatePath('activiti/tareas'),
				resolve : {
					deps : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
					},
					datepicker : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
					},
			        resources: function ($ocLazyLoad) {
			            return $ocLazyLoad.load([
							ASSETS.extra.toastr,
			            ]);
			        },

				}
			})			
			 .state('app.approveCredit', {
					url : '/activiti-creditosform',
					templateUrl : appHelper.templatePath('activiti/creditosform'),
			        params: {
			        	task: null
			        },
			        controller: 'ApproveCreditoController',
					resolve : {
						deps : function($ocLazyLoad) {
							return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
						},
			            jqui: function ($ocLazyLoad) {
			                return $ocLazyLoad.load({
			                    files: ASSETS.core.jQueryUI
			                });
			            },
			            select2: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
			                    ASSETS.forms.select2,
			                ]);
			            },
			            resources: function ($ocLazyLoad) {
			                return $ocLazyLoad.load([
								ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
			                ]);
			            },
						
					}

				}).
				 state('app.approveExpense', {
						url : '/activiti-gastosform',
						templateUrl : appHelper.templatePath('activiti/gastosform'),
				        params: {
				        	task: null
				        },
				        controller: 'ApproveGastoController',
						resolve : {
							deps : function($ocLazyLoad) {
								return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
							},
				            jqui: function ($ocLazyLoad) {
				                return $ocLazyLoad.load({
				                    files: ASSETS.core.jQueryUI
				                });
				            },
				            select2: function ($ocLazyLoad) {
				                return $ocLazyLoad.load([
				                    ASSETS.forms.select2,
				                ]);
				            },
				            resources: function ($ocLazyLoad) {
				                return $ocLazyLoad.load([
									ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
				                ]);
				            },
							
						}

					}).

					 state('app.approveTransfer', {
							url : '/activiti-transferform',
							templateUrl : appHelper.templatePath('activiti/transferform'),
					        params: {
					        	task: null
					        },
					        controller: 'ApproveTransferController',
							resolve : {
								deps : function($ocLazyLoad) {
									return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
								},
					            jqui: function ($ocLazyLoad) {
					                return $ocLazyLoad.load({
					                    files: ASSETS.core.jQueryUI
					                });
					            },
					            select2: function ($ocLazyLoad) {
					                return $ocLazyLoad.load([
					                    ASSETS.forms.select2,
					                ]);
					            },
					            resources: function ($ocLazyLoad) {
					                return $ocLazyLoad.load([
										ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
					                ]);
					            }								
							}
						}).
					

	// Productos
	state('app.productos-lista', {
		url : '/productos-productos',
		templateUrl : appHelper.templatePath('productos/productos'),
        controller: 'ProductoController',
        resolve: {
       	 deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.tables.rwd,
                ]);
            },
           resources: function ($ocLazyLoad) {
               return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
               ]);
           },
           select2: function ($ocLazyLoad) {
               return $ocLazyLoad.load([
                   ASSETS.forms.select2,
               ]);
           },
           multiSelect: function ($ocLazyLoad) {
               return $ocLazyLoad.load([
                   ASSETS.forms.multiSelect,
               ]);
           },
       }

	}).

	state('app.productos-registro', {
		url : '/productos-registro',
		templateUrl : appHelper.templatePath('productos/registro'),
		controller : 'FormProductoController',
        params: {productoItem: null, listaFamilias:null},
        resolve: {
        	bootstrap : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.core.bootstrap, ]);
			},
			bootstrapWysihtml5 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.bootstrapWysihtml5, ]);
			},
            deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.tables.rwd,
                ]);
            },
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },
            select2: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.select2,
                ]);
            },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
                ]);
            },
            selectboxit: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.selectboxit,
                ]);
            },
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
			inputmask : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.inputmask, ]);
			},
            multiSelect: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.multiSelect,
                ]);
            },  
		}
	}).
	state('app.nomina-datosNomina', {
		url : '/nomina-datosNomina',
		templateUrl : appHelper.templatePath('nomina/datosNomina'),
        controller: 'DatosEmpleadoController',
        resolve: {
       	 deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.tables.rwd,
                ]);
            },
           resources: function ($ocLazyLoad) {
               return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
               ]);
           },
           select2: function ($ocLazyLoad) {
               return $ocLazyLoad.load([
                   ASSETS.forms.select2,
               ]);
           },
           multiSelect: function ($ocLazyLoad) {
               return $ocLazyLoad.load([
                   ASSETS.forms.multiSelect,
               ]);
           },
       }

	}).

	state('app.nomina-creditoEmpleado', {
		url : '/nomina-creditoEmpleado',
		templateUrl : appHelper.templatePath('nomina/creditoEmpleado'),
        controller: 'CreditoEmpleadoController',
        resolve: {
       	 deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.tables.rwd,
                ]);
            },
           resources: function ($ocLazyLoad) {
               return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
               ]);
           },
           select2: function ($ocLazyLoad) {
               return $ocLazyLoad.load([
                   ASSETS.forms.select2,
               ]);
           },
           multiSelect: function ($ocLazyLoad) {
               return $ocLazyLoad.load([
                   ASSETS.forms.multiSelect,
               ]);
           },
       }

	}).
	state('app.nomina-reporteNomina', {
	    url: '/nomina-reporteNomina',
	    templateUrl: appHelper.templatePath('reportes/nomina'),
        controller: 'BaseNominaController',
        resolve: {
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate, ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.datatables, 
				                          ASSETS.tables.rwd, 
				                          ASSETS.tables.yadcf,]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
        }
	}).
	state('app.creditoEmpleado-form', {
		url : '/nomina-creditoEmpleadoForm',
		templateUrl : appHelper.templatePath('nomina/creditoEmpleadoForm'),
        params: {credItem: null, listaTipos:null, listaEstatus:null },
        controller: 'CreditoEmpleadoFormController',
        resolve: {
        	bootstrap : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.core.bootstrap, ]);
			},
			bootstrapWysihtml5 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.bootstrapWysihtml5, ]);
			},
            deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.tables.rwd,
                ]);
            },
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },
            select2: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.select2,
                ]);
            },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
                ]);
            },
            selectboxit: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.selectboxit,
                ]);
            },
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
			inputmask : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.inputmask, ]);
			},
            multiSelect: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.multiSelect,
                ]);
            },  
		}
	}).
	
	
	// Catálogos
	state('app.catalogo', {
	    url: '/catalogo-lista',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
	    resolve: {
	        deps: function ($ocLazyLoad) {
	            return $ocLazyLoad.load([
					ASSETS.tables.rwd,
	            ]);
	        }
	    }
	}).

	// Catálogos Tipo Doc
	state('app.catalogo-tipoDocId', {
	    url: '/catalogo-tipoDocId',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_TIP_DOC_ID', catName:'Tipos Doc Identidad'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).
	// Catálogos Tipo Doc
	state('app.catalogo-tipoDoc', {
	    url: '/catalogo-tipoDoc',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_TIP_DOC', catName:'Tipos de documento'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).

	state('app.catalogo-tipoCargo', {
	    url: '/catalogo-tipoCargo',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_CARGO', catName:'Tipos de Cargo'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).

	state('app.catalogo-tipoOcupacion', {
	    url: '/catalogo-tipoOcupacion',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_OCUP', catName:'Ocupaciones'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).

	state('app.catalogo-mediosPago', {
	    url: '/catalogo-mediosPago',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_MEDIOS_PAGO', catName:'Medios de pago'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).

	state('app.catalogo-tiposGasto', {
	    url: '/catalogo-tiposGasto',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_TIPO_GASTO', catName:'Tipos de Gasto'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).
		
	state('app.catalogo-tiposPago', {
	    url: '/catalogo-tiposPago',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_TIPO_PAGO', catName:'Tipos de Pago'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).
	
	state('app.catalogo-tiposActivo', {
	    url: '/catalogo-tiposActivo',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_TIPO_ACTIVO', catName:'Tipos de Activo'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).
	
	state('app.catalogo-tipoProducto', {
	    url: '/catalogo-tipoProducto',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_FAM_PROD', catName:'Familias de producto'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).
	// tipoCredEmpl
	state('app.catalogo-tipoCredEmpl', {
	    url: '/catalogo-tipoCredEmpl',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_TIPO_CRED_EMPL', catName:'Tipos Crédito Empleado'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).

	state('app.catalogo-tipoPerGasto', {
	    url: '/catalogo-tipoPerGasto',
	    templateUrl: appHelper.templatePath('catalogo/lista'),
        controller: 'CatController',
        data: {tipoCat:'CAT_PERIODO_GASTO', catName:'Tipos Periodos para gastos'},
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).
	
	state('app.reglas-reglasGastos', {
	    url: '/reglas-reglasGastos',
	    templateUrl: appHelper.templatePath('reglas/reglasGastos'),
        controller: 'ReglasGastosController',
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).

	state('app.reglas-reglasCredito', {
	    url: '/reglas-reglasCredito',
	    templateUrl: appHelper.templatePath('reglas/reglasCredito'),
        controller: 'ReglasCreditoController',
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
	}).
	
	
	/*
	 * state('app.catalogo-reglasGastosRol', { url: '/catalogo-reglasGastosRol',
	 * templateUrl: appHelper.templatePath('catalogo/reglasGastosRol'),
	 * controller: 'ReglasGastosRolController', resolve: { deps: function
	 * ($ocLazyLoad) { return $ocLazyLoad.load([ ASSETS.tables.rwd, ]); },
	 * resources: function ($ocLazyLoad) { return $ocLazyLoad.load([
	 * ASSETS.forms.jQueryValidate, ]); }, } }).
	 */
	
	state('app.catform', {
        url: '/catalogo-catform',
        templateUrl: appHelper.templatePath('catalogo/catform'),
        params: {
        	parentId:null,
        	parentName:null,
        	catalogoItem: null,
        },
        controller: 'FormCatalogoController',
        resolve: {
            deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.tables.rwd,
                ]);
            },
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },
            select2: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.select2,
                ]);
            },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                ASSETS.forms.jQueryValidate,
                ]);
            }
        }
    }).
    
    state('app.reglasGastosForm', {
        url: '/reglas-reglasGastosForm',
        templateUrl: appHelper.templatePath('reglas/reglasGastosForm'),
        params: {
        	reglaItem:null,
        },
        controller: 'ReglasGastosFormController',
        resolve: {
            deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.tables.rwd,
                ]);
            },
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },
            select2: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.select2,
                ]);
            },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                ASSETS.forms.jQueryValidate,
                ]);
            }
        }
    }).

    state('app.reglasCreditoForm', {
        url: '/reglas-reglasCreditoForm',
        templateUrl: appHelper.templatePath('reglas/reglasCreditoForm'),
        params: {
        	reglaItem:null,
        },
        controller: 'ReglasCreditoFormController',
        resolve: {
            deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.tables.rwd,
                ]);
            },
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },
            select2: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    ASSETS.forms.select2,
                ]);
            },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                ASSETS.forms.jQueryValidate,
                ]);
            }
        }
    }).
   
    /*
	 * state('app.reglasGastosRolForm', { url: '/catalogo-reglasGastosRolForm',
	 * templateUrl: appHelper.templatePath('catalogo/reglasGastosRolForm'),
	 * params: { reglaItem:null, }, controller: 'ReglasGastosRolFormController',
	 * resolve: { deps: function ($ocLazyLoad) { return $ocLazyLoad.load([
	 * ASSETS.tables.rwd, ]); }, jqui: function ($ocLazyLoad) { return
	 * $ocLazyLoad.load({ files: ASSETS.core.jQueryUI }); }, select2: function
	 * ($ocLazyLoad) { return $ocLazyLoad.load([ ASSETS.forms.select2, ]); },
	 * resources: function ($ocLazyLoad) { return $ocLazyLoad.load([
	 * ASSETS.forms.jQueryValidate, ]); } } }).
	 */
	state('app.caja-indicadores', {
	    url: '/caja-caja',
	    templateUrl: appHelper.templatePath('caja/caja'),
        controller: 'CajaTestController',
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
        }
	}).
    
    
	state('app.reportes-cobranza', {
	    url: '/reportes-cobranza',
	    templateUrl: appHelper.templatePath('reportes/cobranza'),
        controller: 'RepController',
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
        }
	}).

	state('app.reportes-caja', {
	    url: '/reportes-caja',
	    templateUrl: appHelper.templatePath('reportes/caja'),
        controller: 'CajaController',
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            			
        }
	}).
	
	state('app.reportes-resumen', {
	    url: '/reportes-resumen',
	    templateUrl: appHelper.templatePath('reportes/resumen'),
        controller: 'ResumenController',
        resolve: {
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.datatables, 
				                          ASSETS.tables.rwd, 
				                          ASSETS.tables.yadcf,]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
        }
	}).

	state('app.reportes-cartera', {
	    url: '/reportes-cartera',
	    templateUrl: appHelper.templatePath('reportes/cartera'),
        controller: 'EstadoCarteraController',
        resolve: {
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.datatables, 
				                          ASSETS.tables.rwd, 
				                          ASSETS.tables.yadcf,]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
        }
	}).
	
	state('app.reportes-visitas', {
	    url: '/reportes-visitas',
	    templateUrl: appHelper.templatePath('reportes/visitas'),
        controller: 'VisitasController',
        resolve: {
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.datatables, 
				                          ASSETS.tables.rwd, 
				                          ASSETS.tables.yadcf,]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
        }
	}).
	state('app.reportes-concentradoCaja', {
	    url: '/reportes-concentradoCaja',
	    templateUrl: appHelper.templatePath('reportes/concentradoCaja'),
        controller: 'ConcentradoCajaController',
        resolve: {
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.datatables, 
				                          ASSETS.tables.rwd, 
				                          ASSETS.tables.yadcf,]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
        }
	}).
	state('app.reportes-traspasos', {
	    url: '/reportes-traspasos',
	    templateUrl: appHelper.templatePath('reportes/traspasos'),
        controller: 'TraspasosRepController',
        resolve: {
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.datatables, 
				                          ASSETS.tables.rwd, 
				                          ASSETS.tables.yadcf,]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
        }
	}).
	state('app.reportes-recompra', {
	    url: '/reportes-recompra',
	    templateUrl: appHelper.templatePath('reportes/recompras'),
        controller: 'ClientesRecomprasRepController',
        resolve: {
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,ASSETS.extra.archivos,
                ]);
            },
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			},
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.datatables, 
				                          ASSETS.tables.rwd, 
				                          ASSETS.tables.yadcf,]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
        }
	}).
    state('login-light',
	{
		url : '/login-light',
		templateUrl : appHelper.templatePath('login-light'),
		controller : 'LoginController',
		resolve : {
			resources : function($ocLazyLoad) {
				return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate, ]);
			},
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },			
		}
	}).

	// Usuarios
	state('app.usuario', {
	    url: '/cuenta-usuario',
	    templateUrl: appHelper.templatePath('cuenta/usuario'),
        params: {
        	tipoObjeto: "SEC_ROLE",
        },
	    controller: 'UserController',
	    resolve: {
	        deps: function ($ocLazyLoad) {
	            return $ocLazyLoad.load([
					ASSETS.tables.rwd,
	            ]);
	        },
	        jqui: function ($ocLazyLoad) {
	            return $ocLazyLoad.load({
	                files: ASSETS.core.jQueryUI
	            });
	        },
	        select2: function ($ocLazyLoad) {
	            return $ocLazyLoad.load([
					ASSETS.forms.select2,
	            ]);
	        },
	        jQueryValidate: function ($ocLazyLoad) {
	            return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
	            ]);
	        }
	    }
	}).
    state('app.usuarioform', {
        url: '/cuenta-form-user',
        params: {
            userId: null,
        },
        templateUrl: appHelper.templatePath('cuenta/usuarioform'),
        controller: 'FormUserController',
        resolve: {
            deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.tables.rwd,
                ]);
            },
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },
            select2: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.select2,
                ]);
            },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
                ]);
            },
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
        }
    }).
    state('app.usuarioformEdit', {
        url: '/cuenta-form-userEdit',
        params: {
            userId: null,
        },
        templateUrl: appHelper.templatePath('cuenta/usuarioformedit'),
        controller: 'FormUserController',
        resolve: {
            deps: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.tables.rwd,
                ]);
            },
            jqui: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ASSETS.core.jQueryUI
                });
            },
            select2: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.select2,
                ]);
            },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                ASSETS.forms.jQueryValidate, ASSETS.extra.toastr,
                ]);
            },
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},            
        }
    }).

    
    state('app.rol', {
        url: '/cuenta-rol',
        templateUrl: appHelper.templatePath('cuenta/rol'),
        params: {
       	 tipoObjeto: "SEC_ROLE",
       	 titulo:"Roles",
        },  
        data: {tipoObjeto: "SEC_ROLE",
          	 titulo:"Roles"},
        controller: 'ObjSecController',
        resolve: {
        	 deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.tables.rwd,
                 ]);
             },
            resources: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
					ASSETS.forms.jQueryValidate,
                ]);
            },
        }
    }).
     state('app.rolform', {
         url: '/cuenta-rolform',
         templateUrl: appHelper.templatePath('cuenta/rolform'),
         params: {
             secObjId: null,
             tipoObjeto: null,
          	 titulo:null
         },
         controller: 'FormRolController',
         resolve: {
             deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
                     ASSETS.tables.rwd,
                 ]);
             },
             jqui: function ($ocLazyLoad) {
                 return $ocLazyLoad.load({
                     files: ASSETS.core.jQueryUI
                 });
             },
             select2: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
                     ASSETS.forms.select2,
                 ]);
             },
             resources: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
                 ASSETS.forms.jQueryValidate,
                 ASSETS.extra.toastr,
                 ]);
             }
         }
     }).
     
     // Status de permisos
     state('app.permisoform', {
         url: '/cuenta-permisoform',
         templateUrl: appHelper.templatePath('cuenta/permisoform'),
         params: {
             secObjId: null,
             tipoObjeto: null,
          	 titulo:null
         },
         controller: 'FormRolController',
         resolve: {
             deps: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
                     ASSETS.tables.rwd,
                 ]);
             },
             jqui: function ($ocLazyLoad) {
                 return $ocLazyLoad.load({
                     files: ASSETS.core.jQueryUI
                 });
             },
             select2: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
                     ASSETS.forms.select2,
                 ]);
             },
             resources: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
                 ASSETS.forms.jQueryValidate,
                 ASSETS.extra.toastr,
                 ]);
             }
         }
     }).
     
     state('app.perfil', {
         url: '/cuenta-perfil',
         templateUrl: appHelper.templatePath('cuenta/perfil'),
         params: {
        	 tipoObjeto: "SEC_PROFILE",
         },
         data: {tipoObjeto: "SEC_OBJECT",
          	 titulo:"Roles"},
         controller: 'ObjSecController',
         resolve: {
         	 deps: function ($ocLazyLoad) {
                  return $ocLazyLoad.load([
  					ASSETS.tables.rwd,
                  ]);
              },
             resources: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.forms.jQueryValidate,
                 ]);
             },
         }
     }).
     
     state('app.permiso', {
         url: '/cuenta-permiso',
         templateUrl: appHelper.templatePath('cuenta/permiso'),
         params: {
        	 tipoObjeto: "SEC_OBJECT",
        	 titulo:"Permisos",
         },
         data: {tipoObjeto: "SEC_OBJECT",
          	 titulo:"Permisos"},
         controller: 'ObjSecController',
         resolve: {
         	 deps: function ($ocLazyLoad) {
                  return $ocLazyLoad.load([
  					ASSETS.tables.rwd,
                  ]);
              },
             resources: function ($ocLazyLoad) {
                 return $ocLazyLoad.load([
 					ASSETS.forms.jQueryValidate,
                 ]);
             },
         }
     }).
     
	state('app.users2', {
		url : '/users-roles_usuarios',
		templateUrl : appHelper.templatePath('users/roles_usuarios'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}

	}).

	state('app.users3', {
		url : '/users-permisos_usuarios',
		templateUrl : appHelper.templatePath('users/permisos_usuarios'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state('app.users4', {
		url : '/users-reg_roles_usuarios',
		templateUrl : appHelper.templatePath('users/reg_roles_usuarios'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}

	}).

	state('app.users5', {
		url : '/users-reg_permisos_usuarios',
		templateUrl : appHelper.templatePath('users/reg_permisos_usuarios'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}

	}).

	state('app.questionary', {
		url : '/questionary-questionary',
		templateUrl : appHelper.templatePath('questionary/questionary'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).state('app.questionarynew', {
		url : '/questionary-new',
		templateUrl : appHelper.templatePath('questionary/questionary-new'),
		controller : 'UIModalsCtrl',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).state('app.quality', {
		url : '/quality-apro-rech-data-p',
		templateUrl : appHelper.templatePath('quality/apro-rech-data-p'),
		controller : 'UIModalsCtrl',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			jqui : function($ocLazyLoad) {
				return $ocLazyLoad.load({
					files : ASSETS.core.jQueryUI
				});
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
		}
	}).

	state('app.quality2', {
		url : '/quality-detalle',
		templateUrl : appHelper.templatePath('quality/detalle'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			jqui : function($ocLazyLoad) {
				return $ocLazyLoad.load({
					files : ASSETS.core.jQueryUI
				});
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
		}
	}).

	state(
			'app.calidad2',
			{
				url : '/calidad2-cuestionario_rechazados',
				templateUrl : appHelper
						.templatePath('calidad2/cuestionario_rechazados'),
				resolve : {
					deps : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
					},
					jqui : function($ocLazyLoad) {
						return $ocLazyLoad.load({
							files : ASSETS.core.jQueryUI
						});
					},
					select2 : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
					}
				}
			}).

	state('app.calidad21', {
		url : '/calidad2-detalle',
		templateUrl : appHelper.templatePath('calidad2/detalle'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			jqui : function($ocLazyLoad) {
				return $ocLazyLoad.load({
					files : ASSETS.core.jQueryUI
				});
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
		}
	}).

	state('app.ejec_dia', {
		url : '/ejec_dia-asignacion_tienda',
		templateUrl : appHelper.templatePath('ejec_dia/asignacion_tienda'),
		controller : 'UIModalsCtrl',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			jqui : function($ocLazyLoad) {
				return $ocLazyLoad.load({
					files : ASSETS.core.jQueryUI
				});
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
		}
	}).

	state('app.ejec_dia2', {
		url : '/ejec_dia-carga_result_lay',
		templateUrl : appHelper.templatePath('ejec_dia/carga_result_lay'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			jqui : function($ocLazyLoad) {
				return $ocLazyLoad.load({
					files : ASSETS.core.jQueryUI
				});
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
		}
	}).

	state('app.ejec_dia3', {
		url : '/ejec_dia-cop_edit_resultados',
		templateUrl : appHelper.templatePath('ejec_dia/cop_edit_resultados'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			jqui : function($ocLazyLoad) {
				return $ocLazyLoad.load({
					files : ASSETS.core.jQueryUI
				});
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
		}
	}).

	state('app.ejec_dia4', {
		url : '/ejec_dia-detalle',
		templateUrl : appHelper.templatePath('ejec_dia/detalle'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			jqui : function($ocLazyLoad) {
				return $ocLazyLoad.load({
					files : ASSETS.core.jQueryUI
				});
			},
			select2 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
			}
		}
	}).

	state('app.data', {
		url : '/data-registro_concesionarios',
		templateUrl : appHelper.templatePath('data/registro_concesionarios'),
		controller : 'UIModalsCtrl',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state(
			'app.data1',
			{
				url : '/data-asignacion_tienda_c',
				templateUrl : appHelper
						.templatePath('data/asignacion_tienda_c'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize,
								ASSETS.extra.toastr, ]);
					},
					dxCharts : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxCharts, ]);
					},
				}
			}).

	state(
			'app.data2',
			{
				url : '/data-reg_proveedores',
				templateUrl : appHelper.templatePath('data/reg_proveedores'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize,
								ASSETS.extra.toastr, ]);
					},
					dxCharts : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxCharts, ]);
					},
				}
			}).

	state(
			'app.data3',
			{
				url : '/data-reg_crear_concesionarios',
				templateUrl : appHelper
						.templatePath('data/reg_crear_concesionarios'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize,
								ASSETS.extra.toastr, ]);
					},
					dxCharts : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxCharts, ]);
					},
				}
			}).

	state('app.data4', {
		url : '/data-tiendas_incluidas',
		templateUrl : appHelper.templatePath('data/tiendas_incluidas'),
		controller : 'UIModalsCtrl',
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).state('app.regions', {
		url : '/data-regions',
		controller : 'OrgMainController',
		templateUrl : appHelper.templatePath('data/regions'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			},
			datepicker : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
			},
	        resources: function ($ocLazyLoad) {
	            return $ocLazyLoad.load([
					ASSETS.extra.toastr,
	            ]);
	        },
            dropzone : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.dropzone]);
			}
		}
	}).state('app.sepomex', {
		url : '/data-sepomex',
		templateUrl : appHelper.templatePath('data/sepomex'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).state('app.params', {
		url : '/data-params',
		templateUrl : appHelper.templatePath('data/params'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state('app.regions1', {
		url : '/data-reg_regions',
		templateUrl : appHelper.templatePath('data/reg_regions'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state('app.typequestionary', {
		url : '/data-typequestionary',
		templateUrl : appHelper.templatePath('data/typequestionary'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state('app.typequestionary1', {
		url : '/data-reg_typequestionary',
		templateUrl : appHelper.templatePath('data/reg_typequestionary'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state('app.typequestion', {
		url : '/data-typequestion',
		templateUrl : appHelper.templatePath('data/typequestion'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state('app.typequestion1', {
		url : '/data-reg_typequestion',
		templateUrl : appHelper.templatePath('data/reg_typequestion'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state('app.waves', {
		url : '/waves-waves',
		templateUrl : appHelper.templatePath('waves/waves'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).state('app.newwaves', {
		url : '/waves_new-waves',
		templateUrl : appHelper.templatePath('waves/new-waves'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state(
			'app.waves1',
			{
				url : '/waves-registro_ntienda',
				templateUrl : appHelper.templatePath('waves/registro_ntienda'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize,
								ASSETS.extra.toastr, ]);
					},
					dxCharts : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxCharts, ]);
					},
				}
			}).

	state('app.secureusers', {
		url : '/secureusers-usuarios',
		templateUrl : appHelper.templatePath('secureusers/usuarios'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	state('app.secureusers2', {
		url : '/secureusers-asignacion_roles',
		templateUrl : appHelper.templatePath('secureusers/asignacion_roles'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).

	// Dashboards
	state(
			'app.dashboards',
			{
				url : '/dashboards-variant-1',
				templateUrl : appHelper.templatePath('dashboards/variant-1'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize,
								ASSETS.extra.toastr, ]);
					},
					dxCharts : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxCharts, ]);
					},
				}
			}).state('app.dashboard-variant-2', {
		url : '/dashboard-variant-2',
		templateUrl : appHelper.templatePath('dashboards/variant-2'),
		resolve : {
			resources : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize, ]);
			},
			dxCharts : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.charts.dxCharts, ]);
			},
		}
	}).state(
			'app.dashboard-variant-3',
			{
				url : '/dashboard-variant-3',
				templateUrl : appHelper.templatePath('dashboards/variant-3'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxGlobalize,
								ASSETS.maps.vectorMaps, ]);
					},
					dxCharts : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.charts.dxCharts, ]);
					},
				}
			}).state(
			'app.dashboard-variant-4',
			{
				url : '/dashboard-variant-4',
				templateUrl : appHelper.templatePath('dashboards/variant-4'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.icons.meteocons,
								ASSETS.maps.vectorMaps, ]);
					}
				}
			}).

	// Update Highlights
	state('app.update-highlights', {
		url : '/update-highlights',
		templateUrl : appHelper.templatePath('update-highlights'),
	}).

	// Layouts
	state('app.layout-and-skins', {
		url : '/layout-and-skins',
		templateUrl : appHelper.templatePath('layout-and-skins'),
	}).

	// UI Elements
	state('app.ui-panels', {
		url : '/ui-panels',
		templateUrl : appHelper.templatePath('ui/panels'),
	}).state('app.ui-buttons', {
		url : '/ui-buttons',
		templateUrl : appHelper.templatePath('ui/buttons')
	}).state('app.ui-tabs-accordions', {
		url : '/ui-tabs-accordions',
		templateUrl : appHelper.templatePath('ui/tabs-accordions')
	}).state('app.ui-modals', {
		url : '/ui-modals',
		templateUrl : appHelper.templatePath('ui/modals'),
		controller : 'UIModalsCtrl'
	}).state('app.ui-breadcrumbs', {
		url : '/ui-breadcrumbs',
		templateUrl : appHelper.templatePath('ui/breadcrumbs')
	}).state('app.ui-blockquotes', {
		url : '/ui-blockquotes',
		templateUrl : appHelper.templatePath('ui/blockquotes')
	}).state('app.ui-progress-bars', {
		url : '/ui-progress-bars',
		templateUrl : appHelper.templatePath('ui/progress-bars')
	}).state('app.ui-navbars', {
		url : '/ui-navbars',
		templateUrl : appHelper.templatePath('ui/navbars')
	}).state('app.ui-alerts', {
		url : '/ui-alerts',
		templateUrl : appHelper.templatePath('ui/alerts')
	}).state('app.ui-pagination', {
		url : '/ui-pagination',
		templateUrl : appHelper.templatePath('ui/pagination')
	}).state('app.ui-typography', {
		url : '/ui-typography',
		templateUrl : appHelper.templatePath('ui/typography')
	}).state('app.ui-other-elements', {
		url : '/ui-other-elements',
		templateUrl : appHelper.templatePath('ui/other-elements')
	}).

	// Widgets
	state(
			'app.widgets',
			{
				url : '/widgets',
				templateUrl : appHelper.templatePath('widgets'),
				resolve : {
					deps : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.maps.vectorMaps,
								ASSETS.icons.meteocons ]);
					}
				}
			}).

	// Mailbox
	state('app.mailbox-inbox', {
		url : '/mailbox-inbox',
		templateUrl : appHelper.templatePath('mailbox/inbox'),
	}).state('app.mailbox-compose', {
		url : '/mailbox-compose',
		templateUrl : appHelper.templatePath('mailbox/compose'),
		resolve : {
			bootstrap : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.core.bootstrap, ]);
			},
			bootstrapWysihtml5 : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.bootstrapWysihtml5, ]);
			},
		}
	}).state('app.mailbox-message', {
		url : '/mailbox-message',
		templateUrl : appHelper.templatePath('mailbox/message'),
	}).

	// Tables
	state('app.tables-basic', {
		url : '/tables-basic',
		templateUrl : appHelper.templatePath('tables/basic'),
	}).state('app.tables-responsive', {
		url : '/tables-responsive',
		templateUrl : appHelper.templatePath('tables/responsive'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.rwd, ]);
			}
		}
	}).state('app.tables-datatables', {
		url : '/tables-datatables',
		templateUrl : appHelper.templatePath('tables/datatables'),
		resolve : {
			deps : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.tables.datatables, ]);
			},
		}
	}).

	// Forms
	state('app.forms-native', {
		url : '/forms-native',
		templateUrl : appHelper.templatePath('forms/native-elements'),
	}).state(
			'app.forms-advanced',
			{
				url : '/forms-advanced',
				templateUrl : appHelper.templatePath('forms/advanced-plugins'),
				resolve : {
					jqui : function($ocLazyLoad) {
						return $ocLazyLoad.load({
							files : ASSETS.core.jQueryUI
						});
					},
					select2 : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.select2, ]);
					},
					selectboxit : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.selectboxit, ]);
					},
					tagsinput : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.tagsinput, ]);
					},
					multiSelect : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.multiSelect, ]);
					},
					typeahead : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.typeahead, ]);
					},
					datepicker : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.datepicker, ]);
					},
					timepicker : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.timepicker, ]);
					},
					daterangepicker : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.core.moment,
								ASSETS.forms.daterangepicker, ]);
					},
					colorpicker : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.forms.colorpicker, ]);
					},
				}
			}).state(
			'app.forms-wizard',
			{
				url : '/forms-wizard',
				templateUrl : appHelper.templatePath('forms/form-wizard'),
				resolve : {
					fwDependencies : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.core.bootstrap,
								ASSETS.core.jQueryUI,
								ASSETS.forms.jQueryValidate,
								ASSETS.forms.inputmask,
								ASSETS.forms.multiSelect,
								ASSETS.forms.datepicker,
								ASSETS.forms.selectboxit,
								ASSETS.forms.formWizard, ]);
					},
					formWizard : function($ocLazyLoad) {
						return $ocLazyLoad.load([]);
					},
				},
			}).state('app.forms-validation', {
		url : '/forms-validation',
		templateUrl : appHelper.templatePath('forms/form-validation'),
		resolve : {
			jQueryValidate : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.jQueryValidate, ]);
			},
		},
	}).state('app.forms-input-masks', {
		url : '/forms-input-masks',
		templateUrl : appHelper.templatePath('forms/input-masks'),
		resolve : {
			inputmask : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.inputmask, ]);
			},
		},
	}).state('app.forms-file-upload', {
		url : '/forms-file-upload',
		templateUrl : appHelper.templatePath('forms/file-upload'),
		resolve : {
			dropzone : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.dropzone, ]);
			},
		}
	}).state(
			'app.forms-wysiwyg',
			{
				url : '/forms-wysiwyg',
				templateUrl : appHelper.templatePath('forms/wysiwyg'),
				resolve : {
					bootstrap : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.core.bootstrap, ]);
					},
					bootstrapWysihtml5 : function($ocLazyLoad) {
						return $ocLazyLoad.load([
								ASSETS.forms.bootstrapWysihtml5, ]);
					},
					uikit : function($ocLazyLoad) {
						return $ocLazyLoad
								.load([ ASSETS.uikit.base,
										ASSETS.uikit.codemirror,
										ASSETS.uikit.marked, ]);
					},
					uikitHtmlEditor : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.uikit.htmleditor, ]);
					},
				}
			}).state('app.forms-sliders', {
		url : '/forms-sliders',
		templateUrl : appHelper.templatePath('forms/sliders'),
		resolve : {
			sliders : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.core.jQueryUI, ]);
			},
		},
	}).state('app.forms-icheck', {
		url : '/forms-icheck',
		templateUrl : appHelper.templatePath('forms/icheck'),
		resolve : {
			iCheck : function($ocLazyLoad) {
				return $ocLazyLoad.load([ ASSETS.forms.icheck, ]);
			},
		}
	}).

	// Extra
	state(
			'app.extra-icons-font-awesome',
			{
				url : '/extra-icons-font-awesome',
				templateUrl : appHelper
						.templatePath('extra/icons-font-awesome'),
				resolve : {
					fontAwesome : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.core.jQueryUI,
								ASSETS.extra.tocify, ]);
					},
				}
			}).state(
			'app.extra-icons-linecons',
			{
				url : '/extra-icons-linecons',
				templateUrl : appHelper.templatePath('extra/icons-linecons'),
				resolve : {
					linecons : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.core.jQueryUI,
								ASSETS.extra.tocify, ]);
					},
				}
			}).state(
			'app.extra-icons-elusive',
			{
				url : '/extra-icons-elusive',
				templateUrl : appHelper.templatePath('extra/icons-elusive'),
				resolve : {
					elusive : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.core.jQueryUI,
								ASSETS.extra.tocify, ASSETS.icons.elusive, ]);
					},
				}
			})
			.state(
					'app.extra-icons-meteocons',
					{
						url : '/extra-icons-meteocons',
						templateUrl : appHelper
								.templatePath('extra/icons-meteocons'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([ ASSETS.core.jQueryUI,
										ASSETS.extra.tocify,
										ASSETS.icons.meteocons, ]);
							},
						}
					}).state(
					'app.extra-profile',
					{
						url : '/extra-profile',
						templateUrl : appHelper.templatePath('extra/profile'),
						resolve : {
							profile : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.core.googleMapsLoader,
										ASSETS.icons.elusive, ]);
							},
						}
					}).state(
					'app.extra-timeline',
					{
						url : '/extra-timeline',
						templateUrl : appHelper.templatePath('extra/timeline'),
						resolve : {
							timeline : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.core.googleMapsLoader, ]);
							},
						}
					}).state(
					'app.extra-timeline-centered',
					{
						url : '/extra-timeline-centered',
						templateUrl : appHelper
								.templatePath('extra/timeline-centered'),
						resolve : {
							elusive : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.core.googleMapsLoader, ]);
							},
						}
					}).state(
					'app.extra-calendar',
					{
						url : '/extra-calendar',
						templateUrl : appHelper.templatePath('extra/calendar'),
						resolve : {
							fullCalendar : function($ocLazyLoad) {
								return $ocLazyLoad.load([ ASSETS.core.jQueryUI,
										ASSETS.core.moment,
										ASSETS.extra.fullCalendar, ]);
							},
						}
					}).state('app.extra-gallery', {
				url : '/extra-gallery',
				templateUrl : appHelper.templatePath('extra/gallery'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.core.jQueryUI, ]);
					},
				}
			}).state('app.extra-notes', {
				url : '/extra-notes',
				templateUrl : appHelper.templatePath('extra/notes'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.xenonLib.notes, ]);
					},
				}
			}).state('app.extra-image-crop', {
				url : '/extra-image-crop',
				templateUrl : appHelper.templatePath('extra/image-cropper'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.extra.cropper, ]);
					},
				}
			}).state('app.extra-portlets', {
				url : '/extra-portlets',
				templateUrl : appHelper.templatePath('extra/portlets'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.core.jQueryUI, ]);
					},
				}
			}).state('app.extra-search', {
				url : '/extra-search',
				templateUrl : appHelper.templatePath('extra/search')
			}).state('app.extra-invoice', {
				url : '/extra-invoice',
				templateUrl : appHelper.templatePath('extra/invoice')
			}).state('app.extra-page-404', {
				url : '/extra-page-404',
				templateUrl : appHelper.templatePath('extra/page-404')
			}).state(
					'app.extra-tocify',
					{
						url : '/extra-tocify',
						templateUrl : appHelper.templatePath('extra/tocify'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([ ASSETS.core.jQueryUI,
										ASSETS.extra.tocify, ]);
							},
						}
					}).state('app.extra-loading-progress', {
				url : '/extra-loading-progress',
				templateUrl : appHelper.templatePath('extra/loading-progress')
			}).state('app.extra-notifications', {
				url : '/extra-notifications',
				templateUrl : appHelper.templatePath('extra/notifications'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.extra.toastr, ]);
					},
				}
			}).state(
					'app.extra-nestable-lists',
					{
						url : '/extra-nestable-lists',
						templateUrl : appHelper
								.templatePath('extra/nestable-lists'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([ ASSETS.uikit.base,
										ASSETS.uikit.nestable, ]);
							},
						}
					}).state('app.extra-scrollable', {
				url : '/extra-scrollable',
				templateUrl : appHelper.templatePath('extra/scrollable')
			}).state('app.extra-blank-page', {
				url : '/extra-blank-page',
				templateUrl : appHelper.templatePath('extra/blank-page')
			}).state(
					'app.extra-maps-google',
					{
						url : '/extra-maps-google',
						templateUrl : appHelper
								.templatePath('extra/maps-google'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.core.googleMapsLoader, ]);
							},
						}
					}).state(
					'app.extra-maps-advanced',
					{
						url : '/extra-maps-advanced',
						templateUrl : appHelper
								.templatePath('extra/maps-advanced'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.core.googleMapsLoader, ]);
							},
						}
					}).state('app.extra-maps-vector', {
				url : '/extra-maps-vector',
				templateUrl : appHelper.templatePath('extra/maps-vector'),
				resolve : {
					resources : function($ocLazyLoad) {
						return $ocLazyLoad.load([ ASSETS.maps.vectorMaps, ]);
					},
				}
			}).

			// Members
			state('app.extra-members-list', {
				url : '/extra-members-list',
				templateUrl : appHelper.templatePath('extra/members-list')
			}).state(
					'app.extra-members-add',
					{
						url : '/extra-members-add',
						templateUrl : appHelper
								.templatePath('extra/members-add'),
						resolve : {
							datepicker : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.forms.datepicker,
										ASSETS.forms.multiSelect,
										ASSETS.forms.select2, ]);
							},
						// sssss
						}
					}).

			// Charts
			state(
					'app.charts-variants',
					{
						url : '/charts-variants',
						templateUrl : appHelper.templatePath('charts/bars'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxGlobalize, ]);
							},
							dxCharts : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxCharts, ]);
							},
						}
					}).state(
					'app.charts-range-selector',
					{
						url : '/charts-range-selector',
						templateUrl : appHelper.templatePath('charts/range'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxGlobalize, ]);
							},
							dxCharts : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxCharts, ]);
							},
						}
					}).state(
					'app.charts-sparklines',
					{
						url : '/charts-sparklines',
						templateUrl : appHelper
								.templatePath('charts/sparklines'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxGlobalize, ]);
							},
							dxCharts : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxCharts, ]);
							},
						}
					}).state(
					'app.charts-gauges',
					{
						url : '/charts-gauges',
						templateUrl : appHelper.templatePath('charts/gauges'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxGlobalize, ]);
							},
							dxCharts : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxCharts, ]);
							},
						}
					}).state(
					'app.charts-bar-gauges',
					{
						url : '/charts-bar-gauges',
						templateUrl : appHelper
								.templatePath('charts/bar-gauges'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxGlobalize, ]);
							},
							dxCharts : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxCharts, ]);
							},
						}
					}).state(
					'app.charts-linear-gauges',
					{
						url : '/charts-linear-gauges',
						templateUrl : appHelper
								.templatePath('charts/gauges-linear'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxGlobalize, ]);
							},
							dxCharts : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxCharts, ]);
							},
						}
					}).state(
					'app.charts-map-charts',
					{
						url : '/charts-map-charts',
						templateUrl : appHelper.templatePath('charts/map'),
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxGlobalize, ]);
							},
							dxCharts : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.charts.dxCharts,
										ASSETS.charts.dxVMWorld, ]);
							},
						}
					}).

			// Logins and Lockscreen
			state(
					'login',
					{
						url : '/login',
						templateUrl : appHelper.templatePath('login'),
						controller : 'LoginCtrl',
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.forms.jQueryValidate,
										ASSETS.extra.toastr, ]);
							},
						}
					}).state(
					'lockscreen',
					{
						url : '/lockscreen',
						templateUrl : appHelper.templatePath('lockscreen'),
						controller : 'LockscreenCtrl',
						resolve : {
							resources : function($ocLazyLoad) {
								return $ocLazyLoad.load([
										ASSETS.forms.jQueryValidate,
										ASSETS.extra.toastr, ]);
							},
						}
					});
});

app
		.constant(
				'ASSETS',
				{
					'core' : {
						'bootstrap' : appHelper
								.assetPath('js/bootstrap.min.js'), // Some
																	// plugins
																	// which do
																	// not
																	// support
																	// angular
																	// needs
																	// this

						'jQueryUI' : [
								appHelper
										.assetPath('js/jquery-ui/jquery-ui.min.js'),
								appHelper
										.assetPath('js/jquery-ui/jquery-ui.structure.min.css'), ],

						'moment' : appHelper.assetPath('js/moment.min.js'),

						'googleMapsLoader' : appHelper
								.assetPath('app/js/angular-google-maps/load-google-maps.js')
					},

					'charts' : {

						'dxGlobalize' : appHelper
								.assetPath('js/devexpress-web-14.1/js/globalize.min.js'),
						'dxCharts' : appHelper
								.assetPath('js/devexpress-web-14.1/js/dx.chartjs.js'),
						'dxVMWorld' : appHelper
								.assetPath('js/devexpress-web-14.1/js/vectormap-data/world.js'),
					},

					'xenonLib' : {
						notes : appHelper.assetPath('js/xenon-notes.js'),
					},

					'maps' : {

						'vectorMaps' : [
								appHelper
										.assetPath('js/jvectormap/jquery-jvectormap-1.2.2.min.js'),
								appHelper
										.assetPath('js/jvectormap/regions/jquery-jvectormap-world-mill-en.js'),
								appHelper
										.assetPath('js/jvectormap/regions/jquery-jvectormap-it-mill-en.js'), ],
					},

					'icons' : {
						'meteocons' : appHelper
								.assetPath('css/fonts/meteocons/css/meteocons.css'),
						'elusive' : appHelper
								.assetPath('css/fonts/elusive/css/elusive.css'),
					},

					'tables' : {
						'rwd' : appHelper
								.assetPath('js/rwd-table/js/rwd-table.min.js'),

						'datatables' : [
								appHelper
										.assetPath('js/datatables/dataTables.bootstrap.css'),
								appHelper
										.assetPath('js/datatables/datatables-angular.js'),
								appHelper
										.assetPath('js/datatables/dataTables.bootstrap.js'),
								appHelper
										.assetPath('js/datatables/tabletools/dataTables.tableTools.min.js'),
								appHelper
										.assetPath('js/datatables/tabletools/dataTables.tableTools.css'),
										],
										
						'yadcf' : [
										appHelper
												.assetPath('js/datatables/yadcf/jquery.dataTables.yadcf.css'),
										appHelper
												.assetPath('js/datatables/yadcf/jquery.dataTables.yadcf.js'), 
										appHelper
												.assetPath('js/datatables/css/jquery.dataTables.min.css'),
										appHelper
												.assetPath('js/datatables/css/buttons.dataTables.min.css'),												
												],
					},

					'forms' : {

						'select2' : [
								appHelper.assetPath('js/select2/select2.css'),
								appHelper
										.assetPath('js/select2/select2-bootstrap.css'),

								appHelper
										.assetPath('js/select2/select2.min.js'), ],

						'daterangepicker' : [
								appHelper
										.assetPath('js/daterangepicker/daterangepicker-bs3.css'),
								appHelper
										.assetPath('js/daterangepicker/daterangepicker.js'), ],

						'colorpicker' : appHelper
								.assetPath('js/colorpicker/bootstrap-colorpicker.min.js'),

						'selectboxit' : appHelper
								.assetPath('js/selectboxit/jquery.selectBoxIt.js'),

						'tagsinput' : appHelper
								.assetPath('js/tagsinput/bootstrap-tagsinput.min.js'),

						'datepicker' : appHelper
								.assetPath('js/datepicker/bootstrap-datepicker.js'),

						'timepicker' : appHelper
								.assetPath('js/timepicker/bootstrap-timepicker.min.js'),

						'inputmask' : appHelper
								.assetPath('js/inputmask/jquery.inputmask.bundle.js'),

						'formWizard' : appHelper
								.assetPath('js/formwizard/jquery.bootstrap.wizard.min.js'),

						'jQueryValidate' : appHelper
								.assetPath('js/jquery-validate/jquery.validate.min.js'),

						'dropzone' : [
								appHelper
										.assetPath('js/dropzone/css/dropzone.css'),
								appHelper
										.assetPath('js/dropzone/dropzone.min.js'), ],

						'typeahead' : [
								appHelper.assetPath('js/typeahead.bundle.js'),
								appHelper.assetPath('js/handlebars.min.js'), ],

						'multiSelect' : [
								appHelper
										.assetPath('js/multiselect/css/multi-select.css'),
								appHelper
										.assetPath('js/multiselect/js/jquery.multi-select.js'), ],

						'icheck' : [
								appHelper.assetPath('js/icheck/skins/all.css'),
								appHelper.assetPath('js/icheck/icheck.min.js'), ],

						'bootstrapWysihtml5' : [
								appHelper
										.assetPath('js/wysihtml5/src/bootstrap-wysihtml5.css'),
								appHelper
										.assetPath('js/wysihtml5/wysihtml5-angular.js') ],
					},

					'uikit' : {
						'base' : [
								appHelper.assetPath('js/uikit/uikit.css'),
								appHelper
										.assetPath('js/uikit/css/addons/uikit.almost-flat.addons.min.css'),
								appHelper.assetPath('js/uikit/js/uikit.min.js'), ],

						'codemirror' : [
								appHelper
										.assetPath('js/uikit/vendor/codemirror/codemirror.js'),
								appHelper
										.assetPath('js/uikit/vendor/codemirror/codemirror.css'), ],

						'marked' : appHelper
								.assetPath('js/uikit/vendor/marked.js'),
						'htmleditor' : appHelper
								.assetPath('js/uikit/js/addons/htmleditor.min.js'),
						'nestable' : appHelper
								.assetPath('js/uikit/js/addons/nestable.min.js'),
					},

					'extra' : {
						'tocify' : appHelper
								.assetPath('js/tocify/jquery.tocify.min.js'),

						'toastr' : appHelper
								.assetPath('js/toastr/toastr.min.js'),

						'fullCalendar' : [
								appHelper
										.assetPath('js/fullcalendar/fullcalendar.min.css'),
								appHelper
										.assetPath('js/fullcalendar/fullcalendar.min.js'), ],

						'cropper' : [
								appHelper
										.assetPath('js/cropper/cropper.min.js'),
								appHelper
										.assetPath('js/cropper/cropper.min.css'), ],
						'archivos' :[
								appHelper
								.assetPath('js/archivos/FileSaver.js'),
								appHelper
								.assetPath('js/archivos/Blob.js')     
						        ]
						             
					}
				});

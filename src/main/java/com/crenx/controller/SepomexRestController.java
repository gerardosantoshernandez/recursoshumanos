package com.crenx.controller;
import java.lang.reflect.Type;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Sepomex;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.SepomexRepository;
import com.crenx.data.domain.vo.CatalogoVO;
import com.crenx.data.domain.vo.SepomexVO;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@RestController
@RequestMapping("/api/sepomex")
public class SepomexRestController {

	@Autowired
	private SepomexRepository sepomexRepository;
	
	@RequestMapping(value = "colonias/{codigo}", method = RequestMethod.GET)
	ResponseEntity<?> colonias(Principal principal, @PathVariable String codigo) {
		Collection<Sepomex> returnList = Lists.newArrayList(sepomexRepository.findByDCodigo(codigo));
		SepomexVO vo =  new SepomexVO();
		vo.setPais("México");
		for (Sepomex oneItem : returnList)
		{
			vo.setEstado(oneItem.getdEstado());
			vo.setMunicipio(oneItem.getdMnpio());
			vo.getColonias().add(oneItem.getDasenta());
		}
		return new ResponseEntity<>(vo, HttpStatus.ACCEPTED);
	}
	
}

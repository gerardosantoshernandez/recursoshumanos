package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.Documento;
import com.crenx.data.domain.vo.DocumentoVO;

public interface DocumentoRepository extends CrudRepository<Documento, String> {
public List<Documento> findByPropiedadesDocsNombreAndPropiedadesDocsValor(String Nombre, String Valor);
public Documento findByIdDocumento(String IdDocumento);


	/**
	 * Método de búsqueda que regresa todos los Documentos en donde sus
	 * propiedades coincidan con algun nombre y algun valor
	 * 
	 * @param nombres
	 * @param valores
	 * @return
	 */
@Query("select new com.crenx.data.domain.vo.DocumentoVO(docto.idDocumento,"
		+ "docto.nombre,"
		+ "docto.descripcion,"
		+ "docto.tipo,"
		+ "docto.referencia,"
		+ "docto.fileName,"
		+ "docto.contentType,"
		+ "docto.idAutor,"
		+ "docto.fechaCreacion,"
		+ "docto.version,"
		+ "docto.idParent"
		+ ", props2.valor, props1.valor) "
		+ "from Documento docto "
		+ "inner join docto.propiedadesDocs as props1 "
		+ "left join docto.propiedadesDocs as props2 with props2.nombre = 'TipoDocumento' "
		+ "where props1.nombre in (:names) "
		+ "and props1.valor in (:values) "
		+ "group by docto.idDocumento, props2.valor, props1.valor "
		+ "order by docto.fechaCreacion")	
	public List<DocumentoVO> findByNamesAndValuesProperties(@Param("names") List<String> nombres,
			@Param("values") List<String> valores);
}
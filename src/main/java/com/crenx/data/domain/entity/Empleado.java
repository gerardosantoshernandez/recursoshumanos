package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;  

@Entity
public class Empleado {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_empleado", unique = true)
	private String idEmpleado;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_documento_identidad", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoDocId;

	//unique=true
	@Column(name = "cve_fiscal",length = 100)
	private String cveFiscal;
	
	@Column(name = "doc_identificacion",length = 50)
	private String docIdentificacion;
	
	@Column(name = "fecha_ingreso", nullable=false)
	private Date fechaIngreso;

	@Column(name = "fecha_baja", nullable=true)
	private Date fechaBaja;

	@Column(name = "nombre",length = 50)
	private String nombre;
	
	@Column(name = "apellido_paterno",length = 50)
	private String apellidoPaterno;
	
	@Column(name = "apellido_materno",length = 50)
	private String apellidoMaterno;

	@Column(name = "correo_electronico",length = 100)
	private String correoElectronico;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_genero", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo genero;
	
	@Column(name = "fecha_nacimiento", nullable=false)
	private Date fechaNacimiento;

	//unique=true
	@Column(name = "curp",length = 20)
	private String curp;

	@Column(name = "fecha_creacion")
	private Date creacion;
	
	@Column(name = "nombre_usuario",length = 50, unique=true)
	private String nombreUsuario;
	
	@OneToOne
	@JoinColumn(name="id_estatus", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo estatusActual;

	@Column(name = "salarioBase")
	private double salarioBase=0;

	@Column(name = "salarioAsimilado")
	private double salarioAsimilado=0;

	@Column(name = "semana_gracia")
	private int semanasGracia = 0;
	
	@Column(name = "clave_empleado",length = 10, unique=true)
	private String cveEmpleado;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_cargo", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo cargoEmpleado;

	//Propiedades nuevas
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_domicilio", nullable=true, referencedColumnName="id_domicilio")
	private Domicilio domicilioEmpleado;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_domicilio_lab", nullable=true, referencedColumnName="id_domicilio")
	private Domicilio domicilioEmpleadoLaboral;
	
	@Column(name = "tel_casa",length = 20)
	private String telefonoCasa;
	
	@Column(name = "tel_celular", length = 20)
	private String telefonoCelular;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_entidad_nacimiento", nullable=true, referencedColumnName="id_catalogo")
	private Catalogo entidadNacimiento;

	public String getTelefonoCasa() {
		return telefonoCasa;
	}

	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public Domicilio getDomicilioEmpleadoLaboral() {
		return domicilioEmpleadoLaboral;
	}

	public void setDomicilioEmpleadoLaboral(Domicilio domicilioEmpleadoLaboral) {
		this.domicilioEmpleadoLaboral = domicilioEmpleadoLaboral;
	}

	public String getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public Catalogo getTipoDocId() {
		return tipoDocId;
	}

	public void setTipoDocId(Catalogo tipoDocId) {
		this.tipoDocId = tipoDocId;
	}

	public String getCveFiscal() {
		return cveFiscal;
	}

	public void setCveFiscal(String cveFiscal) {
		this.cveFiscal = cveFiscal;
	}

	public String getDocIdentificacion() {
		return docIdentificacion;
	}

	public void setDocIdentificacion(String docIdentificacion) {
		this.docIdentificacion = docIdentificacion;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Catalogo getGenero() {
		return genero;
	}

	public void setGenero(Catalogo genero) {
		this.genero = genero;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public Catalogo getCargoEmpleado() {
		return cargoEmpleado;
	}

	public void setCargoEmpleado(Catalogo cargoEmpleado) {
		this.cargoEmpleado = cargoEmpleado;
	}

	public Domicilio getDomicilioEmpleado() {
		return domicilioEmpleado;
	}

	public void setDomicilioEmpleado(Domicilio domicilioEmpleado) {
		this.domicilioEmpleado = domicilioEmpleado;
	}

	public String getIdTipoDocId()
	{
		String idTipodocId = null;
		if (tipoDocId!=null)
		{
			idTipodocId = tipoDocId.getIdCatalogo();
		}
		return idTipodocId;
	}
	
	public String getIdGenero()
	{
		String idGenero = null;
		if (genero!=null)
		{
			idGenero = genero.getIdCatalogo();
		}
		return idGenero;
	}
	public String getIdCargoEmpleado()
	{
		String idCargoEmpleado = null;
		if (cargoEmpleado!=null)
		{
			idCargoEmpleado = cargoEmpleado.getIdCatalogo();
		}
		return idCargoEmpleado;
	}

	public String getNombreCompleto()
	{
		return this.nombre + " " + this.getApellidoPaterno() + " " + this.getApellidoMaterno();
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public Catalogo getEstatusActual() {
		return estatusActual;
	}

	public void setEstatusActual(Catalogo estatusActual) {
		this.estatusActual = estatusActual;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public int getSemanasGracia() {
		return semanasGracia;
	}

	public void setSemanasGracia(int semanasGracia) {
		this.semanasGracia = semanasGracia;
	}

	public String getCveEmpleado() {
		return cveEmpleado;
	}

	public void setCveEmpleado(String cveEmpleado) {
		this.cveEmpleado = cveEmpleado;
	}

	public double getSalarioAsimilado() {
		return salarioAsimilado;
	}

	public void setSalarioAsimilado(double salarioAsimilado) {
		this.salarioAsimilado = salarioAsimilado;
	}
	
	
}

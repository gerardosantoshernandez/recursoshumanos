package com.crenx.data.domain.vo;

public class TablaAmortizacionVO {

	String estatusPago;
	String tipoPago;
	String idAmortizacion;
	double pagoFijo;
	double interes;
	double ivaInteres;
	double amortizacion;
	double saldo;
	String fechaVencimiento;
	String fechaPago;
	String idPago;
	int numPago;
	
	
	public int getNumPago() {
		return numPago;
	}
	public void setNumPago(int numPago) {
		this.numPago = numPago;
	}
	public double getPagoFijo() {
		return pagoFijo;
	}
	public void setPagoFijo(double pagoFijo) {
		this.pagoFijo = pagoFijo;
	}
	public double getInteres() {
		return interes;
	}
	public void setInteres(double interes) {
		this.interes = interes;
	}
	public double getIvaInteres() {
		return ivaInteres;
	}
	public void setIvaInteres(double ivaInteres) {
		this.ivaInteres = ivaInteres;
	}
	public double getAmortizacion() {
		return amortizacion;
	}
	public void setAmortizacion(double amortizacion) {
		this.amortizacion = amortizacion;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public String getEstatusPago() {
		return estatusPago;
	}
	public void setEstatusPago(String estatusPago) {
		this.estatusPago = estatusPago;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getIdAmortizacion() {
		return idAmortizacion;
	}
	public void setIdAmortizacion(String idAmortizacion) {
		this.idAmortizacion = idAmortizacion;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getIdPago() {
		return idPago;
	}
	public void setIdPago(String idPago) {
		this.idPago = idPago;
	}
}

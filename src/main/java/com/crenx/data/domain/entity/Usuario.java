package com.crenx.data.domain.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
@Entity
public class Usuario implements UserDetails  {
 
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_usuario", unique = true)
	private String idUsuario;
	
	@Column(name = "nombreUsuario",length = 50, unique = true)
	private String nombreUsuario;
	
	@Column(name = "hashContrasena",length = 500)
	private String hashContrasena;
	
	@Column(name = "token",length = 50)
	private String token;
	
	@Column(name = "nombre",length = 100)
	private String nombre;
	
	@Column(name = "apellidoMaterno",length = 100)
	private String apellidoMaterno;
	
	@Column(name = "apellidoPaterno",length = 100)
	private String apellidoPaterno;
	
	@Column(name = "correoElectronico",length = 100)
	private String correoElectronico;
	
	@Column(name = "bloqueado")
	private boolean bloqueado;
	
	@Column(name = "creacion")
	private Date creacion;
	
	@OneToOne
	@JoinColumn(name="id_estatus", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo estatusActual;
	
	@Transient
	private Perfil perfil = new Perfil();
	
	/*Relación Usuario-roles*/
	/**/
	@ManyToMany
	@JoinTable(name = "usuario_objetoseguridad", joinColumns = { 
			@JoinColumn(name = "id_usuario", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "id_objetoSeguridad", 
					nullable = false, updatable = false) })	
	@JsonIgnore
	private Collection<ObjetoSeguridad> roles;

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getHashContrasena() {
		return hashContrasena;
	}

	public void setHashContrasena(String hashContrasena) {
		this.hashContrasena = hashContrasena;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public boolean isBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}


	@Override
	public Collection<Perfil> getAuthorities() {
		// TODO Temp role entity
		List<Perfil> perfiles = new ArrayList<Perfil>();
		perfiles.add(this.perfil);
		return perfiles;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return getHashContrasena();
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return getNombreUsuario();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return estatusActual.getClaveInterna().equals("ESTATUS_ACTIVO");
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
		//FIXME: Revisar esto...
		/*
		if (this.estatusActual!=null)
			return this.estatusActual.getClaveInterna().equals("ESTATUS_ACTIVO");
		return false;
		*/
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public void setEstatusActual(Catalogo estatusActual) {
		this.estatusActual = estatusActual;
	}


	public Catalogo getEstatusActual() {
		return estatusActual;
	}

	public Collection<ObjetoSeguridad> getRoles() {
		if (roles==null)
			roles = new ArrayList<ObjetoSeguridad>();
		return roles;
	}

	public void setRoles(Collection<ObjetoSeguridad> roles) {
		this.roles = roles;
	}
	
	
	
	
}

package com.crenx.business;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import com.crenx.business.alfresco.AlfrescoConnectContext;
import com.crenx.business.alfresco.AlfrescoFileManager;
import com.crenx.business.alfresco.AlfrescoSessionFactory;

/**
 * Implementación de File Manager Factory
 * 
 * @author JCHR
 *
 */
public class FileManagerFactoryImpl implements FileManagerFactory {

	private Map<String, String> properties;
	private FileManager manager;
	private FileManagerType type;
	private static String[] validProperties = new String[] { "ruta.archivosguardados", "alfresco.repository.id",
			"alfresco.host", "alfresco.port", "alfresco.user", "alfesco.pwd", "alfresco.default.folder",
			"file.manager.type" };

	/**
	 * Inicializa valores por default
	 */
	private FileManagerFactoryImpl(final Environment env) {
		if (BooleanUtils.isFalse(this.validateProperties(env))) {
			throw new IllegalArgumentException(
					"The Session Factory System can not be created. Missing configuration properties");
		}
		this.type = FileManagerType.valueOf(this.properties.get("file.manager.type").toString());
		if (this.type == null) {
			throw new IllegalArgumentException(
					"The Session Factory System can not be created. Unknow value of property : file.manager.type");
		}
	}

	/**
	 * Crea la instancia de FilemanagerFactory
	 * 
	 * @param env
	 * @return
	 */
	public static FileManagerFactory createFileManagerFactory(final Environment env) {
		return new FileManagerFactoryImpl(env);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crenx.business.FileManagerFactory#createFileManager()
	 */
	@Override
	public FileManager createFileManager() {
		return (this.manager == null) ? this.getFileManager() : this.manager;
	}

	/**
	 * Crea el File Manager adecuado a las propiedades del sistema
	 * 
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private FileManager getFileManager() {
		final String prefixMethod = StringUtils.capitalize(type.name().toLowerCase());
		final Class clazz = this.getClass();
		Method method = null;
		try {
			method = clazz.getDeclaredMethod("get" + prefixMethod + "FileManager");
			this.manager = (FileManager) method.invoke(this);
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new IllegalAccessError("The File Manager can not be created");
		}
		return this.manager;
	}

	/**
	 * Valida que las propiedades necesarias para las creaciones de los File
	 * Managers
	 * 
	 * @param env
	 * @return
	 */
	private boolean validateProperties(final Environment env) {
		boolean valid = true;
		this.properties = new HashMap<String, String>();
		String valueProperty = null;		
		for (String property : validProperties) {			
			if (env.containsProperty(property)) {
				valueProperty = env.getProperty(property);
				if(StringUtils.hasText(valueProperty)){
					properties.put(property, valueProperty);
				}else{
					valid = false;					
				}
			} else {
				valid = false;				
			}
		}
		return valid;
	}

	/**
	 * Crea un File Manager para Alfresco
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	private FileManager getAlfrescoFileManager() {
		AlfrescoFileManager alfrescoManager = null;
		// Creando Session a partir de las propiedades
		final AlfrescoConnectContext context = new AlfrescoConnectContext(properties.get("alfresco.user"),
				properties.get("alfesco.pwd"), properties.get("alfresco.host"),
				Integer.parseInt(properties.get("alfresco.port").toString()));
		final Session session = AlfrescoSessionFactory.createSessionAtomPub(properties.get("alfresco.repository.id"),
				context);
		final AlfrescoFileManager manager = new AlfrescoFileManager(session,properties.get("alfresco.default.folder"),true);
		return manager;
	}

	/**
	 * Crea un File Manager Local
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	private FileManager getLocalFileManager() {
		LocalFileManager localManager = new LocalFileManager(properties.get("ruta.archivosguardados"),true);
		return localManager;
	}	
}

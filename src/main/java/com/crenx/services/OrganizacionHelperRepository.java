package com.crenx.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Entidad que contiene un conjunto de métodos relacionados al manejo de rutas
 * 
 * @author JCHR
 *
 */
public class OrganizacionHelperRepository extends HelperRepository{
	private OrgRepository repository;

	/**
	 * Contructor el cual recibe el repositorio de organizaciones
	 * 
	 * @param repository
	 */
	public OrganizacionHelperRepository(final OrgRepository repository) {
		if (repository == null) {
			throw new IllegalArgumentException("Creating Handler with null value");
		}
		this.repository = repository;
	}

	/**
	 * Retorna un conjunto de organizaciones que coinciden con los valores dados
	 * en el filtro. Si el filtro contiene ids de rutas, serán las rutas que se
	 * regresarán si existen, de los contrario se buscan los filtros
	 * correspondientes a los niveles de organización superiores
	 * 
	 * @param filtro
	 * @return
	 */
	public List<Organizacion> getRutas(final FiltrosOrgVO filtro) {
		final List<Organizacion> rutas = new ArrayList<Organizacion>();
		// Se incluyen las rutas específicas
		rutas.addAll(this.getRutas(
				Lists.newArrayList(Iterables.concat(filtro.getRutasIds(), this.getIdsValues(filtro.getRutas())))));
		// Si no existen rutas específicas en el filtro se buscan las rutas por
		// organizaciones superiores.
		if (rutas.isEmpty()) {
			final List<String> gerencias = Lists
					.newArrayList(Iterables.concat(this.getIdsValues(filtro.getGerencias()), filtro.getGerenciasIds()));
			final List<String> regiones = Lists
					.newArrayList(Iterables.concat(this.getIdsValues(filtro.getRegiones()), filtro.getRegionesIds()));
			final List<String> divisiones = Lists.newArrayList(
					Iterables.concat(this.getIdsValues(filtro.getDivisiones()), filtro.getDivisionesIds()));
			// Se obtienen las rutas
			final List<Organizacion> organizaciones = repository.findRutas((divisiones.isEmpty()) ? null : divisiones,
					(divisiones.isEmpty()) ? LIKE_WILDCARD_ZERO_MORE : null,
					(regiones.isEmpty()) ? null : regiones,
					(regiones.isEmpty()) ? LIKE_WILDCARD_ZERO_MORE :null,
					(gerencias.isEmpty()) ? null : gerencias,
					(gerencias.isEmpty()) ? LIKE_WILDCARD_ZERO_MORE : null);
			rutas.addAll(organizaciones);
		}
		return rutas;
	}

	/**
	 * Obtiene un conjunto de rutas por ciertos ids específicos. Método
	 * null-safe correspondiente al metodo @OrgRepository.findByIdOrgIn
	 * 
	 * @param filtro
	 * @return
	 */
	public List<Organizacion> getRutas(final List<String> ids) {
		final List<Organizacion> rutas = new ArrayList<Organizacion>();
		if (ids != null && ids.size() > 0) {
			rutas.addAll(this.repository.findByIdOrgIn(ids));
		}
		return rutas;
	}

	/**
	 * Obtiene los Ids de una colección de elementos de tipo NameValues
	 * 
	 * @param items
	 * @return
	 */
	private List<String> getIdsValues(final Collection<NameValueVO> items) {
		List<String> ids = new ArrayList<String>();
		if (items == null || items.isEmpty()) {
			ids = new ArrayList<String>();
		} else {
			ids = Lists.newArrayList(Iterables.transform(items, new Function<NameValueVO, String>() {
				@Override
				public String apply(NameValueVO item) {
					return (item == null) ? null : item.getId();
				}
			}));
		}
		return ids;
	}

	/**
	 * Obtiene los Ids de una colección de organizaciones
	 * 
	 * @param items
	 * @return
	 */
	public List<String> getIds(final List<Organizacion> orgs) {
		List<String> ids = new ArrayList<String>();
		if (orgs == null || orgs.isEmpty()) {
			ids = new ArrayList<String>();
		} else {
			ids = Lists.newArrayList(Iterables.transform(orgs, new Function<Organizacion, String>() {
				@Override
				public String apply(Organizacion org) {
					return (org == null) ? null : org.getIdOrg();
				}
			}));
		}
		return ids;
	}
}

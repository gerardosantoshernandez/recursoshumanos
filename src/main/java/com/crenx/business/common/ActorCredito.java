package com.crenx.business.common;

/**
 * Representa los tipos de cada actor que se involucra en un crédito
 * @author JCHR
 *
 */
public enum ActorCredito {

	EJECUTIVO, CLIENTE, CODEUDOR;
}

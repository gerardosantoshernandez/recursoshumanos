package com.crenx.data.domain.vo;

import com.crenx.data.domain.entity.Catalogo;

public class ReglaGastoBRVO {
	private Catalogo estatusGasto;
	private String motivoRechazo;
	private boolean crearGasto = false;
	private boolean runTimeError = false;
	private boolean correccion =  false;
	public Catalogo getEstatusGasto() {
		return estatusGasto;
	}
	public void setEstatusGasto(Catalogo estatusGasto) {
		this.estatusGasto = estatusGasto;
	}
	public String getMotivoRechazo() {
		return motivoRechazo;
	}
	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}
	public boolean isCrearGasto() {
		return crearGasto;
	}
	public void setCrearGasto(boolean crearGasto) {
		this.crearGasto = crearGasto;
	}
	public boolean isRunTimeError() {
		return runTimeError;
	}
	public void setRunTimeError(boolean runTimeError) {
		this.runTimeError = runTimeError;
	}
	public boolean isCorreccion() {
		return correccion;
	}
	public void setCorreccion(boolean correccion) {
		this.correccion = correccion;
	}

	
}

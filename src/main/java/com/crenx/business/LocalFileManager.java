package com.crenx.business;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.util.StringUtils;

/**
 * Clase concreta de un manejados de archivos adecuada para el manejo de
 * archivos en local.
 * 
 * @see FileManager
 * @author JCHR
 *
 */
public class LocalFileManager extends FileManager {

	/**
	 * Contructor default
	 */
	public LocalFileManager() {
		this(null, false);
	}

	/**
	 * @param workDirectory
	 * @param relativePaths
	 */
	public LocalFileManager(final String workDirectory, final boolean relativePaths) {
		this.workDirectory = (StringUtils.hasText(workDirectory)) ? workDirectory
				: System.getProperty("java.io.tmpdir");
		this.relativePaths = relativePaths;
	}

	@Override
	public File save(final byte[] data, final String dirPath, final String fileName, final String mimetype)
			throws IOException, IllegalArgumentException {
		File result = null;
		final Path pathDirectory = this.getPath(dirPath, null);
		if (pathDirectory == null) {
			throw new IllegalArgumentException(
					"Can not be null or empty  dirPath. File Manager is working in absolute paths");
		}
		if (BooleanUtils.isFalse(StringUtils.hasText(fileName))) {
			throw new IllegalArgumentException("Can not be null or empty name");
		}
		// Se comprueba que el directorio exista
		final File fileDir = pathDirectory.toFile();
		if (!fileDir.exists()) {
			if (BooleanUtils.isFalse(fileDir.mkdirs())) {
				throw new IOException("The directory " + dirPath + " can not be created");
			}
		}
		// Se crea el archivo
		result = this.getPath(dirPath, fileName).toFile();
		FileOutputStream fos = null;
		fos = new FileOutputStream(result);
		fos.write(data);
		fos.flush();
		fos.close();
		return result;
	}

	@Override
	public File get(final String dirPath, final String fileName) throws IOException, IllegalArgumentException {
		File result = null;
		final Path pathDirectory = this.getPath(dirPath, null);
		if (pathDirectory == null) {
			throw new IllegalArgumentException(
					"Can not be null or empty  dirPath. File Manager is working in absolute paths");
		}
		if (StringUtils.hasText(fileName)) {
			final Path path = this.getPath(dirPath, fileName);
			result = path.toFile();
			if (!result.exists()) {
				result = null;
			}
		}
		return result;
	}

	@Override
	public void createDirectory(final String parentDirPath, final String dirName)
			throws IOException, IllegalArgumentException {
		File newDir = null;
		if (BooleanUtils.isFalse(StringUtils.hasText(dirName))) {
			throw new IllegalArgumentException("Can not be null or empty name");
		}
		final Path pathDirectory = this.getPath(parentDirPath, null);
		if (pathDirectory == null) {
			throw new IllegalArgumentException(
					"Can not be null or empty  dirPath. File Manager is working in absolute paths");
		}
		// Se verifica si existe el Directorio padre
		final File parent = pathDirectory.toFile();
		if (parent.exists() && parent.isDirectory()) {
			// Se crea el directorio
			newDir = this.getPath(parentDirPath, dirName).toFile();
			if (BooleanUtils.isFalse(newDir.exists())) {
				if (newDir.mkdirs()) {
					newDir.setReadable(true);
					newDir.setWritable(true);
				} else {
					throw new IOException("The directory " + dirName + " can not be created");
				}
			}
		} else {
			throw new IOException("The resource " + parentDirPath + " does not exist");
		}
	}

	public String listFiles(final String directory) {
		String result = directory;
		if (StringUtils.hasText(directory)) {
			result = FileUtils.listFiles(new File(result), 0);
		}
		return result;
	}

	@Override
	public void delete(String dirPath, String fileName) throws IOException, IllegalArgumentException {
		if (StringUtils.hasText(fileName)) {
			final Path pathDirectory = this.getPath(dirPath, null);
			if (pathDirectory == null) {
				throw new IllegalArgumentException(
						"Can not be null or empty  dirPath. File Manager is working in absolute paths");
			}
			final Path path = this.getPath(dirPath, fileName);
			final File fileToDel = path.toFile();
			if (fileToDel.exists()) {
				if (BooleanUtils.isFalse(fileToDel.delete())) {
					throw new IOException("The  file" + fileName + " can not be deleted");
				}
			}
		}
	}

	@Override
	public boolean exists(final String dirPath, final String fileName) {
		Path path = this.getPath(dirPath, fileName);
		return (path != null && path.toFile().exists());
	}

	/**
	 * Obtiene un path de acuerdo a si son relativas o no las direcciones al
	 * directorio de trabajo del File Manager
	 * 
	 * @param dirPath
	 * @param fileName
	 * @param separator
	 * @return
	 */
	protected Path getPath(final String dirPath, final String fileName) throws IllegalArgumentException {
		String basePath = (this.relativePaths) ? this.workDirectory : "";
		if (StringUtils.hasText(dirPath)) {
			basePath = Paths.get(basePath, dirPath).toString();
		}

		if (StringUtils.hasText(fileName)) {
			basePath = Paths.get(basePath, fileName).toString();
		}
		if (BooleanUtils.isFalse(StringUtils.hasText(basePath))) {
			throw new IllegalArgumentException(
					"The directory path is null or empty. File Manager is working with absolute paths.");
		}
		return Paths.get(basePath);
	}

}

package com.crenx.services;

import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crenx.data.domain.entity.Empleado;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.DateRangeVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.NameValueVO;

@Service
public class UtilityServices {

	@Autowired
	private SeguridadServices secServices;
	@Autowired
	private OrgRepository orgRepository;
	@Autowired
	private CatalogoRepository catRepository;
	
	ClienteVO datosValidate = new ClienteVO();
	private String fullName, ap, aptmp, a1, a2, c, valores = " ", rfc, ccociente, rresiduo, n1, m2, v, curp, o3, o4,
			aNombre, aPaterno2, aMaterno2, lw1, lw2, lw3;

	private char c1, c11, l1, l2;
	private int a, b, r1, r2, ax = 1, bx = 0, divisor = 34, dividendo = 0, x = 0, y = 0, suma = 0, cociente, residuo,
			Pi = 13, Vi = 0, D, n, m, cx, dx;

	public Date convertStringToDate(String source, String format) {
		if (source == null)
			return null;
		SimpleDateFormat df = new SimpleDateFormat(format);
		try {
			return df.parse(source);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	public Date justDate(Date theDate) {
		Calendar ss = Calendar.getInstance();
		ss.setTime(theDate);
		ss.set(Calendar.HOUR, 0);
		ss.set(Calendar.MINUTE, 0);
		ss.set(Calendar.SECOND, 0);
		ss.set(Calendar.MILLISECOND, 0);
		return ss.getTime();
	}

	public DateRangeVO getWeekDates(Date desde) {
		DateRangeVO week = new DateRangeVO();
		desde = desde == null ? new Date() : desde;
		Calendar ss = Calendar.getInstance();
		ss.setTime(desde);
		while (ss.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
			ss.add(Calendar.DATE, -1);
		}
		week.setStartDate(ss.getTime());
		ss.add(Calendar.DATE, 7);
		week.setEndDate(ss.getTime());
		return week;
	}

	public Date getBaseVencimiento(Date today, int days, boolean onlyWorkingDays)
	{
		Calendar ss = Calendar.getInstance();
		ss.setTime(today);
		if (!onlyWorkingDays)
			ss.add(Calendar.DATE, (days*-1));
		else
		{
			while (days >=0) {
				
				if (ss.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)
					days--;				
				ss.add(Calendar.DATE, -1);
			}
		}
		return ss.getTime();		
	}
	
	
	public DateRangeVO getMonthDates(Date desde) {
		DateRangeVO week = new DateRangeVO();
		desde = desde == null ? new Date() : desde;
		Calendar ss = Calendar.getInstance();
		ss.setTime(desde);
		ss.set(Calendar.DAY_OF_MONTH, 1);
		week.setStartDate(ss.getTime());
		ss.add(Calendar.MONTH, 1);
		ss.set(Calendar.DAY_OF_MONTH, 1);
		ss.add(Calendar.DATE, -1);
		week.setEndDate(ss.getTime());
		return week;
	}

	public FiltrosOrgVO filtrosById(FiltrosOrgVO filtros) {
		for (NameValueVO ruta : filtros.getRutas()) {
			filtros.getRutasIds().add(ruta.getId());
		}
		for (NameValueVO gerencia : filtros.getGerencias()) {
			filtros.getGerenciasIds().add(gerencia.getId());
		}
		for (NameValueVO region : filtros.getRegiones()) {
			filtros.getRegionesIds().add(region.getId());
		}
		for (NameValueVO division : filtros.getDivisiones()) {
			filtros.getDivisionesIds().add(division.getId());
		}
		return filtros;
	}

	public String validateRFC(String apellidoPaterno, String apellidoMaterno, String nombre, String fechaNacimiento) {

		String aPaterno = null;
		// Normalizamos en la forma NFD (Canonical decomposition)
		aPaterno = Normalizer.normalize(apellidoPaterno, Normalizer.Form.NFD);
		// Reemplazamos los acentos con una una expresión regular de Bloque
		// Unicode
		aPaterno = aPaterno.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		aPaterno = aPaterno
				.replaceAll("[\\-\\+\\.\\^:,',´,#,$,%,/,(,),=,?,¡,¨,*,´,{,},;,-,+,¬,0-9,|,¿,~,,,>,<,@,_,°,!]", "");
		aPaterno = aPaterno.trim(); // Eliminar los posibles espacios en blanco
									// al principio y al final
		aPaterno = aPaterno.toUpperCase();// Convertir a mayúsculas

		String aMaterno = null;
		aMaterno = Normalizer.normalize(apellidoMaterno, Normalizer.Form.NFD);
		aMaterno = aMaterno.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		aMaterno = aMaterno
				.replaceAll("[\\-\\+\\.\\^:,',´,#,$,%,/,(,),=,?,¡,¨,*,´,{,},;,-,+,¬,0-9,|,¿,~,,,>,<,@,_,°,!]", "");
		aMaterno = aMaterno.trim();
		aMaterno = aMaterno.toUpperCase();

		String nom = null;
		nom = Normalizer.normalize(nombre, Normalizer.Form.NFD);
		nom = nom.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		nom = nom.replaceAll("[\\-\\+\\.\\^:,',´,#,$,%,/,(,),=,?,¡,¨,*,´,{,},;,-,+,¬,0-9,|,¿,~,,,>,<,@,_,°,!]", "");
		nom = nom.trim();
		nom = nom.toUpperCase();

		aPaterno2 = apellidoPaterno.replace('ñ', '\001');// Remplazamos la ñ por
															// un caracter no
															// imprimible
		aPaterno2 = Normalizer.normalize(aPaterno2, Normalizer.Form.NFD); // Normalizamos
																			// en
																			// la
																			// forma
																			// NFD
																			// (Canonical
																			// decomposition)
		aPaterno2 = aPaterno2.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");// Reemplazamos
																					// los
																					// acentos
																					// con
																					// una
																					// una
																					// expresión
																					// regular
																					// de
																					// Bloque
																					// Unicode
		aPaterno2 = aPaterno2
				.replaceAll("[\\-\\+\\.\\^:,',´,#,$,%,/,(,),=,?,¡,¨,*,´,{,},;,-,+,¬,0-9,|,¿,~,,,>,<,@,_,°,!]", "");
		aPaterno2 = aPaterno2.replace('\001', 'ñ');// Convertimos el caracter no
													// imprimible de nuevo a ñ.

		System.out.println("Apellido Paterno" + " " + aPaterno2);

		aPaterno2 = aPaterno2.trim();
		aPaterno2 = aPaterno2.toUpperCase();

		aMaterno2 = apellidoMaterno.replace('ñ', '\001');
		aMaterno2 = Normalizer.normalize(aMaterno2, Normalizer.Form.NFD);
		aMaterno2 = aMaterno2.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		aMaterno2 = aMaterno2
				.replaceAll("[\\-\\+\\.\\^:,',´,#,$,%,/,(,),=,?,¡,¨,*,´,{,},;,-,+,¬,0-9,|,¿,~,,,>,<,@,_,°,!]", "");
		aMaterno2 = aMaterno2.replace('\001', 'ñ');

		System.out.println("Apellido Materno" + " " + aMaterno2);

		aMaterno2 = aMaterno2.trim();
		aMaterno2 = aMaterno2.toUpperCase();

		aNombre = nombre.replace('ñ', '\001');
		aNombre = Normalizer.normalize(aNombre, Normalizer.Form.NFD);
		aNombre = aNombre.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		aNombre = aNombre.replaceAll("[\\-\\+\\.\\^:,',´,#,$,%,/,(,),=,?,¡,¨,*,´,{,},;,-,+,¬,0-9,|,¿,~,,,>,<,@,_,°,!]",
				"");
		aNombre = aNombre.replace('\001', 'ñ');

		System.out.println("Nombre" + " " + aNombre);

		aNombre = aNombre.trim();
		aNombre = aNombre.toUpperCase();

		aPaterno2 = aPaterno2 + " ";// Asignando un espacio despúes del apellido
									// paterno para usarlo en la generación de
									// los digitos 11 y 12 de la homoclave
		System.out.println("aPaterno2" + " " + aPaterno2);
		aMaterno2 = aMaterno2 + " ";
		System.out.println("aMaterno2" + " " + aMaterno2);
		aNombre = aNombre + " ";
		System.out.println("aNombre" + " " + aNombre);

		String fecha = fechaNacimiento;
		fecha = fecha.trim();

		String[] arregloSplit;
		boolean estado;

		// Aplicando al nombre
		arregloSplit = nom.split(" ");
		if (arregloSplit.length == 3 && arregloSplit[1].equals("Y")
				|| arregloSplit.length == 3 && arregloSplit[1].equals("DE")
				|| arregloSplit.length == 3 && arregloSplit[1].equals("DEL")
				|| arregloSplit.length == 3 && arregloSplit[1].equals("MC")
				|| arregloSplit.length == 3 && arregloSplit[1].equals("VON")
				|| arregloSplit.length == 3 && arregloSplit[1].equals("MAC")
				|| arregloSplit.length == 3 && arregloSplit[1].equals("VAN")
				|| arregloSplit.length == 3 && arregloSplit[1].equals("MI")) {
			arregloSplit[1].replaceAll("", "");
			// rfc = arregloSplit[1].substring(0,2) + aMaterno.substring(0,1) +
			// nom.substring(0,1) + fecha.substring(2,4) + fecha.substring(5,7)
			// + fecha.substring(8);
			String space = " ";
			nom = arregloSplit[0] + space + arregloSplit[2];// Eliminamos del
															// apellido Paterno
															// que va a ser
															// utilizado en el
															// nombre completo
															// las preposiciones
															// anteriores
			System.out.println("El nombre compuesto es:" + " " + nom);
		} else {
		}

		arregloSplit = nom.split(" ");
		if (arregloSplit.length == 4 && arregloSplit[1].equals("DE") && arregloSplit[2].equals("LA")
				|| arregloSplit.length == 4 && arregloSplit[1].equals("DE") && arregloSplit[2].equals("LAS")
				|| arregloSplit.length == 4 && arregloSplit[1].equals("DE") && arregloSplit[2].equals("LOS")) {
			arregloSplit[1].replaceAll("", "");
			arregloSplit[2].replaceAll("", "");
			String space = " ";
			nom = arregloSplit[0] + space + arregloSplit[3];//// Eliminamos del
															//// apellido
															//// Paterno que va
															//// a ser utilizado
															//// en el nombre
															//// completo las
															//// preposiciones
															//// anteriores y le
															//// asignamos el
															//// apellido de
															//// posición (2)
			System.out.println("El nombre compuesto es:" + " " + nom);
		} else {
		}

		/*
		 * REGLA 1Âª. Se integra la clave con los siguientes datos: 1. La
		 * primera letra del apellido paterno y la siguiente primera vocal del
		 * mismo. 2. La primera letra del apellido materno. 3. La primera letra
		 * del nombre.
		 * 
		 * Ejemplo:
		 * 
		 * Juan Barrios Fernández Apellido paterno Barrios BA Apellido materno
		 * Fernández F Nombre Juan J Resultando de la expresión alfabética: BAFJ
		 * 
		 * Eva Iriarte Méndez Apellido paterno Iriarte II Apellido materno
		 * Méndez M Nombre Eva E Resultado de la expresión alfabética: IIME
		 */
		char[] p = aPaterno.toCharArray();
		estado = true;

		for (int i = 1; i < p.length; i++) {
			String letra = (p[i] + "");// Hace un "casting" al String para poder
										// usar función compareTo
			if ((letra.compareToIgnoreCase("a") == 0 || letra.compareToIgnoreCase("e") == 0
					|| letra.compareToIgnoreCase("i") == 0 || letra.compareToIgnoreCase("o") == 0
					|| letra.compareToIgnoreCase("u") == 0) && estado) { // Buscamos
																			// si
																			// contiene
																			// alguna
																			// vocal
				estado = false;// Para evitar más comparaciones
				ap = p[0] + letra;
			}
		}
		rfc = ap + aMaterno.substring(0, 1) + nom.substring(0, 1) + fecha.substring(2, 4) + fecha.substring(5, 7)
				+ fecha.substring(8);
		System.out.println("Regla 1" + " " + rfc);

		/*
		 * REGLA 2Âª. A continuación se anotará la fecha de nacimiento del
		 * contribuyente, en el siguiente orden: Año: Se tomarán las dos últimas
		 * cifras, escribiéndolas con números arábigos. Mes: Se tomará el mes de
		 * nacimiento en su número de orden, en un año de calendario,
		 * escribiéndolo con números arábigos. Día: Se escribirá con números
		 * arábigos.
		 * 
		 * Ejemplos: Año 1970 70 Mes Diciembre 12 Día 13 13
		 * 
		 * Dando como resultado la expresión numérica: 701213
		 * 
		 * Y complementando con la expresión alfabética numérica tendremos:
		 * 
		 * Juan Barrios Fernández BAFJ 701213 Eva Iriarte Méndez IIME 691117
		 * 
		 * Cuando en el año, mes o día, de la fecha de nacimiento, aparezca
		 * solamente un guarismo se le antepondrá un CERO.
		 * 
		 * Ejemplos: Año 1907 07 Mes Abril 04 Día 1Âº. 01 Como resultado
		 * tendremos la expresión numérica: 070401 Y completando la clave
		 * alfabético numérica tendremos: Juan Barrios Fernández BAFJ-070401
		 * Francisco Ortíz Pérez OIPF-290205 Manuel Martínez Hernández
		 * MAHM-570102 Gabriel Courturier Moreno COMG-600703
		 */
		System.out.println("Regla 1-2" + " " + rfc);

		/*
		 * REGLA 3Âª. Cuando la letra inicial de cualquiera de los apellidos o
		 * nombre sea compuesta, únicamente se anotará la inicial de ésta. En la
		 * Ch la C y en la Ll la L. Ejemplos: Manuel Chávez González CAGM-240618
		 * Felipe Camargo Llamas CALF-450228 Charles Kennedy Truman KETC-511012
		 */

		// Aplicando al apellido paterno
		if (aPaterno != null && aPaterno.length() > 2) {
			if (aPaterno.substring(0, 2).contains("LL") || aPaterno.substring(0, 2).contains("CH")) {
				aPaterno = aPaterno.substring(0, 1) + aPaterno.substring(2, 3);
				rfc = aPaterno.substring(0, 2) + aMaterno.substring(0, 1) + nom.substring(0, 1) + fecha.substring(2, 4)
						+ fecha.substring(5, 7) + fecha.substring(8);
				System.out.println("Regla 1-2-3 apaterno" + " " + rfc);
			} else {
				// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
				// fecha.substring(2,4) + fecha.substring(5,7) +
				// fecha.substring(8);
			}
		}

		// Aplicando al apellido materno
		if (aMaterno.length() > 2) {
			if (aMaterno.substring(0, 2).contains("LL") || aMaterno.substring(0, 2).contains("CH")) {// Mod
				aMaterno = aMaterno.substring(0, 1);
				rfc = aPaterno.substring(0, 2) + aMaterno.substring(0, 1) + nom.substring(0, 1) + fecha.substring(2, 4)
						+ fecha.substring(5, 7) + fecha.substring(8);
				System.out.println("Regla 1-2-3 amaterno" + " " + rfc);
			} else {
				// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
				// fecha.substring(2,4) + fecha.substring(5,7) +
				// fecha.substring(8);
			}
		}
		// Aplicando a nombre
		if (nombre.substring(0, 2).contains("LL") || nombre.substring(0, 2).contains("CH")) {
			nombre = nombre.substring(0, 1);
			rfc = aPaterno.substring(0, 2) + aMaterno.substring(0, 1) + nom.substring(0, 1) + fecha.substring(2, 4)
					+ fecha.substring(5, 7) + fecha.substring(8);
			System.out.println("Regla 1-2-3 nombre" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}

		/*
		 * REGLA 5Âª. Está regla se cumple por default Cuando el apellido
		 * paterno o materno sean compuestos, se tomará para la clasificación la
		 * primera palabra que corresponda a cualquiera de ellos. Ejemplos:
		 * Dolores San Martín Dávalos SADD-180812 Mario Sánchez de la Barquera
		 * Gómez SAGM-190224 Antonio Jiménez Ponce de León JIPA-170808
		 */
		System.out.println("Regla 1-2-3-4-5 Default Rule" + " " + rfc);

		/*
		 * REGLA 6Âª. Cuando el nombre es compuesto, o sea, que está formado por
		 * dos o más palabras, se tomará para la conformación la letra inicial
		 * de la primera, siempre que no sea MARIA o JOSE dado su frecuente uso,
		 * en cuyo caso se tomará la primera letra de la segunda palabra.
		 * Ejemplos: Luz María Fernández Juárez FEJL-200205 José Antonio Camargo
		 * Hernández CAHA-211218 María Luisa Ramírez Sánchez RASL-251112
		 */
		// Aplicando al primer nombre
		arregloSplit = nom.split(" ");
		if (arregloSplit[0].equals("MARIA") && arregloSplit.length >= 2) {
			arregloSplit[0].replaceAll("MARIA", "");
			rfc = ap + aMaterno.substring(0, 1) + arregloSplit[1].substring(0, 1) + fecha.substring(2, 4)
					+ fecha.substring(5, 7) + fecha.substring(8);
			System.out.println("Regla 1-2-3-4-5-6 First name Maria" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}

		arregloSplit = nom.split(" ");
		if (arregloSplit[0].equals("JOSE") && arregloSplit.length >= 2) {
			arregloSplit[0].replaceAll("JOSE", "");
			rfc = ap + aMaterno.substring(0, 1) + arregloSplit[1].substring(0, 1) + fecha.substring(2, 4)
					+ fecha.substring(5, 7) + fecha.substring(8);
			System.out.println("Regla 1-2-3-4-5-6 First name José" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}

		// Aplicando al segundo nombre
		arregloSplit = nom.split(" ");
		if (arregloSplit.length >= 2 && arregloSplit[1].equals("MARIA")) {
			arregloSplit[1].substring(0).replaceAll("MARIA", "");
			rfc = ap + aMaterno.substring(0, 1) + arregloSplit[0].substring(0, 1) + fecha.substring(2, 4)
					+ fecha.substring(5, 7) + fecha.substring(8);
			System.out.println("Regla 1-2-3-4-5-6 Second name Maria" + " " + rfc);
			System.out.println("Regla 1-2-3-4-5-6 Second name Maria" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}

		arregloSplit = nom.split(" ");
		if (arregloSplit.length >= 2 && arregloSplit[1].equals("JOSE")) {
			arregloSplit[1].substring(0).replaceAll("JOSE", "");
			rfc = ap + aMaterno.substring(0, 1) + arregloSplit[0].substring(0, 1) + fecha.substring(2, 4)
					+ fecha.substring(5, 7) + fecha.substring(8);
			System.out.println("Regla 1-2-3-4-5-6 Second name José" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}

		/*
		 * REGLA 7Âª. En los casos en que la persona física tenga un solo
		 * apellido, se conformará con la primera y segunda letras del apellido
		 * paterno o materno, según figure en el acta de nacimiento, más la
		 * primera y segunda letras del nombre. Ejemplos: Juan Martínez
		 * MAJU-420116 Gerarda Zafra ZAGE-251115
		 * 
		 * if(aPaterno.equals("")){ rfc = aMaterno.substring(0,2) +
		 * nom.substring(0,2) + fecha.substring(2,4) + fecha.substring(5,7) +
		 * fecha.substring(8); }else{ rfc = ap + aMaterno.substring(0,1) +
		 * nom.substring(0,1) + fecha.substring(2,4) + fecha.substring(5,7) +
		 * fecha.substring(8); } System.out.println("Regla 1,2,3,4,5,6,7" + " "
		 * + "Without P" + " " + rfc);
		 * 
		 * if(aMaterno.equals("")){ rfc = aPaterno.substring(0,2) +
		 * nom.substring(0,2) + fecha.substring(2,4) + fecha.substring(5,7) +
		 * fecha.substring(8); }else{ rfc = ap + aMaterno.substring(0,1) +
		 * nom.substring(0,1) + fecha.substring(2,4) + fecha.substring(5,7) +
		 * fecha.substring(8); } System.out.println("Regla 1,2,3,4,5,6, 7" + " "
		 * + "Without M" + " " + rfc);
		 */

		/*
		 * REGLA 8Âª. Cuando en el nombre de las personas físicas figuren
		 * artículos, preposiciones, conjunciones o contracciones no se tomarán
		 * como elementos de integración de la clave, ejemplos: Carmen de la
		 * Peña Ramírez PERC-631201 Mario Sánchez de los Cobos SACM-701110
		 * Roberto González y Durán GODR-600101 Juan del Valle Martínez
		 * VAMJ-691001
		 */

		// Aplicando al apellido paterno
		arregloSplit = aPaterno.split(" ");
		if (arregloSplit.length > 2) {// Modificado para la excepción de las
										// preposiciones en la app movil
			if (arregloSplit[0].equals("Y") || arregloSplit[0].equals("DE") || arregloSplit[0].equals("DEL")
					|| arregloSplit[0].equals("MC") || arregloSplit[0].equals("VON") || arregloSplit[0].equals("MAC")
					|| arregloSplit[0].equals("VAN") || arregloSplit[0].equals("MI")) {
				arregloSplit[0].replaceAll("", "");
				rfc = arregloSplit[1].substring(0, 2) + aMaterno.substring(0, 1) + nom.substring(0, 1)
						+ fecha.substring(2, 4) + fecha.substring(5, 7) + fecha.substring(8);
				aPaterno2 = arregloSplit[1] + " ";// Eliminamos del apellido
													// Paterno que va a ser
													// utilizado en el nombre
													// completo las
													// preposiciones anteriores
				System.out.println("Regla 1-2-3-4-5-6-7-8" + " " + "Lastname1, one word" + " " + "RFC: " + rfc);
			} else {
				// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
				// fecha.substring(2,4) + fecha.substring(5,7) +
				// fecha.substring(8);
			}
		}

		arregloSplit = aPaterno.split(" ");
		if (arregloSplit.length > 2) {// Modificado para la excepción de las
										// preposiciones en la app movil
			if (arregloSplit[0].equals("DE") && arregloSplit[1].equals("LA")
					|| arregloSplit[0].equals("DE") && arregloSplit[1].equals("LAS")
					|| arregloSplit[0].equals("DE") && arregloSplit[1].equals("LOS")) {
				arregloSplit[0].replaceAll("", "");
				arregloSplit[1].replaceAll("", "");
				aPaterno2 = arregloSplit[2] + " ";//// Eliminamos del apellido
													//// Paterno que va a ser
													//// utilizado en el nombre
													//// completo las
													//// preposiciones
													//// anteriores y le
													//// asignamos el apellido
													//// de posición (2)
				System.out.println("El tercer elemento en el apellido es:" + " " + arregloSplit[2].substring(0));
				if (arregloSplit[2].length() == 1) {
					rfc = arregloSplit[2] + aMaterno.substring(0, 1) + nom.substring(0, 2) + fecha.substring(2, 4)
							+ fecha.substring(5, 7) + fecha.substring(8);
				}
				if (arregloSplit[2].length() > 1) {
					char[] pa = arregloSplit[2].toCharArray();
					boolean state = true;
					for (int i = 1; i < pa.length; i++) {
						String letter = (pa[i] + "");// Hace un "casting" al
														// String para poder
														// usar función
														// compareTo
						if ((letter.compareToIgnoreCase("a") == 0 || letter.compareToIgnoreCase("e") == 0
								|| letter.compareToIgnoreCase("i") == 0 || letter.compareToIgnoreCase("o") == 0
								|| letter.compareToIgnoreCase("u") == 0) && state) { // Buscamos
																						// si
																						// contiene
																						// alguna
																						// vocal
							state = false;// Para evitar más comparaciones
							aptmp = pa[0] + letter;
						}
					}

					// Sustituir las dos primeras letras del apellido paterno
					// por la primera letra más la primera vocal.
					rfc = aptmp + aMaterno.substring(0, 1) + nom.substring(0, 1) + fecha.substring(2, 4)
							+ fecha.substring(5, 7) + fecha.substring(8);
					System.out.println("Regla 1-2-3-4-5-6-7-8" + " " + "Lastname1, two words" + " " + "RFC: " + rfc);
				}
			}
		} else {

		}

		// Aplicando al apellido materno
		arregloSplit = aMaterno.split(" ");
		if (arregloSplit.length > 2) {// Modificado para la excepción de las
										// preposiciones en la app movil
			if (arregloSplit[0].equals("Y") || arregloSplit[0].equals("DE") || arregloSplit[0].equals("DEL")
					|| arregloSplit[0].equals("MC") || arregloSplit[0].equals("VON") || arregloSplit[0].equals("MAC")
					|| arregloSplit[0].equals("VAN") || arregloSplit[0].equals("MI")) {
				arregloSplit[0].replaceAll("", "");
				rfc = ap + arregloSplit[1].substring(0, 1) + nom.substring(0, 1) + fecha.substring(2, 4)
						+ fecha.substring(5, 7) + fecha.substring(8);
				o3 = arregloSplit[1].substring(0, 1);
				aMaterno2 = arregloSplit[1] + " ";
				System.out.println("Regla 1-2-3-4-5-6-7-8" + " " + "Lastname2, one word" + " " + "RFC: " + rfc);
			} else {
				// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
				// fecha.substring(2,4) + fecha.substring(5,7) +
				// fecha.substring(8);
			}
		}

		arregloSplit = aMaterno.split(" ");
		if (arregloSplit.length > 2) {// Modificado para la excepción de las
										// preposiciones en la app movil
			if (arregloSplit[0].equals("DE") && arregloSplit[1].equals("LA")
					|| arregloSplit[0].equals("DE") && arregloSplit[1].equals("LAS")
					|| arregloSplit[0].equals("DE") && arregloSplit[1].equals("LOS")) {
				arregloSplit[0].replaceAll("", "");
				arregloSplit[1].replaceAll("", "");
				rfc = ap + arregloSplit[2].substring(0, 1) + nom.substring(0, 1) + fecha.substring(2, 4)
						+ fecha.substring(5, 7) + fecha.substring(8);
				o4 = arregloSplit[2].substring(0, 1);
				aMaterno2 = arregloSplit[2] + " ";
				System.out.println("Regla 1-2-3-4-5-6-7-8" + " " + "Lastname2, two words" + " " + "RFC: " + rfc);
			} else {
				// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
				// fecha.substring(2,4) + fecha.substring(5,7) +
				// fecha.substring(8);
			}
		}

		/*
		 * REGLA 4Âª. En los casos en que el apellido paterno de la persona
		 * física se componga de una o dos letras, la clave se formará de la
		 * siguiente manera: 1. La primera letra del apellido paterno. 2. La
		 * primera letra del apellido materno. 3. La primera y segunda letra del
		 * nombre.
		 */
		// Cuando el apellido materno contiene una preposición:

		if (aMaterno.substring(0, 1) != null && aPaterno.length() == 1) {
			rfc = aPaterno + aMaterno.substring(0, 1) + nom.substring(0, 2) + fecha.substring(2, 4)
					+ fecha.substring(5, 7) + fecha.substring(8);
			System.out.println("Regla 1-2-3-4 AP1 + Sin prepocisión" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}
		if (aMaterno.substring(0, 1) != null && aPaterno.length() == 2) {
			rfc = aPaterno.substring(0, 1) + aMaterno.substring(0, 1) + nom.substring(0, 2) + fecha.substring(2, 4)
					+ fecha.substring(5, 7) + fecha.substring(8);
			System.out.println("Regla 1-2-3-4 AP2 + Sin preposición" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}
		if (o3 != null && aPaterno.length() == 1) {//
			rfc = aPaterno + o3 + nom.substring(0, 2) + fecha.substring(2, 4) + fecha.substring(5, 7)
					+ fecha.substring(8);
			System.out.println("Regla 1-2-3-4 One-Letter in lastname 1" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}
		if (o4 != null && aPaterno.length() == 1) {//
			rfc = aPaterno + o4 + nom.substring(0, 2) + fecha.substring(2, 4) + fecha.substring(5, 7)
					+ fecha.substring(8);
			System.out.println("Regla 1-2-3-4 AP1 Con preposición" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}
		if (o3 != null && aPaterno.length() == 2) {//
			rfc = aPaterno.substring(0, 1) + o3 + nom.substring(0, 2) + fecha.substring(2, 4) + fecha.substring(5, 7)
					+ fecha.substring(8);
			System.out.println("Regla 1-2-3-4 Two-Letter in lastname 1" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}
		if (o4 != null && aPaterno.length() == 2) {//
			rfc = aPaterno.substring(0, 1) + o4 + nom.substring(0, 2) + fecha.substring(2, 4) + fecha.substring(5, 7)
					+ fecha.substring(8);
			System.out.println("Regla 1-2-3-4 AP2 Con preposición" + " " + rfc);
		} else {
			// rfc = ap + aMaterno.substring(0,1) + nom.substring(0,1) +
			// fecha.substring(2,4) + fecha.substring(5,7) + fecha.substring(8);
		}

		/*
		 * REGLA 9Âª. Cuando de las cuatro letras que formen la expresión
		 * alfabética, resulte una palabra inconveniente, la ultima letra será
		 * sustituida por una â€œ X â€œ.
		 */

		String bwords[] = { "BACA", "LOCO", "BAKA", "LOKA", "BUEI", "LOKO", "BUEY", "MAME", "CACA", "MAMO", "CACO",
				"MEAR", "CAGA", "MEAS", "CAGO", "MEON", "CAKA", "MIAR", "CAKO", "MION", "COGE", "MOCO", "COGI", "MOKO",
				"COJA", "MULA", "COJE", "MULO", "COJI", "NACA", "COJO", "NACO", "COLA", "PEDA", "CULO", "PEDO", "FALO",
				"PENE", "FETO", "PIPI", "GETA", "PITO", "GUEI", "POPO", "GUEY", "PUTA", "JETA", "PUTO", "JOTO", "QULO",
				"KACA", "RATA", "KACO", "ROBA", "KAGA", "ROBE", "KAGO", "ROBO", "KAKA", "RUIN", "KAKO", "SENO", "KOGE",
				"TETA", "KOGI", "VACA", "KOJA", "VAGA", "KOJE", "VAGO", "KOJI", "VAKA", "KOJO", "VUEI", "KOLA", "VUEY",
				"KULO", "WUEI", "LILO", "WUEY", "LOCA" };
		for (int u = 0; u < bwords.length; u++)// Recorremos el array ct para
												// comparar sus elementos con el
												// valor del cociente
		{
			if ((bwords[u]).equals(rfc.substring(0, 4))) // Comparamos el valor
															// del cociente con
															// los elementos de
															// nuestro arreglo
			{
				StringBuilder myRfc = new StringBuilder(rfc);
				myRfc.setCharAt(3, 'X');
				rfc = myRfc.toString();

				System.out.println("El resultado obtenido es:" + rfc);
			} else {
			}
		}
		/*
		 * REGLA 10Âª.
		 * 
		 * Cuando aparezcan formando parte del nombre, apellido paterno y
		 * apellido materno los caracteres especiales, éstos deben de excluirse
		 * para el cálculo del homónimo y del dígito verificador. Los caracteres
		 * se interpretarán, sí y sólo si, están en forma individual dentro del
		 * nombre, apellido paterno y apellido materno. Ejemplos:
		 * 
		 * Roberto Oâ€™farril Carballo OACR-661121 Rubén Dâ€™angelo Fargo
		 * DAFR-710108 Luz Ma. Fernández Juárez FEJL-830120
		 * 
		 * OBSERVACION: En caso de la mujer, se deberá usar el nombre de
		 * soltera.
		 */
		System.out.println("Resultado Final..." + " " + "Su RFC es:" + " " + rfc);
		curp = rfc;
		// Generación de la Homoclave
		fullName = aPaterno2 + aMaterno2 + aNombre;// Asignación a fullName del
													// nombre completo de la
													// persona.
		System.out.println("Nombre completo" + fullName);

		if (fullName.contains(" ") || fullName.contains("A") || fullName.contains("B") || fullName.contains("C")
				|| fullName.contains("D") || fullName.contains("E") || fullName.contains("F") || fullName.contains("G")
				|| fullName.contains("H") || fullName.contains("I") || fullName.contains("J") || fullName.contains("K")
				|| fullName.contains("L") || fullName.contains("M") || fullName.contains("N") || fullName.contains("O")
				|| fullName.contains("P") || fullName.contains("Q") || fullName.contains("R") || fullName.contains("S")
				|| fullName.contains("T") || fullName.contains("U") || fullName.contains("V") || fullName.contains("W")
				|| fullName.contains("X") || fullName.contains("Y") || fullName.contains("Z")
				|| fullName.contains("Ñ")) {

			fullName = fullName.replace(" ", "00");
			fullName = fullName.replace("A", "11");
			fullName = fullName.replace("B", "12");
			fullName = fullName.replace("C", "13");
			fullName = fullName.replace("D", "14");
			fullName = fullName.replace("E", "15");
			fullName = fullName.replace("F", "16");
			fullName = fullName.replace("G", "17");
			fullName = fullName.replace("H", "18");
			fullName = fullName.replace("I", "19");
			fullName = fullName.replace("J", "21");
			fullName = fullName.replace("K", "22");
			fullName = fullName.replace("L", "23");
			fullName = fullName.replace("M", "24");
			fullName = fullName.replace("N", "25");
			fullName = fullName.replace("O", "26");
			fullName = fullName.replace("P", "27");
			fullName = fullName.replace("Q", "28");
			fullName = fullName.replace("R", "29");
			fullName = fullName.replace("S", "32");
			fullName = fullName.replace("T", "33");
			fullName = fullName.replace("U", "34");
			fullName = fullName.replace("V", "35");
			fullName = fullName.replace("W", "36");
			fullName = fullName.replace("X", "37");
			fullName = fullName.replace("Y", "38");
			fullName = fullName.replace("Z", "39");
			fullName = fullName.replace("Ñ", "40");
			fullName = "0" + fullName; // Agregando un cero al principio del
										// nombre completo
		}

		char[] first = fullName.toCharArray();// Declaración del array de
												// caracteres asignandole lo que
												// contiene el String fullName
												// convertido a chars.
		int second[] = new int[first.length];
		int third[] = new int[first.length];

		for (x = 0; x < first.length - 1; x++) {
			for (y = 0; y < first.length; y++) {
				// System.out.println("[" + x + "] " + first [x] );//Impresión
				// de los índices de manera ilustrativa
				c1 = first[x];// Obteniendo los elementos en el indice 0
				a = Character.getNumericValue(c1);
				a1 = String.valueOf(a);

				c11 = first[x + 1];// Obteniendo los elementos en el indice 1
				b = Character.getNumericValue(c11);
				a2 = String.valueOf(b);

				c = a1 + a2;// Los valores aún en String

				r1 = Integer.parseInt(c);// Primera y segunda cantidad en
											// enteros
				r2 = Integer.parseInt(a2);// Segunda cantidad
				second[x] = r1;
				third[x] = r2;
				third[x] = (second[x] * third[x]);// Multiplicamos los arreglos
													// donde second contiene la
													// primera
													// la "ab" y third contiene
													// sólo "b"
			}

			System.out.println("Variables" + " " + c + " " + " X " + a2 + " " + "=" + " " + third[x]);

		}

		for (int i = 0; i < third.length; i++) {
			suma += third[i];// Sumamos los valores de cada multiplicación
								// almacenados
			valores = Integer.toString(suma);// Convertimos nuestro resultado en
												// una cadena y se lo asignamos
												// a String valores

		}

		valores = valores.substring(valores.length() - 3);// Obtenemos los
															// últimos tres
															// digitos de
															// nuestro resultado
															// y se los pasamos
															// a valores
		dividendo = Integer.parseInt(valores);// Convertimos los tres últimos
												// digitos de nuestro resultado
												// a enteros para hacer la
												// división entre el factor 34

		System.out.println("La suma de ab x b =" + " " + valores);
		System.out.println("El resultado de la suma es:" + " " + suma + " "
				+ "Los tres últimos digitos de la suma en enteros son" + " " + valores);

		if (suma >= divisor) {// Obtenemos el cociente y residuo del divisor y
								// dividendo
			while ((dividendo - divisor) >= bx) {
				bx = divisor * ax;
				ax++;
			}
			System.out.println("El cociente es " + (ax - 1) + " y el residuo es " + (dividendo - bx));

		} else {
			System.out.println("El denominador debe ser menor/ Error al tratar de generar la homoclave");
		}

		cociente = (ax - 1);
		ccociente = Integer.toString(cociente);
		residuo = (dividendo - bx);
		rresiduo = Integer.toString(residuo);

		System.out.println("El cociente  String es " + ccociente + " y el residuo String es" + " " + rresiduo);

		// Asignación de los digitos 11 y 12 de acuerdo al cociente y el residuo
		// Valores resultantes posibles al dividir los tres ultimos digitos
		// obtenidos entre el factor 34 en la suma del resultado de las
		// multiplicaciones.
		String ct[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
				"17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33" };
		String vc[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
				"K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
		// Valores que se asignan a la clave diferenciadora de homonimio en base
		// al coeficiente y al residuo.

		for (int i = 0; i < ct.length; i++)// Recorremos el array ct para
											// comparar sus elementos con el
											// valor del cociente
		{
			if (ccociente.equals(ct[i])) // Comparamos el valor del cociente con
											// los elementos de nuestro arreglo
			{
				ccociente = vc[i];// Asignamos el digito correspondiente al
									// valor de cociente
				break;// Evitamos incremento del valor de ccociente
			} else {
			}
		}

		for (int i = 0; i < ct.length; i++) {
			if (rresiduo.equals(ct[i])) {
				rresiduo = vc[i];
				break;
			} else {
			}
		}

		String dc = rfc + ccociente + rresiduo;

		System.out.println("Prueba" + " " + dc);
		// Asignación de valores a las letras y números del registro federal de
		// contribuyentes formado a 12 posiciones ej: GODE 561231GR
		if (dc.contains("0") || dc.contains("1") || dc.contains("2") || dc.contains("3") || dc.contains("4")
				|| dc.contains("5") || dc.contains("6") || dc.contains("7") || dc.contains("8") || dc.contains("9")
				|| dc.contains("A") || dc.contains("B") || dc.contains("C") || dc.contains("D") || dc.contains("E")
				|| dc.contains("F") || dc.contains("G") || dc.contains("H") || dc.contains("I") || dc.contains("J")
				|| dc.contains("K") || dc.contains("L") || dc.contains("M") || dc.contains("N") || dc.contains("&")
				|| dc.contains("O") || dc.contains("P") || dc.contains("Q") || dc.contains("R") || dc.contains("S")
				|| dc.contains("T") || dc.contains("U") || dc.contains("V") || dc.contains("W") || dc.contains("X")
				|| dc.contains("Y") || dc.contains("Z") || dc.contains(" ") || dc.contains("Ñ")) {

			dc = dc.replace("0", "00");
			dc = dc.replace("1", "01");
			dc = dc.replace("2", "02");
			dc = dc.replace("3", "03");
			dc = dc.replace("4", "04");
			dc = dc.replace("5", "05");
			dc = dc.replace("6", "06");
			dc = dc.replace("7", "07");
			dc = dc.replace("8", "08");
			dc = dc.replace("9", "09");
			dc = dc.replace("A", "10");
			dc = dc.replace("B", "11");
			dc = dc.replace("C", "12");
			dc = dc.replace("D", "13");
			dc = dc.replace("E", "14");
			dc = dc.replace("F", "15");
			dc = dc.replace("G", "16");
			dc = dc.replace("H", "17");
			dc = dc.replace("I", "18");
			dc = dc.replace("J", "19");
			dc = dc.replace("K", "20");
			dc = dc.replace("L", "21");
			dc = dc.replace("M", "22");
			dc = dc.replace("N", "23");
			dc = dc.replace("&", "24");
			dc = dc.replace("O", "25");
			dc = dc.replace("P", "26");
			dc = dc.replace("Q", "27");
			dc = dc.replace("R", "28");
			dc = dc.replace("S", "29");
			dc = dc.replace("T", "30");
			dc = dc.replace("U", "31");
			dc = dc.replace("V", "32");
			dc = dc.replace("W", "33");
			dc = dc.replace("X", "34");
			dc = dc.replace("Y", "35");
			dc = dc.replace("Z", "36");
			dc = dc.replace(" ", "37");
			dc = dc.replace("Ñ", "38");

		}
		System.out.println("Asignación de valores" + " " + dc);

		/*
		 * Una vez asignados los valores se aplicara la siguiente forma tomando
		 * como base el factor 13 en orden descendente a cada letra y número del
		 * R.F.C. para su multiplicación, de acuerdo a la siguiente formula: (Vi
		 * * (Pi + 1)) + (Vi * (Pi + 1)) + ..............+ (Vi * (Pi + 1)) MOD
		 * 11
		 * 
		 * Vi Valor asociado al carácter de acuerdo a la tabla del Anexo III. Pi
		 * Posición que ocupa el i-esimo carácter tomando de derecha a izquierda
		 * es decir P toma los valores de 1 a 12.
		 * 
		 * Ejemplo: D = (16(13) + 25 (12) + 13 (11) + 14 (10) + 05 (9) + 06 (8)
		 * + 01 (7) + 02 (6) + 03 (5) + 01 (4) + 16 (3) + 28 (2)) = 1026 Donde
		 * el resultado será: 1026
		 */

		char[] val = dc.toCharArray();

		for (dx = 0; dx < val.length; dx++) {
			if (cx < val.length) {
				l1 = val[cx];
				n = Character.getNumericValue(l1);
				n1 = String.valueOf(n);

				l2 = val[cx + 1];
				m = Character.getNumericValue(l2);
				m2 = String.valueOf(m);

				v = n1 + m2;
				Vi = Integer.parseInt(v);
				D += (Vi * (Pi));
				System.out.println("Resultado" + " " + "Valores" + v);
				System.out.println("Resultado" + D);
			}
			Pi--;
			cx = cx + 2;
		}
		System.out.println("Resultado" + D);

		/*
		 * El resultado de la suma se divide entre el factor 11.
		 * 
		 * 93 cociente 11 | 1026 036 03 residuo
		 * 
		 * Si el residuo es igual a cero, este será el valor que se le asignara
		 * al dígito verificador. Si el residuo es mayor a cero se restara este
		 * al factor 11: 11-3 =8 Si el residuo es igual a 10 el dígito
		 * verificador será “A”. Si el residuo es igual a cero el dígito
		 * verificador será cero. Por lo tanto â€œ 8 â€œ es el dígito
		 * verificador de este ejemplo: GODE561231GR8.
		 */
		System.out.println("Prueba" + rfc);

		int dvs = 11, aax, bbx;
		aax = (int) (D / dvs);
		bbx = D % dvs;

		System.out.println("El cociente es: " + aax + " " + "El residuo es:" + " " + bbx);

		if (bbx == 0) {
			bbx = 0;
			System.out.println("RESULTADO FINAL..." + " " + "SU RFC ES:" + " " + rfc + ccociente + rresiduo + bbx);
		} else {
		}

		if (bbx > 0) {
			bbx = dvs - bbx;
			System.out.println("RESULTADO FINAL..." + " " + "SU RFC ES:" + " " + rfc + ccociente + rresiduo + bbx);
		} else {
		}

		if (bbx == 10) {
			String xbb = "A";
			System.out.println("RESULTADO FINAL..." + " " + "SU RFC ES:" + " " + rfc + ccociente + rresiduo + xbb);

		} else {
		}

		return rfc;
	}

	public String validateCURP(String apaterno, String amaterno, String nombre, String fechaNacimiento, String genero,
			String entidad) {
		 validateRFC(apaterno, amaterno,nombre, fechaNacimiento);
		// Se obtiene la primera consonante interna del apellido paterno

		lw1 = aPaterno2;
		lw1 = lw1.trim();

		lw2 = aMaterno2;
		lw2 = lw2.trim();

		lw3 = aNombre;
		System.out.println("aNombre" + aNombre);
		lw3 = lw3.trim();

		String vocales = "AEIOU";

		System.out.println("LW1" + lw1);
		System.out.println("LW2" + lw2);
		System.out.println("LW3" + lw3);

		char[] vl1 = lw1.toCharArray();
		char[] vl2 = lw2.toCharArray();
		char[] vl3 = lw3.toCharArray();

		try {
			if (lw1 != null && lw2 != null && lw3 != null) {

				for (int j = 0; lw1.length() > j && lw2.length() > j && lw3.length() > j; j++) {
					for (int k = 0; vocales.length() > k && lw1.length() > j && lw2.length() > j
							&& lw3.length() > j; k++) {

						if (vl1[j] == vocales.charAt(k) || vl2[j] == vocales.charAt(k) || vl3[j] == vocales.charAt(k)) {

							lw1 = lw1.substring(1);

							lw1 = lw1.replace("A", "").replace("E", "").replace("I", "").replace("O", "").replace("U",
									"");

							lw2 = lw2.substring(1);

							lw2 = lw2.replace("A", "").replace("E", "").replace("I", "").replace("O", "").replace("U",
									"");

							lw3 = lw3.substring(1);

							lw3 = lw3.replace("A", "").replace("E", "").replace("I", "").replace("O", "").replace("U",
									"");

							if (lw1.substring(0).equals("")) {

								lw1 = "X";
								System.out.println("LW1X: " + lw1);

							} else {

								lw1 = lw1.substring(0, 1);

								System.out.println("LW1: " + lw1);
							}
							if (lw2.substring(0).equals("")) {

								lw2 = "X";
								System.out.println("LW2X: " + lw2);

							} else {

								lw2 = lw2.substring(0, 1);

								System.out.println("LW2: " + lw2);
							}
							if (lw3.substring(0).equals("")) {

								lw3 = "X";

								System.out.println("LW3X: " + lw3);
							}

							else {

								lw3 = lw3.substring(0, 1);

								System.out.println("LW3: " + lw3);
							}

						}
					}
				}
			} else {

			}

			genero = catRepository.findByIdCatalogo(genero).getNombre();
			if(genero.equals("Femenino")) genero = "M";
			else genero = "H";
			
			entidad = catRepository.findByIdCatalogo(entidad).getNombre();
			entidad = getEntidad(entidad);
			
			if(lw1.equals("Ñ")||lw1.equals("ñ")) lw1 = "X";
			if(lw2.equals("Ñ")||lw2.equals("ñ")) lw2 = "X";
			if(lw3.equals("Ñ")||lw3.equals("ñ")) lw3 = "X";
			
			curp = curp + genero + entidad + lw1 + lw2 + lw3;

		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}

		System.out.println("CURP2: " + curp);

		return curp;
	}
	
	private String getEntidad(String entidad){
		String edo="";
		entidad = entidad.toUpperCase();
		switch (entidad){
		case "AGUASCALIENTES":
			edo = "AS";
			break;
		case "BAJA CALIFORNIA":
			edo = "BC";
			break;
		case "BAJA CALIFORNIA SUR":
			edo = "BS";
			break;
		case "CAMPECHE":
			edo = "CC";
			break;
		case "COAHUILA DE ZARAGOZA":
			edo = "CL";
			break;
		case "COLIMA":
			edo = "CM";
			break;
		case "CHIAPAS":
			edo = "CS";
			break;
		case "CHIHUAHUA":
			edo = "CH";
			break;
		case "DISTRITO FEDERAL":
			edo = "DF";
			break;
		case "DURANGO":
			edo = "DG";
			break;
		case "GUANAJUATO":
			edo = "GT";
			break;
		case "GUERRERO":
			edo = "GR";
			break;
		case "HIDALGO":
			edo = "HG";
			break;
		case "JALISCO":
			edo = "JC";
			break;
		case "ESTADO DE MÉXICO":
			edo = "MC";
			break;
		case "MICHOACÁN DE OCAMPO":
			edo = "MN";
			break;
		case "MORELOS":
			edo = "MS";
			break;
		case "NAYARIT":
			edo = "NT";
			break;
		case "NUEVO LEÓN":
			edo = "NL";
			break;
		case "OAXACA":
			edo = "OC";
			break;
		case "PUEBLA":
			edo = "PL";
			break;
		case "QUERÉTARO":
			edo = "QT";
			break;
		case "QUINTANA ROO":
			edo = "QR";
			break;
		case "SAN LUIS POTOSÍ":
			edo = "SP";
			break;
		case "SINALOA":
			edo = "SL";
			break;
		case "SONORA":
			edo = "SR";
			break;
		case "TABASCO":
			edo = "TC";
			break;
		case "TAMAULIPAS":
			edo = "TS";
			break;
		case "TLAXCALA":
			edo = "TL";
			break;
		case "VERACRUZ DE IGNACIO DE LA LLAVE":
			edo = "VZ";
			break;
		case "YUCATÁN":
			edo = "YN";
			break;
		case "ZACATECAS":
			edo = "ZS";
			break;
		}
		
		return edo;
	}

	public String convertDateToString(Date source, String format) {
		if (source == null)
			return null;
		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(source);
	}

	/*
	@Transactional
	public long generaId(String entidad)
	{
		GeneradorId genId = idRepository.findByEntidad(entidad);
		Date today =  new Date();
		Calendar cal =  Calendar.getInstance();
		cal.setTime(today);
		String folio = String.valueOf(cal.get(Calendar.YEAR)) + "10000000";
		long retVal =  Long.parseLong(folio);
		if (genId == null)
		{
			genId = new GeneradorId();
			genId.setEntidad(entidad);
			genId.setId(retVal);
		}
		retVal = genId.getId() + 1;
		genId.setId(retVal);
		idRepository.save(genId);
		return retVal;
	}*/
	
	public long getFullWeeks(Calendar d1, Calendar d2){

	    Instant d1i = Instant.ofEpochMilli(d1.getTimeInMillis());
	    Instant d2i = Instant.ofEpochMilli(d2.getTimeInMillis());

	    LocalDateTime startDate = LocalDateTime.ofInstant(d1i, ZoneId.systemDefault());
	    LocalDateTime endDate = LocalDateTime.ofInstant(d2i, ZoneId.systemDefault());

	    return ChronoUnit.WEEKS.between(startDate, endDate);
	}	
	
	public Organizacion getRutaTitular(Empleado emp) throws Exception
	{
		//En que Ruta está asignado
		Usuario usr = secServices.getPrincipal(emp.getNombreUsuario());
		List<Organizacion> ruta = orgRepository.findByTitularIdUsuario(usr.getIdUsuario());
		if (ruta.size()>0)
			return ruta.get(0);
		throw new Exception("El empleado no está asociado a una Ruta");		
	}
	
	public long semanaAnio(Date processDate)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(processDate);
		return cal.get(Calendar.WEEK_OF_YEAR);
	}
	
	public int anio(Date processDate)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(processDate);
		return cal.get(Calendar.YEAR);
	}

	public String diaSemana(Date processDate)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(processDate);
		int dow = cal.get(Calendar.DAY_OF_WEEK);
		String retVal = "";
		switch (dow)
		{
			case 1 :  retVal = "Domingo";
			break;
			case 2: retVal = "Lunes";
			break;
			case 3: retVal = "Martes";
			break;
			case 4: retVal = "Miércoles";
			break;
			case 5: retVal = "Jueves";
			break;
			case 6: retVal = "Viernes";
			break;
			case 7: retVal = "Sábado";
			break;
		}
		return retVal;		
	}
	public String mesAnio(Date processDate)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(processDate);
		int dow = cal.get(Calendar.MONTH);
		String retVal = "";
		switch (dow)
		{
			case 0 :  retVal = "Enero";
			break;
			case 1: retVal = "Febrero";
			break;
			case 2: retVal = "Marzo";
			break;
			case 3: retVal = "Abril";
			break;
			case 4: retVal = "Mayo";
			break;
			case 5: retVal = "Junio";
			break;
			case 6: retVal = "Julio";
			break;
			case 7: retVal = "Agosto";
			break;
			case 8: retVal = "Septiembre";
			break;
			case 9: retVal = "Octubre";
			break;
			case 10: retVal = "Noviembre";
			break;
			case 11: retVal = "Diciembre";
			break;
		}
		return retVal;		
	}
	public int trimestre(Date processDate)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(processDate);
		int dow = cal.get(Calendar.MONTH);
		int retVal=0;
		switch (dow)
		{
			case 0 :  
			case 1 :  
			case 2 :  
				retVal = 1;
				break;
			case 3 :  
			case 4 :  
			case 5 :  
				retVal = 2;
				break;
			case 6 :  
			case 7 :  
			case 8 :  
				retVal = 3;
				break;
			case 9 :  
			case 10 :  
			case 11 :  
				retVal = 4;
				break;
		}
		return retVal;		
	}
	public int semestre(Date processDate)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(processDate);
		int dow = cal.get(Calendar.MONTH);
		int retVal=0;
		switch (dow)
		{
			case 0 :  
			case 1 :  
			case 2 :  
			case 3 :  
			case 4 :  
			case 5 :  
				retVal = 1;
				break;
			case 6 :  
			case 7 :  
			case 8 :  
			case 9 :  
			case 10 :  
			case 11 :  
				retVal = 2;
				break;
		}
		return retVal;		
	}

	public DateRangeVO datesFromWeek(int week)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.WEEK_OF_YEAR, week);        
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		DateRangeVO retVal =  new DateRangeVO();
		retVal.setStartDate(cal.getTime());
		cal.set(Calendar.DAY_OF_WEEK,  Calendar.SATURDAY);
		retVal.setEndDate(cal.getTime());
		return retVal;		
	}

}


package com.crenx.data.domain.vo;

import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.UsuarioObjetoSeguridad;

public class UserRoleGroupModelMapper extends PropertyMap<UsuarioObjetoSeguridad,GroupVO>{
	   @Override
	    protected void configure() {
	        map().setId(source.getRole().getIdObjetoSeguridad());
	        map().setName(source.getRole().getLlaveNegocio());
	        map().setType("security-role");
	    }
}

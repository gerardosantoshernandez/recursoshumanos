package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario,String> {
	List<Usuario> findByNombreUsuario(String nombreUsuario);
	List<Usuario> findByRolesIdObjetoSeguridadIn(List<String> roles);
	List<Usuario> findByNombreIsLike(String nombre);
	List<Usuario> findByApellidoPaternoIsLike(String apellidoPaterno);
	List<Usuario> findByRolesIdObjetoSeguridadInAndNombreIsLike(List<String> roles, String nombre);
	List<Usuario> findByRolesIdObjetoSeguridadInAndApellidoPaternoIsLike(List<String> roles, String apellidoPaterno);
	List<Usuario> findByRolesIdObjetoSeguridadInAndNombreIsLikeAndApellidoPaternoIsLike(List<String> roles, String nombre, String apellidoPaterno);
	List<Usuario> findByNombreIsLikeAndApellidoPaternoIsLike(String nombre, String apellidoPterno);
	List<Usuario> findByRolesIdObjetoSeguridadInAndNombreIsLikeAndApellidoPaternoIsLikeAndEstatusActualIdCatalogoIn(List<String> roles, String nombre, String apellidoPaterno, List<String>  estatus);
	List<Usuario> findByNombreIsLikeAndApellidoPaternoIsLikeAndEstatusActualIdCatalogoIn(String nombre, String apellidoPterno, List<String> estatus);
}

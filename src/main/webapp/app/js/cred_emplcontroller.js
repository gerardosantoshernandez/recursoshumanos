'use strict';
angular.module('crenx.credemplcontrollers', [])
.controller(
		'DatosEmpleadoController',
function($scope,$http, $rootScope, apiService, $stateParams, $state,
		$timeout,$q) {
	$scope.filters = {
		tiposCredito : [],
		estatusCredito : [],
		nombre : null,
		apellidoPaterno :  null
	};

	$scope.empleados = [];
	var opts = {
			"closeButton" : true,
			"debug" : false,
			"positionClass" : "toast-top-right",
			"onclick" : null,
			"showDuration" : "300",
			"hideDuration" : "1000",
			"timeOut" : "5000",
			"extendedTimeOut" : "1000",
			"showEasing" : "swing",
			"hideEasing" : "linear",
			"showMethod" : "fadeIn",
			"hideMethod" : "fadeOut"
		};

	$scope.filters = {
			nombre : null,
			apellido :  null
		};

	var filterTextTimeout;
	$scope.$watch('filters.nombre', function(val) {
		if (filterTextTimeout)
			$timeout.cancel(filterTextTimeout);
		filterTextTimeout = $timeout(function() {
			loadData();
		}, 250); // delay 250 ms
	})

	$scope.$watch('filters.apellido', function(val) {
		if (filterTextTimeout)
			$timeout.cancel(filterTextTimeout);
		filterTextTimeout = $timeout(function() {
			loadData();
		}, 250); // delay 250 ms
	})
		
	function prodLoadCompleted(result) {
		$scope.empleados = result.data;
	}

	function loadData() {
		apiService.post('/api/creditoEmp/datosNomina', $scope.filters,
				prodLoadCompleted, serviceError);

	}

	function serviceError(response) {
		toastr.error(
				response.data.sourceMessage,
				response.data.errorMessage,
				opts);
	}
})
.controller(
		'CreditoEmpleadoController',
function($scope, $rootScope, apiService, $stateParams, $state,
		$timeout) {
	$scope.filters = {
		tiposCredito : [],
		estatusCredito : [],
		nombre : null,
		apellidoPaterno :  null
	};

	$scope.creditos = [];
	var opts = {
			"closeButton" : true,
			"debug" : false,
			"positionClass" : "toast-top-right",
			"onclick" : null,
			"showDuration" : "300",
			"hideDuration" : "1000",
			"timeOut" : "5000",
			"extendedTimeOut" : "1000",
			"showEasing" : "swing",
			"hideEasing" : "linear",
			"showMethod" : "fadeIn",
			"hideMethod" : "fadeOut"
		};

	$scope.filters = {
			nombre : null,
			apellidoPaterno :  null
		};

	var filterTextTimeout;
	$scope.$watch('filters.nombre', function(val) {
		if (filterTextTimeout)
			$timeout.cancel(filterTextTimeout);
		filterTextTimeout = $timeout(function() {
			loadData();
		}, 250); // delay 250 ms
	})

	$scope.$watch('filters.apellidoPaterno', function(val) {
		if (filterTextTimeout)
			$timeout.cancel(filterTextTimeout);
		filterTextTimeout = $timeout(function() {
			loadData();
		}, 250); // delay 250 ms
	})
	
	function loadTiposCredito() {
		apiService.post('/api/catalogo/items', 'CAT_TIPO_CRED_EMPL',
				function loadCompleted(result) {
					$scope.listaTiposCredito = result.data;
				}, serviceError);
	}

	function loadEstatusCredito() {
		apiService.post('/api/catalogo/items', 'CAT_ESTATUS_CREDITO',
				function loadCompleted(result) {
					$scope.listaEstatusCredito = result.data;
				}, serviceError);
	}
	
	function prodLoadCompleted(result) {
		$scope.creditos = result.data;
	}

	function loadData() {
		apiService.post('/api/creditoEmp', $scope.filters,
				prodLoadCompleted, serviceError);

	}

	function serviceError(response) {
		toastr.error(
				response.data.sourceMessage,
				response.data.errorMessage,
				opts);
	}

	loadTiposCredito();
	loadEstatusCredito();
}).
controller(
		'BaseNominaController',
		function($scope,$http, $rootScope, apiService, $stateParams, $state,
				$timeout,$q) {
			$scope.filters = {
				divisiones : [],
				regiones : [],
				gerencias : [],
				rutas : [],
				nombre : null,
				apellidoPaterno : null,
				periodo : null,
				noPeriodo : null,
				anio : null,
				tipoReporte:null
			};

			function prepareListItems(control, placeHolderVal) {
				$(control).select2({
					placeholder : placeHolderVal,
					allowClear : true
				}).on(
						'select2-open',
						function() {
							// Adding Custom Scrollbar
							$(this).data('select2').results.addClass(
									'overflow-hidden')
									.perfectScrollbar();
						}).on('change', function(e) {
					var source = $(this).context.id;
					if (source == 'periodo') {
						$scope.filters.periodo = e.val;
						loadData(e.val);
					}
					if (source == 'noPeriodo') {
						$scope.filters.noPeriodo = e.val;
					}

				});
			}

			function loadData(periodo) {
				if (periodo == 'x') {
					var listaItems = [];
					addSelectItems("#noPeriodo", listaItems);
				}
				if (periodo == '1') {// cargo numero semanas
					var listaItems = [];
					for (var i = 1; i <= 52; i++) {
						listaItems.push({
							id : i,
							name : "Semana " + i
						});
					}
					addSelectItems("#noPeriodo", listaItems);
				} else if (periodo == 2) {// cargo numero quincenas
					var listaItems = [];

					for (var i = 1; i <= 24; i++) {
						listaItems.push({
							id : i,
							name : "Quincena " + i
						})
					}
					addSelectItems("#noPeriodo", listaItems);
				}

			}
			jQuery(document).ready(function($) {
				prepareListItems("#periodo", 'Periodo');
				prepareListItems("#noPeriodo", 'NoPeriodo');
			});

			function addSelectItems(selectControl, listaItems) {
				$(selectControl).empty();
				$(selectControl).append(
						$("<option></option>").val("x").text(
								"Seleccione uno"));

				$.each(listaItems, function(key, value) {
					$(selectControl).append(
							$("<option></option>").val(value.id).text(
									value.name));
				});

			}

			$scope.generaReporteNomina = function(tipo) {
				if ($scope.filters.noPeriodo == 'x'
						|| $scope.filters.noPeriodo == null
						|| $scope.filters.periodo == 'x'
						|| $scope.filters.periodo == null
						|| $scope.filters.anio == null
						|| $scope.filters.anio == '')
					return;
			
				$scope.filters.tipoReporte=tipo;
				if(tipo=='html'){
					apiService.post('report/reporteBaseNomina',
							$scope.filters, lcoationLoadCompleted2,
							locationLoadFailed);
				}
				else{
					download();
				}
			}
			function download () {
				var defaultFileName = 'Report.xls';
			    var self = this;
			    var deferred = $q.defer();
			    $http.post('report/reporteBaseNominaExcel',$scope.filters, {responseType: "arraybuffer" }).then(function (data) {
			            var type = data.headers('Content-Type');
			            var disposition = data.headers('Content-Disposition');
			            if (disposition) {
			                var match = disposition.match(/.*filename=\"?([^;\"]+)\"?.*/);
			                if (match[1])
			                    defaultFileName = match[1];
			            }
			            defaultFileName = defaultFileName.replace(/[<>:"\/\\|?*]+/g, '_');
			            var blob = new Blob([data.data], { type: type });
			            saveAs(blob, defaultFileName);
			            deferred.resolve(defaultFileName);                    
			        }, function error(data) {
			           console.log(data.data);
			        });
			    return deferred.promise;
			}
			
			function lcoationLoadCompleted2(result) {
				var myWindow = window.open("", "", "");
				myWindow.document.write(result.data);
				myWindow.document.title="Reporte Nomina";
			    }

			
			function locationLoadFailed(response) {
				alert(response);
			}
			
			function loadData2() {
				apiService.post('/api/nomina/datos',
						$scope.filters, lcoationLoadCompleted,
						locationLoadFailed);
			}

			$scope.reload = function() {
				loadData2();
			}
			
			function lcoationLoadCompleted(result) {
				$scope.itemsBaseNomina = result.data;
			}
			

			$scope.dt = new Date();

			$scope.initDate = new Date('2016-15-20');
			$scope.formats = [ 'yyyy' ];
			$scope.format = $scope.formats[0];

			$scope.cambiaValor = function() {
				$scope.filters.anio = $scope.dt.getFullYear();
			}
			$scope.datepickerOptions = {
				datepickerMode : "'year'",
				minMode : "'year'",
				minDate : "minDate",
				showWeeks : "false",
			};

			
			
			
			
			function loadDataPre() {
				var listaItems = [];
				listaItems.push({
					id : 1,
					name : "Semanal"
				});
				listaItems.push({
					id : 2,
					name : "Quincenal"
				});
				addSelectItems("#periodo", listaItems);

			}
			loadDataPre();

		})

.controller(
		'CreditoEmpleadoFormController',
		function($scope, $rootScope, apiService, $location, $http,
				$cookieStore, $stateParams, $modal, $timeout) {
			
			$scope.listaEstatusCredito = $stateParams.listaEstatusCredito==null?null:$stateParams.listaEstatusCredito;
			$scope.listaTiposCredito = 	$stateParams.listaTiposCredito==null?null:$stateParams.listaTiposCredito;	
			$scope.listaEmpleados = [];
			$scope.filtroEmpleado = {nombre:null, apellido: null}
			$scope.credItem = {};
			
			var opts = {
					"closeButton" : true,
					"debug" : false,
					"positionClass" : "toast-top-right",
					"onclick" : null,
					"showDuration" : "300",
					"hideDuration" : "1000",
					"timeOut" : "5000",
					"extendedTimeOut" : "1000",
					"showEasing" : "swing",
					"hideEasing" : "linear",
					"showMethod" : "fadeIn",
					"hideMethod" : "fadeOut"
				};
			var filterTextTimeout;
			$scope.$watch('filtroEmpleado.nombre', function(val) {
				if (filterTextTimeout)
					$timeout.cancel(filterTextTimeout);
				filterTextTimeout = $timeout(function() {
					loadEmpleados();
				}, 250); // delay 250 ms
			})

			$scope.$watch('filtroEmpleado.apellido', function(val) {
				if (filterTextTimeout)
					$timeout.cancel(filterTextTimeout);
				filterTextTimeout = $timeout(function() {
					loadEmpleados();
				}, 250); // delay 250 ms
			})

			function loadEmpleados() {
				apiService.post('/api/cuenta/empleados', $scope.filtroEmpleado,
						function loadCompleted(result) {
							$scope.listaEmpleados = result.data;
						}, ServiceError);
			}

			function loadTiposCredito() {
				apiService.post('/api/catalogo/items', 'CAT_TIPO_CRED_EMPL',
						function loadCompleted(result) {
							$scope.listaTiposCredito = result.data.items;
						}, ServiceError);
			}

			
			function loadEstatusCredito() {
				apiService.post('/api/catalogo/items', 'CAT_ESTATUS_CREDITO',
						function loadCompleted(result) {
							$scope.listaEstatusCredito = result.data.items;
						}, ServiceError);
			}

			$scope.submitTheForm = function() {
				apiService.post('/api/creditoEmp/saveCreditoEmpl',
						$scope.credItem, ServiceSuccessComplete,
						ServiceError);
			}
			function ServiceSuccessComplete(result) {
				toastr.success(
						"Se guardó satisfactoriamente el crédito.",
						"Guardar crédito", opts);

				$scope.isUpdate = true;
				$scope.user = result.data;
				$scope.errorsubmit = '';

			}

			function ServiceError(response) {
				toastr.error(
						response.data.sourceMessage,
						response.data.errorMessage,
						opts);
			}

			if ($scope.listaEstatusCredito==null)
				loadEstatusCredito();
			if ($scope.listaTiposCredito==null)
				loadTiposCredito();

			if ($stateParams.credItem) {
				$scope.credItem = $stateParams.credItem;
			} else {
				$scope.credItem = {};
			}
			
		});
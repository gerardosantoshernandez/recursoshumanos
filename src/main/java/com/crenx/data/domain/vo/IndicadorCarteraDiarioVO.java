package com.crenx.data.domain.vo;

import java.util.Date;

public class IndicadorCarteraDiarioVO {

	private String idIndicadorCartera;
	private String division;
	private String idDivision;
	private String idRegion;
	private String region;
	private String idGerencia;
	private String gerencia;
	private String idRuta;
	private String ruta;
	private String producto;
	private long plazo=0;
	private Date fecha;
	private long creditosNuevos=0;
	private double creditosNuevosImp=0;
	private long creditosRecompra=0;
	private double creditosRecompraImp=0;
	private long creditosVencidos=0;
	private double creditosVencidosImp=0;
	private long creditoNormal=0;
	private double creditoNormalImp=0;
	private long creditoCorrectivo=0;
	private double creditoCorrectivoImp=0;
	private long creditoRiesgo=0;
	private double creditoRiesgoImp=0;
	private long creditoTotal=0;
	private double creditoTotalImp=0;
	private long creditoIncobrable=0;
	private double creditoIncobrableImp=0;
	private double carteraCapital;
	private String dia;
	private long semana;
	private String mes;
	private long trimestre;
	private long semestre;
	private long anio;
	
	public IndicadorCarteraDiarioVO
	(
			String idDivision,
			String division,
			String idRegion,
			String region,
			String idGerencia,
			String gerencia,
			String idRuta,
			String ruta,
			String producto,
			long plazo,
			Date fecha,
			long creditos_nuevos,
			double creditos_nuevos_imp,
			long creditos_recompra,
			double creditos_recompra_imp,
			long creditos_vencidos,
			double creditos_vencidos_imp,
			long creditos_normal,
			double creditos_normal_imp,
			long creditos_correctivo,
			double creditos_correctivo_imp,
			long creditos_riesgo,
			double creditos_riesgo_imp,
			long creditos_total,
			double creditos_total_imp,
			long creditos_incobrable,
			double creditos_incobrable_imp,
			double cartera_capital
	)
	{
		this.idDivision = idDivision;
		this.division = division;
		this.idRegion = idRegion;
		this.region = region;
		this.idGerencia = idGerencia;
		this.gerencia = gerencia;
		this.idRuta = idRuta;
		this.ruta = ruta;
		this.producto = producto;
		this.plazo = plazo;
		this.fecha = fecha;
		this.creditosNuevos = creditos_nuevos;
		this.creditosNuevosImp = creditos_nuevos_imp;
		this.creditosRecompra = creditos_recompra;
		this.creditosRecompraImp = creditos_recompra_imp;
		this.creditosVencidos = creditos_vencidos;
		this.creditosVencidosImp = creditos_vencidos_imp;
		this.creditoNormal = creditos_normal;
		this.creditoNormalImp = creditos_normal_imp;
		this.creditoCorrectivo = creditos_correctivo;
		this.creditoCorrectivoImp = creditos_correctivo_imp;
		this.creditoRiesgo = creditos_riesgo;
		this.creditoRiesgoImp = creditos_riesgo_imp;
		this.creditoTotal = creditos_total;
		this.creditoTotalImp = creditos_total_imp;
		this.creditoIncobrable = creditos_incobrable;
		this.creditoIncobrableImp = creditos_incobrable_imp;	
		this.carteraCapital = cartera_capital;
	}
			
	public IndicadorCarteraDiarioVO
	(
			String tipoPeriodo,
			String periodo,
			long creditos_nuevos,
			double creditos_nuevos_imp,
			long creditos_recompra,
			double creditos_recompra_imp,
			long creditos_vencidos,
			double creditos_vencidos_imp,
			long creditos_normal,
			double creditos_normal_imp,
			long creditos_correctivo,
			double creditos_correctivo_imp,
			long creditos_riesgo,
			double creditos_riesgo_imp,
			long creditos_total,
			double creditos_total_imp,
			long creditos_incobrable,
			double creditos_incobrable_imp,
			double cartera_capital
	)
	{
		switch (tipoPeriodo)
		{
			case "dia": this.dia = periodo;
			break;
			case "semana" : this.semana = Long.valueOf(periodo);
			break;
			case "mes" :  this.mes = periodo;
			break;
			case "trimestre" : this.trimestre = Long.valueOf(periodo);
			break;
			case "semestre" : this.semestre = Long.valueOf(periodo);
			break;
			case "anio" : this.anio = Long.valueOf(periodo);
			break;			
		}
		this.creditosNuevos = creditos_nuevos;
		this.creditosNuevosImp = creditos_nuevos_imp;
		this.creditosRecompra = creditos_recompra;
		this.creditosRecompraImp = creditos_recompra_imp;
		this.creditosVencidos = creditos_vencidos;
		this.creditosVencidosImp = creditos_vencidos_imp;
		this.creditoNormal = creditos_normal;
		this.creditoNormalImp = creditos_normal_imp;
		this.creditoCorrectivo = creditos_correctivo;
		this.creditoCorrectivoImp = creditos_correctivo_imp;
		this.creditoRiesgo = creditos_riesgo;
		this.creditoRiesgoImp = creditos_riesgo_imp;
		this.creditoTotal = creditos_total;
		this.creditoTotalImp = creditos_total_imp;
		this.creditoIncobrable = creditos_incobrable;
		this.creditoIncobrableImp = creditos_incobrable_imp;	
		this.carteraCapital = cartera_capital;
		
	}
	
	
	public String getIdIndicadorCartera() {
		return idIndicadorCartera;
	}
	public void setIdIndicadorCartera(String idIndicadorCartera) {
		this.idIndicadorCartera = idIndicadorCartera;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public long getPlazo() {
		return plazo;
	}
	public void setPlazo(long plazo) {
		this.plazo = plazo;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public long getCreditosNuevos() {
		return creditosNuevos;
	}
	public void setCreditosNuevos(long creditosNuevos) {
		this.creditosNuevos = creditosNuevos;
	}
	public double getCreditosNuevosImp() {
		return creditosNuevosImp;
	}
	public void setCreditosNuevosImp(double creditosNuevosImp) {
		this.creditosNuevosImp = creditosNuevosImp;
	}
	public long getCreditosRecompra() {
		return creditosRecompra;
	}
	public void setCreditosRecompra(long creditosRecompra) {
		this.creditosRecompra = creditosRecompra;
	}
	public double getCreditosRecompraImp() {
		return creditosRecompraImp;
	}
	public void setCreditosRecompraImp(double creditosRecompraImp) {
		this.creditosRecompraImp = creditosRecompraImp;
	}
	public long getCreditosVencidos() {
		return creditosVencidos;
	}
	public void setCreditosVencidos(long creditosVencidos) {
		this.creditosVencidos = creditosVencidos;
	}
	public double getCreditosVencidosImp() {
		return creditosVencidosImp;
	}
	public void setCreditosVencidosImp(double creditosVencidosImp) {
		this.creditosVencidosImp = creditosVencidosImp;
	}
	public long getCreditoNormal() {
		return creditoNormal;
	}
	public void setCreditoNormal(long creditoNormal) {
		this.creditoNormal = creditoNormal;
	}
	public double getCreditoNormalImp() {
		return creditoNormalImp;
	}
	public void setCreditoNormalImp(double creditoNormalImp) {
		this.creditoNormalImp = creditoNormalImp;
	}
	public long getCreditoCorrectivo() {
		return creditoCorrectivo;
	}
	public void setCreditoCorrectivo(long creditoCorrectivo) {
		this.creditoCorrectivo = creditoCorrectivo;
	}
	public double getCreditoCorrectivoImp() {
		return creditoCorrectivoImp;
	}
	public void setCreditoCorrectivoImp(double creditoCorrectivoImp) {
		this.creditoCorrectivoImp = creditoCorrectivoImp;
	}
	public long getCreditoRiesgo() {
		return creditoRiesgo;
	}
	public void setCreditoRiesgo(long creditoRiesgo) {
		this.creditoRiesgo = creditoRiesgo;
	}
	public double getCreditoRiesgoImp() {
		return creditoRiesgoImp;
	}
	public void setCreditoRiesgoImp(double creditoRiesgoImp) {
		this.creditoRiesgoImp = creditoRiesgoImp;
	}
	public long getCreditoTotal() {
		return creditoTotal;
	}
	public void setCreditoTotal(long creditoTotal) {
		this.creditoTotal = creditoTotal;
	}
	public double getCreditoTotalImp() {
		return creditoTotalImp;
	}
	public void setCreditoTotalImp(double creditoTotalImp) {
		this.creditoTotalImp = creditoTotalImp;
	}
	public long getCreditoIncobrable() {
		return creditoIncobrable;
	}
	public void setCreditoIncobrable(long creditoIncobrable) {
		this.creditoIncobrable = creditoIncobrable;
	}
	public double getCreditoIncobrableImp() {
		return creditoIncobrableImp;
	}
	public void setCreditoIncobrableImp(double creditoIncobrableImp) {
		this.creditoIncobrableImp = creditoIncobrableImp;
	}

	public double getCarteraCapital() {
		return carteraCapital;
	}



	public void setCarteraCapital(double carteraCapital) {
		this.carteraCapital = carteraCapital;
	}



	public String getDia() {
		return dia;
	}



	public void setDia(String dia) {
		this.dia = dia;
	}



	public long getSemana() {
		return semana;
	}



	public void setSemana(long semana) {
		this.semana = semana;
	}



	public String getMes() {
		return mes;
	}



	public void setMes(String mes) {
		this.mes = mes;
	}






	public long getTrimestre() {
		return trimestre;
	}



	public void setTrimestre(long trimestre) {
		this.trimestre = trimestre;
	}






	public long getSemestre() {
		return semestre;
	}



	public void setSemestre(long semestre) {
		this.semestre = semestre;
	}



	public long getAnio() {
		return anio;
	}



	public void setAnio(long anio) {
		this.anio = anio;
	}



	public String getIdDivision() {
		return idDivision;
	}



	public void setIdDivision(String idDivision) {
		this.idDivision = idDivision;
	}



	public String getIdRegion() {
		return idRegion;
	}



	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}



	public String getIdGerencia() {
		return idGerencia;
	}



	public void setIdGerencia(String idGerencia) {
		this.idGerencia = idGerencia;
	}



	public String getIdRuta() {
		return idRuta;
	}



	public void setIdRuta(String idRuta) {
		this.idRuta = idRuta;
	}

	
}

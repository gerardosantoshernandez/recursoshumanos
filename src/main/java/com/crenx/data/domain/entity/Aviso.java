package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.FetchType;

@Entity
public class Aviso {
	@Id @GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "id_aviso", unique = true)
	private String idAviso;
	
	@Column(name = "asunto",length = 100)
	private String asunto;
	
	@Column(name = "descripcion", length = 200)
	private String descripcion;
	
	@Column(name = "fecha_aviso")
	private Date fechaAviso;
	
	@Column(name = "hora_inicial",length = 20)
	private String horaInicial;
	
	@Column(name = "hora_final",length = 20)
	private String horaFinal;
	
	@Column(name = "lugar", length = 200)
	private String lugar;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_empleado", nullable = true, referencedColumnName = "id_empleado")
	private Empleado idEmpleado;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_aviso", nullable = false, referencedColumnName = "id_catalogo")
	private Catalogo idTipoAviso;

	public String getIdAviso() {
		return idAviso;
	}

	public void setIdAviso(String idAviso) {
		this.idAviso = idAviso;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Date getFechaAviso() {
		return fechaAviso;
	}

	public void setFechaAviso(Date fechaAviso) {
		this.fechaAviso = fechaAviso;
	}

	public String getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(String horaInicial) {
		this.horaInicial = horaInicial;
	}

	public String getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public Empleado getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Empleado idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public Catalogo getIdTipoAviso() {
		return idTipoAviso;
	}

	public void setIdTipoAviso(Catalogo idTipoAviso) {
		this.idTipoAviso = idTipoAviso;
	}
	
}

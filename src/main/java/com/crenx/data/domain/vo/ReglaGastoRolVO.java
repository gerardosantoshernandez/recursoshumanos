package com.crenx.data.domain.vo;

public class ReglaGastoRolVO {
	private String idRegla;
	private String idGasto;
	private String nombreGasto;
	private String idRol;
	private String nombreRol;
	public String getIdRegla() {
		return idRegla;
	}
	public void setIdRegla(String idRegla) {
		this.idRegla = idRegla;
	}
	public String getIdGasto() {
		return idGasto;
	}
	public void setIdGasto(String idGasto) {
		this.idGasto = idGasto;
	}
	public String getNombreGasto() {
		return nombreGasto;
	}
	public void setNombreGasto(String nombreGasto) {
		this.nombreGasto = nombreGasto;
	}
	public String getIdRol() {
		return idRol;
	}
	public void setIdRol(String idRol) {
		this.idRol = idRol;
	}
	public String getNombreRol() {
		return nombreRol;
	}
	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}

}

package com.crenx.data.domain.vo;

import java.util.Date;

import javax.persistence.Column;

import com.crenx.data.domain.entity.Domicilio;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CrearUserVO {

	private String idUsuario;
	private String nombreUsuario;
	private String contrasena;
	private String contrasenaConfirm;
	private String correoElectronico;
	private String nombre;
	private String apellidoMaterno;
	private String apellidoPaterno;
	
	@JsonProperty(required=true)
	private String tipoDocId;
	private String docIdentificacion;
	private String idGenero;
	@JsonProperty(required=true)
	private String fechaNacimiento;
	private String fechaIngreso;
	private String curp;
	private String idCargo;
	private String cveFiscal;
	private boolean activo=true;
	
	private double salarioBase=0;
	private double salarioAsimilado=0;
	private int semanasGracia = 0;	
	private String cveEmpleado;
	private String fechaBaja;
	
	private Domicilio domicilioEmpleado;
	private Domicilio domicilioEmpleadoLaboral;
	private String telefonoCasa;
	private String telefonoCelular;
	protected String idEntidadNacimiento;
	
	public String getIdEntidadNacimiento() {
		return idEntidadNacimiento;
	}
	public void setIdEntidadNacimiento(String idEntidadNacimiento) {
		this.idEntidadNacimiento = idEntidadNacimiento;
	}
	public String getTelefonoCasa() {
		return telefonoCasa;
	}
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}
	public String getTelefonoCelular() {
		return telefonoCelular;
	}
	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}
	public Domicilio getDomicilioEmpleadoLaboral() {
		return domicilioEmpleadoLaboral;
	}
	public void setDomicilioEmpleadoLaboral(Domicilio domicilioEmpleadoLaboral) {
		this.domicilioEmpleadoLaboral = domicilioEmpleadoLaboral;
	}
	public Domicilio getDomicilioEmpleado() {
		return domicilioEmpleado;
	}
	public void setDomicilioEmpleado(Domicilio domicilioEmpleado) {
		this.domicilioEmpleado = domicilioEmpleado;
	}
	public String getCveFiscal() {
		return cveFiscal;
	}
	public void setCveFiscal(String cveFiscal) {
		this.cveFiscal = cveFiscal;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getContrasenaConfirm() {
		return contrasenaConfirm;
	}
	public void setContrasenaConfirm(String contrasenaConfirm) {
		this.contrasenaConfirm = contrasenaConfirm;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getTipoDocId() {
		return tipoDocId;
	}
	public void setTipoDocId(String tipoDocId) {
		this.tipoDocId = tipoDocId;
	}
	public String getDocIdentificacion() {
		return docIdentificacion;
	}
	public void setDocIdentificacion(String docIdentificacion) {
		this.docIdentificacion = docIdentificacion;
	}
	public String getIdGenero() {
		return idGenero;
	}
	public void setIdGenero(String idGenero) {
		this.idGenero = idGenero;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getIdCargo() {
		return idCargo;
	}
	public void setIdCargo(String idCargo) {
		this.idCargo = idCargo;
	}
	public String getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	public double getSalarioBase() {
		return salarioBase;
	}
	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}
	public double getSalarioAsimilado() {
		return salarioAsimilado;
	}
	public void setSalarioAsimilado(double salarioAsimilado) {
		this.salarioAsimilado = salarioAsimilado;
	}
	public int getSemanasGracia() {
		return semanasGracia;
	}
	public void setSemanasGracia(int semanasGracia) {
		this.semanasGracia = semanasGracia;
	}
	public String getCveEmpleado() {
		return cveEmpleado;
	}
	public void setCveEmpleado(String cveEmpleado) {
		this.cveEmpleado = cveEmpleado;
	}
	public String getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	
	
	
}

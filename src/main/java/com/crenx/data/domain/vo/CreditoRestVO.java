package com.crenx.data.domain.vo;

public class CreditoRestVO {

	String tipo;
	double pagadoReal;
	CreditoVO original;
	CreditoVO destino;
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public CreditoVO getOriginal() {
		return original;
	}
	public void setOriginal(CreditoVO original) {
		this.original = original;
	}
	public CreditoVO getDestino() {
		return destino;
	}
	public void setDestino(CreditoVO destino) {
		this.destino = destino;
	}
	public double getPagadoReal() {
		return pagadoReal;
	}
	public void setPagadoReal(double pagadoReal) {
		this.pagadoReal = pagadoReal;
	}
	
}

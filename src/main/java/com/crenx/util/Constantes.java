package com.crenx.util;

public final class Constantes {
	public final static String NOMBRE_PROPERTIES = "rutas.properties";
	public final static String PROPIEDAD_RUTA = "ruta.archivosguardados";
	public final static String IDCLIENTE = "IdCliente";
	public final static String IDGASTO = "IdGasto";
	public final static String IDORG = "IdOrg";
	public final static String IDEMPLEADO = "IdEmpleado";
	public final static String IDCREDITIO = "IdCredito";
	public final static String TIPO_DOCUMENTO = "TipoDocumento";
	//FIXME Esto puede ser optimizado bajo un Enum
	public final static String[] IDS_ENTIDADES = new String[] { IDCLIENTE, IDCREDITIO, IDGASTO, IDORG, IDEMPLEADO };

	public final static String CAT_FIG = "CAT_FIG";
	public final static String CAT_TIP_DOC_ID = "CAT_TIP_DOC_ID";
	public final static String CAT_GEN = "CAT_GEN";
	public final static String CAT_PAIS = "CAT_PAIS";
	public final static String CAT_ENTIDAD_FED = "CAT_ENTIDAD_FED";
	public final static String CAT_OCUP = "CAT_OCUP";
	public final static String CAT_TIP_DOC = "CAT_TIP_DOC";
	public final static String CAT_FAM_PROD = "CAT_FAM_PROD";
	public final static String CAT_UNI_PLAZO = "CAT_UNI_PLAZO";
	public final static String CAT_FREC_COBRO = "CAT_FREC_COBRO";
	public final static String CAT_TIPO_GASTO = "CAT_TIPO_GASTO";
	public final static String CAT_MOTIVO_NOPAGO = "CAT_MOTIVO_NOPAGO";
	public final static String PERIODO_DIARIO = "PERIODO_DIARIO";

}

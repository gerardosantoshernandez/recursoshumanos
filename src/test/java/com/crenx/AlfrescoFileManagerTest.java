package com.crenx;

import java.io.File;

import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.data.RepositoryInfo;
import org.apache.chemistry.opencmis.commons.exceptions.CmisConnectionException;
import org.junit.Assert;
import org.junit.Test;

import com.crenx.business.FileUtils;
import com.crenx.business.alfresco.AlfrescoConnectContext;
import com.crenx.business.alfresco.AlfrescoFileManager;
import com.crenx.business.alfresco.AlfrescoSessionFactory;

/**
 * Prubas unitarias de los File Managers
 * 
 * @author JCHR
 *
 */
public class AlfrescoFileManagerTest {

	private final static String ALFRESCO_USER = "admin";
	private final static String ALFRESCO_PWD = "isolalfresco";
	private final static String ALFRESCO_HOST = "127.0.0.1";
	private final static int ALFRESCO_PORT = 8089;
	private final static String ALFRESCO_REPO = "-default-";

	/**
	 * Prueba la creación del archivo en Alfresco. Esta prueba siempre es
	 * exitosa. Para su correcta validación revisar el log de la prueba para
	 * 
	 */
	@Test
	public void testAlfrescoConnection() {
		try {
			final Session session = this.getAlfrescoConnection();
			Assert.assertNotNull(session);
			RepositoryInfo info = session.getRepositoryInfo();
			Assert.assertEquals("-default-", info.getId());
		} catch (CmisConnectionException exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * Prueba la creación del archivo en Alfresco. Esta prueba es exitosa en dos
	 * escenarios: 1.- Error en conexión 2.- El salvado del documento sea
	 * exitoso.
	 */
	@Test
	public void testCreateFile() {
		try {
			final Session session = this.getAlfrescoConnection();
			Assert.assertNotNull(session);
			final AlfrescoFileManager manager = new AlfrescoFileManager(session);
			// Creación usando rutas obsolutas
			File file = manager.save("Estoy en Alfresco".getBytes(), "/Shared", "test_Crenx.txt", "text/plain");
			// Creación usando rutas relativas
			Assert.assertNotNull(file);
			manager.setRelativePaths(true);
			file = manager.save("Estoy en Alfresco".getBytes(), "Shared", "test1_Crenx.txt", "text/plain");
			Assert.assertNotNull(file);
		} catch (CmisConnectionException cmisException) {
			cmisException.printStackTrace();
		} catch (Exception exception) {
			Assert.fail(exception.getMessage());
		}
	}

	/**
	 * Prueba de la obtención de un determinado Documento en Alfresco.Esta
	 * prueba es exitosa en dos escenarios: 1.- Error en conexión 2.- La
	 * obtención del documento es exitosa
	 * 
	 * Este test depende del anterior testAlfrescoCreateFile, po lo que es
	 * importante mantener el orden de los TEST
	 */
	@Test
	public void testAlfrescoGetFile() {
		try {
			final Session session = this.getAlfrescoConnection();
			Assert.assertNotNull(session);
			final AlfrescoFileManager manager = new AlfrescoFileManager(session);
			// Obtención con rutas absolutas
			File file = manager.get("/Shared", "test_Crenx.txt");
			Assert.assertNotNull(file);
			String content = FileUtils.fileToString(file);
			Assert.assertTrue("Estoy en Alfresco".compareTo(content.trim()) == 0);
			// Obtención con rutas relativas
			manager.setRelativePaths(true);
			file = manager.get("Shared", "test1_Crenx.txt");
			Assert.assertNotNull(file);
			content = FileUtils.fileToString(file);
			Assert.assertTrue("Estoy en Alfresco".compareTo(content.trim()) == 0);

		} catch (CmisConnectionException cmisException) {
			cmisException.printStackTrace();
		} catch (Exception exception) {
			Assert.fail(exception.getMessage());
		}
	}

	/**
	 * Prueba el borrado de un determinado Documento en Alfresco.Esta prueba es
	 * exitosa en dos escenarios: 1.- Error en conexión 2.- el borrado del
	 * documento es exitoso
	 * 
	 * Este test depende del anterior testAlfrescoCreateFile, po lo que es
	 * importante mantener el orden de los TEST
	 */
	@Test
	public void testAlfrescoDeleteFile() {
		try {
			final Session session = this.getAlfrescoConnection();
			Assert.assertNotNull(session);
			final AlfrescoFileManager manager = new AlfrescoFileManager(session);
			// Borrado con rutas absolutas
			manager.delete("/Shared", "test_Crenx.txt");
			Assert.assertFalse(manager.exists("/Shared", "test_Crenx.txt"));
			// Borrado con rutas relativas
			manager.setRelativePaths(true);
			manager.delete("Shared", "test1_Crenx.txt");
			Assert.assertFalse(manager.exists("Shared", "test1_Crenx.txt"));
		} catch (CmisConnectionException cmisException) {
			cmisException.printStackTrace();
		} catch (Exception exception) {
			Assert.fail(exception.getMessage());
		}
	}

	/**
	 * Prueba la creación de un Folder en Alfresco.Esta prueba es exitosa en dos
	 * escenarios: 1.- Error en conexión 2.- La creación del folder es exitoso
	 * 
	 */
	@Test
	public void testAlfrescoCreateDirectory() {
		try {
			final Session session = this.getAlfrescoConnection();
			Assert.assertNotNull(session);
			final AlfrescoFileManager manager = new AlfrescoFileManager(session);
			// Con rutas absolutas
			manager.createDirectory("/Shared", "test");
			Assert.assertTrue(manager.exists("/Shared/test", null));
			// Con rutas relativas
			manager.setRelativePaths(true);
			manager.createDirectory("Shared", "test1");
			Assert.assertTrue(manager.exists("Shared/test1", null));
		} catch (CmisConnectionException cmisException) {
			cmisException.printStackTrace();
		} catch (Exception exception) {
			Assert.fail(exception.getMessage());
		}
	}

	/**
	 * Retorna una conexión de Alfresco. Importante cambiar los datos de
	 * conexión en las constantes de esta clase
	 * 
	 * @return
	 */
	private Session getAlfrescoConnection() {
		final AlfrescoConnectContext context = new AlfrescoConnectContext(AlfrescoFileManagerTest.ALFRESCO_USER,
				AlfrescoFileManagerTest.ALFRESCO_PWD, AlfrescoFileManagerTest.ALFRESCO_HOST,
				AlfrescoFileManagerTest.ALFRESCO_PORT);
		return AlfrescoSessionFactory.createSessionAtomPub(AlfrescoFileManagerTest.ALFRESCO_REPO, context);
	}
}

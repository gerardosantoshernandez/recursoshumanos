package com.crenx.data.domain.vo;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

public class FiltrosOrgVO {

	Collection<NameValueVO> divisiones;
	Collection<NameValueVO> regiones;
	Collection<NameValueVO> gerencias;
	Collection<NameValueVO> rutas;
	
	Collection<String> divisionesIds;
	Collection<String> regionesIds;
	Collection<String> gerenciasIds;
	Collection<String> rutasIds;
	
	String nombre;
	String apellidoPaterno;
	String tipoNodo;
	String fechaInicial;
	String fechaFinal;
	String idTipoMov;
	String idTipoOperacion;
	
	boolean cajaRojo=false;
	
	String periodo;
	String nomPeriodo;
	long noPeriodo;
	int anio;
	String tipoReporte;
	
	public Collection<NameValueVO> getDivisiones() {
		if (divisiones==null)
			divisiones = new ArrayList<NameValueVO>();
		return divisiones;
	}
	public void setDivisiones(Collection<NameValueVO> divisiones) {
		this.divisiones = divisiones;
	}
	public Collection<NameValueVO> getRegiones() {
		if (regiones==null)
			regiones = new ArrayList<NameValueVO>();
		return regiones;
	}
	public void setRegiones(Collection<NameValueVO> regiones) {
		this.regiones = regiones;
	}
	public Collection<NameValueVO> getGerencias() {
		if (gerencias==null)
			gerencias = new ArrayList<NameValueVO>();
		return gerencias;
	}
	public void setGerencias(Collection<NameValueVO> gerencias) {
		this.gerencias = gerencias;
	}
	public Collection<NameValueVO> getRutas() {
		if (rutas==null)
			rutas = new ArrayList<NameValueVO>();
		return rutas;
	}
	public void setRutas(Collection<NameValueVO> rutas) {
		this.rutas = rutas;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getTipoNodo() {
		return tipoNodo;
	}
	public void setTipoNodo(String tipoNodo) {
		this.tipoNodo = tipoNodo;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public Collection<String> getDivisionesIds() {
		if (divisionesIds==null)
			divisionesIds = new ArrayList<String>();
		return divisionesIds;
	}
	public void setDivisionesIds(Collection<String> divisionesIds) {
		this.divisionesIds = divisionesIds;
	}
	public Collection<String> getRegionesIds() {
		if (regionesIds==null)
			regionesIds = new ArrayList<String>();
		return regionesIds;
	}
	public void setRegionesIds(Collection<String> regionesIds) {
		this.regionesIds = regionesIds;
	}
	public Collection<String> getGerenciasIds() {
		if (gerenciasIds==null)
			gerenciasIds = new ArrayList<String>();
		return gerenciasIds;
	}
	public void setGerenciasIds(Collection<String> gerenciasIds) {
		this.gerenciasIds = gerenciasIds;
	}
	public Collection<String> getRutasIds() {
		if (rutasIds==null)
			rutasIds = new ArrayList<String>();
		return rutasIds;
	}
	public void setRutasIds(Collection<String> rutasIds) {
		this.rutasIds = rutasIds;
	}
	public boolean isCajaRojo() {
		return cajaRojo;
	}
	public void setCajaRojo(boolean cajaRojo) {
		this.cajaRojo = cajaRojo;
	}
	public String getIdTipoMov() {
		return idTipoMov;
	}
	public void setIdTipoMov(String idTipoMov) {
		this.idTipoMov = idTipoMov;
	}
	public String getIdTipoOperacion() {
		return idTipoOperacion;
	}
	public void setIdTipoOperacion(String idTipoOperacion) {
		this.idTipoOperacion = idTipoOperacion;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	
	public long getNoPeriodo() {
		return noPeriodo;
	}
	public void setNoPeriodo(long noPeriodo) {
		this.noPeriodo = noPeriodo;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public String getTipoReporte() {
		return tipoReporte;
	}
	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
	public String getNomPeriodo() {
		return nomPeriodo;
	}
	public void setNomPeriodo(String nomPeriodo) {
		this.nomPeriodo = nomPeriodo;
	}
	
	/**
	 * Agrega un elemento al filtro 
	 * @param id
	 * @param name
	 * @param tipo
	 */
	public void addOrganizacion(String id, String name, String tipo) {
		if (StringUtils.isNotBlank(tipo) && StringUtils.isNotBlank(id)) {
			if (tipo.equals("D")) {
				this.getDivisiones().add(new NameValueVO(id, name));
			} else {
				if (tipo.equals("R")) {
					this.getRegiones().add(new NameValueVO(id, name));
				} else {
					if (tipo.equals("G")) {
						this.getGerencias().add(new NameValueVO(id, name));
					} else {
						if (tipo.equals("RR")) {
							this.getRutas().add(new NameValueVO(id, name));
						}
					}
				}
			}
		}
	}
	
}

package com.crenx.data.domain.vo;

import java.util.List;

public class FiltrosVacacionesVO {

	private String nombre;
	private List<String> asunto;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<String> getAsunto() {
		return asunto;
	}
	public void setAsunto(List<String> asunto) {
		this.asunto = asunto;
	}
}

package com.crenx.data.domain.vo;

import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Documento;

public class DocumentoModelMap extends PropertyMap<Documento, DocumentoVO> {

	@Override
	protected void configure() {
		// TODO Auto-generated method stub
		 map().setIdDocumento(source.getIdDocumento());
		 map().setNombre(source.getNombre());
		 map().setFileName(source.getFileName());
		 map().setFechaCreacion(source.getFechaCreacion());
	}

}

package com.crenx.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crenx.data.domain.entity.Empleado;
import com.crenx.data.domain.entity.ObjetoSeguridad;
import com.crenx.data.domain.entity.ObjetoSeguridadRelaciones;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Perfil;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.entity.UsuarioObjetoSeguridad;
import com.crenx.data.domain.repository.EmpleadoRepository;
import com.crenx.data.domain.repository.ObjetoSeguridadRelacionesRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.repository.UsuarioObjetoSeguridadRepository;
import com.google.common.collect.Lists;
import java.util.HashMap;

@Service
public class EmpleadoServices {

	@Autowired 
	EmpleadoRepository empRepository;
	@Autowired 
	OrgRepository orgRepository;
	@Autowired
	ObjetoSeguridadRelacionesRepository objSecRelRepository;
	@Autowired
	UsuarioObjetoSeguridadRepository usrObjRepository;
	
	public Perfil loadPerfil(Usuario user) throws Exception
	{
		Perfil retVal = new Perfil();
		Empleado emp = getEmpleadoNombreusuario(user.getNombreUsuario());
		retVal.setIdEmpleado(emp.getIdEmpleado());
		
		Organizacion titular = getTitularOrg(user.getIdUsuario());
		Organizacion suplente = getSuplenteOrg(user.getIdUsuario());
		if (titular!=null)
		{
			if (titular.getEstatus().getClave().equals("0"))
				throw new Exception ("El usuario está asignado a [" + titular.getNombre() + "] y esta unidad está deshabilitada" );
			retVal.setIdTitOrg(titular.getIdOrg());
			retVal.setNombreTitOrg(titular.getNombre());
			retVal.setTipoTitOrg(titular.getTipoNodo());
			retVal.setIdParentTit(titular.getParent().getIdOrg());
			retVal.setNombreParentTit(titular.getParent().getNombre());
		}
		if (suplente!=null)
		{
			if (suplente.getEstatus().getClave().equals("0"))
				throw new Exception ("El usuario está asignado a [" + suplente.getNombre() + "] y esta unidad está deshabilitada" );
			retVal.setIdSupOrg(suplente.getIdOrg());
			retVal.setNombreSupOrg(suplente.getNombre());
			retVal.setTipoSupOrg(suplente.getTipoNodo());
		}
		retVal.setRoles(rolesToList(user.getIdUsuario()));
		retVal.setPermisos(rolePermisosToList(user.getIdUsuario()));		
		return retVal;
	}
	public List<String> rolePermisosToList(String idUsuario)
	{
		ArrayList<String> listaPermisos = new ArrayList<String>();
		HashMap<String,String>  permisos = new HashMap<String,String>();
		List<UsuarioObjetoSeguridad> roles = usrObjRepository.findByUsuarioIdUsuario(idUsuario);
	
		for (UsuarioObjetoSeguridad oneRol: roles)
		{
			List<ObjetoSeguridadRelaciones>  realciones = objSecRelRepository.findByParentIdObjetoSeguridadAndChildEstatusActualClaveInterna(oneRol.getRole().getIdObjetoSeguridad(), "ESTATUS_ACTIVO");
			
			for (ObjetoSeguridadRelaciones oneRel:realciones)
			{
				if (!permisos.containsKey(oneRel.getChild().getLlaveNegocio()))
				{
					permisos.put(oneRel.getChild().getLlaveNegocio(), oneRel.getChild().getLlaveNegocio());
					listaPermisos.add(oneRel.getChild().getLlaveNegocio());
				}
			}
		}
		return listaPermisos;
	}

	
	public List<String> rolesToList(String idUsuario)
	{
		ArrayList<String> listaRoles = new ArrayList<String>();
	
		List<UsuarioObjetoSeguridad> roles = usrObjRepository.findByUsuarioIdUsuario(idUsuario);
		for (UsuarioObjetoSeguridad oneRol: roles)
		{
			listaRoles.add(oneRol.getRole().getLlaveNegocio());
		}
		return listaRoles;
	}
	
	public Empleado getEmpleadoNombreusuario(String nombreUsuario)
	{
		Empleado emp = empRepository.findByNombreUsuario(nombreUsuario);
		return emp;		
	}
	
	public Organizacion getTitularOrg(String idEmpleado)
	{
		Collection<Organizacion> org = orgRepository.findByTitularIdUsuario(idEmpleado);
		if (org.iterator().hasNext())
			return org.iterator().next();
		else return null;		
	}

	public Organizacion getSuplenteOrg(String idEmpleado)
	{
		Collection<Organizacion> org = orgRepository.findBySuplenteIdUsuario(idEmpleado);
		if (org.iterator().hasNext())
			return org.iterator().next();
		else return null;		
	}


}

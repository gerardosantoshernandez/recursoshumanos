package com.crenx.data.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Domicilio;

public interface DomicilioRepository extends CrudRepository<Domicilio,String> {

}

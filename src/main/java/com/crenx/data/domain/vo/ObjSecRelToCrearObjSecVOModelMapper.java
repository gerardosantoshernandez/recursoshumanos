package com.crenx.data.domain.vo;

import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.ObjetoSeguridadRelaciones;

public class ObjSecRelToCrearObjSecVOModelMapper extends PropertyMap<ObjetoSeguridadRelaciones, CrearObjSecVO>{
	   @Override
	    protected void configure() {
			map().setIdObjetoSeguridad(source.getChild().getIdObjetoSeguridad());
			map().setNombre(source.getChild().getNombre());
			map().setIdObjetoSeguridadParent(source.getParent().getIdObjetoSeguridad());
	    }
	   
}
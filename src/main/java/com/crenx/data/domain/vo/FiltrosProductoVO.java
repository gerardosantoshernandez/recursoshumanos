package com.crenx.data.domain.vo;
import java.util.List;

public class FiltrosProductoVO {
	private String nombre;
	private List<String> familias;
	private List<String> categorias;
	
	public List<String> getCategorias() {
		return categorias;
	}
	public void setCategorias(List<String> categorias) {
		this.categorias = categorias;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<String> getFamilias() {
		return familias;
	}
	public void setFamilias(List<String> familias) {
		this.familias = familias;
	}

	
}

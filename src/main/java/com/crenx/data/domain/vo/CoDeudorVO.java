package com.crenx.data.domain.vo;

public class CoDeudorVO extends ClienteVO{

	private String idCodeudor;	
	private String nombreCompleto;	
	public String getIdCodeudor() {
		return idCodeudor;
	}
	public void setIdCodeudor(String idCodeudor) {
		this.idCodeudor = idCodeudor;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
	/**
	 * Agrega un documento al codeudor
	 * 
	 * @param docto
	 *            . Docuemtno a agregar
	 * @param match
	 *            . Si es true el documento será agregado siempre y cuando este
	 *            este asignado al codeudor. Si es false siempre se agrega
	 */
	@Override
	public void agregaDocumento(final DocumentoVO docto, boolean match) {
		boolean add = (docto != null);
		if (add && match) {
			add = (this.idCodeudor.equals(docto.getObjetoRelacionado()));
		}
		if (add) {
			this.getDocumentos().add(docto);
		}
	}
}

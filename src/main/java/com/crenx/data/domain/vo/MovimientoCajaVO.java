package com.crenx.data.domain.vo;

public class MovimientoCajaVO {

	private String idMovCaja;
	private String idOrg;
	private String nombreOrg;
	private String idTipoMov;
	private String tipoMov;
	private String idTipoOperacion;
	private String tipoOperacion;
	private String idTipoGasto;
	private String tipoGasto;
	private String referencia;
	private String autorizador;
	private String fechaEmision;
	private String fechaSistema;
	private double cargo;
	private double abono;
	private String idUsuario;
	private String idDispositivo;
	private String referenciaLarga;
	private String division;
	private String region;
	private String gerencia;
	private String idDocto;
	private String observaciones;
	private String estatusMovimiento;
	private String ruta;
	
	
	
	
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getEstatusMovimiento() {
		return estatusMovimiento;
	}
	public void setEstatusMovimiento(String estatusMovimiento) {
		this.estatusMovimiento = estatusMovimiento;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getIdMovCaja() {
		return idMovCaja;
	}
	public void setIdMovCaja(String idMovCaja) {
		this.idMovCaja = idMovCaja;
	}
	public String getIdOrg() {
		return idOrg;
	}
	public void setIdOrg(String idOrg) {
		this.idOrg = idOrg;
	}
	public String getNombreOrg() {
		return nombreOrg;
	}
	public void setNombreOrg(String nombreOrg) {
		this.nombreOrg = nombreOrg;
	}
	public String getIdTipoMov() {
		return idTipoMov;
	}
	public void setIdTipoMov(String idTipoMov) {
		this.idTipoMov = idTipoMov;
	}
	public String getTipoMov() {
		return tipoMov;
	}
	public void setTipoMov(String tipoMov) {
		this.tipoMov = tipoMov;
	}
	public String getIdTipoOperacion() {
		return idTipoOperacion;
	}
	public void setIdTipoOperacion(String idTipoOperacion) {
		this.idTipoOperacion = idTipoOperacion;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public String getIdTipoGasto() {
		return idTipoGasto;
	}
	public void setIdTipoGasto(String idTipoGasto) {
		this.idTipoGasto = idTipoGasto;
	}
	public String getTipoGasto() {
		return tipoGasto;
	}
	public void setTipoGasto(String tipoGasto) {
		this.tipoGasto = tipoGasto;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getAutorizador() {
		return autorizador;
	}
	public void setAutorizador(String autorizador) {
		this.autorizador = autorizador;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getFechaSistema() {
		return fechaSistema;
	}
	public void setFechaSistema(String fechaSistema) {
		this.fechaSistema = fechaSistema;
	}
	public double getCargo() {
		return cargo;
	}
	public void setCargo(double cargo) {
		this.cargo = cargo;
	}
	public double getAbono() {
		return abono;
	}
	public void setAbono(double abono) {
		this.abono = abono;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public String getReferenciaLarga() {
		return referenciaLarga;
	}
	public void setReferenciaLarga(String referenciaLarga) {
		this.referenciaLarga = referenciaLarga;
	}

	/**
	 * Regresa el Id del Documento que fue asociado a un gasto o una
	 * trasferencia
	 * 
	 * @return El id del documento
	 */
	public String getIdDocto() {
		return idDocto;
	}

	/**
	 * Pone el Id del Documento que fue asociado a un gasto o una trasferencia
	 * 
	 * @param idDocto
	 *            Id del documento
	 */
	public void setIdDocto(String idDocto) {
		this.idDocto = idDocto;
	}
}

package com.crenx.data.domain.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Producto {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_producto", unique = true)
	private String idProducto;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="familia_prodcto_id", nullable=true, referencedColumnName="id_catalogo")
	private Catalogo familiaProducto;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="categoria_prodcto_id", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo categoriaProducto;
	
	@Column(name = "nombre",length = 100)
	private String nombre;
	
	@Column(name = "descripcion",length = 500)
	private String descripcion;
	
	@Column(name = "precio",length = 500)
	private double precio;
	
	@Column(name = "fecha_creación")
	private Date FechaCreacion;
	
	@Column(name = "estatus")
	private byte estatus;

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public Catalogo getFamiliaProducto() {
		return familiaProducto;
	}

	public void setFamiliaProducto(Catalogo familiaProducto) {
		this.familiaProducto = familiaProducto;
	}

	public Catalogo getCategoriaProducto() {
		return categoriaProducto;
	}

	public void setCategoriaProducto(Catalogo categoriaProducto) {
		this.categoriaProducto = categoriaProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Date getFechaCreacion() {
		return FechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}

	public byte getEstatus() {
		return estatus;
	}

	public void setEstatus(byte estatus) {
		this.estatus = estatus;
	}
	
}

package com.crenx.services;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.crenx.business.FileManager;
import com.crenx.business.FileManagerFactory;
import com.crenx.business.common.FilterElement;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Documento;
import com.crenx.data.domain.entity.PropiedadesDoc;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.DocumentoRepository;
import com.crenx.data.domain.repository.PropiedadesDocRepository;
import com.crenx.data.domain.vo.DocumentoVO;
import com.crenx.data.domain.vo.FiltroDocumentosVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.util.Constantes;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

@Service
public class DocumentoServices {
	private final Logger LOG = LogManager.getLogger(DocumentoServices.class);
	@Autowired
	private DocumentoRepository documentoRepository;
	@Autowired
	private FileManagerFactory fileManagerFactory;
	@Autowired
	private CatalogoRepository catRepository;
	@Autowired
	private PropiedadesDocRepository propsRepository;

	/**
	 * Regresa el detalle de un determinado documento
	 * 
	 * @param idDocto
	 * @return
	 */
	public DocumentoVO obtenerDetalleDocumento(final String idDocto) throws IllegalArgumentException {
		final DocumentoVO result = new DocumentoVO();
		final Documento docto = documentoRepository.findByIdDocumento(idDocto);
		if (docto == null) {
			throw new IllegalArgumentException("El documento con id " + idDocto + " no existe registrado");
		}
		final List<PropiedadesDoc> propiedades = docto.getPropiedadesDocs();
		result.setContentType(docto.getContentType());
		result.setDescripcion(docto.getDescripcion());
		result.setFechaCreacion(docto.getFechaCreacion());
		result.setFileName(docto.getFileName());
		result.setIdAutor(docto.getIdAutor());
		result.setIdDocumento(docto.getIdDocumento());
		result.setIdParent(docto.getIdParent());
		result.setNombre(docto.getNombre());
		result.setReferencia(docto.getReferencia());
		result.setTipo(docto.getTipo());
		if (propiedades != null && propiedades.size() > 0) {
			Optional<PropiedadesDoc> tipoDocto = Iterables.tryFind(propiedades, new Predicate<PropiedadesDoc>() {
				@Override
				public boolean apply(PropiedadesDoc prop) {
					return (prop != null && prop.getNombre().equals(Constantes.TIPO_DOCUMENTO));
				}
			});

			Optional<PropiedadesDoc> relacion = Iterables.tryFind(propiedades, new Predicate<PropiedadesDoc>() {
				@Override
				public boolean apply(PropiedadesDoc prop) {
					return (prop != null && Arrays.asList(Constantes.IDS_ENTIDADES).contains(prop.getNombre()));
				}
			});
			result.setTipoDocumento((tipoDocto.isPresent()) ? tipoDocto.get().getValor() : null);
			result.setObjetoRelacionado((relacion.isPresent()) ? tipoDocto.get().getValor() : null);
		}
		return result;
	}

	/**
	 * Obtiene la lista de detalles de los documentos que estan referenciados a
	 * una determinada propiedad
	 * 
	 * @param name
	 * @param value
	 * @return
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public List<DocumentoVO> obtenerDetalleDocumentos(final FiltroDocumentosVO filtro) {
		final List<String> names = new ArrayList<String>();
		final List<String> values = new ArrayList<String>();
		List<DocumentoVO> result = null;
		if (filtro != null) {
			final Field[] fields = FiltroDocumentosVO.class.getDeclaredFields();
			Object value = null;
			for (Field field : fields) {
				try {
					Method getter = FiltroDocumentosVO.class
							.getDeclaredMethod("get" + StringUtils.capitalize(field.getName()), new Class[] {});
					value = getter.invoke(filtro, new Object[] {});
				} catch (Exception exception) {
					LOG.error("DocumentoController : ", exception);
					value = null;
				}
				if (value != null) {
					FilterElement filter = field.getAnnotation(FilterElement.class);
					names.add(filter.name());
					values.add(value.toString());
				}
			}
		}
		if (!values.isEmpty()) {
			result = Lists.newArrayList(documentoRepository.findByNamesAndValuesProperties(names, values));
		}
		return result;
	}

	/**
	 * Registro los datos de un Documento y lo guarda en un determinado
	 * repositorio
	 * 
	 * @param file
	 *            - Multi part que repersenta el archivo. Este contiene la
	 *            secuencia de bytes.
	 * @param idEntity
	 *            - ID de la entidad al que pertenece el Documento: cliente,
	 *            usuario, movimiento caja, etcétera.
	 * @param idTipoDocto.
	 *            ID del tipo de documento. Ver catálogo de tipos de documento
	 * @param propiedades.
	 *            Por default al guardar un Documento se viculan dos propiedades
	 *            que son: la propiedad del tipo de documento (idTipoDocto) y la
	 *            propiedad de la entidad (idEntity). Si se desea vincular más
	 *            propiedades se debe de pasar este parámetro que es una lista
	 *            de valores NameValueVO donde NameValueVO.name sera el nombre
	 *            de la propiedad y NameValueVO.id su valor.
	 * @param entityType.
	 *            El tipo al cual pertenece la entidad a la que pertenece el
	 *            Documento: cliente, usuario, movimiento caja, etcétera.
	 * @param salvarEnRepositorio.
	 *            Si es true guarda el Documento en el repositorio configurado.
	 *            Ver com.crenx. RepositoryConfiguration
	 * @throws IllegalArgumentException:
	 * @throws IOException
	 */
	public void salvarDocumento(final MultipartFile file, final String idEntity, final String idTipoDocto,
			final List<NameValueVO> propiedades, final String entityType, final boolean salvarEnRepositorio)
			throws IllegalArgumentException, IOException {
		if (StringUtils.isBlank(idEntity) && StringUtils.isNotBlank(entityType)) {
			throw new IllegalArgumentException("El id de la entidad es null o vacio");
		}
		if (file == null) {
			throw new IllegalArgumentException("El archivo es null" + idTipoDocto);
		}
		final Catalogo tipo = catRepository.findByIdCatalogo(idTipoDocto);
		// Referencia al tipo de documento
		if (tipo == null) {
			throw new IllegalArgumentException("No existe el catalogo de tipo de documento: " + idTipoDocto);
		}
		final String mimeType = file.getContentType();
		final String filename = file.getOriginalFilename();
		final String nombreArchivo = UUID.randomUUID().toString() + filename;

		Documento documento = new Documento();
		documento.setNombre(filename);
		documento.setDescripcion(filename);
		documento.setFileName(nombreArchivo);
		documento.setContentType(mimeType);
		documento.setFechaCreacion(new Date());
		final PropiedadesDoc referenciaEntidad = new PropiedadesDoc();
		referenciaEntidad.setNombre(entityType);
		referenciaEntidad.setValor(idEntity);
		referenciaEntidad.setDocumento(documento);
		final PropiedadesDoc referenciaDoctoTipo = new PropiedadesDoc();
		referenciaDoctoTipo.setNombre(Constantes.TIPO_DOCUMENTO);
		referenciaDoctoTipo.setValor(tipo.getNombre());
		referenciaDoctoTipo.setDocumento(documento);
		documento.setPropiedadesDocs(Lists.newArrayList(referenciaEntidad, referenciaDoctoTipo));
		if (salvarEnRepositorio) {
			final FileManager manager = this.fileManagerFactory.createFileManager();
			manager.save(file.getBytes(), null, nombreArchivo, mimeType);
		}
		documento.getPropiedadesDocs().addAll(this.getPropiedades(propiedades, documento));
		documento = documentoRepository.save(documento);
	}

	/**
	 * Guarda una nueva propiedad relacionada a un documento en específico. Se
	 * hace uso del objeto NameValueVO en donde su atributo id y nombre seran
	 * los valores de valor y nombre respectivamente de la propiedad
	 * 
	 * @param idDocto
	 * @param nombre
	 * @param valor
	 */
	public void agregarPropiedadesDocumento(final String idDocto, final List<NameValueVO> propiedades) {
		if (StringUtils.isNotBlank(idDocto) && propiedades != null) {
			final Documento docto = this.documentoRepository.findByIdDocumento(idDocto);
			if (docto != null) {
				propsRepository.save(this.getPropiedades(propiedades, docto));
			}
		}
	}

	/**
	 * Elimina los registros del Documento lógica y físicamente
	 * 
	 * @param idDocto
	 *            . Id del documento a eliminar
	 * @param eliminarDeRepositorio
	 *            . Si es true, el Documento se elimina físicamente del
	 *            repositorio donde se encuentra contenido
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	public void eliminarDocumento(final String idDocto, final boolean eliminarDeRepositorio)
			throws IllegalArgumentException, IOException {
		Documento doc = documentoRepository.findByIdDocumento(idDocto);
		if (eliminarDeRepositorio) {
			final FileManager manager = this.fileManagerFactory.createFileManager();
			manager.delete(null, doc.getFileName());
		}
		documentoRepository.delete(doc);
	}

	/**
	 * @param idDocto
	 * @return
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	public File obtenerDocumento(final String idDocto) throws IllegalArgumentException, IOException {
		Documento doc = documentoRepository.findByIdDocumento(idDocto);
		final FileManager manager = this.fileManagerFactory.createFileManager();
		return manager.get(null, doc.getFileName());
	}

	/**
	 * Convierte un conjunto de elementos nombre-valor en propiedades
	 * 
	 * @param propiedades
	 * @return
	 */
	private List<PropiedadesDoc> getPropiedades(final List<NameValueVO> propiedades, final Documento docto) {
		final List<PropiedadesDoc> props = new ArrayList<PropiedadesDoc>();
		if (propiedades != null) {
			PropiedadesDoc propDocto = null;
			for (NameValueVO propiedad : propiedades) {
				if (propiedad != null && StringUtils.isNotBlank(propiedad.getId())
						&& StringUtils.isNotBlank(propiedad.getName())) {
					propDocto = new PropiedadesDoc();
					propDocto.setNombre(propiedad.getName());
					propDocto.setValor(propiedad.getId());
					propDocto.setDocumento(docto);
					props.add(propDocto);
				}
			}
		}
		return props;
	}
}

package com.crenx.controller;
import java.lang.reflect.Type;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.vo.CajaVO;
import com.crenx.data.domain.vo.ClienteTraspasoVO;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.CreditoTraspasoVO;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltrosOrgClienteVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.FondeoVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.data.domain.vo.OrgToNameValueVOModelMapper;
import com.crenx.data.domain.vo.OrgToTreeModelMap;
import com.crenx.data.domain.vo.OrgVO;
import com.crenx.data.domain.vo.TraspasoClienteVO;
import com.crenx.security.UserAuthentication;
import com.crenx.services.CatalogoServices;
import com.google.common.reflect.TypeToken;

@RestController
@RequestMapping("/api/org")
public class OrgRestController {

	@Autowired
	private OrgRepository orgRepository;
	@Autowired
	private CatalogoServices catServices;
	
	
	@RequestMapping(value="tree", method = RequestMethod.POST)
	ResponseEntity<?>  getTree(@RequestBody String orgId) {
		try
		{
			ArrayList<OrgVO> result = new ArrayList<OrgVO>();
			OrgVO orgVo = new OrgVO();
			Organizacion tree = null;
			if (orgId.equals("null"))
				tree = orgRepository.findByClave("ROOT");
			else
				tree = orgRepository.findOne(orgId);
			ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new OrgToTreeModelMap());
			orgVo = mapper.map(tree, OrgVO.class);
			tree = catServices.getChildren(tree, orgVo, null, mapper, false);
			result.add(orgVo);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		}
		catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al recuperar los datos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}			
	}

	@RequestMapping(value="tipoNodo", method = RequestMethod.POST)
	ResponseEntity<?>  getPorTipoNodo(@RequestBody String tipoNodo) {
		try
		{
			List<Organizacion> items = orgRepository.findByTipoNodo(tipoNodo);

			ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new OrgToNameValueVOModelMapper());
			Type listType = new TypeToken<List<NameValueVO>>() {}.getType();
			List<NameValueVO> listaNodos = mapper.map(items, listType);
			return new ResponseEntity<>(listaNodos, HttpStatus.CREATED);
		}
		catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al recuperar los datos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}			
	}
	
	
	
	@RequestMapping(value="filteredTree", method = RequestMethod.POST)
	FiltrosOrgVO getFilteredTree(@RequestBody String orgId) {
		Organizacion tree = catServices.getNodoById(orgId);
		OrgVO orgVo = new OrgVO();
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new OrgToTreeModelMap());
		orgVo = mapper.map(tree, OrgVO.class);
		FiltrosOrgVO filtros = new FiltrosOrgVO();
		tree = catServices.getChildren(tree, orgVo, filtros, mapper, true);
		switch(tree.getTipoNodo())
		{
		case "D":
			filtros.getDivisiones().add(new NameValueVO(tree.getIdOrg(), tree.getNombre()));
			break;
		case "R":
			filtros.getRegiones().add(new NameValueVO(tree.getIdOrg(), tree.getNombre()));
			break;
		case "G":
			filtros.getGerencias().add(new NameValueVO(tree.getIdOrg(), tree.getNombre()));
			break;		
		case "RR":
			filtros.getRutas().add(new NameValueVO(tree.getIdOrg(), tree.getNombre()));
			break;		
		}
		return filtros;		
	}
	
	
	@RequestMapping(value="transferTree", method = RequestMethod.POST)
	FiltrosOrgVO getTransferTree(@RequestBody String orgId) {
		Organizacion tree = catServices.getNodoById(orgId);
		OrgVO orgVo = new OrgVO();
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new OrgToTreeModelMap());
		orgVo = mapper.map(tree, OrgVO.class);
		FiltrosOrgVO filtros = new FiltrosOrgVO();
		catServices.getDirectChildren(tree.getParent(), orgVo, filtros, mapper, true);
		filtros.getRutas().add(new NameValueVO(tree.getParent().getIdOrg(), tree.getParent().getNombre()));
		//addAlltoRutas(filtros);
		FiltrosOrgVO childFiltros = new FiltrosOrgVO();
		catServices.getChildren(tree, orgVo, filtros, mapper, true);
		addAlltoRutas(filtros);
		removeCurrent(filtros, tree);
		return filtros;		
	}

	private void removeCurrent(FiltrosOrgVO filtros, Organizacion tree)
	{
		for (NameValueVO oneItem : filtros.getRutas())
		{
			if (oneItem.getId().equals(tree.getIdOrg()))
			{
				filtros.getRutas().remove(oneItem);
				break;
			}
		}
	}
	private void addAlltoRutas(FiltrosOrgVO filtros)
	{
		for (NameValueVO oneItem : filtros.getDivisiones())
		{
			filtros.getRutas().add(oneItem);
		}
		for (NameValueVO oneItem : filtros.getRegiones())
		{
			filtros.getRutas().add(oneItem);
		}
		for (NameValueVO oneItem : filtros.getGerencias())
		{
			filtros.getRutas().add(oneItem);
		}
	}

	@RequestMapping(value="limitedTree", method = RequestMethod.POST)
	FiltrosOrgVO getLimitedTree(@RequestBody FiltrosOrgClienteVO filtros) {
		FiltrosOrgVO retVal = new FiltrosOrgVO();
		retVal.setTipoNodo(filtros.getTipoNodo());
		for (String oneFilter: filtros.getFiltros())
		{
			Organizacion tree = catServices.getNodoById(oneFilter);
			OrgVO orgVo = new OrgVO();
			ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new OrgToTreeModelMap());
			orgVo = mapper.map(tree, OrgVO.class);
			tree = catServices.getChildren(tree, orgVo, retVal, mapper, true);			
		}
		return retVal;		
	}
	
	
	
	@RequestMapping(value="unAssigned", method = RequestMethod.GET)
	ResponseEntity<?>  getAssignedUnits() {
		try
		{
			return new ResponseEntity<>(catServices.getAssignedNodes(), HttpStatus.ACCEPTED);
		}
		catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al recuperar los datos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}			
	}
	
	@RequestMapping(value="calendarios", method = RequestMethod.GET)
	ResponseEntity<?>  getCalendarios() {
		try
		{
			return new ResponseEntity<>(catServices.allCalendario(), HttpStatus.ACCEPTED);
		}
		catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al recuperar los datos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}			
	}
	
	@RequestMapping(value="savenode", method = RequestMethod.POST)
	ResponseEntity<?>  saveTree(Principal principal,@RequestBody OrgVO orgNode) {
		try
		{
			
			return new ResponseEntity<>(catServices.saveNodo(orgNode), HttpStatus.CREATED);
		}
		catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}			
	}	
}

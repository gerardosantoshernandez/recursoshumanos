package com.crenx.data.domain.vo;

import java.util.Calendar;
import java.util.Date;

public class HorarioVO {

	
	
	public final int Domingo = 64;	
	public final int Lunes = 1;
	public final int Martes = 2;
	public final int Miercoles = 4;
	public final int Jueves = 8;
	public final int Viernes = 16;
	public final int Sabado = 32;
	
	private String idCalendario;
	private String nombreCalendario;
	private String nombreHorario;
	private String idHorario;
	private String horaInicial;
	private String horaFinal;
	private boolean domingo;
	private boolean lunes;
	private boolean martes;
	private boolean miercoels;
	private boolean jueves;
	private boolean viernes;
	private boolean sabado;
	private int diasActivos;
		
	public String getIdCalendario() {
		return idCalendario;
	}

	public void setIdCalendario(String idCalendario) {
		this.idCalendario = idCalendario;
	}

	public String getNombreCalendario() {
		return nombreCalendario;
	}

	public void setNombreCalendario(String nombreCalendario) {
		this.nombreCalendario = nombreCalendario;
	}

	public String getNombreHorario() {
		return nombreHorario;
	}

	public void setNombreHorario(String nombreHorario) {
		this.nombreHorario = nombreHorario;
	}

	public String getIdHorario() {
		return idHorario;
	}

	public void setIdHorario(String idHorario) {
		this.idHorario = idHorario;
	}

	public String getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(String horaInicial) {
		this.horaInicial = horaInicial;
	}

	public String getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}

	public boolean isDomingo() {
		return domingo;
	}

	public void setDomingo(boolean domingo) {
		this.domingo = domingo;
	}

	public boolean isLunes() {
		return lunes;
	}

	public void setLunes(boolean lunes) {
		this.lunes = lunes;
	}

	public boolean isMartes() {
		return martes;
	}

	public void setMartes(boolean martes) {
		this.martes = martes;
	}

	public boolean isMiercoels() {
		return miercoels;
	}

	public void setMiercoels(boolean miercoels) {
		this.miercoels = miercoels;
	}

	public boolean isJueves() {
		return jueves;
	}

	public void setJueves(boolean jueves) {
		this.jueves = jueves;
	}

	public boolean isViernes() {
		return viernes;
	}

	public void setViernes(boolean viernes) {
		this.viernes = viernes;
	}

	public boolean isSabado() {
		return sabado;
	}

	public void setSabado(boolean sabado) {
		this.sabado = sabado;
	}

	public int getDiasActivos() {
		return diasActivos;
	}

	public void setDiasActivos(int diasActivos) {
		this.diasActivos = diasActivos;
	}

	public int getDomingo() {
		return Domingo;
	}

	public int getLunes() {
		return Lunes;
	}

	public int getMartes() {
		return Martes;
	}

	public int getMiercoles() {
		return Miercoles;
	}

	public int getJueves() {
		return Jueves;
	}

	public int getViernes() {
		return Viernes;
	}

	public int getSabado() {
		return Sabado;
	}

	
}

package com.crenx.data.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Catalogo;

import scala.concurrent.duration.FiniteDuration;

public interface CatalogoRepository extends CrudRepository<Catalogo,String> {

	Catalogo findByClaveInterna(String claveInterna);
	List<Catalogo> findByParentId(String parentId);
	List<Catalogo> findByClaveInternaAndClave(String claveInterna, String clave);	
	Catalogo findByNombre(String nombre);
	Catalogo findByIdCatalogo(String idCatalogo);
	List<Catalogo> findByClaveInternaIn(List<String> items);
	List<Catalogo> findByIdCatalogoIn(List<String> items);
	Catalogo findByNombreAndClaveInternaIsLike(String nombre, String clave);
	List<Catalogo> findByClaveInternaIsLike(String patron);
}

package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.UsuarioObjetoSeguridad;

public interface UsuarioObjetoSeguridadRepository extends CrudRepository<UsuarioObjetoSeguridad,String> {

	List<UsuarioObjetoSeguridad> findByUsuarioIdUsuarioAndRoleIdObjetoSeguridad(String idUsuario, String idObjetoSeguridad);
	List<UsuarioObjetoSeguridad> findByUsuarioIdUsuario(String idUsuario);
	List<UsuarioObjetoSeguridad> findByUsuarioNombreUsuario(String userName);
	List<UsuarioObjetoSeguridad> findByUsuarioNombreUsuarioAndRoleIdObjetoSeguridad(String userName, String idObjetoSeguridad);
	
}


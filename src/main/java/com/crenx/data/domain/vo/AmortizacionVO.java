package com.crenx.data.domain.vo;

import java.util.Collection;

public class AmortizacionVO {

	double comision = 0;
	double montoComision = 0;
	double ivaComision = 0;
	double totalComision = 0;
	double pagoFijo = 0;

	Collection<TablaAmortizacionVO> tablaAmoritzacion;

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public double getMontoComision() {
		return montoComision;
	}

	public void setMontoComision(double montoComision) {
		this.montoComision = montoComision;
	}

	public double getIvaComision() {
		return ivaComision;
	}

	public void setIvaComision(double ivaComision) {
		this.ivaComision = ivaComision;
	}

	public double getTotalComision() {
		return totalComision;
	}

	public void setTotalComision(double totalComision) {
		this.totalComision = totalComision;
	}

	public Collection<TablaAmortizacionVO> getTablaAmoritzacion() {
		return tablaAmoritzacion;
	}

	public void setTablaAmoritzacion(Collection<TablaAmortizacionVO> tablaAmoritzacion) {
		this.tablaAmoritzacion = tablaAmoritzacion;
	}

	public double getPagoFijo() {
		return pagoFijo;
	}

	public void setPagoFijo(double pagoFijo) {
		this.pagoFijo = pagoFijo;
	}

	
	
}

package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Horario;


public interface HorarioRepository extends CrudRepository<Horario,String> {

	List<Horario> findByCalendarioIdCalendario(String idCalendario);
}

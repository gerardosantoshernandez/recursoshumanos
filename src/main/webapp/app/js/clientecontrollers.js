'use strict';

angular
		.module('crenx.clientecontrollers', [])
		.controller(
				'ClienteController',
				function($scope, $rootScope, apiService, $stateParams, $state,
						$timeout) {
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						nombre : null,
						apellidoPaterno : null
					};
					$scope.clientes = [];
					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.figura = $state.current.params.figura;
					$scope.cliente = {};
					$scope.emptyForm = true;

					// Permisos
					var permisos = $rootScope.repository.loggedUser.permissions;
					$scope.crearCliente = ($.inArray('OBJ_CLI_CREAR', permisos) > -1) ? true
							: false;
					$scope.verCliente = ($.inArray('OBJ_CLI_VER', permisos) > -1) ? true
							: false;
					$scope.crearCodeudor = ($.inArray('OBJ_CLI_CO_CREAR',
							permisos) > -1) ? true : false;
					$scope.verCodeudor = ($.inArray('OBJ_CO_VER', permisos) > -1) ? true
							: false;
					$scope.historiaCred = ($.inArray('OBJ_CLI_HIST', permisos) > -1) ? true
							: false;
					$scope.verDocs = ($.inArray('OBJ_CLI_DOCS_VER', permisos) > -1) ? true
							: false;

					var nivel = $rootScope.repository.loggedUser.tipoTitOrg;
					var idTitOrg = $rootScope.repository.loggedUser.idTitOrg;
					/*
					 * switch(nivel) { case "D" :
					 * $scope.filters.divisiones.push({id:idTitOrg, name:"no
					 * importa"}); break; case "R" :
					 * $scope.filters.regiones.push({id:idTitOrg, name:"no
					 * importa"}); break; case "G" :
					 * $scope.filters.gerencias.push({id:idTitOrg, name:"no
					 * importa"}); break; case "RR" :
					 * $scope.filters.rutas.push({id:idTitOrg, name:"no
					 * importa"}); break; }
					 */
					var filterTextTimeout;
					$scope.$watch('filters.divisiones', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					$scope.$watch('filters.regiones', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					$scope.$watch('filters.gerencias', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					$scope.$watch('filters.rutas', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					$scope.$watch('filters.nombre', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					$scope.$watch('filters.apellidoPaterno', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadData();
							} else {
								loadFilteredTree(e.val, source)

							}
						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}

						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
									// $scope.filters.rutas = $scope.rutas;
									// loadData();

								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
									$scope.filters.rutas = result.data.rutas;
									$scope.filters.gerencias = [];
									$scope.filters.regiones = [];
									$scope.filters.divisiones = [];
									loadData();
								}, locationLoadFailed);
					}

					 /**
                     *                                       
                     *Valida si existen criterios de búsqueda 
                    **/
                    function isEmptyForm (){
                            $scope.emptyForm = ($scope.filters.divisiones.length == 0
                                            && $scope.filters.gerencias.length == 0
                                            && $scope.filters.regiones.length == 0
                                            && $scope.filters.rutas.length == 0
                                            && ($scope.filters.nombre == null || $scope.filters.nombre == '')
                                            && ($scope.filters.apellidoPaterno == null || $scope.filters.apellidoPaterno == '')) ;
                            return $scope.emptyForm;
                    }

					
					function loadData() {
						if(isEmptyForm()){
							$scope.clientes = new Array();
						}else{
							apiService.post('/api/cliente', $scope.filters,
									lcoationLoadCompleted, locationLoadFailed);
						}
					}
					function lcoationLoadCompleted(result) {
						$scope.clientes = result.data;
					}

					function locationLoadFailed(response) {
						alert(response);
					}

					loadOrgTree();
					// loadFilteredTree($scope.filters, $scope.nivel);

				})
		.controller(
				'ClienteTraspasoController',
				function($scope, $rootScope, apiService, $stateParams, $state,
						$timeout, $modal) {
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						nombre : "",
						apellidoPaterno : ""
					};
					$scope.clientes = [];
					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.cliente = {};
					$scope.rutaOrigen = null;
					$scope.rutaDestino = null;
					$scope.vtodos = false;
					$scope.aux = false;
					$scope.totalcartera = 0;
					$scope.loading = true;
					$scope.validRutas = false;
					$scope.emptyForm=true;
					// Permisos
					var permisos = $rootScope.repository.loggedUser.permissions;
					$scope.crearCliente = ($.inArray('OBJ_CLI_CREAR', permisos) > -1) ? true
							: false;
					$scope.crearCodeudor = ($.inArray('OBJ_CLI_CO_CREAR',
							permisos) > -1) ? true : false;
					$scope.historiaCred = ($.inArray('OBJ_CLI_HIST', permisos) > -1) ? true
							: false;
					$scope.documentos = ($.inArray('OBJ_CLI_DOCS', permisos) > -1) ? true
							: false;

					var nivel = $rootScope.repository.loggedUser.tipoTitOrg;
					var idTitOrg = $rootScope.repository.loggedUser.idTitOrg;
					var filterTextTimeout;
					$scope.$watch('filters.nombre', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					$scope.$watch('filters.apellidoPaterno', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							if(source !== 'ruta')
								loadFilteredTree(e.val, source)
												});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
					});

					$scope.changeRutas = function() {
						$scope.filters.rutas = [];
						$scope.filters.rutas.push({
							id : $scope.rutaOrigen,
							name : "no importa"
						});
						loadData();

					}

					$scope.validaRutas = function() {
						if ($scope.rutaOrigen === $scope.rutaDestino) {
							$scope.validRutas = false;
							return false;
						} else {
							$scope.rutaDestinoAux = $scope.rutaDestino;
							$scope.validRutas = true;
						}
					}

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$(selectControl).append(
								$("<option></option>").val(0).text(
										"Selecciona una opción"));
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});
					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {

									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;

									addSelectItems("#division",
											result.data.divisiones);
									addSelectItems("#region",
											result.data.regiones);
									addSelectItems("#gerencia",
											result.data.gerencias);
									addSelectItems("#ruta", result.data.rutas);
								}, locationLoadFailed);

					}

					function loadRutas() {
						apiService
								.post(
										'/api/org/tipoNodo',
										"RR",
										function(result) {
											$scope.rutasDestino = result.data;
											addSelectItems("#rutaDestino",
													$scope.rutasDestino);
											$scope.rutaDestinoAux = $scope.rutasDestino[0].id;
										}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
								}, locationLoadFailed);
					}

					function loadData() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0 || $scope.filters.rutas[0].id != 0
								|| $scope.filters.nombre != ""
								|| $scope.filters.apellidoPaterno != "") {
							$scope.emptyForm =false;
							apiService.post('/api/cliente/getClientesTraspado',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);

						} else
							$scope.emptyForm =true;

					}

					function lcoationLoadCompleted(result) {
						$scope.clientes = result.data;
						$scope.vtodos = false;
					}

					function locationLoadFailed(response) {
						alert(response);

					}

					$scope.traspasar = function(modal_id, modal_size,
							modal_backdrop) {
						// modal
						loadCatalogo("CAT_MOTIVO_TRANSF_CLIE");
						$rootScope.motivo = "";
						$rootScope.fechaOpe = "";
						$rootScope.noClientes = 0;
						$rootScope.totalcartera = 0;
						var i = 0;
						for (i = 0; i < $scope.clientes.length; i++) {
							if ($scope.clientes[i].traspasar) {
								$rootScope.noClientes = $rootScope.noClientes + 1;
								if ($scope.clientes[i].creditos.length > 0) {
									if ($scope.clientes[i].creditos[0].estatusCredito == 'Activo')
										$rootScope.totalcartera = $rootScope.totalcartera
												+ $scope.clientes[i].creditos[0].saldo;
								}

							}

						}

						$rootScope.currentModal = $modal
								.open({
									templateUrl : 'TraspasoModal',
									size : modal_size,
									backdrop : typeof modal_backdrop == 'undefined' ? true
											: modal_backdrop
								});

					};

					$rootScope.ejecutarTraspaso = function(item, event) {
						var param = {
							idRutaDestino : $scope.rutaDestinoAux,
							clientes : $scope.clientes,
							motivo : this.motivo,
							fecha : this.fechaOpe
						}
						console.log(param);

						apiService
								.post(
										'/api/org/traspaso',
										param,
										function(result) {
											toastr
													.success(
															$rootScope.noClientes
																	+ " cliente(s) traspasado(s) correctamente",
															"Traspasar Cliente(s)",
															opts);
											$rootScope.currentModal.close();
											loadData();
										}, locationLoadFailed);
					}

					function loadCatalogo(catType) {
						apiService.post('/api/catalogo/items', catType,
								function(result)

								{
									$rootScope.catmotivo = result.data;
								}, serviceError);

					}
					function serviceError(response) {
						toastr.error(response.data.sourceMessage,
								response.data.errMessage, opts);
					}
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					$scope.selectAll = function() {
						var val = $scope.vtodos;
						angular.forEach($scope.clientes, function(child) {
							child['traspasar'] = val;
						});
						// $scope.vtodos= !val;
					}

					loadOrgTree();
					loadRutas();
				})
		.controller(
				'ClienteUnificacionController',
				function($scope, $rootScope, apiService, $stateParams, $state,
						$timeout, $modal) {

					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						nombre : null,
						apellidoPaterno : null
					};
					
					$scope.filtersTitular = {
							divisiones : [],
							regiones : [],
							gerencias : [],
							rutas : [],
							nombre : null,
							apellidoPaterno : null
						};
					$scope.creditoNuevo ={
							idCliente : null,
							idRuta : null,
							monto : null,
							comision : null,
							plazo : null,
							tasa : null,
							pago : null
					}
					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];

					$scope.creditos = [];
					$scope.creditosSeleccionados = [];
				
					$scope.titular = {};

					var nivel = $rootScope.repository.loggedUser.tipoTitOrg;
					var idTitOrg = $rootScope.repository.loggedUser.idTitOrg;

					var filterTextTimeout;
					$scope.$watch('filters.nombre', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					$scope.$watch('filtersTitular.apellidoPaterno', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadClientes();
						}, 250); // delay 250 ms
					})
					$scope.$watch('filtersTitular.nombre', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadClientes();
						}, 250); // delay 250 ms
					})

					$scope.$watch('filters.apellidoPaterno', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})
					
					$scope.$watch('titular', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadCoDeudores();
						}, 250); // delay 250 ms
					})

					$scope.$watch('montoNeg', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							if ($scope.montoNeg >= 6000)
								$scope.requiereCod = true;
							else
								$scope.requiereCod = false;
						
						}, 250); // delay 250 ms
					})
					
					
					
					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								$scope.filters.regiones = [];
								$scope.filters.divisiones = [];
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadData();
							}else if(source=='ruta2'){
								$scope.filtersTitular.rutas = [];
								$scope.filtersTitular.regiones = [];
								$scope.filtersTitular.divisiones = [];
								$scope.filtersTitular.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filtersTitular.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadClientes();
							}
								else {
								loadFilteredTree(e.val, source)
								// loadData();
							}
							
							
						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta');
						
						prepareListItems("#division2", 'División');
						prepareListItems("#region2", 'Región');
						prepareListItems("#gerencia2", 'Gerencia');
						prepareListItems("#ruta2", 'Ruta');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
									
									
									addSelectItems("#division2",
											result.data.divisiones);
									addSelectItems("#region2",result.data.regiones);
									addSelectItems("#gerencia2",result.data.gerencias);
									addSelectItems("#ruta2", result.data.rutas);
									// $scope.filters.rutas= $scope.rutas;
									// loadData();
								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
									$scope.filters.rutas = result.data.rutas;
									$scope.filters.gerencias = [];
									$scope.filters.regiones = [];
									$scope.filters.divisiones = [];
									loadData();

								}, locationLoadFailed);
					}

					function loadData() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0
								|| $scope.filters.nombre != null
								|| $scope.filters.apellidoPaterno != null) {
							apiService.post('/api/cliente/getCreditosActivos',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						} else
							alert("Introduzca un filtro");

					}
					function lcoationLoadCompleted(result) {
						$scope.creditos = result.data;
					}

					function locationLoadFailed(response) {
						toastr
								.error(response.data.sourceMessage, "Error",
										opts);
					}
					$scope.saldoAcumulado=0;
					/** *****************Creditos***************** */
					$scope.addCredit = function (item){
						if($scope.creditosSeleccionados.lastIndexOf(item)!== -1 ){
							alert("El crédito ya ha sido seleccionado");
						}else{
							$scope.creditosSeleccionados.push(item);
							$scope.saldoAcumulado=$scope.saldoAcumulado + item.saldo;
							$scope.montoNeg = $scope.saldoAcumulado; 
						}
							
					}
					
					$scope.removeCredit = function (item){
						var index =  $scope.creditosSeleccionados.lastIndexOf(item);
						$scope.saldoAcumulado=$scope.saldoAcumulado - item.saldo;
						$scope.creditosSeleccionados.splice(index,1);
						$scope.montoNeg = $scope.saldoAcumulado;
					}

					function serviceError(response) {
						toastr.error(response.data.sourceMessage,
								response.data.errMessage, opts);
					}
					function ServiceSuccessComplete(result) {
						toastr.success(
								"Se unificaron satisfactoriamente los créditos",
								"Unificar créditos", opts);
						$state.transitionTo('app.clientescreditos', {
							cliente : $scope.titular
						});
					}
					function loadClientes() {
						if ($scope.filtersTitular.divisiones.length != 0
								|| $scope.filtersTitular.gerencias.length != 0
								|| $scope.filtersTitular.regiones.length != 0
								|| $scope.filtersTitular.rutas.length != 0
								|| $scope.filtersTitular.nombre != null
								|| $scope.filtersTitular.apellidoPaterno != null ) {
							apiService.post('/api/cliente/getClientesSinCreditoA', $scope.filtersTitular,
									function(result){
								$scope.clientes = result.data;
							}, locationLoadFailed);
						} else
							alert("Introduzca un filtro");

					}

					
					$scope.producto = null;
					$scope.cambioProducto = function() {
						var item;
						for (item in $scope.productos) {
							if ($scope.productos[item].idProducto == $scope.creditoNuevo.idProducto) {
								$scope.producto = $scope.productos[item];
								break;
							}
						}
						$scope.creditoNuevo.idProducto = $scope.producto.idProducto;
						$scope.creditoNuevo.idUnidadPlazo = $scope.producto.idUnidadPlazo;
						$scope.creditoNuevo.idFrecuenciaCobro = $scope.producto.idFrecuenciaCobro;
						$scope.creditoNuevo.monto = $scope.montoNeg;
						$scope.creditoNuevo.plazo = $scope.producto.plazo;
						$scope.creditoNuevo.pago = $scope.producto.pagoXmil;
						$scope.creditoNuevo.unidadPlazo = $scope.producto.unidadPlazo;
						$scope.creditoNuevo.frecuenciaCobro = $scope.producto.frecuenciaCobro;
						$scope.creditoNuevo.comision = $scope.producto.comision;
						$scope.creditoNuevo.tasa = $scope.producto.tasa;
						$scope.creditoNuevo.minimo = $scope.producto.minimo;
						$scope.creditoNuevo.maximo = $scope.producto.maximo;
						if ($scope.producto.permiteEditar)
							$scope.productDisabled = false;
						else
							$scope.productDisabled = true;

						$("#monto").attr({
							"max" : $scope.producto.maximo,
							"min" : $scope.producto.minimo,
							"step" : 1000
						});

					}
					function loadCatalogoProducto() {
						apiService.post('/api/producto', {
							familias : [],
							nombre : null
						}, function(result) {
							$scope.productos = result.data;
						}, locationLoadFailed);

					}
					
					$scope.calculaSaldo = function() {
						$scope.montoNeg = Math.round($scope.saldoAcumulado
								* (1 + $scope.comisionRest / 100));
						if ($scope.montoNeg >= 6000)
							$scope.requiereCod = true;
						else
							$scope.requiereCod = false;
					}
					function loadCoDeudores() {
						var idCliente = $scope.titular.clienteId || $scope.titular.idCliente;
						apiService.post('/api/cliente/codeudores',idCliente, function(result) {
									$scope.codeudores = result.data;
								}, locationLoadFailed);
					}
				
					$scope.submitTheForm  = function (){
						$scope.creditoNuevo.monto = $scope.montoNeg;
						$scope.creditoNuevo.idCliente= $scope.titular.idCliente;
						$scope.creditoNuevo.idRuta= $scope.titular.idRuta;
						$scope.creditoNuevo.nombreCliente= $scope.titular.nombreCliente || $scope.titular.nombre;
						var datos ={
								creditos : $scope.creditosSeleccionados,
								destino : $scope.creditoNuevo
						}
						
						if(datos.creditos.length <= 0){
							$scope.mensaje="No se han seleccionado créditos";
							toastr.error($scope.mensaje,"Error"
									, opts);
						}
						else if($scope.creditoNuevo.idCliente == null){
							$scope.mensaje="No se ha seleccionado titular";
							toastr.error($scope.mensaje,"Error"
									, opts);
						}else if($scope.creditoNuevo.idProducto == null){
							$scope.mensaje="No se ha seleccionado producto";
							toastr.error($scope.mensaje,"Error"
									, opts);
						}else if($scope.requiereCod== true && $scope.creditoNuevo.idCoDedudor == null){
							$scope.mensaje="Se requiere un codeudor";
							toastr.error($scope.mensaje,"Error"
									, opts);
						}else if($scope.creditoNuevo.fechaEmision == null){
							$scope.mensaje="Seleccione fecha de emisión";
							toastr.error($scope.mensaje,"Error"
									, opts);
						}
						else{
							apiService.post('/api/cliente/unifCreditos',datos,ServiceSuccessComplete ,locationLoadFailed)
							$scope.isValido = true;
						}
							
					}
					var opts = {
							"closeButton" : true,
							"debug" : false,
							"positionClass" : "toast-top-right",
							"onclick" : null,
							"showDuration" : "300",
							"hideDuration" : "1000",
							"timeOut" : "5000",
							"extendedTimeOut" : "1000",
							"showEasing" : "swing",
							"hideEasing" : "linear",
							"showMethod" : "fadeIn",
							"hideMethod" : "fadeOut"
						};
				
					$scope.validar= function(){
						console.log("hi");
						$scope.isValido = true;
					}
					
					
					loadOrgTree();
					loadCatalogoProducto();

				})
		.controller(
				'ClienteFormController',
				function($scope, $rootScope, apiService, $location, $timeout,
						$http, $cookieStore, $stateParams, $modal, $state) {
					$scope.cliente = $stateParams.cliente;
					$scope.rutas = $stateParams.rutas;
					$scope.clienteId = $stateParams.clienteId;
					$scope.figura = $stateParams.figura;
					$scope.auxDocumento = {};

					// Permisos
					var permisos = $rootScope.repository.loggedUser.permissions;
					$scope.crearCliente = ($.inArray('OBJ_CLI_CREAR', permisos) > -1) ? true
							: false;
					$scope.verCliente = ($.inArray('OBJ_CLI_VER', permisos) > -1) ? true
							: false;

					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};
					if ($scope.clienteId) {
						$scope.isUpdate = true;
						/*
						 * apiService.get('/api/cliente/detailcte/' +
						 * $scope.clienteId, null, function
						 * ServiceSuccessComplete(result) { $scope.cliente =
						 * result.data; $scope.tipoFigura =
						 * $scope.cliente.idTipoFigura; $scope.tipoDocId =
						 * $scope.cliente.idTipoDocumentoIdentidad;
						 * $scope.genero = $scope.cliente.idGenero;
						 * $scope.paisNacimiento =
						 * $scope.cliente.idPaisNacimiento;
						 * $scope.entidadNacimiento =
						 * $scope.cliente.idEntidadNacimiento; $scope.ocupacion =
						 * $scope.cliente.idOcupacion; $scope.tipoDocId = null; },
						 * ServiceError);
						 */

					} else {
						$scope.isUpdate = false;
						$scope.cliente = {};
						$scope.tipoFigura = null;
						$scope.tipoDocId = null;
						$scope.genero = null;
						$scope.paisNacimiento = null;
						$scope.entidadNacimiento = null;
						$scope.ocupacion = null;
						$scope.tipoDocId = null;
					}

					var filterTextTimeout;
					$scope
							.$watch(
									'cliente.domicilioResidencial.codigoPostal',
									function(val) {
										if (filterTextTimeout)
											$timeout.cancel(filterTextTimeout);
										filterTextTimeout = $timeout(
												function(val) {
													if ($scope.cliente.domicilioResidencial == undefined)
														return;
													if ($scope.cliente.domicilioResidencial.codigoPostal == undefined)
														return;
													if ($scope.cliente.domicilioResidencial.codigoPostal.length == 5)
														loadColonias(
																$scope.cliente.domicilioResidencial.codigoPostal,
																"R");
												}, 250); // delay 250 ms
									})
					$scope
							.$watch(
									'cliente.domicilioLaboral.codigoPostal',
									function(val) {
										if (filterTextTimeout)
											$timeout.cancel(filterTextTimeout);
										filterTextTimeout = $timeout(
												function(val) {
													if ($scope.cliente.domicilioLaboral == undefined)
														return;
													if ($scope.cliente.domicilioLaboral.codigoPostal == undefined)
														return;
													if ($scope.cliente.domicilioLaboral.codigoPostal.length == 5)
														loadColonias(
																$scope.cliente.domicilioLaboral.codigoPostal,
																"L");
												}, 250); // delay 250 ms
									})
					function loadColonias(codigo, tipo) {
						apiService
								.get(
										'/api/sepomex/colonias/' + codigo,
										null,
										function ServiceSuccessComplete(result) {

											if (tipo == "R") {
												$scope.cliente.domicilioResidencial.municipio = result.data.municipio;
												$scope.cliente.domicilioResidencial.estado = result.data.estado;
												$scope.cliente.domicilioResidencial.pais = result.data.pais;

												$("#municipioRes").value = result.data.municipio;
												$("#estado_res").value = result.data.estado;
												$("#paisRes").value = result.data.pais;
												$("#colonia_residencial")
														.empty();
												$("#colonia_residencial")
														.append(
																new Option(
																		"----Seleccione una----",
																		""));
												$
														.each(
																result.data.colonias,
																function(key,
																		value) {
																	var selected = $scope.cliente.domicilioResidencial.colonia == value ? true
																			: false;
																	var opt = new Option(
																			value,
																			value);
																	$(
																			"#colonia_residencial")
																			.append(
																					opt);
																	if (selected)
																		opt
																				.setAttribute(
																						"selected",
																						selected);
																});

											} else {
												$scope.cliente.domicilioLaboral.municipio = result.data.municipio;
												$scope.cliente.domicilioLaboral.estado = result.data.estado;
												$scope.cliente.domicilioLaboral.pais = result.data.pais;

												$("#municipio").value = result.data.municipio;
												$("#estado_emp").value = result.data.estado;
												$("#paisLaboral").value = result.data.pais;
												$("#colonia").empty();
												$("#colonia")
														.append(
																new Option(
																		"----Seleccione una----",
																		""));
												$
														.each(
																result.data.colonias,
																function(key,
																		value) {
																	var selected = $scope.cliente.domicilioLaboral.colonia == value ? true
																			: false;
																	var opt = new Option(
																			value,
																			value);
																	$(
																			"#colonia")
																			.append(
																					opt);
																	if (selected)
																		opt
																				.setAttribute(
																						"selected",
																						selected);
																});

											}

										}, ServiceError);

					}
					$scope.ValidaRfc = function(rfc) {
						var rfcStr;
						rfcStr = rfc;
						if (rfc
								.match('^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?')) {

						} else {
							alert('RFC Incorrecto');
						}

					}
					$scope.ValidaCURP = function(curp) {
						var strCurp;
						strCurp = curp;
						if (curp
								.match(/^([a-z]{4})([0-9]{6})([a-z]{6})([0-9]{2})$/i)) {

						} else {
							alert('CURP Incorrecto');
						}

					}

					$scope.ValidaCP = function(cp) {
						var strCp;
						strCp = cp;
						if (cp.match(/^\d{5}$/)) {

						} else {
							alert('Código Postal Incorrecto');
						}

					}

					$scope.copyDom = function() {
						// $scope.cliente.domicilioLaboral =
						// $scope.cliente.domicilioResidencial;
						loadColonias(
								$scope.cliente.domicilioLaboral.codigoPostal,
								"R");
						// $scope.cliente.domicilioResidencial =
						// $scope.cliente.domicilioLaboral;
						$scope.cliente.domicilioResidencial = clone($scope.cliente.domicilioLaboral);

					}
					function clone(obj) {
						if (obj === null || typeof obj !== 'object') {
							return obj;
						}

						var temp = obj.constructor();
						for ( var key in obj) {
							temp[key] = clone(obj[key]);
						}

						return temp;
					}
					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								});

					}

					jQuery(document)
							.ready(
									function($) {
										prepareListItems("#figura", 'Figura');
										prepareListItems("#ruta", 'Ruta');
										prepareListItems("#tipodocId",
												'Tipo Doc. Identificación');
										prepareListItems("#genero",
												'Género sexual');
										prepareListItems("#pais",
												'País de nacimiento');
										prepareListItems("#estadoNac",
												'Estado de nacimiento');
										prepareListItems("#ocupacion",
												'Ocupación');
										prepareListItems("#tipoDoc",
												'Tipo de documento');
										// prepareListItems("#colonia",
										// 'Colonia');
									});

					// Ya no se usa

					function loadCatalogo(catType) {
						apiService.post('/api/catalogo/items', catType,
								catalogComplete, ServiceError);

					}

					function catalogComplete(result) {
						if (result.data.parent.claveInterna == 'CAT_FIG') {

							$scope.allTipoFigura = result.data;
							angular
									.forEach(
											$scope.allTipoFigura.items,
											function(value, key) {
												if ($scope.figura == value.claveInterna) {
													$scope.cliente.idTipoFigura = value.idCatalogo;
													$scope.ItemTipoFigura = [ value ];
												}
											});
							// addSelectedItems("#figura", result.data.items,
							// $scope.tipoFigura);
						}
						if (result.data.parent.claveInterna == 'CAT_TIP_DOC_ID') {
							// addSelectedItems("#tipodocId", result.data.items,
							// $scope.tipoDocId);
							$scope.allTipoDocId = result.data;
						}
						if (result.data.parent.claveInterna == 'CAT_GEN') {
							// addSelectedItems("#genero", result.data.items,
							// $scope.genero);
							$scope.allGenero = result.data;
						}
						if (result.data.parent.claveInterna == 'CAT_PAIS') {
							// addSelectedItems("#pais", result.data.items,
							// $scope.paisNacimiento);
							$scope.allPais = result.data;
						}
						if (result.data.parent.claveInterna == 'CAT_ENTIDAD_FED') {
							// addSelectedItems("#estadoNac", result.data.items,
							// $scope.entidadNacimiento);
							$scope.allEntidadNac = result.data;
						}
						if (result.data.parent.claveInterna == 'CAT_OCUP') {
							// addSelectedItems("#ocupacion", result.data.items,
							// $scope.ocupacion);
							$scope.allOcupacion = result.data;
						}
						if (result.data.parent.claveInterna == 'CAT_TIP_DOC') {
							// addSelectedItems("#tipoDoc", result.data.items,
							// $scope.tipoDocId);
							$scope.allTipoDoc = result.data;
						}

					}

					function addRutas(selectedValue) {
						if (selectedValue == undefined)
							return

						

						$.each($scope.rutas, function(key, value) {
							var selected = selectedValue == value.id ? true
									: false;
							var opt = new Option(value.name, value.id);
							$("#ruta").append(opt);
							if (selected)
								opt.setAttribute("selected", selected);
						});
					}

					$scope.submitTheForm = function(item, event) {
						apiService.post('/api/cliente/savecliente',
								$scope.cliente, ServiceSuccessComplete,
								ServiceError);
					}

					function ServiceSuccessComplete(result) {

						toastr.success(
								"Se guardó satisfactoriamente el cliente.",
								"Guardar cliente", opts);

						$scope.errorsubmit = '';
						$state.transitionTo('app.clientes', {
							figura : "FIG_CLIENTE"
						});

					}

					function ServiceError(response) {
						toastr
								.error(response.data.sourceMessage, "Error",
										opts);
					}

					loadCatalogo('CAT_FIG');
					loadCatalogo('CAT_TIP_DOC_ID');
					loadCatalogo('CAT_GEN');
					loadCatalogo('CAT_PAIS');
					loadCatalogo('CAT_ENTIDAD_FED');
					loadCatalogo('CAT_OCUP');
					loadCatalogo('CAT_TIP_DOC');

					addRutas();

					$scope.canEdit = !$scope.crearCliente;
					$scope.cliente.nombreRefFamiliar = "";
					$scope.cliente.nombreRefPersonal = "";

					$scope.getRequeridos = function(algo) {
						$scope.carRequeridos = algo.clave
						$scope.cliente.idTipoDocumentoIdentidad = algo.idCatalogo;
					}

					$scope.generaRFC = function() {

						if (($scope.cliente.nombre != "" && $scope.cliente.nombre != null)
								&& ($scope.cliente.apellidoPaterno != "" && $scope.cliente.apellidoPaterno != null)
								&& ($scope.cliente.apellidoMaterno != "" && $scope.cliente.apellidoMaterno != null)
								&& ($scope.cliente.fechaNacimiento != "" && $scope.cliente.fechaNacimiento != null)) {
							apiService
									.post(
											'/api/cliente/getRFC',
											$scope.cliente,
											function(result) {
												$scope.cliente.cveFiscal = result.data.valor;
											}, ServiceError);

						} else
							return;
					}

					$scope.generaCURP = function() {

						if (($scope.cliente.nombre != "" && $scope.cliente.nombre != null)
								&& ($scope.cliente.apellidoPaterno != "" && $scope.cliente.apellidoPaterno != null)
								&& ($scope.cliente.apellidoMaterno != "" && $scope.cliente.apellidoMaterno != null)
								&& ($scope.cliente.fechaNacimiento != "" && $scope.cliente.fechaNacimiento != null)
								&& ($scope.cliente.idGenero != "" && $scope.cliente.idGenero != null)
								&& ($scope.cliente.idEntidadNacimiento != "" && $scope.cliente.idEntidadNacimiento != null)) {
							apiService
									.post(
											'/api/cliente/getCURP',
											$scope.cliente,
											function(result) {
												$scope.cliente.curp = result.data.valor;
											}, ServiceError);

						} else
							return;
					}

				})
		.controller(
				'CreditosClienteController',
				function($scope, $http, $rootScope, apiService, $stateParams,
						$state, $timeout, $q, $modal) {
					$scope.selectedCredito = false;
					$scope.habilitarPago = false;
					$scope.creditos = [];
					$scope.cliente = $stateParams.cliente;

					// Permisos
					var permisos = $rootScope.repository.loggedUser.permissions;
					$scope.crearCredito = ($
							.inArray('OBJ_CRED_CREAR', permisos) > -1) ? true
							: false;
					$scope.editarCredito = ($
							.inArray('OBJ_CRED_EDIT', permisos) > -1) ? true
							: false;
					$scope.reestructurar = ($.inArray('OBJ_CRED_REEST',
							permisos) > -1) ? true : false;
					$scope.pagar = ($.inArray('OBJ_PAGO_CREAR', permisos) > -1) ? true
							: false;

					$scope.creditoSelected = function(cliente, credito) {
						$scope.producto = credito.producto;
						$scope.estatusCredito = credito.estatusCredito;
						$scope.saldo = credito.saldo;
						$scope.plazo = credito.plazo;
						$scope.creditoItem = credito;
						$scope.cliente = cliente;

						if ($scope.selectedCredito) {
							$scope.selectedCredito = false
							return;
						}
						apiService.post('/api/cliente/pagos/',
								credito.idCredito, function(result) {
									$scope.tablaAmortizacion = result.data;
									$scope.selectedCredito = true;
									if (credito.saldo > 0)
										$scope.habilitarPago = true;
									else
										$scope.habilitarPago = false;

								}, locationLoadFailed);
					}
					function loadData() {
						var id;// = $scope.cliente.idCliente;

						if (angular.isDefined($scope.cliente)) {
							id = $scope.cliente.idCliente;
							$rootScope.cliente = $scope.cliente;
						} else if (angular.isDefined($rootScope.cliente)) {
							id = $rootScope.cliente.idCliente;
							$scope.cliente = $rootScope.cliente;
						}
						apiService.get('/api/cliente/creditos/' + id, null,
								lcoationLoadCompleted, locationLoadFailed);

					}
					function lcoationLoadCompleted(result) {
						$scope.creditos = result.data;
					}

					function locationLoadFailed(response) {
						alert(response);
					}
					loadData();

					$scope.generaEstadoCuenta = function(idCredito) {
						download(idCredito);
					}
					function download(idCredito) {
						var defaultFileName = 'EstadoCuenta.pdf';
						var self = this;
						var deferred = $q.defer();
						$http
								.post('/api/estadoCuenta/getEstadoCuenta',
										idCredito, {
											responseType : "arraybuffer"
										})
								.then(
										function(data) {
											var type = data
													.headers('Content-Type');
											var disposition = data
													.headers('Content-Disposition');
											if (disposition) {
												var match = disposition
														.match(/.*filename=\"?([^;\"]+)\"?.*/);
												if (match[1])
													defaultFileName = match[1];
											}
											defaultFileName = defaultFileName
													.replace(/[<>:"\/\\|?*]+/g,
															'_');
											var blob = new Blob([ data.data ],
													{
														type : type
													});
											saveAs(blob, defaultFileName);
											deferred.resolve(defaultFileName);
										}, function error(data) {
											console.log(data.data);
										});
						return deferred.promise;
					}

					// Cancelacion de credito
					// Cancelacion de crédito
					$rootScope.cancelaCredito = function(idCredito, modal_id,
							modal_size, modal_backdrop) {
						loadCatalogo("CAT_MOTIVO_CANCEL_CRED");
						$scope.idCreditoC = idCredito;
						$rootScope.operacion = "Cancelar crédito";
						$rootScope.obs = "";
						$rootScope.fechaEmi = "";
						$rootScope.currentModal = $modal
								.open({
									templateUrl : 'EliminarModal',
									size : modal_size,
									backdrop : typeof modal_backdrop == 'undefined' ? true
											: modal_backdrop
								});
					};

					$rootScope.cancelCredito = function(item, event) {
						var creditoVO = {};
						creditoVO.idCredito = $scope.idCreditoC;
						creditoVO.fechaCancelacion = this.fechaOpe;
						creditoVO.motivoCancelacion = this.idMotivoCanc;
						creditoVO.observaciones = this.obs;
						apiService
								.post(
										'/api/cliente/cancelarCredito',
										creditoVO,
										function(result) {
											toastr
													.success(
															"Se canceló satisfactoriamente el movimiento",
															"Cancelar movimiento",
															opts);
											$rootScope.currentModal.close();
											loadData();
										}, serviceError);

					}
					function serviceError(response) {
						toastr.error(response.data.sourceMessage,
								response.data.errMessage, opts);
					}
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					// Loading AJAX Content
					$scope.openAjaxModal = function(modal_id, url_location) {

						$rootScope.currentModal = $modal
								.open({
									templateUrl : modal_id,
									resolve : {
										ajaxContent : function($http) {
											return $http
													.get(url_location)
													.then(
															function(response) {
																$rootScope.modalContent = $sce
																		.trustAsHtml(response.data);
															},
															function(response) {
																$rootScope.modalContent = $sce
																		.trustAsHtml('<div class="label label-danger">Cannot load ajax content! Please check the given url.</div>');
															});
										}
									}
								});

						$rootScope.modalContent = $sce
								.trustAsHtml('Modal content is loading...');
					}
					function loadCatalogo(catType) {
						apiService.post('/api/catalogo/items', catType,
								function(result) {
									$rootScope.catmotivo = result.data;
								}, serviceError);

					}

				})
		.controller(
				'CreditosController',
				function($scope, $rootScope, apiService, $stateParams, $state,
						$timeout, $modal) {
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						nombre : null,
						apellidoPaterno : null
					};
					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];

					$scope.selectedCredito = false;
					$scope.habilitarPago = false;
					$scope.creditos = [];
					$scope.cliente = $stateParams.cliente;
					$scope.emptyForm = true;
					// Permisos
					var permisos = $rootScope.repository.loggedUser.permissions;
					$scope.crearCredito = ($
							.inArray('OBJ_CRED_CREAR', permisos) > -1) ? true
							: false;
					$scope.editarCredito = ($
							.inArray('OBJ_CRED_EDIT', permisos) > -1) ? true
							: false;
					$scope.reestructurar = ($.inArray('OBJ_CRED_REEST',
							permisos) > -1) ? true : false;
					$scope.pagar = ($.inArray('OBJ_PAGO_CREAR', permisos) > -1) ? true
							: false;

					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					var nivel = $rootScope.repository.loggedUser.tipoTitOrg;
					var idTitOrg = $rootScope.repository.loggedUser.idTitOrg;

					/*
					 * switch(nivel) { case "D" :
					 * $scope.filters.divisiones.push({id:idTitOrg, name:"no
					 * importa"}); break; case "R" :
					 * $scope.filters.regiones.push({id:idTitOrg, name:"no
					 * importa"}); break; case "G" :
					 * $scope.filters.gerencias.push({id:idTitOrg, name:"no
					 * importa"}); break; case "RR" :
					 * $scope.filters.rutas.push({id:idTitOrg, name:"no
					 * importa"}); break; }
					 */
					var filterTextTimeout;
					$scope.$watch('filters.nombre', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					$scope.$watch('filters.apellidoPaterno', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								$scope.filters.regiones = [];
								$scope.filters.divisiones = [];
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadData();
							} else {
								loadFilteredTree(e.val, source)
								// loadData();
							}
						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
									// $scope.filters.rutas= $scope.rutas;
									// loadData();
								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
									$scope.filters.rutas = result.data.rutas;
									$scope.filters.gerencias = [];
									$scope.filters.regiones = [];
									$scope.filters.divisiones = [];
									loadData();

								}, locationLoadFailed);
					}

					function loadData() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0
								|| $scope.filters.nombre != null
								|| $scope.filters.apellidoPaterno != null) {
							$scope.emptyForm = false;
							apiService.post('/api/cliente/carteracreditos',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						} else
							$scope.emptyForm = true;

					}
					function lcoationLoadCompleted(result) {
						$scope.creditos = result.data;
					}

					function locationLoadFailed(response) {
						toastr
								.error(response.data.sourceMessage, "Error",
										opts);
					}

					/** *****************Creditos***************** */

					$scope.creditoSelected = function(cliente, credito) {
						$scope.producto = credito.producto;
						$scope.estatusCredito = credito.estatusCredito;
						$scope.saldo = credito.saldo;
						$scope.plazo = credito.plazo;
						$scope.creditoItem = credito;
						$scope.cliente = cliente;

						if ($scope.selectedCredito) {
							$scope.selectedCredito = false
							return;
						}
						apiService.post('/api/cliente/pagos/',
								credito.idCredito, function(result) {
									$scope.tablaAmortizacion = result.data;
									$scope.selectedCredito = true;
									if (credito.saldo > 0)
										$scope.habilitarPago = true;
									else
										$scope.habilitarPago = false;

								}, locationLoadFailed);
					}

					// Cancelacion de crédito
					$rootScope.cancelaCredito = function(idCredito, modal_id,
							modal_size, modal_backdrop) {
						loadCatalogo("CAT_MOTIVO_CANCEL_CRED");
						$scope.idCreditoC = idCredito;
						$rootScope.operacion = "Cancelar crédito";
						$rootScope.obs = "";
						$rootScope.fechaEmi = "";
						$rootScope.currentModal = $modal
								.open({
									templateUrl : 'EliminarModal',
									size : modal_size,
									backdrop : typeof modal_backdrop == 'undefined' ? true
											: modal_backdrop
								});
					};

					$rootScope.cancelCredito = function(item, event) {
						var creditoVO = {};
						creditoVO.idCredito = $scope.idCreditoC;
						creditoVO.fechaCancelacion = this.fechaOpe;
						creditoVO.motivoCancelacion = this.idMotivoCanc;
						creditoVO.observaciones = this.obs;
						apiService
								.post(
										'/api/cliente/cancelarCredito',
										creditoVO,
										function(result) {
											toastr
													.success(
															"Se canceló satisfactoriamente el movimiento",
															"Cancelar movimiento",
															opts);
											$rootScope.currentModal.close();
											loadData();
										}, serviceError);

					}
					function serviceError(response) {
						toastr.error(response.data.sourceMessage,
								response.data.errMessage, opts);
					}
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					// Loading AJAX Content
					$scope.openAjaxModal = function(modal_id, url_location) {

						$rootScope.currentModal = $modal
								.open({
									templateUrl : modal_id,
									resolve : {
										ajaxContent : function($http) {
											return $http
													.get(url_location)
													.then(
															function(response) {
																$rootScope.modalContent = $sce
																		.trustAsHtml(response.data);
															},
															function(response) {
																$rootScope.modalContent = $sce
																		.trustAsHtml('<div class="label label-danger">Cannot load ajax content! Please check the given url.</div>');
															});
										}
									}
								});

						$rootScope.modalContent = $sce
								.trustAsHtml('Modal content is loading...');
					}

					function loadCatalogo(catType) {
						apiService.post('/api/catalogo/items', catType,
								function(result) {
									$rootScope.catmotivo = result.data;
								}, serviceError);

					}

					loadOrgTree();

				})

		.controller(
				'EditPagoFormController',
				function($scope, $rootScope, apiService, $stateParams, $state,
						$timeout) {
					$scope.idPago = $stateParams.idPago;
					$scope.editarPago = $scope.idPago != null;
					$scope.pago = {};
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					function ServiceError(response) {
						$scope.errorsubmit = response.data;

						var errorDesc = "";
						if (response.data.runTimeError)
							errorDesc = "Error: " + response.data.motivoRechazo;
						else
							errorDesc = "Error: "
									+ response.data.estatusCredito.nombre + " "
									+ response.data.motivoRechazo;

						toastr.error(errorDesc, "Guardar pago", opts);
						$state.transitionTo('app.clientescreditos', {
							cliente : $scope.cliente
						});

					}

					function loadPago() {
						apiService.post('/api/cliente/loadPago', $scope.idPago,
								function(result) {
									$scope.pago = result.data;
								}, ServiceError);

					}

					$scope.submitTheForm = function() {
						$scope.pago.abono = $scope.pago.totalPago
								- $scope.pago.comision;
						apiService.post('/api/cliente/deletepago',
								$scope.idPago, ServiceSuccessComplete,
								ServiceError);
					}

					function ServiceSuccessComplete(result) {

						toastr.success(
								"Se canceló satisfactoriamente el pago.",
								"Cancelar pago", opts);
						$state.transitionTo('app.clientescreditos', {
							cliente : $scope.cliente
						});
						$scope.errorsubmit = '';

					}
					loadPago();
				})

		.controller(
				'PagoFormController',
				function($scope, $rootScope, apiService, $stateParams, $state,
						$timeout) {
					$scope.credito = $stateParams.credito;
					$scope.cliente = $stateParams.cliente;
					$scope.abonos = [];
					$scope.pago = {
						idCredito : $scope.credito.idCredito,
						abono : $scope.credito.pago,
						comision : $scope.credito.saldoComision,
						totalPago : 0,
						pagoAdicional : 0
					};
					$scope.pagadoReal = 0;
					$scope.pagos = 1;
					$scope.maxAdicional = $scope.credito.pago - 1;
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					var pendientes = $scope.credito.plazo
							- $scope.credito.totPagos + 1;
					for (var i = 1; i < pendientes; i++) {
						$scope.abonos.push(i);

					}

					console.log('PagoFormController');
					$scope.maximo = Number($scope.credito.pago) * pendientes;
					function ServiceError(response) {
						$scope.errorsubmit = response.data;

						var errorDesc = "";
						if (response.data.runTimeError)
							errorDesc = "Error: " + response.data.motivoRechazo;
						else
							errorDesc = "Error: "
									+ response.data.estatusCredito.nombre + " "
									+ response.data.motivoRechazo;

						toastr.error(errorDesc, "Guardar pago", opts);
						$state.transitionTo('app.clientescreditos', {
							cliente : $scope.cliente
						});

					}

					$scope.submitTheForm = function(item, event) {
						$scope.pago.abono = $scope.pago.totalPago
								- $scope.pago.comision;
						apiService.post('/api/cliente/savepago', $scope.pago,
								ServiceSuccessComplete, ServiceError);
					}
					$scope.calculaPago = function() {
						$scope.pago.totalPago = Number($scope.pago.comision)
								+ (Number($scope.pago.abono) * $scope.credito.pago)
								+ Number($scope.pago.pagoAdicional);
					}

					function setSelectedValue() {
						$("#abono").val($scope.credito.pago);
					}

					function ServiceSuccessComplete(result) {

						toastr.success("Se guardó satisfactoriamente el pago.",
								"Guardar pago", opts);
						$state.transitionTo('app.clientescreditos', {
							cliente : $scope.cliente
						});
						$scope.errorsubmit = '';

					}
					jQuery(document).ready(function($) {
						setSelectedValue();
					});

				})

		.controller(
				'CreditoFormController',
				function($scope, $rootScope, apiService, $location, $timeout,
						$http, $cookieStore, $stateParams, $modal, $state) {

					$scope.productos = {};
					$scope.codeudores = {};
					$scope.cliente = $stateParams.cliente;
					$scope.clienteId = $scope.cliente != null ? $scope.cliente.idCliente
							: $stateParams.credito.idCliente;
					$scope.tipo = $stateParams.tipo;
					$scope.productDisabled = true;
					$scope.credito = $stateParams.credito;
					$scope.requiereCod = false;

					if ($stateParams.credito) {
						if ($stateParams.tipo == 'REST') {
							apiService.get('/api/cliente/detailcte/'
									+ $scope.clienteId, null, function(result) {
								$scope.cliente = result.data;
								$scope.credito = {
									idCliente : $scope.clienteId,
									idRuta : $scope.cliente.idRuta,
									monto : null,
									comision : null,
									plazo : null,
									tasa : null,
									pago : null
								};
								$scope.creditoOri = $stateParams.credito;
							}, ServiceError);

						} else if ($stateParams.tipo == 'NEGO') {
							apiService
									.get(
											'/api/cliente/detailcte/'
													+ $scope.clienteId,
											null,
											function(result) {
												$scope.cliente = result.data;
												apiService
														.post(
																'/api/parametros/getParam',
																'COMISION_REESTRUCTURA',
																function(result) {
																	$scope.creditoOri = $stateParams.credito;
																	$scope.comisionRest = parseFloat(result.data.valorNum);
																	$scope.credito = {
																		idCliente : $scope.clienteId,
																		idRuta : $scope.cliente.idRuta,
																		monto : null,
																		comision : null,
																		plazo : null,
																		tasa : null,
																		pago : null
																	};
																	$scope.credito.monto = Math
																			.round($scope.creditoOri.saldo
																					* (1 + $scope.comisionRest / 100));
																	$scope.montoNeg = $scope.creditoOri.saldo;

																}, ServiceError);
											}, ServiceError);

						} else {
							$scope.credito = $stateParams.credito;
						}
					} else
						$scope.credito = {
							idCliente : $scope.clienteId,
							idRuta : $scope.cliente.idRuta,
							monto : null,
							comision : null,
							plazo : null,
							tasa : null,
							pago : null
						};
					if (!$scope.credito.fechaEmision) {
						var today = new Date();
						$scope.credito.fechaEmision = today.toISOString()
								.substring(0, 10);
					}

					$scope.producto = null;
					$scope.cambioProducto = function() {
						var item;
						for (item in $scope.productos) {
							if ($scope.productos[item].idProducto == $scope.credito.idProducto) {
								$scope.producto = $scope.productos[item];
								break;
							}
						}
						$scope.credito.idProducto = $scope.producto.idProducto;
						$scope.credito.idUnidadPlazo = $scope.producto.idUnidadPlazo;
						$scope.credito.idFrecuenciaCobro = $scope.producto.idFrecuenciaCobro;
						if ($scope.tipo == 'NEGO')
							$scope.credito.monto = $scope.montoNeg;
						else
							$scope.credito.monto = $scope.producto.minimo;
						$scope.credito.plazo = $scope.producto.plazo;
						$scope.credito.pago = $scope.producto.pagoXmil;
						$scope.credito.unidadPlazo = $scope.producto.unidadPlazo;
						$scope.credito.frecuenciaCobro = $scope.producto.frecuenciaCobro;
						$scope.credito.comision = $scope.producto.comision;
						$scope.credito.tasa = $scope.producto.tasa;
						$scope.credito.minimo = $scope.producto.minimo;
						$scope.credito.maximo = $scope.producto.maximo;
						if ($scope.producto.permiteEditar)
							$scope.productDisabled = false;
						else
							$scope.productDisabled = true;

						$("#monto").attr({
							"max" : $scope.producto.maximo,
							"min" : $scope.producto.minimo,
							"step" : 1000
						});

					}

					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								});

					}

					jQuery(document).ready(function($) {
						prepareListItems("#producto", 'Producto');
						prepareListItems("#codeudor", 'CoDeudores');
					});

					function addSelectedItems(selectControl, listaItems,
							selectedValue) {
						$
								.each(
										listaItems,
										function(key, value) {
											var selected = selectedValue == value.idCatalogo ? true
													: false;
											var opt = new Option(value.nombre,
													value.idCatalogo);
											$(selectControl).append(opt);
											if (selected)
												opt.setAttribute("selected",
														selected);
										});
					}

					function loadCatalogoProducto() {
						apiService.post('/api/producto', {
							familias : [],
							nombre : null
						}, function(result) {
							$scope.productos = result.data;
						}, ServiceError);

					}
					function loadCoDeudores() {
						apiService.post('/api/cliente/codeudores',
								$scope.clienteId, function(result) {
									$scope.codeudores = result.data;
								}, ServiceError);
					}

					// loadCliente();
					loadCoDeudores();
					loadCatalogoProducto();

					$scope.updateRangeValue = function(control) {
						alert($scope.credito.monto);
						$('#intNumberValue').innerHTML = $scope.credito.monto;

					}

					function ServiceError(response) {
						$scope.errorsubmit = response.data;

						var errorDesc = "";
						if (response.data.runTimeError)
							errorDesc = "Error: " + response.data.motivoRechazo;
						else
							errorDesc = "Error: "
									+ response.data.estatusCredito.nombre + " "
									+ response.data.motivoRechazo;

						toastr.error(errorDesc, response.data.errMessage, opts);
						$state.transitionTo('app.clientescreditos', {
							cliente : $scope.cliente
						});

					}

					$scope.submitTheForm = function(item, event) {
						if ($scope.tipo == null)
							apiService.post('/api/cliente/savecredito',
									$scope.credito, ServiceSuccessComplete,
									ServiceError);
						if ($scope.tipo == 'REST' || $scope.tipo == 'NEGO') {
							var datos = {
								tipo : $scope.tipo,
								pagadoReal : $scope.pagadoReal,
								original : $scope.creditoOri,
								destino : $scope.credito
							};
							apiService.post('/api/cliente/restcredito', datos,
									ServiceSuccessComplete, ServiceError);

						}

					}

					$scope.calculaSaldo = function() {
						$scope.montoNeg = Math.round($scope.creditoOri.saldo
								* (1 + $scope.comisionRest / 100));
						if ($scope.montoNeg >= 6000)
							$scope.requiereCod = true;
						else
							$scope.requiereCod = false;
					}

					function ServiceSuccessComplete(result) {

						toastr.success(
								"Se guardó satisfactoriamente el crédito.",
								"Guardar crédito", opts);
						$state.transitionTo('app.clientescreditos', {
							cliente : $scope.cliente
						});
					}

					function loadCliente() {
						apiService.get('/api/cliente/detailcte/'
								+ $scope.clienteId, null, function(result) {
							$scope.cliente = result.data;
						}, ServiceError);
					}

				})
		.controller(
				'CoDeudorController',
				function($scope, $rootScope, apiService, $stateParams, $state,
						$timeout) {
					$scope.cliente = $stateParams.cliente;
					$scope.figura = $state.current.params.figura;

					// Permisos
					var permisos = $rootScope.repository.loggedUser.permissions;
					$scope.crearCodeudor = ($.inArray('OBJ_CLI_CO_CREAR',
							permisos) > -1) ? true : false;
					$scope.verCodeudor = ($.inArray('OBJ_CO_VER', permisos) > -1) ? true
							: false;

					function loadData() {
						var id;
						if (angular.isDefined($scope.cliente)) {
							id = $scope.cliente.idCliente;
							$rootScope.cliente = $scope.cliente;
						} else if (angular.isDefined($rootScope.cliente)) {
							id = $rootScope.cliente.idCliente;
							$scope.cliente = $rootScope.cliente;
						}

						apiService.post('/api/cliente/codeudores',
								$scope.cliente.idCliente,
								lcoationLoadCompleted, locationLoadFailed);
					}
					function lcoationLoadCompleted(result) {
						$scope.clientes = result.data;
					}

					function locationLoadFailed(response) {
						alert(response);
					}

					loadData();

				});
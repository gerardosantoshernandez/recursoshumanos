package com.crenx.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Domicilio;
import com.crenx.data.domain.entity.Empleado;
import com.crenx.data.domain.entity.ObjetoSeguridad;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Producto;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.repository.CajaRepository;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.DomicilioRepository;
import com.crenx.data.domain.repository.EmpleadoRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.repository.ParametrosRepository;
import com.crenx.data.domain.repository.ProductoRepository;
import com.crenx.data.domain.repository.UsuarioRepository;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.CrearObjSecVO;
import com.crenx.data.domain.vo.CreditoVO;
import com.crenx.util.Constantes;
@Service
public class MigracionServices {
	@Autowired
	private CatalogoRepository catRepository;
	@Autowired
	private OrgRepository orgRepository;
	@Autowired
	private ParametrosRepository paramRepository;
	@Autowired
	private CajaRepository cajaRepository;
	@Autowired
	private UtilityServices utServices;
	@Autowired
	private CatalogoServices catServices;
	@Autowired
	private ProductoServices prodServices;
	@Autowired
	private ProductoRepository prodRepository;
	@Autowired
	private UsuarioRepository usrRepository;
	@Autowired
	private SeguridadServices secServices;
	@Autowired
	private DomicilioRepository domRepository;
	@Autowired
	private EmpleadoRepository empRepository;

	private String getSourcePath()
	{
		Properties prop= new Properties();
		try
		{
			InputStream input= getClass().getClassLoader().getResourceAsStream(Constantes.NOMBRE_PROPERTIES);
			if(input!= null){
				prop.load(input);
			}
		}catch(Exception ex)
		{
			System.out.println("getSourcePath: " + ex.getMessage());			
			
		}		
		String retVal = prop.getProperty("migracion.path");	
		File folder = new File(retVal);
		if (!folder.exists())
			retVal = "/Users/scadena/Documents/Temp/";
		System.out.println("File path: " + retVal);
		return retVal;
	}
	
	
	public synchronized void migraArchivos()
	{
		//FIXME: Configurar Ruta
		String ruta = getSourcePath();
		File folder = new File(ruta);

		String[] extensions = new String[] { "cli" };
		List<File> files = (List<File>) FileUtils.listFiles(folder, extensions, true);
		
		for (File oneFile : files)
		{
			processFile(oneFile, "admin");
			moveFile(ruta, oneFile.getName());
		}
		
	}
	
	private void moveFile(String path, String name)
	{
		Path pathS = FileSystems.getDefault().getPath(path, name);
		try {
			Files.move(pathS, pathS.resolveSibling(name + "_done"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error la mover el archivo: "+ name + ": " + e.getMessage());
		}
	}
	
	private void processFile(File file, String principalName)
	{
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    String fileName = file.getName();
		    String ruta = fileName.substring(0, fileName.indexOf("."));

		    while (line != null) {
		        line = br.readLine();
			    processRecord(ruta, line, principalName);

		    }
		    //Close and Move
		}catch(Exception ex)
		{
			System.out.println("processFile: " + ex.getMessage());			
			
		}
		
	}
	
	@Transactional
	private void processRecord(String ruta, String line, String principalName)
	{
		
		try
		{
			Organizacion nodo = orgRepository.findByClave(ruta);
			Catalogo tipoFigura =  catServices.getCatalogoCveInterna("FIG_CLIENTE");
			Catalogo genero = catServices.getCatalogoByNombre("Masculino");			
			Catalogo frecCobro = catServices.getCatalogoCveInterna("PERIODO_DIARIO");	
			Catalogo unidadPlazo = catServices.getCatalogoByNombre("Días");	
			List<Producto> productos = prodRepository.findByNombreIsLike("Personal");
			String idProducto = productos.get(0).getIdProducto();
			
			String[] campos = line.split(",");
					
			ClienteVO input = new ClienteVO();		
			input.setIdRuta(nodo.getIdOrg());
			input.setNombre(campos[6]);
			input.setApellidoPaterno("...");
			input.setIdTipoFigura(tipoFigura.getIdCatalogo());
			input.setIdGenero(genero.getIdCatalogo());
			input.setTelCelular(campos[14]);
			Domicilio dom = new Domicilio();
			dom.setCalle(campos[13]);
			input.setDomicilioLaboral(dom);
			int numPagos = Integer.valueOf(campos[9]);
			int numPagados = Integer.valueOf(campos[10]);
			int numPendientes = Integer.valueOf(campos[11]);
			double montoOriginal = Double.parseDouble(campos[4]);
			double montoOriginalInt = Double.parseDouble(campos[5]);
			double saldo = Double.parseDouble(campos[7]);
			
			String plazo = campos[8];
			String tmp = plazo.substring(0, plazo.length()-1);
			double pago = Double.parseDouble(tmp);
			
			short iPlazo =(short)numPendientes;
			
			double valorCapital = Double.parseDouble(campos[4]);
			double valorTotal = Double.parseDouble(campos[5]);
			double interesNeto = (valorTotal-valorCapital);
			double tasa = interesNeto/valorCapital;
			double interesDiario = interesNeto/numPagos;
			double monto = Math.round(saldo/(1+tasa));
		
			CreditoVO credVO = new CreditoVO();

			Date fechaEmision = utServices.convertStringToDate(campos[1], "dd-MM-yy");
			Date fechaEmiDummy = new Date();
			Date fechaUltimoPago = utServices.convertStringToDate(campos[2], "dd-MM-yy");
			int diasVen = Integer.valueOf(campos[3]);
			String cal = String.valueOf((1-(diasVen/iPlazo))*100);			
			
			
			
			credVO.setCalificacion(Integer.valueOf(cal));
			credVO.setComision(0);
			credVO.setId(Long.valueOf(campos[0]));
			credVO.setMonto(montoOriginal);
			credVO.setSaldo(montoOriginal);
			credVO.setSaldoInsoluto(saldo);
			credVO.setPago(pago);
			credVO.setPlazo((short)numPagos);
			credVO.setTasa(tasa*100);
			credVO.setFechaEmision(utServices.convertDateToString(fechaEmision, "yyyy-MM-dd"));
			credVO.setIdRuta(nodo.getIdOrg());
			credVO.setIdFrecuenciaCobro(frecCobro.getIdCatalogo());
			credVO.setIdUnidadPlazo(unidadPlazo.getIdCatalogo());
			credVO.setIdProducto(idProducto);
			credVO.setProducto(idProducto);

		}catch(Exception ex)
		{
			System.out.println("processRecord: " + ex.getMessage());			
		}
	}
	
	
	public synchronized void migraUsuarios()
	{
		String ruta = getSourcePath();
		File folder = new File(ruta);
		String[] extensions = new String[] { "usu" };
		List<File> files = (List<File>) FileUtils.listFiles(folder, extensions, true);
		
		for (File oneFile : files)
		{
			processUsrFile(oneFile, "admin");
			moveFile(ruta, oneFile.getName());
		}
		
	}
	private void processUsrFile(File file, String principalName)
	{
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    String fileName = file.getName();
		    String ruta = fileName.substring(0, fileName.indexOf("."));
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode("Usuario123");
			Catalogo estatus = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");

		    while (line != null) {
		        line = br.readLine();
			    processUsrRecord(line, principalName, hashedPassword, estatus);

		    }
		    //Close and Move
		}catch(Exception ex)
		{
			System.out.println("processUsrFile: " + ex.getMessage());			
		}
		
	}
	
	
	private void processUsrRecord(String line, String principalName, String password, Catalogo estatus)
	{
		try
		{			
			String[] campos = line.split(",");
			String apePat = campos[0];
			String apeMat = campos[1];
			String nombre = campos[2];
			String userName = campos[3];
			String email = campos[4];
			String cargo = campos[5];
			String rol = campos[6];
			String fechaIngreso = campos[7];
			String ruta = campos[8];
			
			Usuario usuario = secServices.getPrincipal(userName);
			if(usuario==null){				
				usuario = new Usuario();			
				usuario.setHashContrasena(password);
				usuario.setCorreoElectronico(email);
				usuario.setNombreUsuario(userName);
				usuario.setNombre(nombre);
				usuario.setApellidoPaterno(apePat);
				usuario.setApellidoMaterno(apeMat);
				usuario.setCreacion(new Date());
				usuario.setEstatusActual(estatus);
				usrRepository.save(usuario);
				Empleado empl = new Empleado();
				empl.setApellidoMaterno(usuario.getApellidoMaterno());
				empl.setApellidoPaterno(usuario.getApellidoPaterno());
				empl.setCorreoElectronico(usuario.getCorreoElectronico());
				empl.setNombre(usuario.getNombre());
				empl.setCreacion(new Date());
				empl.setFechaIngreso(utServices.convertStringToDate(fechaIngreso, "yyyy-MM-dd"));
				empl.setFechaNacimiento(new Date());
				empl.setCurp("CURP");
				empl.setCveFiscal("RFC");
				empl.setDocIdentificacion("123456789");
				Domicilio dom = new Domicilio("76146", "Calle Lomas de Cruillas", "  ", "  ", "Lomas del Márquez", "QRO",  "Qro", "Mexico");
				domRepository.save(dom);
				empl.setDomicilioEmpleado(dom);
				empl.setTipoDocId(catServices.getCatalogoByNombre("Licencia de manejar"));
				empl.setGenero(catServices.getCatalogoByNombre("Masculino"));
				empl.setEstatusActual(estatus);
				empl.setCargoEmpleado(catServices.getCatalogoByNombre(cargo));
				empl.setSalarioAsimilado(0.0);
				empl.setSalarioBase(0.0);
				empl.setSemanasGracia(0);
				empl.setFechaIngreso(new Date());
				empl.setNombreUsuario(usuario.getNombreUsuario());
				empRepository.save(empl);

				CrearObjSecVO userRel = new  CrearObjSecVO();
				userRel.setIdUsuario(usuario.getIdUsuario());
				ObjetoSeguridad administrador = catServices.encuentraObjeto(rol);
				userRel.setIdObjetoSeguridadParent(administrador.getIdObjetoSeguridad());
				secServices.agregaUsuarioRol(userRel);			

				boolean suplente = ruta.indexOf("SSXX-")>=0;
				if (suplente)
				{
					ruta = ruta.substring(5,ruta.length());
				}
				Organizacion nodo = orgRepository.findByClave(ruta);
				if (nodo!=null)
				{
					if (suplente)
						nodo.setSuplente(usuario);
					else
						nodo.setTitular(usuario);
					orgRepository.save(nodo);
					
				}
			}			
			
		}catch(Exception ex)
		{
			System.out.println("processUsrRecord: " + ex.getMessage());
		}
	}
	
}

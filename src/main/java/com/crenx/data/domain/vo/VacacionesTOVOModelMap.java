package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Vacaciones;

public class VacacionesTOVOModelMap extends PropertyMap<Vacaciones, VacacionesVO>{
	@Override
    protected void configure() {
		map().setIdVacaciones(source.getIdVacaciones());
		map().setIdEmpleado(source.getIdEmpleado().getIdEmpleado());
		map().setIdAsunto(source.getAsunto().getIdCatalogo());
		map().setAsunto(source.getAsunto().getNombre());
		map().setEmpleado(source.getIdEmpleado().getNombre());
		
		using(dateToString).map(source.getFechaSolicitada()).setFechaSolicitada(null);

    }
   
   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
	   protected String convert(Date source) {
		   if (source==null)
			   return "";
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(source);
	   }
	 };


}

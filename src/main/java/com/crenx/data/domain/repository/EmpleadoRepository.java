package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Empleado;

public interface EmpleadoRepository extends CrudRepository<Empleado,String> {
	Empleado findByNombreUsuario(String nombreUsuario);
	List<Empleado> findByNombreLikeAndApellidoPaternoLike(String nombre, String apellidoPaterno);
	List<Empleado> findByFechaBajaIsNull();
}

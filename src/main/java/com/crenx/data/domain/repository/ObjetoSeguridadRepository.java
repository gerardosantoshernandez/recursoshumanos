package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.ObjetoSeguridad;

public interface ObjetoSeguridadRepository extends CrudRepository<ObjetoSeguridad,String> {

	List<ObjetoSeguridad> findByLlaveNegocio(String llaveNegocio);
	
	List<ObjetoSeguridad> findByTipoObje(String tipoObje);
	
}

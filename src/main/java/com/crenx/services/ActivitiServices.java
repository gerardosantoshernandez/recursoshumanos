package com.crenx.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crenx.data.domain.vo.OrgToTreeModelMap;
import com.crenx.data.domain.vo.OrgVO;
import com.crenx.data.domain.vo.TaskModelMap;
import com.crenx.data.domain.vo.TaskVO;

@Service
public class ActivitiServices {
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private SeguridadServices secServices;


    @Transactional
    public ProcessInstance createInstance(String processId, Map<String, Object> variables)
    {
    	ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processId, variables);
    	return processInstance;
    }
    
    public List<Task> getTaskbyCandidateGroup(String candidateGroup)
    {
    	return taskService.createTaskQuery().taskCandidateGroup(candidateGroup).list();
    	
    }

    public List<Task> getMyTasks(List<String> candidateGroups, String userName)
    {
    	userName = "%" + userName + "%";
    	List<Task> misTareas = taskService.createTaskQuery().taskAssigneeLike(userName).list();
    	List<Task> groupTasks = taskService.createTaskQuery().taskCandidateGroupIn(candidateGroups).list();
    	misTareas.addAll(groupTasks);
    	return misTareas;    	
    }

    public TaskVO getTaskByVarValue(String variableName,
            						Object variableValue)
    {
    	Execution exec = runtimeService.createExecutionQuery().processVariableValueEquals(variableName, variableValue).singleResult();
    	String instanceId = exec.getId();
    	
    	//Task task = taskService.createTaskQuery().taskVariableValueEquals(variableName, variableValue).singleResult();
    	Task task = taskService.createTaskQuery().executionId(instanceId).singleResult();
    	
    	ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new TaskModelMap());
		TaskVO theTask = mapper.map(task, TaskVO.class);
    	return theTask;   
    }

    
    public List<TaskVO> getMyTasks(String userName)
    {
    	List<String> groups = secServices.getStrUserGroups(userName);
    	List<Task> misTareas = getMyTasks(groups, userName);
    	ArrayList<TaskVO> retValue =  new ArrayList<TaskVO>();
    	for (Task unaTarea: misTareas)
    	{
			ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new TaskModelMap());
			TaskVO laTarea = mapper.map(unaTarea, TaskVO.class);
    		retValue.add(laTarea);
    	}
    	
    	return retValue;    	
    }

    public TaskVO getTask(String taskId, String userId)
    {
    	
    	Task task = taskService.createTaskQuery().taskId(taskId).includeProcessVariables().singleResult();  			
    	ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new TaskModelMap());
		TaskVO theTask = mapper.map(task, TaskVO.class);
    	return theTask;   
    }
    
    public void setVariableValue(String taskId, String varName, Object varValue)
    {
    	Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
    	String instanceId = task.getProcessInstanceId();    	
    	taskService.setVariable(taskId, varName, varValue);
    	runtimeService.setVariableLocal(instanceId, varName, varValue);
    	runtimeService.setVariable(instanceId, varName, varValue);
    }
    
    public void completeTask(String taskId, String userId)
    {
    	taskService.complete(taskId);
    }
}

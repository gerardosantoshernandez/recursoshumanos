package com.crenx.controller;
import java.security.Principal;
import java.util.Collection;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Producto;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.ProductoRepository;
import com.crenx.data.domain.vo.FiltrosProductoVO;
import com.crenx.data.domain.vo.ProductoToVOModelMap;
import com.crenx.data.domain.vo.ProductoVO;
import com.crenx.data.domain.vo.ProductoVOToProductoModelMap;
import com.crenx.services.CatalogoServices;
import com.crenx.services.ProductoServices;
import com.crenx.services.SeguridadServices;
import com.google.common.collect.Lists;

@RestController
@RequestMapping("/api/producto")
public class ProductoRestController {

	@Autowired
	private ProductoRepository productoRepository;
	@Autowired
	private ProductoServices productoServices;	
	
	@Autowired
	CatalogoServices catServices;

	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> getProductos(@RequestBody FiltrosProductoVO filtros) {
		return new ResponseEntity<>(productoServices.filtrarProductos(filtros),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="saveproducto", method = RequestMethod.POST)
	Producto createProducto(@RequestBody ProductoVO prodVO) {
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ProductoVOToProductoModelMap());	
		//Catalogo famProducto = catServices.getCatalogoId(prodVO.getIdFamiliaProducto());
		Catalogo cateProducto = catServices.getCatalogoId(prodVO.getIdCateProducto());
		Producto prod = mapper.map(prodVO, Producto.class);
		//prod.setFamiliaProducto(famProducto);
		prod.setCategoriaProducto(cateProducto);
		productoRepository.save(prod);
		return prod;
	}
	
}




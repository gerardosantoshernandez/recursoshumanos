package com.crenx.data.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Convocatoria;

public interface ConvocatoriaRepository extends CrudRepository<Convocatoria, String>{

}

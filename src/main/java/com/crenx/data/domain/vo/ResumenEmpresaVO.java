package com.crenx.data.domain.vo;

public class ResumenEmpresaVO {

	String ruta;
	String gerencia;
	String region;
	String division;
	String tipoOperacion;
	double monto=0;
	double abono=0;
	double comision=0;
	double desembolso=0;
	long nuevosCreditos;
	long nuevosClientes=0;
	long totalClientes=0;
	double caja=0;
	double cartera=0;
	double carteraCapital=0;
	double carteraNueva=0;
		
	public ResumenEmpresaVO(){}
	public ResumenEmpresaVO(
			String ruta,
			String gerencia,
			String region,
			String division,
			double monto,
			double abono,
			double comision, 
			long nuevosCreditos)
	{
		this.ruta = ruta;
		this.gerencia=gerencia;
		this.region = region;
		this.division = division;
		this.monto = monto;
		this.abono = abono;
		this.comision = comision;
		this.nuevosCreditos = nuevosCreditos;
	}
	public ResumenEmpresaVO(
			String ruta,
			String gerencia,
			String region,
			String division,
			double monto,
			double abono,
			double comision)
	{
		this.ruta = ruta;
		this.gerencia=gerencia;
		this.region = region;
		this.division = division;
		this.monto = monto;
		this.abono = abono;
		this.comision = comision;
	}
	public ResumenEmpresaVO(
			String ruta,
			String gerencia,
			String region,
			String division,
			double carteraTotal,
			double carteraCapital)
	{
		this.ruta = ruta;
		this.gerencia=gerencia;
		this.region = region;
		this.division = division;
		this.cartera = carteraTotal;
		this.carteraCapital = carteraCapital;
	}

	public ResumenEmpresaVO(
			String ruta,
			String gerencia,
			String region,
			String division,
			long nuevosClientes,
			long totalClientes,
			double carteraNueva)
	{
		this.ruta = ruta;
		this.gerencia=gerencia;
		this.region = region;
		this.division = division;
		this.nuevosClientes = nuevosClientes;
		this.totalClientes = totalClientes;
		this.carteraNueva = carteraNueva;
	}


	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public double getAbono() {
		return abono;
	}
	public void setAbono(double abono) {
		this.abono = abono;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public double getDesembolso() {
		return desembolso;
	}
	public void setDesembolso(double desembolso) {
		this.desembolso = desembolso;
	}
	public double getCaja() {
		return caja;
	}
	public void setCaja(double caja) {
		this.caja = caja;
	}
	public double getCartera() {
		return cartera;
	}
	public void setCartera(double cartera) {
		this.cartera = cartera;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public double getCarteraCapital() {
		return carteraCapital;
	}
	public void setCarteraCapital(double carteraCapital) {
		this.carteraCapital = carteraCapital;
	}
	public long getNuevosCreditos() {
		return nuevosCreditos;
	}
	public void setNuevosCreditos(long nuevosCreditos) {
		this.nuevosCreditos = nuevosCreditos;
	}
	public long getNuevosClientes() {
		return nuevosClientes;
	}
	public void setNuevosClientes(long nuevosClientes) {
		this.nuevosClientes = nuevosClientes;
	}
	public long getTotalClientes() {
		return totalClientes;
	}
	public void setTotalClientes(long totalClientes) {
		this.totalClientes = totalClientes;
	}
	public double getCarteraNueva() {
		return carteraNueva;
	}
	public void setCarteraNueva(double carteraNueva) {
		this.carteraNueva = carteraNueva;
	}
	
}

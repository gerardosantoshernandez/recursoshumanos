'use strict';
angular.module('crenx.reembolsoscontrollers', []).
controller('ReembolsosController', function ($scope, $rootScope, apiService,$http, $stateParams, $state, $timeout, $modal) {
	$scope.tree = [];

    $scope.locationRegion = {};
    $scope.allLocations = [];
    $scope.isDisabled = false;
	$scope.esGerencia=false;
	$scope.esRuta=false;
	$rootScope.nodeSelected = false;
	$rootScope.nodeType = null;
	$rootScope.operacion = "";
	$rootScope.allUsuarios = [];

	var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
	
/*	$(document).ready(function() {
		$(".btn-pref .btn").click(function () {
		    $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
		    // $(".tab").addClass("active"); // instead of this do the below 
		    $(this).removeClass("btn-default").addClass("btn-primary");   
		});
		});*/
	
//Permisos
	var permisos = $rootScope.repository.loggedUser.permissions;
	$scope.regFondeo = ($.inArray('OBJ_FONDEAR', permisos) > -1)?true:false;
	$scope.regGastos = ($.inArray('OBJ_REG_GASTOS', permisos) > -1)?true:false;
	$scope.regTransfer = ($.inArray('OBJ_REG_TRANSFER', permisos) > -1)?true:false;
	$scope.editar = ($.inArray('OBJ_ORG_EDITAR', permisos) > -1)?true:false;
	$scope.crear = ($.inArray('OBJ_ORG_CREAR', permisos) > -1)?true:false;
		
	//From UIModalsCtrl
	// Open Simple Modal
	$scope.openModal = function(modal_id, modal_size, modal_backdrop)
	{
		$rootScope.currentModal = $modal.open({
			templateUrl: modal_id,
			size: modal_size,
			backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
		});
	};
	    

	// Loading AJAX Content
	$scope.openAjaxModal = function(modal_id, url_location)
	{
		$rootScope.currentModal = $modal.open({
			templateUrl: modal_id,
			resolve: {
				ajaxContent: function($http)
				{
					return $http.get(url_location).then(function(response){
						$rootScope.modalContent = $sce.trustAsHtml(response.data);
					}, function(response){
						$rootScope.modalContent = $sce.trustAsHtml('<div class="label label-danger">Cannot load ajax content! Please check the given url.</div>');
					});
				}
			}
		});

		$rootScope.modalContent = $sce.trustAsHtml('Modal content is loading...');
	}
	//End From UIModalsCtrl
	
	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}
	
	function loadUnAssignedNodes()
	{
        apiService.get('/api/org/unAssigned', null,
                function (result)
                {
    				$rootScope.nodosSinAsignar = result.data;
                },
                serviceError);		
	}
	
    function loadOrgTree(parentId) {
        apiService.post('/api/org/transferTree', parentId,
                    function (result)
                    {
        				$rootScope.allRutaDestino = [];
        				/*
        				for (var i=0; i<result.data.divisiones.length; i++)
    					{
        					if (result.data.divisiones[i].id!=$rootScope.orgNode.id)
        						$rootScope.allRutaDestino.push(result.data.divisiones[i]);
    					}
        				for (var i=0; i<result.data.regiones.length; i++)
    					{
        					if (result.data.regiones[i].id!=$rootScope.orgNode.id)
        						$rootScope.allRutaDestino.push(result.data.regiones[i]);
    					}
        				for (var i=0; i<result.data.gerencias.length; i++)
    					{
        					if (result.data.gerencias[i].id!=$rootScope.orgNode.id)
        						$rootScope.allRutaDestino.push(result.data.gerencias[i]);
    					}
    					*/
        				for (var i=0; i<result.data.rutas.length; i++)
    					{
        					if (result.data.rutas[i].id!=$rootScope.orgNode.id)
        						$rootScope.allRutaDestino.push(result.data.rutas[i]);
    					}
			        	//$rootScope.allRutaDestino = result.data.rutas;
        				//addSelectItems("#ruta",$scope.rutas);
                    },
                    serviceError);

    }
	
	
	
	function loadTree() {
    	var orgId = $rootScope.repository.loggedUser.idTitOrg==null?"null":$rootScope.repository.loggedUser.idTitOrg;
        apiService.post('/api/org/tree', orgId,
                function loadCompleted(result) {
        					$scope.tree = result.data;

        					$('#tree').treeview({
        			            data: $scope.tree,
        			            onNodeSelected: function (event, data) {
        			            	
        			            	$rootScope.orgNode = data;
        			            	$rootScope.esGerencia=data.tipoNodo=="G"?true:false;
        			            	$rootScope.esRuta=data.tipoNodo=="RR"?true:false;
        			            	$rootScope.isDisabled = false;     
        			            	$rootScope.nodeSelected = true;
        			            	//FIXME: Filtrar los usuarios por tipo (Gerente, Regional, divisional, Etc)
        			            },
        			            onNodeUnselected: function (event, data) {
        			            	$rootScope.isDisabled = true;
        			           
        			            }
        			        });
        			        $scope.allLocations = result.data;
        			        $scope.loadingAllLocations = false;        
        },
                serviceError);

    }
	
    function loadCatalogo(catType) {
        apiService.post('/api/catalogo/items', catType,
        		function (result)
        		{
        			$rootScope.allGastos = result.data;        	
        		},
        		serviceError);

    }

    $rootScope.getRegla= function (idTipoGasto){
		apiService.post('/api/catalogo/getReglasGasto',idTipoGasto,
				function(result){
			$rootScope.reglasGastoCFDI = false;
			var i = 0;
			for (i=0;i<result.data.length;i++){
				if(result.data[i].requiereCFDI==true)
					$rootScope.reglasGastoCFDI = result.data[i].requiereCFDI;
			}
		},serviceError);
	}
    
	function catCalendarios()
	{
		apiService.get('/api/org/calendarios', null,
				function (result)
				{
					$rootScope.allCalendario = result.data;
				},
	            serviceError);
	}
	function catUsuarios()
	{
		apiService.post('/api/cuenta', {roles:[],},
				function (result)
				{
					$rootScope.allUsuarios = result.data;
				},
	            serviceError);
	}

	function serviceError(response) {
	    toastr.error(response.data.sourceMessage, response.data.errMessage, opts);
    }
    $rootScope.submitTheForm = function (item, event) {
        apiService.post('/api/org/savenode', $rootScope.orgNode,
            function (result) {
                $scope.rol = result.data;
                toastr.success("Se guardo satisfactoriamente", "Creación de estructura de la organización", opts);
                $scope.isUpdate = true;
                $rootScope.currentModal.close();
                loadTree();
            }
            , serviceError);
    };
    $rootScope.reset = function () {
    	this.resetDropzone();
    	this.file = null;
    	this.fileAdded = false;
    	
    }
  $rootScope.submitMovCaja = function (item, event) {
	  	if(this.fileAdded){
	  		$rootScope.submitMovCajaMultipart(item, event, this.file);
	  	}else{
	  		var fondeoVO = {};
	    	fondeoVO.fechaEmision = $rootScope.mc.fechaEmision;
	    	fondeoVO.monto = $rootScope.mc.monto;
	    	fondeoVO.idOrg = $rootScope.orgNode.id;
	    	fondeoVO.idDispositivo = "WebApp";
	    	fondeoVO.referencia = $rootScope.mc.referencia;
	    	fondeoVO.referenciaLarga = $rootScope.mc.referenciaLarga;
	    	fondeoVO.idOrgDestino = $rootScope.mc.idOrgDestino;
	    	fondeoVO.tipoMov = $rootScope.mc.tipoMov;
	    	fondeoVO.idTipoGasto = $rootScope.mc.idTipoGasto;
	       apiService.post('/api/org/saveMovCaja', fondeoVO,
	            function (result) {
	                toastr.success("Se registró satisfactoriamente el "+fondeoVO.tipoMov, fondeoVO.tipoMov, opts);
	                $rootScope.currentModal.close();
	            }
	            , serviceError);
	  	}    	
    };


    $rootScope.submitMovCajaMultipart = function (item, event, _file) {
    	var fd = new FormData();
    	//La imagen es obtenida a traves de la directiva fileModel
    	//cambiar el nombre de la variable con respecto a modelo usado en el imput
    	var file = _file;
    	var fondeo = {};
    	fondeo.fechaEmision =  $rootScope.mc.fechaEmision;
    	fondeo.monto = $rootScope.mc.monto;
    	fondeo.idOrg = $rootScope.orgNode.id;
    	fondeo.idDispositivo = "WebApp";
    	fondeo.referencia = $rootScope.mc.referencia;
    	fondeo.referenciaLarga = $rootScope.mc.referenciaLarga;
    	fondeo.idOrgDestino = $rootScope.mc.idOrgDestino;
    	fondeo.tipoMov = $rootScope.mc.tipoMov;
    	fondeo.idTipoGasto = $rootScope.mc.idTipoGasto;
    	
    	fd.append('fondeo', new Blob([JSON.stringify(fondeo)], {
            type: "application/json"
        }));
    	fd.append('file',file);
    	
        $http.post('/api/org/saveMovCajaMultipart', fd, {
            transformRequest: angular.identity, 
            headers: {'Content-Type': undefined}
            })
            .success(function(response){
            	toastr.success("Se registró satisfactoriamente el fondeo", "Fondeo", opts);
                $rootScope.currentModal.close();
            })
            .error(serviceError)
    };
    

    loadTree();
    catCalendarios();
    catUsuarios();

});

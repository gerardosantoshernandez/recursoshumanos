package com.crenx.data.domain.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrgVO {

	private String id;
	private String text;
	private String key;
	private List<OrgVO> nodes;
	private String tipoNodo;
	private String idCalendario;
	private String idTitular;
	private String idSuplente;
	private String titular;
	private String suplente;
	private int diaSupervision;
	private String idStatus;
	private String telFijo;
	private String telCelular;
	private String idParent;
	private int cuentaContable;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public List<OrgVO> getNodes() {
		return nodes;
	}
	public void setNodes(List<OrgVO> nodes) {
		this.nodes = nodes;
	}
	public String getTipoNodo() {
		return tipoNodo;
	}
	public void setTipoNodo(String tipoNodo) {
		this.tipoNodo = tipoNodo;
	}
	public String getIdCalendario() {
		return idCalendario;
	}
	public void setIdCalendario(String idCalendario) {
		this.idCalendario = idCalendario;
	}
	public String getIdTitular() {
		return idTitular;
	}
	public void setIdTitular(String idTitular) {
		this.idTitular = idTitular;
	}
	public String getIdSuplente() {
		return idSuplente;
	}
	public void setIdSuplente(String idSuplente) {
		this.idSuplente = idSuplente;
	}

	public int getDiaSupervision() {
		return diaSupervision;
	}
	public void setDiaSupervision(int diaSupervision) {
		this.diaSupervision = diaSupervision;
	}
	public String getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}
	public String getTelFijo() {
		return telFijo;
	}
	public void setTelFijo(String telFijo) {
		this.telFijo = telFijo;
	}
	public String getTelCelular() {
		return telCelular;
	}
	public void setTelCelular(String telCelular) {
		this.telCelular = telCelular;
	}
	public String getIdParent() {
		return idParent;
	}
	public void setIdParent(String idParent) {
		this.idParent = idParent;
	}
	public int getCuentaContable() {
		return cuentaContable;
	}
	public void setCuentaContable(int cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getSuplente() {
		return suplente;
	}
	public void setSuplente(String suplente) {
		this.suplente = suplente;
	}

}

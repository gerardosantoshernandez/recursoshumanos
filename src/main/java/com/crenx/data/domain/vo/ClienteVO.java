package com.crenx.data.domain.vo;

import java.util.ArrayList;
import java.util.List;

import com.crenx.data.domain.entity.Domicilio;
public class ClienteVO {

	protected long id;	
	protected String idCliente;	
	protected String idTipoFigura;
	protected String cveFiscal;
	protected String idTipoDocumentoIdentidad;
	protected String docIdentificacion;
	protected String nombre;
	protected String apellidoPaterno;
	protected String apellidoMaterno;
	protected String correoElectronico;
	protected String idGenero;
	protected String fechaNacimiento;
	protected String curp;
	protected String idPaisNacimiento;
	protected String idEntidadNacimiento;
	protected String idOcupacion;		
	protected String idDomicilioRes;
	protected String idDomicilioCobro;
	protected String idDomicilioLab;
	protected String telResidencia;
	protected String telCelular;
	protected String telLab;
	protected String nombreRefPersonal;
	protected String nombreRefFamiliar;
	protected String nombreEmpresa;
	protected String telRefPersonal;
	protected String telRefFamiliar;
	protected String idRuta;	
	protected Domicilio domicilioResidencial;
	protected Domicilio domicilioLaboral;
	protected String ruta;
	protected String parentId;
	protected String geoPosicion;
	protected boolean traspasar = false;
	protected List<CoDeudorVO> codeudores;
	protected List<DocumentoVO> documentos;
	protected String usuario;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public String getIdTipoFigura() {
		return idTipoFigura;
	}
	public void setIdTipoFigura(String idTipoFigura) {
		this.idTipoFigura = idTipoFigura;
	}
	public String getIdTipoDocumentoIdentidad() {
		return idTipoDocumentoIdentidad;
	}
	public void setIdTipoDocumentoIdentidad(String idTipoDocumentoIdentidad) {
		this.idTipoDocumentoIdentidad = idTipoDocumentoIdentidad;
	}
	public String getDocIdentificacion() {
		return docIdentificacion;
	}
	public void setDocIdentificacion(String docIdentificacion) {
		this.docIdentificacion = docIdentificacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public String getIdGenero() {
		return idGenero;
	}
	public void setIdGenero(String idGenero) {
		this.idGenero = idGenero;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getIdPaisNacimiento() {
		return idPaisNacimiento;
	}
	public void setIdPaisNacimiento(String idPaisNacimiento) {
		this.idPaisNacimiento = idPaisNacimiento;
	}
	public String getIdEntidadNacimiento() {
		return idEntidadNacimiento;
	}
	public void setIdEntidadNacimiento(String idEntidadNacimiento) {
		this.idEntidadNacimiento = idEntidadNacimiento;
	}
	public String getIdOcupacion() {
		return idOcupacion;
	}
	public void setIdOcupacion(String idOcupacion) {
		this.idOcupacion = idOcupacion;
	}
	public String getIdDomicilioRes() {
		return idDomicilioRes;
	}
	public void setIdDomicilioRes(String idDomicilioRes) {
		this.idDomicilioRes = idDomicilioRes;
	}
	public String getIdDomicilioCobro() {
		return idDomicilioCobro;
	}
	public void setIdDomicilioCobro(String idDomicilioCobro) {
		this.idDomicilioCobro = idDomicilioCobro;
	}
	public String getIdDomicilioLab() {
		return idDomicilioLab;
	}
	public void setIdDomicilioLab(String idDomicilioLab) {
		this.idDomicilioLab = idDomicilioLab;
	}
	public String getTelResidencia() {
		return telResidencia;
	}
	public void setTelResidencia(String telResidencia) {
		this.telResidencia = telResidencia;
	}
	public String getTelCelular() {
		return telCelular;
	}
	public void setTelCelular(String telCelular) {
		this.telCelular = telCelular;
	}
	public String getTelLab() {
		return telLab;
	}
	public void setTelLab(String telLab) {
		this.telLab = telLab;
	}
	public String getNombreRefPersonal() {
		return nombreRefPersonal;
	}
	public void setNombreRefPersonal(String nombreRefPersonal) {
		this.nombreRefPersonal = nombreRefPersonal;
	}
	public String getNombreRefFamiliar() {
		return nombreRefFamiliar;
	}
	public void setNombreRefFamiliar(String nombreRefFamiliar) {
		this.nombreRefFamiliar = nombreRefFamiliar;
	}
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	public String getTelRefPersonal() {
		return telRefPersonal;
	}
	public void setTelRefPersonal(String telRefPersonal) {
		this.telRefPersonal = telRefPersonal;
	}
	public String getTelRefFamiliar() {
		return telRefFamiliar;
	}
	public void setTelRefFamiliar(String telRefFamiliar) {
		this.telRefFamiliar = telRefFamiliar;
	}

	public String getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(String idRuta) {
		this.idRuta = idRuta;
	}
	public Domicilio getDomicilioResidencial() {
		return domicilioResidencial;
	}
	public void setDomicilioResidencial(Domicilio domicilioResidencial) {
		this.domicilioResidencial = domicilioResidencial;
	}
	public Domicilio getDomicilioLaboral() {
		return domicilioLaboral;
	}
	public void setDomicilioLaboral(Domicilio domicilioLaboral) {
		this.domicilioLaboral = domicilioLaboral;
	}
	public String getCveFiscal() {
		return cveFiscal;
	}
	public void setCveFiscal(String cveFiscal) {
		this.cveFiscal = cveFiscal;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getGeoPosicion() {
		return geoPosicion;
	}
	public void setGeoPosicion(String geoPosicion) {
		this.geoPosicion = geoPosicion;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public boolean isTraspasar() {
		return traspasar;
	}
	public void setTraspasar(boolean traspasar) {
		this.traspasar = traspasar;
	}
	public List<CoDeudorVO> getCodeudores() {
		return codeudores;
	}
	public void setCodeudores(List<CoDeudorVO> codeudores) {
		this.codeudores = codeudores;
	}
	/**
	 * @return the documentos
	 */
	public List<DocumentoVO> getDocumentos() {
		if(this.documentos == null){
			this.documentos = new ArrayList<DocumentoVO>();
		}
		return documentos;
	}
	/**
	 * @param documentos the documentos to set
	 */
	public void setDocumentos(List<DocumentoVO> documentos) {
		this.documentos = documentos;
	}

	/**
	 * Agrega un documento al cliente
	 * 
	 * @param docto
	 *            . Docuemtno a agregar
	 * @param match
	 *            . Si es true el documento será agregado siempre y cuando este
	 *            este asignado al cliente. Si es false siempre se agrega
	 */
	public void agregaDocumento(final DocumentoVO docto, boolean match) {
		boolean add = (docto != null);
		if (add && match) {
			add = (this.idCliente.equals(docto.getObjetoRelacionado()));
		}
		if (add) {
			this.getDocumentos().add(docto);
		}
	}
	
	/**
	 * Agrega un documento a los codeudores
	 * 
	 * @param docto
	 *            . Docuemtno a agregar
	 * @param match
	 *            . Si es true el documento será agregado al codedudor al cual
	 *            esta relacionado. Si es false se agrega a todos los codeudores
	 */
	public void agregaDocumentoCodeudores(final DocumentoVO docto, boolean match) {
		if(this.codeudores != null){
			for(CoDeudorVO codeudor: this.codeudores){
				codeudor.agregaDocumento(docto, match);
			}
		}
	}
}

package com.crenx.data.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Caja {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_caja", unique = true)
	private String idCaja;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_org", nullable=false, referencedColumnName="id_org")
	private Organizacion unidad;

	@Column(name = "fecha_corte")
	private Date fechaCorte;

	@Column(name = "saldo_incial")
	private double saldoInicial;
	
	@Column(name = "saldo_final")
	private double saldoFinal;

	@Column(name = "total_cargos")
	private double totalCargos;

	@Column(name = "total_abonos")
	private double totalAbonos;

	public String getIdCaja() {
		return idCaja;
	}

	public void setIdCaja(String idCaja) {
		this.idCaja = idCaja;
	}

	public Organizacion getUnidad() {
		return unidad;
	}

	public void setUnidad(Organizacion unidad) {
		this.unidad = unidad;
	}

	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public double getSaldoInicial() {
		return saldoInicial;
	}

	public void setSaldoInicial(double saldoInicial) {
		this.saldoInicial = saldoInicial;
	}

	public double getSaldoFinal() {
		return saldoFinal;
	}

	public void setSaldoFinal(double saldoFinal) {
		this.saldoFinal = saldoFinal;
	}

	public double getTotalCargos() {
		return totalCargos;
	}

	public void setTotalCargos(double totalCargos) {
		this.totalCargos = totalCargos;
	}

	public double getTotalAbonos() {
		return totalAbonos;
	}

	public void setTotalAbonos(double totalAbonos) {
		this.totalAbonos = totalAbonos;
	}
		
}

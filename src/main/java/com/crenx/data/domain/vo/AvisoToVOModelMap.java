package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.activiti.engine.impl.transformer.DateToString;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Aviso;
import com.crenx.data.domain.entity.Producto;

public class AvisoToVOModelMap extends PropertyMap<Aviso, AvisoVO>{
	@Override
	protected void configure(){
		map().setIdAviso(source.getIdAviso());
		map().setIdEmpleado(source.getIdEmpleado().getIdEmpleado());
		map().setIdTipoAviso(source.getIdTipoAviso().getIdCatalogo());
		map().setTipoAviso(source.getIdTipoAviso().getNombre());
		map().setEmpleado(source.getIdEmpleado().getNombre());
		
		using(dateToString).map(source.getFechaAviso()).setFechaAviso(null);
	}
	
	Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
	};
}

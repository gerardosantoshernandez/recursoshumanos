package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.PropiedadesDoc;

public interface PropiedadesDocRepository extends CrudRepository<PropiedadesDoc, String> {
	PropiedadesDoc findByDocumentoIdDocumentoAndNombre(String idDocumento,String nombre);
	List<PropiedadesDoc> findByValor(String idCliente);
	List<PropiedadesDoc> findByDocumentoIdDocumentoIn(List<String> docs);	
	List<PropiedadesDoc> findByValorIn(List<String> docs);	
}

package com.crenx.data.domain.vo;

public class DetalleCreditoPagosVO {
	private ClienteVO cliente;
	private CreditoVO credito;
	private int numeroTotalPagos;
	private int numeroPagados;
	private long numeroVencidos;
	private double  totalMontoVencido;
	private double   pagoPactado;
	private String ruta;
	private String gerencia;
	private String region;
	private String division;
	private VisitaVO visitas;
	
	public double getTotalMontoVencido() {
		return totalMontoVencido;
	}
	public void setTotalMontoVencido(double totalMontoVencido) {
		this.totalMontoVencido = totalMontoVencido;
	}
	public double getPagoPactado() {
		return pagoPactado;
	}
	public void setPagoPactado(double pagoPactado) {
		this.pagoPactado = pagoPactado;
	}
	//private List<TablaAmortizacionVO> pagos;
	
	public int getNumeroPagados() {
		return numeroPagados;
	}
	public int getNumeroTotalPagos() {
		return numeroTotalPagos;
	}
	public void setNumeroTotalPagos(int numeroTotalPagos) {
		this.numeroTotalPagos = numeroTotalPagos;
	}
	public void setNumeroPagados(int numeroPagados) {
		this.numeroPagados = numeroPagados;
	}
	public long getNumeroVencidos() {
		return numeroVencidos;
	}
	public void setNumeroVencidos(long numeroVencidos) {
		this.numeroVencidos = numeroVencidos;
	}
	public ClienteVO getCliente() {
		return cliente;
	}
	public void setCliente(ClienteVO cliente) {
		this.cliente = cliente;
	}
	public CreditoVO getCredito() {
		return credito;
	}
	public void setCredito(CreditoVO credito) {
		this.credito = credito;
	}
/*	public List<TablaAmortizacionVO> getPagos() {
		return pagos;
	}
	public void setPagos(List<TablaAmortizacionVO> pagos) {
		this.pagos = pagos;
	}*/
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public VisitaVO getVisitas() {
		return visitas;
	}
	public void setVisitas(VisitaVO visitas) {
		this.visitas = visitas;
	}
	
	
}

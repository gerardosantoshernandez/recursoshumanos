package com.crenx.data.domain.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Objeto de transferencia que contiene que representa el filtro para realizar
 * peticiones de consulta de los movimientos de la caja
 * 
 * @author JCHR
 *
 */
public class FiltroMovimientoCajaVO {

	/**
	 * Ids de las organizaciones que realizarón los movimientos de caja
	 */
	private List<String> orgs;
	/**
	 * Ids de los tipos movimientos correspondientes a los movimientos de la
	 * caja
	 */
	private List<String> tiposMovimiento;
	/**
	 * Ids de los tipos de operaciones correspondientes a los movimientos de la
	 * caja
	 */
	private List<String> tiposOperacion;

	/**
	 * Fecha inicial de consulta de movimientos caja en formato YYYY-MM-DD.
	 */
	private String fchInicio;
	/**
	 * Fecha final de consulta de movimientos caja en formato YYYY-MM-DD.
	 */
	private String fchFin;

	/**
	 * @return
	 */
	public List<String> getOrgs() {
		if (this.orgs == null) {
			this.orgs = new ArrayList<String>();
		}
		return orgs;
	}

	/**
	 * @param orgs
	 */
	public void setOrgs(List<String> orgs) {
		this.orgs = orgs;
	}

	/**
	 * @return
	 */
	public List<String> getTiposMovimiento() {
		if (this.tiposMovimiento == null) {
			this.tiposMovimiento = new ArrayList<String>();
		}
		return tiposMovimiento;
	}

	/**
	 * @param tiposMovimiento
	 */
	public void setTiposMovimiento(List<String> tiposMovimiento) {
		this.tiposMovimiento = tiposMovimiento;
	}

	/**
	 * @return
	 */
	public List<String> getTiposOperacion() {
		if (this.tiposOperacion == null) {
			this.tiposOperacion = new ArrayList<String>();
		}
		return tiposOperacion;
	}

	/**
	 * @param tiposOperacion
	 */
	public void setTiposOperacion(List<String> tiposOperacion) {
		this.tiposOperacion = tiposOperacion;
	}

	/**
	 * @return the fch_inicio
	 */
	public String getFchInicio() {
		return fchInicio;
	}

	/**
	 * @param fch_inicio the fch_inicio to set
	 */
	public void setFchInicio(String fch_inicio) {
		this.fchInicio = fch_inicio;
	}

	/**
	 * @return the fch_fin
	 */
	public String getFchFin() {
		return fchFin;
	}

	/**
	 * @param fch_fin the fch_fin to set
	 */
	public void setFchFin(String fch_fin) {
		this.fchFin = fch_fin;
	}
	
}

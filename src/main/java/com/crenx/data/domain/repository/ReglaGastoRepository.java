package com.crenx.data.domain.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.ReglaGasto;

public interface ReglaGastoRepository extends CrudRepository<ReglaGasto,String>{

	List<ReglaGasto> findByRolLlaveNegocioInAndGastoIdCatalogoIn(List<String> roles, List<String> ids);
	List<ReglaGasto> findByRolLlaveNegocioAndGastoIdCatalogo(String rol, String idGasto);
	List<ReglaGasto> findByRolLlaveNegocioInAndGastoIdCatalogo(List<String> roles, String idGasto);
	List<ReglaGasto> findByRolLlaveNegocioIn(List<String> roles);
	
}

'use strict';
angular.module('crenx.avisocontrollers', [])
.controller(
		'AvisoController',
function($scope, $rootScope, apiService, $stateParams, $state,
		$timeout,$modal) {
			
			$scope.openModal = function(modal_id, modal_size, modal_backdrop)
			{
				$rootScope.currentModal = $modal.open({
					templateUrl: modal_id,
					size: modal_size,
					backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
				});
			};
			
	$scope.filters = {
		categorias : [],
		asunto : null
	};
	$scope.listaAvisos = {
		parent : null,
		items : null
	};
	$scope.avisos = [];

	var filterTextTimeout;
	$scope.$watch('filters.categorias', function(val) {
		if (filterTextTimeout)
			$timeout.cancel(filterTextTimeout);
		filterTextTimeout = $timeout(function() {
			loadData();
		}, 250); // delay 250 ms
	})

	$scope.$watch('filters.asunto', function(val) {
		if (filterTextTimeout)
			$timeout.cancel(filterTextTimeout);
		filterTextTimeout = $timeout(function() {
			loadData();
		}, 250); // delay 250 ms
	})

	function loadFamilias() {
		apiService.post('/api/catalogo/items', 'CAT_TIP_AVISO',
				function loadCompleted(result) {
					$scope.listaAvisos = result.data;
				}, serviceError);

	}

	function prodLoadCompleted(result) {
		$scope.avisos = result.data;
	}

	function loadData() {
		apiService.post('/api/aviso', $scope.filters,
				prodLoadCompleted, serviceError);

	}

	function serviceError(response) {
		alert(response);
	}

	loadFamilias();
}).controller(
'FormAvisoController',
function($scope, $rootScope, apiService, $location, $http,
		$cookieStore, $stateParams, $modal) {
	$scope.listaAvisos = {
		parent : null,
		items : null
	};
	$scope.listaUnidadPlazo = {
		parent : null,
		items : null
	};
	$scope.listaFrecuenciaCobro = {
		parent : null,
		items : null
	};

	function loadItemsCatFamilia() {
		apiService.post('/api/catalogo/items', 'CAT_TIP_AVISO',
				function loadCompleted(result) {
					$scope.listaAvisos = result.data;
				}, ServiceError);
	}

	function loadItemsCatPlazo() {
		apiService.post('/api/catalogo/items', 'CAT_UNI_PLAZO',
				function loadCompleted(result) {
					$scope.listaUnidadPlazo = result.data;
				}, ServiceError);
	}

	function loadItemsCatFrecCobro() {
		apiService.post('/api/catalogo/items',
				'CAT_FREC_COBRO',
				function loadCompleted(result) {
					$scope.listaFrecuenciaCobro = result.data;
				}, ServiceError);
	}

	$scope.submitTheForm = function() {
		apiService.post('/api/aviso/saveaviso',
				$scope.avisoItem, ServiceSuccessComplete,
				ServiceError);
	}
	function ServiceSuccessComplete(result) {

		var opts = {
			"closeButton" : true,
			"debug" : false,
			"positionClass" : "toast-top-right",
			"onclick" : null,
			"showDuration" : "300",
			"hideDuration" : "1000",
			"timeOut" : "5000",
			"extendedTimeOut" : "1000",
			"showEasing" : "swing",
			"hideEasing" : "linear",
			"showMethod" : "fadeIn",
			"hideMethod" : "fadeOut"
		};

		toastr.success(
				"Se guardó satisfactoriamente el producto.",
				"Guardar producto", opts);

		$scope.isUpdate = true;
		$scope.user = result.data;
		$scope.errorsubmit = '';

	}

	function ServiceError(response) {
		$scope.errorsubmit = response.data;
	}
	if ($stateParams.avisoItem) {
		$scope.avisoItem = $stateParams.avisoItem;
		$scope.isUpdate = true;
	} else {

		$scope.avisoItem = {};
	}
	if (!$stateParams.listaAvisos) {
		loadItemsCatFamilia();
	} else {
		$scope.listaAvisos = $stateParams.listaAvisos;

	}

	loadItemsCatPlazo();
	loadItemsCatFrecCobro();

	if ($stateParams.avisoItem) {
		$scope.avisoItem = $stateParams.avisoItem;
	} else {
		$scope.avisoItem = {};
	}

});

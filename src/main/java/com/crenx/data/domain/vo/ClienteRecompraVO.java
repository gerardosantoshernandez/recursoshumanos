package com.crenx.data.domain.vo;

import java.util.List;

import com.crenx.data.domain.entity.Domicilio;

public class ClienteRecompraVO {

	protected long id;	
	protected String idCliente;	
	protected String ruta;
	protected List<CreditoVO> creditos;
	protected String usuario;
	protected String nombreCompleto;
	
	
	
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
	public List<CreditoVO> getCreditos() {
		return creditos;
	}
	public void setCreditos(List<CreditoVO> creditos) {
		this.creditos = creditos;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	

}

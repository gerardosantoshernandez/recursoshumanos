package com.crenx.data.domain.vo;

import java.util.Collection;

import com.crenx.data.domain.entity.Catalogo;

public class CatalogoVO {

	private Catalogo parent;
	private Collection<Catalogo> items;
	public Catalogo getParent() {
		return parent;
	}
	public void setParent(Catalogo parent) {
		this.parent = parent;
	}
	public Collection<Catalogo> getItems() {
		return items;
	}
	public void setItems(Collection<Catalogo> items) {
		this.items = items;
	}
	
	
	
}

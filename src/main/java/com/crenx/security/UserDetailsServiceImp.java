package com.crenx.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.crenx.data.domain.entity.Perfil;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.repository.UsuarioRepository;
import com.crenx.services.EmpleadoServices;

@Service
public class UserDetailsServiceImp implements UserDetailsService {

	@Autowired
	private UsuarioRepository userRepo;
	@Autowired
	private EmpleadoServices empServices;
	
	private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

	@Override
	public final Usuario loadUserByUsername(String username) throws UsernameNotFoundException {
		List<Usuario> users = userRepo.findByNombreUsuario(username);
		if(users.isEmpty()){
			throw new UsernameNotFoundException("user not found");
		}
		final Usuario user = users.get(0);		
		try
		{
			detailsChecker.check(user);
		}catch(Exception ex)
		{
			throw new UsernameNotFoundException(ex.getMessage());			
		}
		return user;
	}
	
	public final Usuario loadUserDetails(String userName)
	{
		List<Usuario> users = userRepo.findByNombreUsuario(userName);
		if(users.isEmpty()){
			throw new UsernameNotFoundException("user not found");
		}
		final Usuario user = users.get(0);	
		try
		{
			detailsChecker.check(user);
		}catch(Exception ex)
		{
			throw new UsernameNotFoundException(ex.getMessage());			
		}
		Perfil perfil =  null;
		try
		{
			perfil = empServices.loadPerfil(user);
		}catch(Exception ex)
		{
			throw new UsernameNotFoundException(ex.getMessage());			
		}
		user.setPerfil(perfil);		
		return user;
		
	}
	
}

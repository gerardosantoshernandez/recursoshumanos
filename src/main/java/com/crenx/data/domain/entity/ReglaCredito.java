package com.crenx.data.domain.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class ReglaCredito {
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_regla", unique = true)
	private String idRegla;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_rol", nullable=false, referencedColumnName="id_objetoSeguridad")
	private ObjetoSeguridad rol;
	
	@Column(name = "monto_maximo")
	private double maximo=0;

	public String getIdRegla() {
		return idRegla;
	}

	public void setIdRegla(String idRegla) {
		this.idRegla = idRegla;
	}

	public ObjetoSeguridad getRol() {
		return rol;
	}

	public void setRol(ObjetoSeguridad rol) {
		this.rol = rol;
	}

	public double getMaximo() {
		return maximo;
	}

	public void setMaximo(double maximo) {
		this.maximo = maximo;
	}
	
}



package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Calendario;


public interface CalendarioRepository extends CrudRepository<Calendario,String> {

	List<Calendario> findByNombre(String nombre);
}

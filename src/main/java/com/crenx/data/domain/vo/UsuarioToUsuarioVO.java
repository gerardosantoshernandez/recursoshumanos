package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Usuario;

public class UsuarioToUsuarioVO extends PropertyMap<Usuario,ListaUsuarioVO>{

	@Override
	protected void configure() {
		// TODO Auto-generated method stub
		map().setIdUsuario(source.getIdUsuario());
		map().setNombreUsuario(source.getNombreUsuario());
		map().setNombre(source.getNombre());
		map().setApellidoMaterno(source.getApellidoMaterno());
		map().setApellidoPaterno(source.getApellidoPaterno());
		map().setCorreoElectronico(source.getCorreoElectronico());
		//bloqueado n ose ocmo mapearlo
		map().setEstatusActual(source.getEstatusActual().getNombre());
				
	}

	Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				return df.format(source);
		   }
	};
}

package com.crenx.data.domain.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Organizacion {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_org", unique = true, length = 40)
	private String idOrg;
	
	
	@Column(name = "clave",length = 10)
	private String clave;
	
	@Column(name = "nombre",length = 250)
	private String nombre;
	
	@Column(name = "tipo_nodo",length = 10)
	private String tipoNodo;
	
	@Column(name = "nivel")
	private int nivel;

	//En esta caso el calendario lo deben heredar las rutas de la gerencia
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_calendario", nullable=true, referencedColumnName="id_calendario")
	private Calendario calendario;
	
	
	/*
	 * @JsonIgnore En este caso evita que al cargar el nodo recupere toda la lista de sus ancestros
	 * NOTA: Este no va en la UI
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_parent", nullable=true, referencedColumnName="id_org")
	@JsonIgnore
	private Organizacion parent;


	/*
	 * @JsonIgnore En este caso evita que al cargar el nodo recupere todos sus hijos
	 * NOTA: Tampoco va en la UI
	 */
	@OneToMany(fetch=FetchType.LAZY, mappedBy="parent")
	@OrderColumn(name="nivel")
	@Cascade( {CascadeType.ALL} )
	@JsonIgnore
	private List<Organizacion> children = new ArrayList<Organizacion>();
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_titular", nullable=true, referencedColumnName="id_usuario")
	private Usuario titular;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_suplente", nullable=true, referencedColumnName="id_usuario")
	private Usuario suplente;
	
	/* Combo  Lunes, Martes Miercoles Jueves Viernes Sabado Domingo
	 * 	int Domingo = 64;	
		int Lunes = 1;
		int Martes = 2;
		int Miercoles = 4;
		int Jueves = 8;
		int Viernes = 16;
		int Sabado = 32;
	 *  
	 *  
	 *  */
	@Column(name = "dia_supervision")
	private int diaSupervision;

	/*CAT_ESTATUS_GEN*/
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_estatus", nullable=true, referencedColumnName="id_catalogo")
	private Catalogo estatus;

	@Column(name = "tel_fijo",length = 20)
	private String telFijo;
	
	@Column(name = "tel_celular",length = 20)
	private String telCelular;
	
	@Column(name = "cuenta_contable")
	private int cuentaContable=0;
	
	public String getIdOrg() {
		return idOrg;
	}

	public void setIdOrg(String idOrg) {
		this.idOrg = idOrg;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipoNodo() {
		return tipoNodo;
	}

	public void setTipoNodo(String tipoNodo) {
		this.tipoNodo = tipoNodo;
	}

	public Calendario getCalendario() {
		return calendario;
	}

	public void setCalendario(Calendario calendario) {
		this.calendario = calendario;
	}

	public Organizacion getParent() {
		return parent;
	}

	public void setParent(Organizacion parent) {
		this.parent = parent;
	}

	public List<Organizacion> getChildren() {
		return children;
	}

	public void setChildren(List<Organizacion> children) {
		this.children = children;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}


	public Usuario getTitular() {
		return titular;
	}

	public void setTitular(Usuario titular) {
		this.titular = titular;
	}

	public Usuario getSuplente() {
		return suplente;
	}

	public void setSuplente(Usuario suplente) {
		this.suplente = suplente;
	}

	public int getDiaSupervision() {
		return diaSupervision;
	}

	public void setDiaSupervision(int diaSupervision) {
		this.diaSupervision = diaSupervision;
	}

	public Catalogo getEstatus() {
		return estatus;
	}

	public void setEstatus(Catalogo estatus) {
		this.estatus = estatus;
	}

	public String getTelFijo() {
		return telFijo;
	}

	public void setTelFijo(String telFijo) {
		this.telFijo = telFijo;
	}

	public String getTelCelular() {
		return telCelular;
	}

	public void setTelCelular(String telCelular) {
		this.telCelular = telCelular;
	}

	public int getCuentaContable() {
		return cuentaContable;
	}

	public void setCuentaContable(int cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
	
	public String getTitFullName()
	{
		String fullName = "";
		if (titular!=null)
		{
			fullName = titular.getNombre() + " " + titular.getApellidoPaterno() + " " + titular.getApellidoMaterno();
		}
		return fullName;
	}

	public String getSupFullName()
	{
		String fullName = "";
		if (suplente!=null)
		{
			fullName = suplente.getNombre() + " " + suplente.getApellidoPaterno() + " " + suplente.getApellidoMaterno();
		}
		return fullName;
	}
	
}

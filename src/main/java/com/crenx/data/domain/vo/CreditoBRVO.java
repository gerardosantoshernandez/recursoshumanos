package com.crenx.data.domain.vo;

import com.crenx.data.domain.entity.Catalogo;

public class CreditoBRVO {

	private Catalogo estatusCredito;
	private boolean requiereAval;
	private int creditosActivos;
	private String motivoRechazo;
	private boolean crearCredito = false;
	private boolean runTimeError = false;
	private boolean correccion =  false;
	
	
	public boolean isRunTimeError() {
		return runTimeError;
	}
	public void setRunTimeError(boolean runTimeError) {
		this.runTimeError = runTimeError;
	}
	public Catalogo getEstatusCredito() {
		return estatusCredito;
	}
	public void setEstatusCredito(Catalogo estatusCredito) {
		this.estatusCredito = estatusCredito;
	}
	public boolean isRequiereAval() {
		return requiereAval;
	}
	public void setRequiereAval(boolean requiereAval) {
		this.requiereAval = requiereAval;
	}
	public int getCreditosActivos() {
		return creditosActivos;
	}
	public void setCreditosActivos(int creditosActivos) {
		this.creditosActivos = creditosActivos;
	}
	public String getMotivoRechazo() {
		return motivoRechazo;
	}
	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}
	public boolean isCrearCredito() {
		return crearCredito;
	}
	public void setCrearCredito(boolean crearCredito) {
		this.crearCredito = crearCredito;
	}
	public boolean isCorreccion() {
		return correccion;
	}
	public void setCorreccion(boolean correccion) {
		this.correccion = correccion;
	}

	
	
}

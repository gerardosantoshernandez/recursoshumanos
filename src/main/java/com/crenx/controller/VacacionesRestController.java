package com.crenx.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Empleado;
import com.crenx.data.domain.entity.Vacaciones;
import com.crenx.data.domain.repository.EmpleadoRepository;
import com.crenx.data.domain.repository.VacacionesRepository;
import com.crenx.data.domain.vo.FiltrosVacacionesVO;
import com.crenx.data.domain.vo.VacacionesVO;
import com.crenx.data.domain.vo.VacacionesVOToVacacionesModelMap;
import com.crenx.services.CatalogoServices;
import com.crenx.services.VacacionesServices;

@RestController
@RequestMapping("/api/vacaciones")
public class VacacionesRestController {
	@Autowired
	private VacacionesRepository vacacionesRepository;
	@Autowired
	private EmpleadoRepository empleadoRepository;
	@Autowired
	private VacacionesServices vacacionesServices;
	@Autowired
	CatalogoServices catServices;
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> getVacaciones(@RequestBody FiltrosVacacionesVO filtros) {
		return new ResponseEntity<>(vacacionesServices.filtrarVacaciones(filtros),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="savevacaciones",method = RequestMethod.POST)
	Vacaciones createAnuncio(@RequestBody VacacionesVO vacacionesVO){
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new VacacionesVOToVacacionesModelMap());
		//Empleado empleado = catServices.getEmpleadoId(vacacionesVO.getIdEmpleado());
		//empleado.setIdEmpleado("402880845c94637d015c946850fe0235");
		Catalogo tipoVacaciones = catServices.getCatalogoId(vacacionesVO.getIdAsunto());
		Vacaciones vacaciones = mapper.map(vacacionesVO, Vacaciones.class);
		vacaciones.setAsunto(tipoVacaciones);
		//vacaciones.setIdEmpleado(empleado);
		vacacionesRepository.save(vacaciones);
		return vacaciones;
	}
}

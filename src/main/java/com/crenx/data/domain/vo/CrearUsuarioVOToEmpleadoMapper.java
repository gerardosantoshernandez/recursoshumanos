package com.crenx.data.domain.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Empleado;
public class CrearUsuarioVOToEmpleadoMapper extends PropertyMap<CrearUserVO,Empleado>{
	   @Override
	    protected void configure() {
			using(stringToDate).map(source.getFechaNacimiento()).setFechaNacimiento(null);
			using(stringToDate).map(source.getFechaIngreso()).setFechaIngreso(null);
			using(stringToDate).map(source.getFechaBaja()).setFechaBaja(null);

	    }
	   Converter<String, Date> stringToDate = new AbstractConverter<String, Date>() {
		   protected Date convert(String source) {
			   if (source==null)
				   return null;
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				try {
					return df.parse(source);
				} catch (ParseException e) {
					System.out.println(e.getMessage());
					return null;
				}
		   }
		 };

	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };
		 
}

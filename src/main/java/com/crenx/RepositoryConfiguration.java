package com.crenx;

import javax.persistence.EntityManagerFactory;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EntityScan(basePackages = {"com.crenx.data.domain.entity"})
@EnableJpaRepositories(basePackages = {"com.crenx.data.domain.repository"})
@EnableTransactionManagement
public class RepositoryConfiguration {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	
	@Bean
	  public PlatformTransactionManager transactionManager() {

	    JpaTransactionManager txManager = new JpaTransactionManager();
	    txManager.setEntityManagerFactory(entityManagerFactory);
	    return txManager;
	  }
	
	@Bean
	   public SessionFactory getSessionFactory() {
	  if(entityManagerFactory.unwrap(SessionFactory.class) == null){
	        throw new NullPointerException("factory is not a hibernate factory");
	      }
	      return  entityManagerFactory.unwrap(SessionFactory.class);
	   }
}

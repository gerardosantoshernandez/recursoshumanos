angular.module('crenx.documentoscontrollers',[]).
controller('DocumentosController', function ($scope, $rootScope,$modal,apiService,$http,$stateParams,$location, $timeout,  $cookieStore, documentService) {  
		var modalInstance;
		$scope.entity = $stateParams.entidad;
		$scope.entityType = $stateParams.figura;
		$scope.documentTypes = new Array();
		$scope.isNotSelectedType = false;
		$scope.documents = {};
		$scope.document = {};
		$scope.docToDelete={};
		$scope.mensaje='';		
		$scope.credits = null;
		$scope.idCredit='';
		$scope.numCredit='';
		$scope.oneCredit = true;
		$scope.idCatPagare='';
		$scope.showcredit=false;		
		//Si la aplicación es invocada desde el mobile no se debe mostrara la descarga de documentos
		$rootScope.allowDownload= !($rootScope.mobile_interface);
		//Permisos
		var permisos = $rootScope.repository.loggedUser.permissions;
		//Si la aplicación es invocada desde el mobile no se debe mostrara la carga de documentos
		$scope.crearDocumento = ($.inArray('OBJ_CLI_DOCS_CREAR',
							permisos) > -1 && !($rootScope.mobile_interface)) ? true
							: false;
		$scope.verDocs = ($.inArray('OBJ_CLI_DOCS_VER', permisos) > -1)?true:false;				
		complementScope();
		loadData();
		$rootScope.cleanEnvDocuments();
		
		/**
		 * Carga los datos iniciales
		 * @returns
		 */
		function loadData(){
			loadDocuments();
			loadDocumentTypes();
		}

		/**
		 * Carga los documentos de la entidad
		 * @returns
		 */
		function loadDocuments(){
		    apiService.get($scope.urlLoad, null,
		    		function(result){
		    			$scope.documents = result.data;
		    		},
		    		function(response){    			  
		    			$scope.mensaje='No se pueden cargar los documentos'+response;
		          		jQuery('#modalMensaje').modal('show');
		    		});
		}

		/**
		 * Carga el catalogo de los tipos de documento
		 * @returns
		 */
		function loadDocumentTypes() {
		    apiService.post('/api/catalogo/items', 'CAT_TIP_DOC',
		    	function(result){
		    		$scope.documentTypes = result.data;
		    		$scope.idCatPagare = '';
		    		//Se obtiene una referencia al id del tipo de documento pagare del catálogo
		    		if($scope.documentTypes  && $scope.documentTypes.items){
		    			var items = $scope.documentTypes.items;
		    			if(items.length > 0){
		        			for(var idx = 0; idx < items.length ;idx++){
		        				if(items[idx].nombre == 'Pagaré'){
		        					$scope.idCatPagare = items[idx].idCatalogo;
		        					break;
		        				}
		        			}
		        		}
		    		}
		    	},
		    	function(response){
		    		alert('No se puede cargar catalogo de documentos'+response)
		    	});
		}

		/**
		 * Complementa los datos del Scope de este controller
		 * @returns
		 */
		function complementScope(){
			// Se agregan a scope las variables comunes
			
			// Limpia el área de drop
			$scope.reset = function() {
			    $scope.resetDropzone();
			}
			//Guarda la referencia del documento a eliminar
			$scope.holdDocToDelete=function(documentoItem){		
				$scope.docToDelete=documentoItem;
			}
						
			//Borra el documento
			$scope.deleteDocument =function (){
				apiService.get('/api/documentos/eliminarDocumento/' + $scope.docToDelete.idDocumento, null,
			    		function(result){
			    			jQuery('#modalEliminar').modal('hide');
			    			$scope.mensaje='El documento se ha eliminado';
			    			jQuery('#modalMensaje').modal('show');
			    			loadDocuments();
			    		},
			    		function(response){
			    			$scope.mensaje='No se puede eliminar el documento'+response;
			        		jQuery('#modalMensaje').modal('show');
			    		}
			    );
		    	$scope.docToDelete = {};
			 }
			
			//Descarga de Documento
			 $scope.downloadDocument = function (docto){
				 var url_location = "/api/documentos/consultarDocumento/"+docto.idDocumento;
				 	$rootScope.downloadDocument(url_location, docto.nombre);
				 	$rootScope.currentDocument = null;
			 };
			 
			//Visualización del documento
			 $scope.display = function (docto, modal_id){
				 $rootScope.currentDocument = docto;
				 var url_location = "/api/documentos/consultarDocumento/"+docto.idDocumento;
				 $rootScope.displayDocument(url_location, modal_id);				 
			 };
			 
			 //Crea la sección modal
			 $scope.showModal= function(){
				 modalInstance= $modal.open({templateUrl:'app/tpls/documentos/adjuntar-documentos.html'
					 ,controller: 'ModalInstanceCtrl'
					 ,backdrop  : 'static',
					  keyboard  : false})
			};
			
			 complementScopeClient();
			 complementScopeEmpleado();
			 
			 /**
			  * Salva el documento
			  */
			 $scope.uploadFile = function() {
					if($scope.document.idTipoDoc){
						$scope.isNotSelectedType=false;				
					}else{
						$scope.isNotSelectedType=true;
						return;
					}
					$http.post($scope.urlUpload, $scope.getFormData(), {
							transformRequest: angular.identity, 
							headers: {'Content-Type': undefined}
				         })
				         .success(function(response){
				        		$scope.resetDropzone();
				        		loadDocuments();
				        		$scope.mensaje='El archivo se guardo exitosamente';
				        		jQuery('#modalMensaje').modal('show');
				        		$scope.file=null;
				         })
				         .error(function(response){
				        	 var error= response.Error;
				 			$scope.resetDropzone();
				 			$scope.mensaje='Ocurrio un error al guardar el archivo: ' + error;
				    		jQuery('#modalMensaje').modal('show');
				         });
				};
		}
		/**
		 * Complementa el scope con la información si la entidad es de tipo Cliente
		 * @returns
		 */
		function complementScopeClient(){
			if($scope.entityType == 'FIG_CLIENTE'|| $scope.entityType == 'FIG_CODEUDOR'){
				$scope.urlLoad = '/api/documentos/obtenerDocCliente/'+$scope.entity.idCliente;
				
				$scope.urlUpload = '/api/cliente/upload';
				
				//Muesta u oculta el campo de texto donde se muestra el crédito activo 
				//cuando el tipo de documento seleccionado es un pagare
				$scope.showCreditField = function(){
					//se compara si el tipo documento seleccionado es igual a un pagare.
					//Solo la opción del cliente es permitido a una figura CLIENTE
					$scope.showcredit =($scope.idCatPagare == $scope.document.idTipoDoc 
							&& $scope.entityType == 'FIG_CLIENTE');
					if($scope.showcredit && $scope.credits == null){
						//Se buscan los creditos del cliente
						 apiService.get('/api/cliente/creditos/' + $scope.entity.idCliente, null,
						    		function(result){
							 			if(result.data && result.data.length > 0){
							 				$scope.credits = new Array();
							 				var credit;
							 				for(var idx = 0; idx < result.data.length; idx++){
							 					credit = result.data[idx];
							 					//Si las claves de los estatus cambian actualizar la expresión regular
							 					//usada en esta condición
							 					if(credit.estatusCredito && 
							 							credit.estatusCredito.match(/(Activo|Pendiente)/g)){
							 						$scope.credits.push(credit);
							 					}
							 				}
							 				if($scope.credits.length == 1){
							 					$scope.idCredit = $scope.credits[0].idCredito;
							 					$scope.numCredit = $scope.credits[0].id;
							 				}else{
							 					$scope.oneCredit = false;
							 				}
							 			}			 			
						    		  },
						    		function(response){
						    			  $scope.idCredit = '';
						    			  $scope.mensaje='No se pueden cargar los creditos'+response;
						          		jQuery('#modalMensaje').modal('show');
						    			  });
							}
				};
				
				$scope.getFormData = function(){
					var file = $scope.file;
				     var fd = new FormData();
				     fd.append('file', file);
				     fd.append('id',$scope.entity.idCliente);
				     fd.append('idDocumento',$scope.document.idTipoDoc);
				     if($scope.showcredit){
				    	 fd.append('idCredito',$scope.idCredit);
				     };
				     return fd;
				};
			}
		}

		/**
		 * Complementa el scope con la información si la entidad es de tipo Empleado
		 * @returns
		 */
		function complementScopeEmpleado(){
			if($scope.entityType == 'FIG_EMPLEADO'){
				$scope.urlLoad = '/api/documentos/obtenerDocEmpleado/'+$scope.entity.idUsuario;
				
				$scope.urlUpload = '/api/documentos/upload';
				
				$scope.showCreditField = function(){
					$scope.showcredit = false;				
				};
				
				$scope.getFormData = function(){
					var file = $scope.file;
				     var fd = new FormData();
				     fd.append('file', file);
				     fd.append('idEntity',$scope.entity.idUsuario);
				     fd.append('entityType','IdEmpleado');
				     fd.append('idTipoDocto',$scope.document.idTipoDoc);
				     return fd;
				};				
			}
		}
}).controller('ModalInstanceCtrl', function ($scope, $modalInstance,apiService) {
    $scope.cerrarModal = function(){
       $modalInstance.close();
    }
    
});

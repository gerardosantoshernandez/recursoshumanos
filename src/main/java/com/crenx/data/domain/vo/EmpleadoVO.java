package com.crenx.data.domain.vo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.crenx.data.domain.entity.Catalogo;

public class EmpleadoVO {
	private String idEmpleado;
	private String cveFiscal;
	private String docIdentificacion;
	private String fechaIngreso;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String correoElectronico;
	private String nombreUsuario;
	private String nombreCompleto;
	private String fechaBaja;
	private double salarioBase=0;
	private double salarioAsimilado=0;
	private int semanasGracia = 0;
	private String cveEmpleado;
	
	public String getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getCveFiscal() {
		return cveFiscal;
	}
	public void setCveFiscal(String cveFiscal) {
		this.cveFiscal = cveFiscal;
	}
	public String getDocIdentificacion() {
		return docIdentificacion;
	}
	public void setDocIdentificacion(String docIdentificacion) {
		this.docIdentificacion = docIdentificacion;
	}
	public String getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getNombreCompleto() {
		nombreCompleto = this.nombre + " " + this.apellidoPaterno + " " + this.apellidoMaterno;
		return nombreCompleto;
	}
	public double getSalarioBase() {
		return salarioBase;
	}
	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}
	public double getSalarioAsimilado() {
		return salarioAsimilado;
	}
	public void setSalarioAsimilado(double salarioAsimilado) {
		this.salarioAsimilado = salarioAsimilado;
	}
	public int getSemanasGracia() {
		return semanasGracia;
	}
	public void setSemanasGracia(int semanasGracia) {
		this.semanasGracia = semanasGracia;
	}
	public String getCveEmpleado() {
		return cveEmpleado;
	}
	public void setCveEmpleado(String cveEmpleado) {
		this.cveEmpleado = cveEmpleado;
	}
	public String getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	
}

package com.crenx.business;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.util.StringUtils;

/**
 * Realiza el manejo de archivos dentro de la aplicaci�n
 * 
 * @author JCHR
 *
 */
public abstract class FileManager {
	protected String workDirectory;
	protected boolean relativePaths;

	/**
	 * Guarda un archivo
	 * 
	 * @param data
	 *            Los bytes que representan al archivo
	 * @param dirPath
	 *            Path absoluto donde el archivo debe ser creado
	 * @param fileName
	 *            Nombre del archivo
	 * @param mimetype
	 *            . El MIME del archivo
	 * @return
	 * @throws IOException
	 */
	public abstract File save(byte[] data, String dirPath, String fileName, String mimetype)
			throws IOException, IllegalArgumentException;

	/**
	 * Obtiene un archivo de una determinada ruta
	 * 
	 * @param dirPath
	 *            Path absoluto del directorio donde el archivo esta contenido
	 * @param fileName
	 *            Nombre del archivo
	 * @return
	 * @throws IOException
	 */
	public abstract File get(String dirPath, String fileName) throws IOException, IllegalArgumentException;

	/**
	 * Borra un archivo que se encuentra dentro un directorio especifico
	 * 
	 * @param dirPath
	 *            Path del directorio donde se encuentra el archivo
	 * @param fileName
	 *            Nombre del archivo a borrar
	 * @throws IOException
	 */
	public abstract void delete(String dirPath, String fileName) throws IOException, IllegalArgumentException;

	/**
	 * Crea un directorio
	 * 
	 * @param parentDirPath
	 *            Path del directorio donde será creado el nuevo directorio
	 * @param dirName
	 *            Nombre del directorio a crear
	 */
	public abstract void createDirectory(String parentDirPath, String dirName)
			throws IOException, IllegalArgumentException;

	/**
	 * Comprueba que el archivo exista, si el nombre del archivo va en null se
	 * comprobará la existencia de el directorio
	 * 
	 * @param dirPath
	 *            Path del directorio donde se comprobara la existencia del
	 *            archivo
	 * @param fileName
	 *            Nombre del archivo
	 * @return true si existe
	 */
	public abstract boolean exists(String dirPath, String fileName);

	/**
	 * Pone el directorio donde el file manager trabajara por default.
	 * 
	 * @param defaultDirectory
	 */
	public void setWorkDirectory(String workDirectory) {
		if (StringUtils.hasText(workDirectory)) {
			this.workDirectory = workDirectory;
		}
	}

	/**
	 * Regresa el directorio de trabaja del File manager
	 * 
	 * @return
	 */
	public String getWorkDirectory() {
		return workDirectory;
	}

	/**
	 * Indica si las direcciones que cada uno de los métodos pide serán
	 * relativas a el directorio de trabajo del File manager o absolutas
	 * 
	 * @return
	 */
	public boolean isRelativePaths() {
		return this.relativePaths;
	}

	/**
	 * Si es true el parametro de entrada le indica al File Manager que maneje
	 * todas las direcciones relativas al directorio de trabajo del File Manager
	 * 
	 * @param relative
	 * @return
	 */
	public void setRelativePaths(boolean relative) {
		this.relativePaths = relative;
	}

	/**
	 * Obtiene un path de acuerdo a si son relativas o no las direcciones al
	 * directorio de trabajo del File Manager
	 * 
	 * @param dirPath
	 * @param fileName
	 * @param separator
	 * @return
	 */
	protected String getPath(final String dirPath, final String fileName, char separator)
			throws IllegalArgumentException {
		String path = (this.relativePaths) ? this.workDirectory : "";
		if (StringUtils.hasText(dirPath)) {
			path = FileUtils.concatPath(path, dirPath, separator);
		}
		if (StringUtils.hasText(fileName)) {
			path = FileUtils.concatPath(path, fileName, separator);
		}
		if (BooleanUtils.isFalse(StringUtils.hasText(path))) {
			throw new IllegalArgumentException(
					"The directory path is null or empty. File Manager is working with absolute paths.");
		}
		return path;
	}

}

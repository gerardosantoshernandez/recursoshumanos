package com.crenx.data.domain.repository;
import java.util.List;
import com.crenx.data.domain.entity.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<Producto,String>{

	List<Producto> findByFamiliaProducto_IdCatalogoInAndNombre(List<String> familias, String nombre);
	List<Producto> findByCategoriaProducto_IdCatalogoInAndNombre(List<String> categorias, String nombre);
	List<Producto> findByFamiliaProducto_IdCatalogoIn(List<String> familias);
	List<Producto> findByNombreIsLike(String nombre);
	List<Producto> findByIdProducto(String idProducto);
	
}


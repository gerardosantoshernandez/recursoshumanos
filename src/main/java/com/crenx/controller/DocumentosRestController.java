package com.crenx.controller;

import java.io.File;
import java.security.Principal;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.crenx.data.domain.vo.DocumentoVO;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltroDocumentosVO;
import com.crenx.services.DocumentoServices;
import com.google.common.io.Files;

@RestController
@RequestMapping("/api/documentos")
public class DocumentosRestController {

	private final Logger LOG = LogManager.getLogger(DocumentosRestController.class);

	@Autowired
	private DocumentoServices doctoService;

	/**
	 * Obtiene los documentos que fueron referenciados a un determinado cliente
	 * 
	 * @param clienteId
	 * @return
	 */
	@RequestMapping(value = "obtenerDocCliente/{clienteId}", method = RequestMethod.GET)
	public ResponseEntity<?> obtenerDocumentosPorCliente(@PathVariable String clienteId) {
		LOG.info("Obteniendo documentos del cliente:" + clienteId);
		final FiltroDocumentosVO filtro = new FiltroDocumentosVO();
		filtro.setIdCliente(clienteId);
		return new ResponseEntity<>(doctoService.obtenerDetalleDocumentos(filtro), HttpStatus.OK);
	};

	/**
	 * Obtiene los documentos que fueron referenciados a una determinada
	 * Organizacion organizacion
	 * 
	 * @param clienteId
	 * @return
	 */
	@RequestMapping(value = "obtenerDocOrg/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<?> obtenerDocumentosPorOrganizacion(@PathVariable String orgId) {
		LOG.info("Obteniendo Documentos de la organización:" + orgId);
		final FiltroDocumentosVO filtro = new FiltroDocumentosVO();
		filtro.setIdMovCajaTrasferencia(orgId);
		return new ResponseEntity<>(doctoService.obtenerDetalleDocumentos(filtro), HttpStatus.OK);
	};

	/**
	 * Obtiene los documentos que fueron referenciados a un determinado Gasto
	 * organizacion
	 * 
	 * @param clienteId
	 * @return
	 */
	@RequestMapping(value = "obtenerDocGasto/{IdGasto}", method = RequestMethod.GET)
	public ResponseEntity<?> obtenerDocumentosPorGasto(@PathVariable String IdGasto) {
		LOG.info("Obteniendo Documentos del gasto:" + IdGasto);
		final FiltroDocumentosVO filtro = new FiltroDocumentosVO();
		filtro.setIdMovCajaGasto(IdGasto);
		return new ResponseEntity<>(doctoService.obtenerDetalleDocumentos(filtro), HttpStatus.OK);
	};

	/**
	 * Obtiene los documentos que fueron referenciados a un determinado Empleado
	 * organizacion
	 * 
	 * @param clienteId
	 * @return
	 */
	@RequestMapping(value = "obtenerDocEmpleado/{IdEmpl}", method = RequestMethod.GET)
	public ResponseEntity<?> obtenerDocumentosPorEmpleado(@PathVariable String IdEmpl) {
		LOG.info("Obteniendo Documentos del empleado:" + IdEmpl);
		final FiltroDocumentosVO filtro = new FiltroDocumentosVO();
		filtro.setIdEmpleado(IdEmpl);
		return new ResponseEntity<>(doctoService.obtenerDetalleDocumentos(filtro), HttpStatus.OK);
	};

	/**
	 * Obtiene los documentos necesarios para una aprobación de crédito
	 * 
	 * @param idCliente
	 * @param idCredito
	 * @return
	 */
	@RequestMapping(value = "obtenerDoctosPorFiltro", method = RequestMethod.POST)
	public ResponseEntity<?> obtenerDocumentosPorFiltro(@RequestBody FiltroDocumentosVO filtro) {
		return new ResponseEntity<>(doctoService.obtenerDetalleDocumentos(filtro), HttpStatus.OK);
	};

	/**
	 * @param docId
	 * @return
	 */
	@RequestMapping(value = "eliminarDocumento/{docId}", method = RequestMethod.GET)
	public ResponseEntity<?> eliminarDocumento(@PathVariable String docId) {
		try {
			this.doctoService.eliminarDocumento(docId, true);
			return new ResponseEntity<>("{}", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("{}", HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * @param docId
	 * @return
	 */
	@RequestMapping(value = "consultarDocumento/{docId}", method = RequestMethod.GET)
	public ResponseEntity<?> descargarDocumento(@PathVariable String docId) {
		return this.getDocumento(docId, false);
	}

	/**
	 * @param docId
	 * @return
	 */
	@RequestMapping(value = "consultarDocumentoB64/{docId}", method = RequestMethod.GET)
	public ResponseEntity<?> descargarDocumentoBase64(@PathVariable String docId) {
		return this.getDocumento(docId, true);
	}

	/**
	 * Guarda un documento relacionado a alguna entidad del sistema.
	 * 
	 * @param principal
	 * @param idEntity.
	 *            El id de la entidad(empleado, cliente, gasto, organización ,
	 *            etcétera) a la cual se relaciona el documento
	 * @param file.
	 *            El stream de bytes del documento
	 * @param idTipoDocto.
	 *            El id del catálogo de tipos de documentos
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "upload")
	public ResponseEntity<?> upload(final Principal principal, @RequestParam("idEntity") final String idEntity,
			@RequestParam("entityType") final String entityType, @RequestParam("file") final MultipartFile file,
			@RequestParam("idTipoDocto") final String idTipoDocto) {
		try {
			this.doctoService.salvarDocumento(file, idEntity, idTipoDocto, null, entityType, true);
		} catch (Exception e) {
			MultiValueMap headers = new MultiValueMap();
			headers.put("Error", e.getMessage());
			return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("{}", HttpStatus.OK);
	}

	/**
	 * @param docId
	 * @return
	 */
	private ResponseEntity<?> getDocumento(final String docId, boolean base64) {
		try {
			final DocumentoVO docto = this.doctoService.obtenerDetalleDocumento(docId);
			final File archivo = this.doctoService.obtenerDocumento(docId);
			byte[] data = Files.toByteArray(archivo);
			HttpHeaders headers = new HttpHeaders();
			headers.add("content-disposition", "attachment; filename=" + docto.getFileName());
			headers.add("fn",docto.getFileName());
			headers.setContentType(MediaType.parseMediaType(
					StringUtils.isBlank(docto.getContentType()) ? "application/pdf" : docto.getContentType()));
			return new ResponseEntity<>((base64) ? Base64.encodeBase64(data) : data, headers, HttpStatus.OK);
		} catch (Exception e) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al cargar el archivo");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
		}
	}
}

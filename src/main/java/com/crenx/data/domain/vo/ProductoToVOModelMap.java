package com.crenx.data.domain.vo;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Producto;

public class ProductoToVOModelMap extends PropertyMap<Producto, ProductoVO>{
	   @Override
	    protected void configure() {
			map().setIdProducto(source.getIdProducto());
			map().setIdFamiliaProducto(source.getFamiliaProducto().getIdCatalogo());
			map().setIdCateProducto(source.getCategoriaProducto().getIdCatalogo());
			map().setFamiliaProducto(source.getFamiliaProducto().getNombre());
			map().setCategoriaProducto(source.getCategoriaProducto().getNombre());
			
			using(dateToString).map(source.getFechaCreacion()).setFechaCreacion(null);

	    }
	   
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };

}

package com.crenx.data.domain.vo;

import org.springframework.web.multipart.MultipartFile;

public class FondeoVO {

	//
	String idMC;
	String idMCDestino;
	String tipoMov;
	String idDispositivo;
	String usuario;
	String fechaEmision;
	double monto;
	String referencia;
	String idOrg;
	String idOrgDestino;
	String referenciaLarga;
	String idTipoGasto;
	String nombreGasto;
	String descripcion;
	MultipartFile file;
	boolean aprobado;
	String taskId;
	String idDocto;

	public String getIdMC() {
		return idMC;
	}

	public void setIdMC(String idMC) {
		this.idMC = idMC;
	}

	public String getIdDispositivo() {
		return idDispositivo;
	}

	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getIdOrg() {
		return idOrg;
	}

	public void setIdOrg(String idOrg) {
		this.idOrg = idOrg;
	}

	public String getReferenciaLarga() {
		return referenciaLarga;
	}

	public void setReferenciaLarga(String referenciaLarga) {
		this.referenciaLarga = referenciaLarga;
	}

	public String getIdOrgDestino() {
		return idOrgDestino;
	}

	public void setIdOrgDestino(String idOrgDestino) {
		this.idOrgDestino = idOrgDestino;
	}

	public String getTipoMov() {
		return tipoMov;
	}

	public void setTipoMov(String tipoMov) {
		this.tipoMov = tipoMov;
	}

	public String getIdTipoGasto() {
		return idTipoGasto;
	}

	public void setIdTipoGasto(String idTipoGasto) {
		this.idTipoGasto = idTipoGasto;
	}

	public String getNombreGasto() {
		return nombreGasto;
	}

	public void setNombreGasto(String nombreGasto) {
		this.nombreGasto = nombreGasto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isAprobado() {
		return aprobado;
	}

	public void setAprobado(boolean aprobado) {
		this.aprobado = aprobado;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getIdMCDestino() {
		return idMCDestino;
	}

	public void setIdMCDestino(String idMCDestino) {
		this.idMCDestino = idMCDestino;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	/**
	 * Regresa el Id del Documento que fue asociado a un gasto o una
	 * trasferencia
	 * 
	 * @return El id del documento
	 */
	public String getIdDocto() {
		return idDocto;
	}

	/**
	 * Pone el Id del Documento que fue asociado a un gasto o una trasferencia
	 * 
	 * @param idDocto
	 *            Id del documento
	 */
	public void setIdDocto(String idDocto) {
		this.idDocto = idDocto;
	}

}

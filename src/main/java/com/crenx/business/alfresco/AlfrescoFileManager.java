package com.crenx.business.alfresco;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.IOUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.util.StringUtils;

import com.crenx.business.FileManager;
import com.crenx.business.FileUtils;

/**
 * Clase concreta de un manejados de archivos adecuada para la interacción con
 * Alfresco. Esta implementación esta basada en la integración con CMIS
 * 
 * 
 * @see FileManager
 * 
 * @author JCHR
 *
 */
public class AlfrescoFileManager extends FileManager {

	private Session session;

	/**
	 * 
	 * @param session
	 *            Session CMIS Alfresco
	 */
	public AlfrescoFileManager(final Session session) throws IllegalArgumentException {
		this(session, null, false);
	}

	/**
	 * 
	 * @param session
	 *            Session CMIS Alfresco
	 */
	public AlfrescoFileManager(final Session session, final String workDirectory, final boolean relativePaths)
			throws IllegalArgumentException {
		if (session == null) {
			throw new IllegalArgumentException("It Is necesary a valid Alfresco  Session");
		}
		this.session = session;
		this.workDirectory = (StringUtils.hasText(workDirectory)) ? workDirectory : session.getRootFolder().getPath();
		this.relativePaths = relativePaths;
	}

	/**
	 * Este método regresa el archivo temporal con el cual se envio al
	 * repositorio de Alfresco
	 * 
	 * @see com.crenx.business.FileManager#save(byte[], java.lang.String,
	 *      java.lang.String, java.lang.String)
	 */
	@Override
	public File save(byte[] data, final String dirPath, final String fileName, final String mimetype)
			throws IOException, IllegalArgumentException {
				
		if (BooleanUtils.isFalse(StringUtils.hasText(fileName))) {
			throw new IllegalArgumentException("Can not be null name");
		}
		Folder parentFolder = null;
		// Se busca el Folder contenedor
		CmisObject object = AlfrescoUtils.getObjectByPath(session, this.getPath(dirPath, null, '/'));
		if (object instanceof Folder) {
			parentFolder = (Folder) object;
		} else {
			throw new IOException("El directorio contenedor no ha sido encontrado en Alfresco");
		}
		// Se ponen las propiedades del documento
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
		properties.put(PropertyIds.NAME, fileName);

		// Se crea un archivo temporal
		File file = File.createTempFile(fileName, null);
		FileOutputStream output = null;
		output = new FileOutputStream(file);
		output.write(data);
		output.flush();
		output.close();

		InputStream input = null;
		try {
			// Creación del stream de contenido
			input = new FileInputStream(file);
			ContentStream contentStream = this.session.getObjectFactory().createContentStream(fileName, file.length(),
					mimetype, input);
			// Create versioned document object
			parentFolder.createDocument(properties, contentStream, VersioningState.MAJOR);
			input.close();
			// Se etiqueta para que el archivo sea borrado al finalizar la JVM
			file.deleteOnExit();
		} finally {
			IOUtils.closeQuietly(input);
		}
		return file;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crenx.business.FileManager#get(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public File get(final String dirPath, final String fileName) throws IOException, IllegalArgumentException {
		File tmpFile = null;
		if (StringUtils.hasText(fileName)) {
			String path = this.getPath(dirPath, fileName, '/');
			CmisObject object = AlfrescoUtils.getObjectByPath(session, path);
			if (object != null && object instanceof Document) {
				final ContentStream contentStream = ((Document) object).getContentStream();
				tmpFile = File.createTempFile(contentStream.getFileName(), null);
				FileUtils.writeInputStreamToFile(contentStream.getStream(), tmpFile);
				tmpFile.deleteOnExit();
			}
		}
		return tmpFile;
	}

	@Override
	public void createDirectory(final String parentDirPath, final String dirName) throws IOException {
		if (BooleanUtils.isFalse(StringUtils.hasText(dirName))) {
			throw new IllegalArgumentException("The parameter parentDirPath can not be null or empty");
		}		
		final CmisObject folder = AlfrescoUtils.getObjectByPath(session, this.getPath(parentDirPath, null, '/'));

		if (folder == null) {
			throw new IOException("The resource " + parentDirPath + " does not exist in Alfresco");
		}

		if (BooleanUtils.isFalse(folder instanceof Folder)) {
			throw new IOException("The resource " + parentDirPath + " is not a Folder");
		}

		if (AlfrescoUtils.createFolder(this.session, (Folder) folder, dirName) == null) {
			throw new IOException("The directory  can not be created");
		}
	}

	@Override
	public void delete(final String dirPath, final String fileName) throws IOException {
		if (StringUtils.hasText(fileName)) {		
			final String path = this.getPath(dirPath, fileName, '/');
			final CmisObject cmisObject = AlfrescoUtils.getObjectByPath(session, path);
			if (cmisObject != null) {
				try {
					cmisObject.delete(true);
				} catch (Exception exception) {
					throw new IOException("The  document" + fileName + " can not be deleted");
				}
			}
		}

	}

	@Override
	public boolean exists(final String dirPath, final String fileName) {
		final String path = this.getPath(dirPath, fileName, '/');
		return (path != null && AlfrescoUtils.getObjectByPath(session, path) != null);
	}
		

}

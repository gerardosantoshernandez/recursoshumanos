package com.crenx.business;

/**
 * @author JCHR
 *
 */
public interface FileManagerFactory {

	/**
	 * Creal el File Manager
	 * 
	 * @return
	 */
	FileManager createFileManager();
}

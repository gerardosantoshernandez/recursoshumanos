package com.crenx.data.domain.vo;

import java.util.Date;

public class AnuncioVO {

	private String idAviso;
	private String asunto;
	private String descripcion;
	private Date fechaAviso;
	private Date fechaAusente;
	private Date fechaRegreso;
	private String idEmpleado;
	public String getIdAviso() {
		return idAviso;
	}
	public void setIdAviso(String idAviso) {
		this.idAviso = idAviso;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFechaAviso() {
		return fechaAviso;
	}
	public void setFechaAviso(Date fechaAviso) {
		this.fechaAviso = fechaAviso;
	}
	public Date getFechaAusente() {
		return fechaAusente;
	}
	public void setFechaAusente(Date fechaAusente) {
		this.fechaAusente = fechaAusente;
	}
	public Date getFechaRegreso() {
		return fechaRegreso;
	}
	public void setFechaRegreso(Date fechaRegreso) {
		this.fechaRegreso = fechaRegreso;
	}
	public String getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	
}

package com.crenx.data.domain.vo;

import java.util.Date;

public class CajaVO {

	private String idCaja;
	private String idRuta;
	private String nombre;
	private Date fechaCorte;
	private double saldoInicial;
	private double saldoFinal;
	private double totalCargos;
	private double totalAbonos;
	private String fechaC;
	
	
	public String getFechaC() {
		return fechaC;
	}
	public void setFechaC(String fechaC) {
		this.fechaC = fechaC;
	}
	public String getIdCaja() {
		return idCaja;
	}
	public void setIdCaja(String idCaja) {
		this.idCaja = idCaja;
	}
	public String getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(String idRuta) {
		this.idRuta = idRuta;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	public double getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(double saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public double getSaldoFinal() {
		return saldoFinal;
	}
	public void setSaldoFinal(double saldoFinal) {
		this.saldoFinal = saldoFinal;
	}
	public double getTotalCargos() {
		return totalCargos;
	}
	public void setTotalCargos(double totalCargos) {
		this.totalCargos = totalCargos;
	}
	public double getTotalAbonos() {
		return totalAbonos;
	}
	public void setTotalAbonos(double totalAbonos) {
		this.totalAbonos = totalAbonos;
	}
	
	
}
